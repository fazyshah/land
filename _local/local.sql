-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2019 at 08:32 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `landtrack`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'Gulshan e Iqbal', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(2, 'Clifton', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(3, 'DHA Phase 5', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(4, 'Touheed Commercial', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(5, 'Gulistan e Johar', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(6, 'Gulburg', 2, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(7, 'Bahria Town Lahore', 2, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(8, 'Purana lahore', 2, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(9, 'Anarkali', 2, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(10, 'Blue Area', 3, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(11, 'Sector I-10', 3, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(12, 'Sector G9', 3, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(13, 'Markaz I-10/2', 3, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(14, 'Qasimabad', 4, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(15, 'Latifabad', 4, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(16, 'Haider Chowk', 4, '2018-12-13 14:24:10', '2018-12-13 14:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Karachi', '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(2, 'Lahore', '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(3, 'Islamabad', '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(4, 'Hyderabad', '2018-12-13 14:24:10', '2018-12-13 14:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_11_000000_create_user_types_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_09_01_112304_entrust_setup_tables', 1),
(5, '2018_09_02_130406_create_system_settings_table', 1),
(6, '2018_11_01_084551_create_cities_table', 1),
(7, '2018_11_01_084553_create_area_table', 1),
(8, '2018_12_13_123715_create_property_types_table', 1),
(9, '2018_12_13_123806_create_property_purposes_table', 1),
(10, '2018_12_13_123844_create_property_table', 1),
(11, '2018_12_18_204508_create_sub_types_table', 2),
(14, '2019_01_06_113215_create_project_types_table', 3),
(15, '2019_01_06_113216_create_property_projects_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'home', 'Home', 'Home Page', '2018-12-13 14:24:09', '2018-12-13 14:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_types`
--

CREATE TABLE `project_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_types`
--

INSERT INTO `project_types` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Residential Plot', 1, NULL, NULL),
(2, 'Commercial Plot', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `beds` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `baths` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `img` text COLLATE utf8mb4_unicode_ci,
  `exp_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `featured` tinyint(1) DEFAULT '0',
  `purpose_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `title`, `slogan`, `price`, `beds`, `baths`, `area`, `details`, `img`, `exp_date`, `status`, `featured`, `purpose_id`, `type_id`, `area_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Placeat cum in est minus aut.', 'Praesentium laudantium officia.', '15 Crore', '4', '3', '3000 Sq. Yd.', 'Autem animi nihil consequatur et. At enim dolor enim perspiciatis repellendus. Iste sed ullam officia doloremque quo et id. Quam et repudiandae quo et.', '1.jpg', '2019-01-26 18:54:34', 1, 1, 2, 3, 16, 2, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(2, 'Quasi alias recusandae et ullam autem.', 'Optio rerum.', '3.5 Crore', '3', '4', '150 Sq. Yd.', 'Laudantium tenetur dolore rerum autem quisquam aspernatur nemo. Et voluptas aut sunt fugit.', '2.jpg', '2019-01-26 18:54:42', 1, 1, 3, 3, 15, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(3, 'Non eveniet voluptas rerum nesciunt.', 'Et earum.', '6.5 Crore', '6', '5', '1500 Sq. Yd.', 'Iure quae dolorem reprehenderit velit enim voluptates non. Sequi iste ut a assumenda qui facilis aut est. Beatae temporibus excepturi qui ea.', '3.jpg', '2019-01-26 18:54:48', 1, 1, 2, 3, 16, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(4, 'Aliquam non consequatur.', 'Sequi neque.', '12 Crore', '5', '3', '1500 Sq. Yd.', 'Tempora iure ratione omnis ab tempora sint est. Unde at voluptas omnis. Iure facere ut voluptate libero placeat pariatur odit.', '4.jpg', '2019-01-26 18:55:54', 1, 1, 1, 2, 11, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(5, 'Odit odit praesentium odit id.', 'Quia reprehenderit est.', '2.5 Crore', '6', '5', '3000 Sq. Yd.', 'Saepe doloribus quia architecto quos error. Dolorem itaque excepturi rerum vitae natus vitae officiis. Vero ut enim maxime rerum earum.', '5.jpg', '2019-01-26 18:55:51', 1, 1, 2, 4, 16, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(6, 'Exercitationem temporibus et sit omnis.', 'Delectus quisquam.', '3 Crore', '6', '2', '1500 Sq. Yd.', 'Eaque sit deserunt non aut accusamus dolorum vero. Quos sunt adipisci ullam esse in ut illum iste. Explicabo et vel dolor consequatur quis ducimus. Dignissimos eos ratione placeat fugiat.', '6.jpg', '2019-01-26 18:55:48', 1, 1, 3, 5, 8, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(7, 'Facilis aut et.', 'Impedit soluta illo.', '7 Crore', '6', '4', '600 Sq. Yd.', 'Fugiat quae consequatur et quod mollitia. Aperiam et ut excepturi aut possimus. Aliquid ea officiis porro dolor quasi.', '7.jpg', '2019-01-26 18:55:45', 1, 1, 2, 1, 3, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(8, 'Ab distinctio exercitationem necessitatibus ad alias.', 'Nobis et.', '8 Crore', '6', '5', '600 Sq. Yd.', 'Non omnis nihil quis inventore. Beatae architecto vero vitae placeat. Eos vero ut incidunt quidem modi aspernatur. Et et tempore quis dolores atque hic.', '8.jpg', '2019-01-26 18:55:42', 1, 1, 2, 1, 4, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(9, 'Aut modi qui tempora quam porro.', 'Nihil blanditiis.', '6 Crore', '4', '3', '1000 Sq. Yd.', 'Ducimus in et dolorum officia accusantium perferendis. Minus nihil cupiditate fuga voluptatibus voluptatem soluta. Doloribus et sunt qui unde.', '9.jpg', '2019-01-26 18:55:40', 1, 1, 1, 5, 6, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(10, 'Corporis et cum sed inventore sint.', 'Blanditiis iste.', '3 Crore', '5', '1', '3000 Sq. Yd.', 'In voluptates eius nobis. Enim repudiandae aspernatur voluptatem vitae dicta voluptatum. Cum sint quis non.', '10.jpg', '2019-01-26 18:55:37', 1, 1, 3, 5, 7, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(11, 'Et deleniti repellendus qui repellendus nihil.', 'Et dicta at.', '4 Crore', '5', '1', '120 Sq. Yd.', 'Architecto voluptas architecto amet quia maxime ut. Impedit optio occaecati adipisci iure. Non sint reiciendis explicabo qui nisi iure nesciunt.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:11', 1, 0, 2, 5, 16, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(12, 'Iure qui consequatur sint.', 'Omnis sint quis.', '5.6 Crore', '4', '4', '300 Sq. Yd.', 'Nulla nulla deserunt velit ut facere sint. Inventore delectus temporibus perspiciatis quasi nam. Optio fugit dignissimos quis. Inventore nesciunt repellat nam asperiores dolor.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:11', 1, 0, 3, 5, 16, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(13, 'Quas distinctio sint aut sapiente eum.', 'Nisi et eos.', '8 Crore', '6', '1', '1500 Sq. Yd.', 'Fugit commodi qui fugit molestiae amet odio. Est alias architecto temporibus exercitationem delectus natus veritatis placeat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:11', 1, 0, 1, 3, 14, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(14, 'Maiores dolor iure dolorum.', 'Voluptatibus perspiciatis accusantium.', '6 Crore', '3', '3', '1500 Sq. Yd.', 'Ex ex aut eos fugit dolorem. Illum ab aut laboriosam ex corrupti earum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:11', 1, 0, 1, 1, 11, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(15, 'Dolores saepe culpa nesciunt.', 'Deleniti accusantium.', '3 Crore', '7', '3', '200 Sq. Yd.', 'Consequatur saepe harum aperiam excepturi totam. Est maiores sunt fuga et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:11', 1, 0, 2, 1, 5, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(16, 'Et doloribus hic distinctio culpa animi.', 'Velit est.', '2.5 Crore', '4', '5', '150 Sq. Yd.', 'Quibusdam debitis ad ab quod ut omnis. Et non minus ullam ut. Eligendi repellendus quia omnis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:11', 1, 0, 1, 1, 10, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(17, 'Laborum tempora nesciunt animi odio.', 'Omnis facere.', '3 Crore', '3', '3', '600 Sq. Yd.', 'Reiciendis dolores repudiandae eaque. Suscipit magni debitis magnam magnam quod nemo quod. Voluptatem nostrum eos quibusdam quas quibusdam. Sed architecto est asperiores molestiae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:11', 1, 0, 2, 5, 9, 2, '2018-12-13 14:24:11', '2018-12-13 14:24:11'),
(18, 'Et iusto nisi.', 'Numquam dignissimos distinctio.', '8 Crore', '3', '1', '200 Sq. Yd.', 'Culpa autem est quia ut eaque rem illum aperiam. Et totam ea dolorem aut unde quia. Quis debitis soluta doloribus quia voluptatem dolores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 2, 5, 12, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(19, 'Id pariatur ipsam non.', 'Ducimus voluptate perferendis.', '8 Crore', '6', '2', '1000 Sq. Yd.', 'Accusantium odit ipsa expedita sunt aut. Ullam veritatis non aut vel voluptates atque nisi. Molestiae veniam non ad et quia. Sed repudiandae eos non qui repudiandae illo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 2, 2, 5, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(20, 'Non omnis occaecati non.', 'Id aut nam.', '6.5 Crore', '4', '5', '1000 Sq. Yd.', 'Quae rem fuga sit laudantium autem. Rem dolores et non maxime laborum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 2, 1, 4, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(21, 'Et quibusdam reprehenderit dicta nihil.', 'Et commodi expedita.', '15 Crore', '5', '4', '1000 Sq. Yd.', 'Consequatur rerum et vel iusto quia animi occaecati quia. Vel pariatur perspiciatis voluptates eaque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 3, 4, 2, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(22, 'Aut dicta quisquam eum veritatis.', 'Eius natus.', '5.6 Crore', '3', '3', '1500 Sq. Yd.', 'Quos accusamus assumenda mollitia minus sunt. Aut ut quia hic voluptas quis aliquid. Nulla a est rerum. Laudantium laboriosam est cum doloremque non nihil porro.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 1, 1, 7, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(23, 'Reiciendis impedit nobis.', 'Voluptatem nulla.', '4 Crore', '3', '1', '1500 Sq. Yd.', 'Dicta aut ipsum perferendis eaque. Magnam repudiandae sunt est. Alias nulla magnam nihil.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 3, 3, 5, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(24, 'At vitae ipsa qui.', 'Temporibus sit.', '4 Crore', '4', '4', '300 Sq. Yd.', 'A veritatis fugiat magnam quo voluptatem dolor exercitationem. Ut enim rerum et aut omnis laborum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 2, 5, 6, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(25, 'Blanditiis sed totam.', 'Libero esse.', '2.5 Crore', '6', '4', '120 Sq. Yd.', 'Et ab voluptas suscipit quia ea. Rerum pariatur laboriosam ut molestias qui rerum. Rerum necessitatibus facere impedit illo qui maiores. Quisquam totam quam non ea.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 3, 3, 2, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(26, 'Facere quas placeat sed excepturi.', 'Fuga facilis.', '3 Crore', '5', '4', '1000 Sq. Yd.', 'Nemo minima sequi natus voluptas. Nobis quae dolorem omnis expedita adipisci. Assumenda occaecati excepturi soluta cum totam excepturi labore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 1, 4, 3, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(27, 'Non commodi voluptas eaque excepturi.', 'Excepturi quas.', '4 Crore', '7', '3', '1500 Sq. Yd.', 'Non hic amet repellendus possimus pariatur consequatur. Et ipsam labore quo hic quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 2, 4, 16, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(28, 'Nihil sit inventore nisi.', 'Aut deserunt quasi.', '7 Crore', '4', '4', '3000 Sq. Yd.', 'Quia pariatur est a voluptas ullam. Explicabo deserunt voluptatem aut quia sint est. Est voluptatem et autem sed illum quaerat ex. Qui impedit est quas reprehenderit alias.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 3, 3, 5, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(29, 'Dolorum nisi enim doloremque.', 'Eum ratione.', '4 Crore', '5', '3', '120 Sq. Yd.', 'Ea minima ipsum voluptas. Non porro esse rerum impedit reprehenderit. Velit vero adipisci sed sunt omnis. Harum sed voluptatem qui distinctio.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 3, 2, 4, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(30, 'Dolor commodi aliquid quibusdam et.', 'Asperiores ab.', '6.5 Crore', '7', '4', '200 Sq. Yd.', 'Consequatur velit quas magni doloremque est. Et minus voluptates quis temporibus inventore. Est dolorem veniam recusandae tempore culpa libero. Voluptatem inventore dolores ea porro.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 3, 4, 6, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(31, 'Velit eveniet earum officiis qui.', 'A illum.', '6.5 Crore', '4', '4', '3000 Sq. Yd.', 'Eos molestias et ut qui dolores. Dolorem numquam laboriosam quis repellendus porro molestias dolores. Ad ab deserunt mollitia id delectus eum deleniti mollitia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 2, 1, 4, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(32, 'Omnis sint molestias repudiandae earum dolor.', 'Tenetur magni.', '5.6 Crore', '7', '2', '300 Sq. Yd.', 'Saepe tempora nemo sed quisquam et minus molestiae excepturi. Quod magnam nihil et quam deleniti et sed. Explicabo repellendus ut ducimus enim vero et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:12', 1, 0, 2, 2, 8, 2, '2018-12-13 14:24:12', '2018-12-13 14:24:12'),
(33, 'Omnis incidunt iusto consequatur aut.', 'Quas qui.', '6.5 Crore', '3', '2', '150 Sq. Yd.', 'Voluptatem animi neque aut voluptatibus eveniet ut. Totam et vitae laborum earum possimus. Consequatur dolorum perspiciatis rem veniam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 4, 13, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(34, 'Quis nam amet deleniti ex.', 'Nisi veritatis.', '12 Crore', '7', '4', '120 Sq. Yd.', 'Nihil a sapiente dolor earum magnam. Aut maxime aspernatur quis necessitatibus alias voluptatem. Nihil eveniet sit laborum sed voluptate. Accusamus quisquam sed voluptate repudiandae et ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 2, 3, 13, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(35, 'Hic natus cumque qui nihil.', 'Sed et voluptatem.', '8 Crore', '6', '2', '300 Sq. Yd.', 'Minus necessitatibus eum est aliquam explicabo. Atque aut est expedita. Repudiandae corporis molestiae suscipit aliquid.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 5, 12, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(36, 'Eligendi facere aspernatur totam.', 'Quia mollitia.', '5.6 Crore', '5', '3', '120 Sq. Yd.', 'A similique vero itaque. Quidem voluptas aliquam aut optio. Eos neque hic tempore voluptatibus eaque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 2, 5, 3, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(37, 'Voluptatibus neque ipsa suscipit beatae.', 'Omnis quia.', '2.5 Crore', '7', '5', '120 Sq. Yd.', 'Officiis est rerum voluptatem voluptatem. Tempore et voluptatem commodi amet itaque. Possimus in maxime voluptates.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 3, 1, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(38, 'Magni est molestias eius distinctio.', 'Dolor perspiciatis.', '15 Crore', '4', '3', '150 Sq. Yd.', 'Ad doloribus aliquam numquam ducimus quia error cum iste. Asperiores tempora iure facere et et occaecati quis. Modi aliquid non corrupti itaque officiis reiciendis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 2, 2, 9, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(39, 'Sapiente tempora repudiandae eveniet.', 'Fugiat sunt officia.', '2 Crore', '7', '5', '200 Sq. Yd.', 'Placeat rem voluptate harum dolorem temporibus repellendus. Ut id adipisci ut. Assumenda sunt quae et qui voluptatum dolores. A doloribus praesentium quidem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 2, 1, 1, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(40, 'Quam dolorem nihil in sed.', 'Sint id.', '5 Crore', '4', '2', '1500 Sq. Yd.', 'Corrupti architecto vel magnam blanditiis vero sed iusto. Sit corrupti eos doloribus optio eum. Voluptatem laudantium et error minus earum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 4, 4, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(41, 'Iusto repellat est nam.', 'Et voluptatum reprehenderit.', '4 Crore', '7', '5', '3000 Sq. Yd.', 'Nesciunt eum et dolor dolor aut beatae tenetur. Omnis quia tempora adipisci veritatis. Placeat maiores aut soluta placeat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 1, 16, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(42, 'Minima illo ipsum delectus quod quis.', 'Blanditiis quis dolorem.', '3 Crore', '7', '1', '200 Sq. Yd.', 'Eligendi id qui eos nostrum praesentium voluptatem iste. Ut fugiat qui temporibus quibusdam aut. Incidunt impedit officiis excepturi qui. Consequatur excepturi voluptatum doloremque et repellat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 4, 10, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(43, 'Aut a tenetur sunt eum ipsa.', 'Dolore excepturi.', '5.6 Crore', '7', '4', '1000 Sq. Yd.', 'Voluptas aut eos ut corrupti magnam. Minus modi quasi aut neque non aut. Asperiores eum praesentium fuga perferendis in.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 2, 4, 5, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(44, 'Cumque eveniet ratione doloribus commodi eveniet.', 'In id eveniet.', '9 Crore', '7', '4', '3000 Sq. Yd.', 'Consequuntur ratione molestias non nostrum eligendi in cum. Voluptas sunt molestias a facere officiis esse. Ab assumenda sequi eaque nobis asperiores quod.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 4, 14, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(45, 'Temporibus nulla omnis.', 'Rem quia enim.', '6.5 Crore', '6', '3', '600 Sq. Yd.', 'Et inventore quasi non praesentium facere et ut ut. Impedit rem quam est et nesciunt vel earum provident.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 1, 8, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(46, 'Nemo a enim odio.', 'Facilis voluptates.', '9 Crore', '4', '4', '1500 Sq. Yd.', 'Occaecati laboriosam similique ipsam illo architecto eos minus aspernatur. Beatae reprehenderit quaerat ipsum fugiat aut minus est quia. Sit voluptas quis similique sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 3, 5, 13, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(47, 'Eaque repudiandae iusto rerum sint.', 'Ut labore debitis.', '9 Crore', '4', '1', '1500 Sq. Yd.', 'Occaecati rerum et porro pariatur non ab quisquam. Voluptatem sequi hic accusantium voluptatem qui optio dolor. Eveniet ipsa sint exercitationem dignissimos. Quam et aliquid autem aliquid ducimus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:13', 1, 0, 2, 1, 14, 2, '2018-12-13 14:24:13', '2018-12-13 14:24:13'),
(48, 'Autem ipsam perspiciatis.', 'Tempore quos.', '12 Crore', '5', '3', '300 Sq. Yd.', 'Et et vitae aliquid quam eligendi. Enim illo et distinctio impedit optio. Occaecati architecto rerum pariatur provident natus incidunt. Occaecati est sint atque eos cumque est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 3, 1, 8, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(49, 'Alias necessitatibus autem.', 'Tenetur eum.', '8 Crore', '3', '3', '1500 Sq. Yd.', 'Vero aut quae voluptatem. Ea ut adipisci et recusandae illum molestias nemo sed. Eligendi rerum qui nemo reprehenderit. Quo voluptatum in minima. Ipsum quis impedit saepe sit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 3, 1, 16, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(50, 'Sunt vel consequuntur sit.', 'Voluptatibus architecto eum.', '5 Crore', '5', '5', '600 Sq. Yd.', 'Deserunt officia corporis velit repellendus nobis delectus vitae. Aliquid non voluptas omnis delectus nemo. Officiis labore velit alias voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 4, 12, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(51, 'Voluptas consectetur rerum eaque necessitatibus.', 'Quia voluptatem.', '9 Crore', '7', '3', '120 Sq. Yd.', 'Quia delectus id atque culpa blanditiis. Voluptas odio dolor dolor nihil omnis. Labore officia deleniti quod quo. Impedit fuga qui est itaque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 2, 15, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(52, 'Et eum qui neque.', 'Nemo sit.', '6.5 Crore', '3', '3', '3000 Sq. Yd.', 'Dolorum omnis voluptate ut nulla tenetur accusantium. Cupiditate quidem laborum in fuga consequatur consectetur. Facilis est qui rerum quis vel voluptatibus repellat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 5, 10, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(53, 'Et nihil id ut.', 'Aut dolore.', '2 Crore', '7', '2', '150 Sq. Yd.', 'Minus iusto illo quia. Perspiciatis ut corporis sequi facere harum neque. Ut rerum consequatur veniam non omnis. Et quis possimus ad illum. Amet nostrum dolores facilis dolor placeat sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 2, 8, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(54, 'Sed possimus quis officia incidunt.', 'Dolorem quia.', '4 Crore', '4', '5', '300 Sq. Yd.', 'Fugit magni illum nihil distinctio ad consectetur et. Magni ipsa modi et odio repellat. Molestiae soluta totam ipsa incidunt. Et sed magni eum quia pariatur placeat voluptatem ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 1, 1, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(55, 'Quaerat ea tenetur consequatur rerum.', 'Aut harum quasi.', '5.6 Crore', '5', '2', '600 Sq. Yd.', 'Quos fugit beatae velit et sunt. Sed quia dolores quis rerum fuga est. Accusamus optio quod facere qui atque. Deserunt eum voluptatem fugit porro.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 2, 7, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(56, 'Ex consequatur ut molestiae et.', 'Qui rerum.', '7 Crore', '5', '5', '3000 Sq. Yd.', 'Accusamus quidem et recusandae non enim natus ea assumenda. Vero fugiat quis ut asperiores enim at earum. Quae possimus eligendi voluptatem debitis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 3, 5, 10, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(57, 'Rem officiis ducimus sed.', 'Quam neque.', '15 Crore', '4', '1', '600 Sq. Yd.', 'Maxime quae atque facere porro nostrum. Adipisci illo dolores voluptatem blanditiis aut. Dolor labore quis et doloremque eaque iusto nihil. Et iusto illo non sunt id quos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 3, 3, 1, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(58, 'Quos sed ratione magnam hic.', 'Cupiditate quia deleniti.', '3.5 Crore', '6', '4', '600 Sq. Yd.', 'Saepe optio dignissimos dolor. Fugiat voluptatem voluptatum maiores ipsa.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 3, 3, 16, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(59, 'Et incidunt quidem adipisci.', 'Sunt velit odio.', '15 Crore', '7', '4', '1500 Sq. Yd.', 'Odit maxime et enim itaque dolor molestias numquam. Praesentium qui veritatis earum rerum iusto quas repellendus. Est suscipit et iure est est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 1, 16, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(60, 'Et delectus ut.', 'Tempora ipsa eligendi.', '4 Crore', '6', '4', '600 Sq. Yd.', 'Consequatur beatae deleniti suscipit. Expedita illo ad expedita autem. Sunt unde et libero aut et amet quos voluptate. Est consequuntur nemo vel quam soluta aperiam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 1, 3, 6, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(61, 'Qui beatae saepe id ab.', 'Est sit.', '8 Crore', '4', '5', '600 Sq. Yd.', 'Explicabo laborum sequi ducimus ab. Quis unde sunt dignissimos facilis et. Fugiat omnis nesciunt et ad deleniti laboriosam ducimus. Et officiis facere sed placeat in iste quam dolor.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 5, 9, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(62, 'Enim possimus perferendis voluptate.', 'Voluptate modi.', '4 Crore', '6', '2', '1000 Sq. Yd.', 'Maxime vel ut laudantium laborum eos culpa voluptatem. Quas dolorum blanditiis blanditiis et. Error consequatur alias delectus debitis vel maxime aut. Aut molestiae quia quas iste et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 3, 7, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(63, 'Qui necessitatibus consequatur sit ipsa.', 'Rerum odio.', '2.5 Crore', '6', '5', '1000 Sq. Yd.', 'Non dolores quasi possimus et. Impedit vero rem ullam itaque nam omnis dolores officia. Voluptatem maiores sunt esse dolorum vero. Facere velit ex perspiciatis earum voluptas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:14', 1, 0, 2, 2, 13, 2, '2018-12-13 14:24:14', '2018-12-13 14:24:14'),
(64, 'Qui aspernatur omnis cupiditate ut sint.', 'Veniam dolores.', '9 Crore', '6', '1', '150 Sq. Yd.', 'Voluptas autem maxime qui fuga nisi. Ut sint eum voluptatum quas at ducimus. Ut temporibus placeat provident reprehenderit sed quos provident. Incidunt corrupti est eius culpa id velit et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 1, 1, 16, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(65, 'Maxime aspernatur dolorum.', 'Ipsa sunt.', '7 Crore', '6', '2', '150 Sq. Yd.', 'Ab esse qui quasi nisi eligendi. Id itaque sint consequatur ratione. Soluta aut hic animi hic nemo quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 1, 4, 9, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(66, 'Dolorum necessitatibus nihil sequi non.', 'Culpa quasi perspiciatis.', '3 Crore', '5', '2', '300 Sq. Yd.', 'Soluta rerum doloremque nesciunt qui excepturi voluptatem fugiat. Minima laudantium rem amet aut. Ut aliquam labore fugiat est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 2, 4, 15, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(67, 'Quam et odio ut et neque.', 'Excepturi perspiciatis exercitationem.', '7 Crore', '5', '1', '1000 Sq. Yd.', 'Modi non fugiat est reiciendis tenetur dignissimos a. Fugiat temporibus quia quae iusto fugit fuga. Illum sunt esse corporis modi dolor odio. Corrupti rem quis nostrum debitis aspernatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 3, 4, 1, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(68, 'Odit ut consectetur laborum incidunt ut.', 'Ratione vel.', '15 Crore', '6', '2', '120 Sq. Yd.', 'Et velit ea qui. Et id eveniet hic perspiciatis quo. Eaque sed accusantium at enim odio corporis. Adipisci adipisci ut id beatae eveniet molestiae ut. Et id vero iste amet atque porro doloremque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 2, 3, 4, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(69, 'Enim nulla quia.', 'Adipisci veritatis.', '6.5 Crore', '3', '5', '300 Sq. Yd.', 'Veritatis inventore odio quia possimus fugiat est. Ipsum quidem quasi qui amet. Quia fuga voluptatem eos quia voluptas error enim quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 3, 2, 15, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(70, 'Aliquam non aut itaque.', 'Sint recusandae placeat.', '8 Crore', '5', '3', '300 Sq. Yd.', 'Est omnis excepturi aperiam voluptatem. Veritatis fugit dolor corporis similique quam facere. Eaque sit omnis iusto exercitationem tempora ipsa.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 3, 2, 16, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(71, 'Eos sit nemo eius sit fugit.', 'Perferendis ad.', '3.5 Crore', '4', '4', '1500 Sq. Yd.', 'At recusandae itaque sit est voluptatem porro. Veniam vero vero facere non. Quaerat dolorem saepe ullam fuga.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 3, 4, 15, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(72, 'Et laboriosam nobis qui quam.', 'Rem illum dicta.', '3 Crore', '4', '2', '1000 Sq. Yd.', 'Mollitia nisi commodi expedita ut sint. Dicta similique hic deserunt. In inventore quas autem. Nisi nulla vitae quae voluptates dolores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 1, 5, 11, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(73, 'Sit harum sit non.', 'Dolorem delectus enim.', '7 Crore', '4', '5', '120 Sq. Yd.', 'Sit minima alias deserunt dolorem aut nulla iusto. Rem earum nam labore molestiae qui. Debitis repellendus est sit qui reiciendis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 3, 1, 14, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(74, 'Sint ratione eveniet expedita sint consequatur.', 'Velit eligendi dolores.', '3 Crore', '7', '5', '1500 Sq. Yd.', 'Molestiae consequatur tempore illo cumque ex quae earum. Praesentium repellat sunt maiores rerum. Porro omnis omnis reiciendis recusandae eaque magni ea.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 1, 1, 13, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(75, 'Sed dolorem sunt at ex.', 'Magnam doloremque rerum.', '12 Crore', '4', '2', '1500 Sq. Yd.', 'Sint perferendis sit qui cumque quisquam sit. Architecto soluta labore enim dolores odit. Dolores dolores consectetur voluptas distinctio.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 3, 3, 16, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(76, 'Vel et deserunt non.', 'Consequatur a.', '12 Crore', '5', '3', '1000 Sq. Yd.', 'Sunt quis labore alias et dicta provident doloribus exercitationem. Ut eius tempore dolor laborum totam blanditiis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 1, 1, 10, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(77, 'Nemo accusamus et et reprehenderit.', 'Velit rerum.', '5.6 Crore', '3', '4', '1000 Sq. Yd.', 'Culpa dolor minima provident ut libero consequatur. Et asperiores nihil repellendus quibusdam in. Dolorum nisi qui tempore quia doloremque consequatur hic. Ipsum consequatur maxime eligendi cum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 3, 4, 7, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(78, 'Consequatur suscipit ea architecto quia.', 'Saepe illum.', '5 Crore', '3', '1', '1500 Sq. Yd.', 'Dolor repellat ipsa est quia voluptatem. Hic autem quia dolor. Qui aut voluptatem eveniet omnis animi nemo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 1, 4, 11, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(79, 'Voluptate ullam quis aut tempore impedit.', 'Iste explicabo.', '5 Crore', '3', '1', '200 Sq. Yd.', 'Aspernatur reiciendis adipisci dolor magnam quasi. Incidunt dolor vel sit odio cumque nobis ad.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:15', 1, 0, 1, 3, 10, 2, '2018-12-13 14:24:15', '2018-12-13 14:24:15'),
(80, 'Voluptates sed quo occaecati.', 'Voluptas dolorum.', '9 Crore', '5', '2', '1000 Sq. Yd.', 'Maxime nihil eveniet quia quisquam. Dolorem tempore officia recusandae reprehenderit ut sit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 3, 5, 13, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(81, 'Asperiores maxime quas.', 'Adipisci vel.', '4 Crore', '5', '1', '300 Sq. Yd.', 'Odit quasi amet quos dolor. Dicta harum dolor consequatur beatae molestiae sit voluptas et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 3, 5, 1, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(82, 'Sapiente quam accusamus nihil.', 'Vitae dolor repellat.', '3.5 Crore', '4', '3', '3000 Sq. Yd.', 'Molestias quidem et aliquid blanditiis. Animi eos dolorem quo aperiam. In eligendi et esse vero. Sit est ut quos ad necessitatibus quos occaecati.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 2, 1, 12, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(83, 'Laudantium dolorem omnis autem nihil aperiam.', 'Eum aut ex.', '3.5 Crore', '6', '5', '300 Sq. Yd.', 'Eaque fugit non iste. Consectetur fuga vel qui voluptatem modi. Minus cumque est fuga aut sit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 1, 4, 13, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(84, 'Esse delectus minus ullam sunt.', 'Veritatis quis ea.', '4 Crore', '6', '4', '120 Sq. Yd.', 'Cumque pariatur qui est placeat et dolorem eaque. Corporis maxime dolore aut ea ut fugit consequatur. Quia libero quos non. Voluptas culpa ducimus cumque molestiae deserunt rerum aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 1, 5, 10, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(85, 'Nihil labore voluptatem architecto incidunt.', 'Reprehenderit dolorum.', '15 Crore', '4', '1', '600 Sq. Yd.', 'Necessitatibus dolores sed explicabo et quae tempora. Enim quo hic itaque a id cupiditate a ratione. Hic laboriosam excepturi non fugit eum in ad.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 2, 4, 3, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(86, 'Qui provident accusantium soluta inventore.', 'Vel amet.', '2 Crore', '3', '5', '120 Sq. Yd.', 'Et ut in quo id in aliquid. Eos sit suscipit eum corrupti voluptate reiciendis. Sequi dolor vel sapiente magni ex. Temporibus ut est tenetur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 3, 1, 10, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(87, 'Voluptate eius qui et qui.', 'Voluptatem eos nemo.', '3.5 Crore', '5', '5', '3000 Sq. Yd.', 'Et minus aut totam. Sed possimus reiciendis velit eius aliquid. Dolores ipsum impedit officia tempore ipsa ut. Incidunt autem dolor vel tenetur laborum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 3, 1, 9, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(88, 'Accusamus voluptatem a ea aut.', 'Quam sapiente est.', '9 Crore', '6', '5', '3000 Sq. Yd.', 'Ea eaque repellat quia perspiciatis. Earum dolorem voluptatibus nulla quibusdam qui ea quo quisquam. Assumenda rerum autem a soluta voluptates. Sequi cupiditate est reiciendis eum quae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 1, 3, 6, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(89, 'Repellendus omnis architecto eveniet.', 'Beatae ut.', '15 Crore', '4', '4', '1500 Sq. Yd.', 'Consequatur reprehenderit asperiores ea molestias. Laborum asperiores corporis sit et molestiae. Voluptatum in suscipit est velit molestiae corporis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 2, 5, 12, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(90, 'Repellat exercitationem nobis.', 'Iure quasi.', '8 Crore', '3', '2', '120 Sq. Yd.', 'Deleniti natus id quod dignissimos est. Nostrum dolorem voluptatem quis voluptates magnam. Modi rerum ipsa temporibus atque. Voluptatem in totam nesciunt possimus laborum aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 1, 5, 5, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(91, 'Quod ab doloribus.', 'Et autem iste.', '6 Crore', '4', '1', '120 Sq. Yd.', 'Est qui et dolorem laudantium et quia. Eum nostrum est omnis laudantium nostrum. Ab voluptatem dolorem non ut enim.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 2, 3, 5, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(92, 'Ut nihil quia nulla.', 'Et in.', '7 Crore', '5', '1', '200 Sq. Yd.', 'Nemo et voluptatem fugiat blanditiis aliquid quo. Recusandae aut voluptates et cum ex. Odit vitae porro magni deleniti adipisci provident. Nesciunt odit omnis autem porro quis esse eos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 3, 2, 10, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(93, 'Voluptas maxime doloribus et nulla ad.', 'Eaque placeat architecto.', '9 Crore', '6', '2', '200 Sq. Yd.', 'Tempora blanditiis pariatur aliquid est. Debitis aut repudiandae sunt tempora atque. Et repudiandae non corporis quaerat. Qui sit itaque aut velit illum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 1, 1, 4, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(94, 'Recusandae consequatur sit excepturi iusto ullam.', 'Dolor et.', '2.5 Crore', '5', '2', '200 Sq. Yd.', 'Sunt et esse nemo illo. In officia et ut et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 2, 3, 8, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(95, 'Vel veritatis culpa.', 'Ducimus est.', '2.5 Crore', '6', '2', '600 Sq. Yd.', 'Quia beatae in quo sunt distinctio. Consequatur enim voluptas dolor voluptatibus. Provident illum deserunt voluptatibus rerum vel nihil quo. Saepe aperiam aut doloremque eius.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 3, 2, 9, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(96, 'Non quas veritatis dolorum molestiae voluptatem.', 'Consequuntur eos.', '2 Crore', '5', '3', '3000 Sq. Yd.', 'Et iure et quia aut mollitia similique est. Quod numquam sed corporis minima quia voluptatum. Voluptas minima odio eaque velit. Aliquid ut voluptatum labore optio eligendi ab eligendi quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:16', 1, 0, 1, 2, 4, 2, '2018-12-13 14:24:16', '2018-12-13 14:24:16'),
(97, 'Ad consequatur modi et sit.', 'Nobis deserunt autem.', '3.5 Crore', '3', '3', '1000 Sq. Yd.', 'Dignissimos qui ullam sequi itaque quaerat voluptatem aut. Eos sequi adipisci tempore et. Aliquid necessitatibus ut debitis a illo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 3, 5, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(98, 'Accusamus vel aut.', 'Eaque sed labore.', '3.5 Crore', '4', '2', '150 Sq. Yd.', 'Nam qui perferendis non officia facilis. Eos aut reiciendis omnis ut in sed similique. Libero quia rerum qui minima. Et iste magnam vel reiciendis sequi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 2, 7, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(99, 'Numquam maxime quis maiores adipisci.', 'Soluta quaerat.', '12 Crore', '5', '3', '300 Sq. Yd.', 'Earum rerum commodi sit voluptates aut. Sit consectetur quo voluptatem in. Autem nam eum et ut consectetur. Ea commodi minima cum. Et esse culpa sit quas et voluptas impedit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 1, 3, 8, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(100, 'Labore tenetur dolores itaque temporibus.', 'Debitis sed vel.', '12 Crore', '7', '3', '120 Sq. Yd.', 'Itaque officiis quod corrupti quas deleniti repellendus. Voluptas voluptatem nihil quaerat velit. Voluptate sit doloribus voluptas asperiores doloremque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 1, 3, 12, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(101, 'Aut sed qui dicta modi.', 'Beatae rerum eos.', '3.5 Crore', '4', '5', '1000 Sq. Yd.', 'Blanditiis veritatis et consequatur aut earum. Sequi consequatur mollitia officiis. Illum officia et consequatur autem omnis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 2, 5, 3, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(102, 'Voluptates voluptate eos vero temporibus ad.', 'Fugiat natus.', '6.5 Crore', '5', '5', '1500 Sq. Yd.', 'Hic nisi provident impedit voluptas sed illum minima nobis. Magnam magni sunt est. Ratione praesentium culpa recusandae vero perspiciatis dolor libero. Maxime vitae ipsam unde velit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 4, 9, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(103, 'Eius eos ut odio ab autem.', 'Dolorem in ullam.', '15 Crore', '7', '3', '1500 Sq. Yd.', 'Aliquam assumenda quisquam dolores. Sint non quia aliquam. Quaerat itaque ut inventore iusto qui iure eum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 1, 3, 11, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(104, 'Perspiciatis suscipit ut quia et.', 'Est aut.', '15 Crore', '5', '3', '200 Sq. Yd.', 'Sunt consequatur culpa illo sint nemo quisquam commodi. Qui rerum voluptates voluptate dolorem debitis quia dolorem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 1, 5, 1, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(105, 'Excepturi expedita qui ducimus.', 'Fugiat culpa totam.', '2.5 Crore', '7', '4', '150 Sq. Yd.', 'Non sint tenetur dolorum sit labore. Ipsa id autem recusandae possimus ut. Aliquam earum est molestiae ipsa consectetur et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 5, 4, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(106, 'Voluptate quis impedit odit mollitia.', 'Ea rerum dolor.', '8 Crore', '4', '4', '1000 Sq. Yd.', 'Qui ea fugiat id velit eius eum in. Reprehenderit enim sequi ut fugiat asperiores quia doloribus sit. Quibusdam aut unde iste quia recusandae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 4, 12, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(107, 'Totam qui velit officiis.', 'Rerum ullam reiciendis.', '8 Crore', '6', '3', '120 Sq. Yd.', 'Assumenda tempore nemo et reiciendis. Adipisci facere vel quis sit suscipit odio. Ab nulla quibusdam eveniet aut sed aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 5, 1, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(108, 'Quis earum doloribus accusamus ipsam velit.', 'Et et.', '12 Crore', '3', '5', '200 Sq. Yd.', 'Aperiam vero libero consequatur est. Dolorem rerum cum saepe nemo. Sed illum similique aperiam ullam. Vel totam deserunt non non expedita eveniet. Itaque vitae corrupti possimus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 2, 12, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(109, 'Unde molestiae alias consectetur.', 'Enim enim sunt.', '2.5 Crore', '3', '2', '150 Sq. Yd.', 'Corporis quis accusantium voluptas. In accusantium molestiae nostrum et eum id nam. Voluptatem ad explicabo id eligendi autem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 2, 1, 14, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(110, 'Nulla enim nesciunt.', 'Maiores ipsum ut.', '2 Crore', '3', '4', '1500 Sq. Yd.', 'Aut minus doloribus possimus quia. Ut sed nihil hic. Quos blanditiis omnis amet explicabo similique ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 1, 1, 4, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(111, 'Qui in et maxime.', 'Quo et dolor.', '2.5 Crore', '5', '3', '150 Sq. Yd.', 'Doloremque est dolores ipsam quis. Voluptate fuga et tempore recusandae. Non esse temporibus sed pariatur numquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 1, 5, 7, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(112, 'Quod ea omnis ut.', 'Voluptates aliquid facere.', '3.5 Crore', '7', '4', '1500 Sq. Yd.', 'Tempore omnis quia in adipisci accusantium aut. Et ut laboriosam voluptates debitis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 2, 3, 5, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(113, 'Minima totam enim perspiciatis dolores placeat.', 'Dolor sed.', '8 Crore', '3', '2', '1000 Sq. Yd.', 'Quis dolor quam repellendus deleniti quibusdam est. Tempora expedita laboriosam nihil porro. Illo reiciendis velit qui quos. Et quasi est fugiat doloremque ex quasi pariatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 3, 4, 6, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(114, 'Sit est aspernatur molestiae magni.', 'Sint voluptatem.', '2.5 Crore', '3', '4', '3000 Sq. Yd.', 'A maxime et rerum optio. Eligendi et harum non. Ipsam praesentium ipsa dolor hic voluptatum quia asperiores quis. Aliquam dolor sit impedit tempore cupiditate velit debitis sit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:17', 1, 0, 1, 3, 5, 2, '2018-12-13 14:24:17', '2018-12-13 14:24:17'),
(115, 'Provident quia quos et quidem voluptatem.', 'Delectus non est.', '5 Crore', '5', '1', '200 Sq. Yd.', 'Enim enim ullam qui voluptatem fugit. Illo a perferendis laudantium voluptatibus alias omnis debitis. Quia qui quibusdam ut quibusdam illo. Iure aut officiis earum ab similique inventore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 5, 13, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(116, 'Omnis sint molestias earum.', 'Minima molestias quis.', '7 Crore', '6', '4', '1500 Sq. Yd.', 'Enim velit eveniet error cupiditate illo. Vero architecto aperiam et vel sint fugit. Ex consequatur assumenda eveniet inventore soluta.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 2, 3, 7, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(117, 'Autem praesentium dolor amet voluptas.', 'Ipsam ea id.', '6.5 Crore', '7', '3', '200 Sq. Yd.', 'Iste alias officiis laborum voluptas doloremque sit sint. Et rerum qui quia nisi eos. Facere sit perspiciatis harum provident alias aut omnis repellendus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 1, 5, 1, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(118, 'Pariatur dolor qui quia.', 'Ut optio numquam.', '9 Crore', '4', '1', '3000 Sq. Yd.', 'Blanditiis laborum quod vel aut est. Adipisci et est vitae dolorum ut velit molestias. Explicabo cupiditate qui quia commodi quaerat suscipit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 2, 5, 4, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(119, 'Corrupti suscipit voluptatibus saepe ut similique.', 'Quis minima et.', '9 Crore', '3', '2', '1500 Sq. Yd.', 'Repellat soluta debitis molestias cupiditate dolorem. Odio illum et aut harum explicabo cumque ad. Voluptatem recusandae voluptatem quo deleniti.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 4, 10, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(120, 'Velit ipsam officiis ut.', 'Molestias qui cum.', '6.5 Crore', '4', '1', '300 Sq. Yd.', 'Dolorem corrupti labore facilis. Earum culpa enim repellendus accusantium. Qui vitae est pariatur adipisci sed sapiente ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 2, 3, 11, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(121, 'Totam magnam maiores voluptatum excepturi mollitia.', 'Architecto sit.', '5.6 Crore', '6', '1', '600 Sq. Yd.', 'Ut est dolores omnis quisquam. Quos qui quae maxime aut hic veritatis. Voluptatem iste iure architecto id praesentium. Nihil error et voluptas sint commodi velit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 2, 2, 9, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(122, 'Laborum delectus aut.', 'Eveniet eos.', '5 Crore', '3', '3', '1500 Sq. Yd.', 'Sit iste molestias accusamus voluptas velit dolorem sit voluptatem. Est minus reiciendis sapiente non dolorum numquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 3, 2, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(123, 'Quae eos magnam saepe sed.', 'Totam earum.', '3 Crore', '6', '1', '1000 Sq. Yd.', 'Natus aut qui et debitis illo. Quae voluptatibus illum dolore animi possimus. Ut et debitis eius magni et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 1, 2, 10, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(124, 'Fuga dolor eum eum.', 'Ea voluptas.', '6 Crore', '6', '2', '200 Sq. Yd.', 'Voluptate nisi reiciendis voluptate vero aut suscipit suscipit. Et enim veritatis consequatur ipsa earum. Et dolor facilis laudantium at non modi quisquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 4, 14, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(125, 'Tempore et totam vel.', 'Natus sed.', '2.5 Crore', '7', '4', '150 Sq. Yd.', 'Beatae molestias rerum dolor consectetur voluptatem non. Neque voluptas deserunt quasi omnis eum praesentium similique ut. Non delectus ullam voluptatem quidem. Dolore et aliquam culpa qui alias et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 3, 1, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(126, 'Ullam nobis quisquam voluptatibus.', 'Asperiores deserunt.', '7 Crore', '3', '4', '3000 Sq. Yd.', 'Aut sed fuga aut non consequuntur molestiae animi. Quod atque et magni. Saepe vitae ullam magnam. Ad sapiente animi assumenda voluptatem qui. Autem a est perspiciatis excepturi aliquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 5, 15, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(127, 'Sint a cumque dolorum et.', 'Deleniti sint ipsa.', '6 Crore', '7', '1', '3000 Sq. Yd.', 'Aut ut laboriosam temporibus. Deserunt assumenda dolorem ut rerum natus. Aut iusto eius ducimus qui nulla. Sunt fugit error id perferendis et ut doloremque. Et culpa qui et nam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 1, 5, 2, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(128, 'Esse odit velit tempore.', 'Sit iure.', '2 Crore', '3', '5', '300 Sq. Yd.', 'Sequi fugiat reiciendis sit dolorem accusantium sapiente optio. Hic aut repellendus quia mollitia eligendi. Ut assumenda a aspernatur enim sapiente sint et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 1, 2, 6, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(129, 'Cum nesciunt ut tempora.', 'Minus facere quo.', '6.5 Crore', '5', '3', '1500 Sq. Yd.', 'Quo qui enim impedit deleniti. Alias consectetur illo ipsum. Placeat ut totam beatae consequatur eius non laborum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 2, 2, 8, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(130, 'Sed beatae molestias.', 'Nisi omnis explicabo.', '8 Crore', '5', '2', '3000 Sq. Yd.', 'Dicta culpa eaque tenetur minus ipsa sunt. Quos veritatis corrupti illo eum. Minima aliquam aut error id iste sed. Dolorem qui amet eum doloribus aut qui voluptate consequuntur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 4, 15, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(131, 'Soluta eum ducimus.', 'Aut consequatur eos.', '2 Crore', '3', '2', '3000 Sq. Yd.', 'Inventore nam nostrum ut facere ea at fugit. Explicabo cupiditate tempore qui optio. Nesciunt porro voluptatem facere adipisci et deleniti autem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 3, 3, 1, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(132, 'Quia minima sed ab eum.', 'Est est corrupti.', '2 Crore', '4', '3', '300 Sq. Yd.', 'Dolorem excepturi ea dignissimos dolorem sed in eos. Ea aut vero et minima sapiente ab. Sunt et non hic tempora eum nam dolores blanditiis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 1, 4, 4, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18'),
(133, 'Asperiores sed eum veniam.', 'Dolores id.', '8 Crore', '7', '2', '1500 Sq. Yd.', 'Aliquid et sit totam ex. Incidunt minima tempora dolores excepturi. Repellendus aut eos vitae consequatur minus ad harum. Ratione eaque eius qui id.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:18', 1, 0, 1, 5, 4, 2, '2018-12-13 14:24:18', '2018-12-13 14:24:18');
INSERT INTO `property` (`id`, `title`, `slogan`, `price`, `beds`, `baths`, `area`, `details`, `img`, `exp_date`, `status`, `featured`, `purpose_id`, `type_id`, `area_id`, `user_id`, `created_at`, `updated_at`) VALUES
(134, 'Nobis ut sequi.', 'Vel qui consequuntur.', '12 Crore', '7', '2', '120 Sq. Yd.', 'Aut assumenda rem ut quae aliquam. Non ad et nisi possimus vel. Non nobis accusamus ducimus molestias. Assumenda ut dicta doloremque rerum harum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 1, 5, 15, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(135, 'Nesciunt corrupti occaecati voluptatum in.', 'Aspernatur qui.', '3.5 Crore', '3', '4', '3000 Sq. Yd.', 'Rerum magnam deserunt et. Eum ullam sit excepturi aliquam in hic nihil. Officia sed qui aut quisquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 3, 1, 1, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(136, 'Nobis ut autem amet.', 'Doloremque ipsam facere.', '12 Crore', '3', '1', '150 Sq. Yd.', 'Autem odit quo ipsam et aliquam. Suscipit qui iste fuga quaerat impedit qui et. Id praesentium quidem ab quibusdam omnis labore numquam pariatur. Illo est totam eius blanditiis eos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 1, 3, 10, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(137, 'Doloribus omnis amet corrupti.', 'Error sapiente ut.', '9 Crore', '4', '2', '3000 Sq. Yd.', 'Aperiam aut reprehenderit voluptate et ea incidunt dolorem. Sint et sit dignissimos rem et nesciunt. Quia soluta id voluptas in ut et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 3, 1, 15, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(138, 'Necessitatibus asperiores ea.', 'Et dolore et.', '9 Crore', '6', '5', '1000 Sq. Yd.', 'Vero velit natus doloribus exercitationem. Vel inventore quo delectus dicta aut consequatur. Id et debitis amet beatae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 3, 4, 3, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(139, 'Saepe reprehenderit quas quam.', 'Eius placeat alias.', '8 Crore', '7', '5', '3000 Sq. Yd.', 'Et voluptas sit fuga numquam. Saepe est qui sint debitis ut eum amet. Atque autem laboriosam aut iure qui qui impedit. Voluptatum sed est enim beatae voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 2, 4, 4, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(140, 'Libero fuga harum laborum et rerum.', 'Repellendus dolore qui.', '3 Crore', '3', '3', '120 Sq. Yd.', 'Deleniti harum impedit molestias nisi quo odio ullam doloremque. Dolores dolores repudiandae nihil repellat aut architecto. Incidunt ratione iure voluptate.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 2, 5, 14, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(141, 'Est quod nemo aut veniam voluptates.', 'Mollitia ut pariatur.', '6.5 Crore', '6', '5', '200 Sq. Yd.', 'Voluptatem quidem iusto totam beatae. Accusantium non qui adipisci veritatis. Dolores earum quia qui aut. Quia tenetur et corporis corporis. Suscipit ea eos voluptas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 3, 4, 7, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(142, 'Non fugiat eius.', 'Eligendi et ea.', '5.6 Crore', '4', '4', '1500 Sq. Yd.', 'Soluta sunt veniam dolores et inventore. Consequatur ad officia in voluptate repudiandae fugit nulla. Autem enim perferendis beatae atque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 2, 1, 10, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(143, 'Iure nihil deserunt quis et.', 'Culpa nisi.', '2 Crore', '6', '4', '1000 Sq. Yd.', 'Rerum impedit excepturi animi id. Fugiat qui et dolor. Et dolor qui tenetur sit tempora. Et consequatur laboriosam eveniet voluptate modi accusamus quis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 2, 1, 7, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(144, 'Voluptatum excepturi inventore sint illum.', 'Placeat ut.', '9 Crore', '3', '5', '120 Sq. Yd.', 'Fugiat dolore perspiciatis id qui explicabo. Doloremque quia mollitia sunt et. Voluptate debitis cupiditate rem expedita voluptatum aut nobis nihil.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 2, 1, 8, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(145, 'Ut modi possimus delectus.', 'Aperiam rerum ea.', '5 Crore', '5', '2', '150 Sq. Yd.', 'Quis provident eos qui voluptatem. Et dolorem ut numquam nisi numquam et. Minus facere necessitatibus porro molestiae asperiores vitae voluptas cum. Doloribus saepe sed et illum minima.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 3, 4, 9, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(146, 'Iusto quaerat quo doloribus et blanditiis.', 'Officiis asperiores harum.', '3 Crore', '4', '2', '300 Sq. Yd.', 'Sunt id quo ex ipsa quod est. Distinctio perferendis velit minus. Odit nam mollitia illum sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 3, 1, 10, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(147, 'Pariatur dolor et inventore ut.', 'Rerum qui et.', '7 Crore', '7', '4', '600 Sq. Yd.', 'Temporibus quis explicabo non hic sequi maxime. Qui qui officia laudantium atque dolorem doloribus minus. Voluptatem facere et quia voluptatem. Rerum a voluptate corrupti nihil.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 1, 2, 9, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(148, 'Occaecati voluptatem occaecati.', 'Sed rerum.', '6 Crore', '6', '1', '3000 Sq. Yd.', 'Quam vel et eum consequuntur ex harum. Inventore debitis nihil inventore architecto. Cum itaque voluptatem vel architecto.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:19', 1, 0, 2, 4, 6, 2, '2018-12-13 14:24:19', '2018-12-13 14:24:19'),
(149, 'Repudiandae impedit dolorem aut tempore.', 'Quam et reiciendis.', '4 Crore', '4', '2', '1500 Sq. Yd.', 'Ea et voluptas dolor expedita vel. Dignissimos eum nisi laudantium animi perspiciatis rerum quod.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 3, 5, 1, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(150, 'Est ea dolore necessitatibus.', 'Dolore saepe et.', '15 Crore', '7', '2', '150 Sq. Yd.', 'Et at et unde mollitia nostrum. Officiis numquam doloremque unde molestias illo dolores eos. Voluptates et doloremque qui odit natus ea.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 1, 1, 4, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(151, 'Quia veritatis dolor consequatur.', 'Quibusdam tempora quos.', '7 Crore', '5', '5', '1000 Sq. Yd.', 'Quisquam iusto recusandae vero cupiditate doloremque culpa veritatis. Cupiditate ut quibusdam eum rem laborum explicabo dignissimos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 1, 5, 5, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(152, 'In ut nesciunt et doloribus fugiat.', 'Aut perferendis.', '5 Crore', '7', '4', '1500 Sq. Yd.', 'Rerum ut hic est. Velit odit dolor ducimus. Non iusto voluptates magni sint voluptates placeat pariatur. Id asperiores quisquam rerum laboriosam. Optio quia voluptatibus ratione odit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 1, 3, 9, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(153, 'Veritatis ut aut quia.', 'Velit rerum quidem.', '5 Crore', '3', '3', '120 Sq. Yd.', 'Suscipit ipsum ea dolores voluptate blanditiis. Ipsa vel consequuntur et aperiam molestias quam quisquam. Neque alias hic tenetur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 3, 4, 14, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(154, 'Doloribus totam cum omnis.', 'Nihil nobis qui.', '2.5 Crore', '6', '5', '600 Sq. Yd.', 'Iste optio a rem dolores aspernatur sunt. Id corporis omnis doloremque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 2, 1, 10, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(155, 'Ipsum quia voluptate inventore ea.', 'Sit magni recusandae.', '9 Crore', '7', '2', '1500 Sq. Yd.', 'Corporis qui eum quam in eos consequatur. In sint sit rem saepe ea qui ad sint. Magni nihil aut consequuntur. Nisi vel sed quis omnis sed dolorum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 2, 3, 6, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(156, 'Ex fugit laborum earum officia.', 'Temporibus officia ea.', '7 Crore', '7', '2', '200 Sq. Yd.', 'Ducimus doloremque aut quia et blanditiis magni exercitationem. Earum earum et eius autem quia voluptas adipisci. Est iste qui voluptate necessitatibus. Rerum consequatur molestias quis voluptas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 2, 5, 7, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(157, 'Sit quam animi veniam qui.', 'Ullam reiciendis.', '3 Crore', '5', '1', '1500 Sq. Yd.', 'Aut illum autem dolor placeat. Sed pariatur occaecati atque aut voluptates. Officia quia quasi sed ea. Eius non veritatis eum laudantium laborum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 3, 3, 11, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(158, 'Provident dolorem illum.', 'Eos est minus.', '4 Crore', '6', '3', '120 Sq. Yd.', 'Harum accusamus voluptatem earum. Et in aliquam nam qui quia doloremque repellendus. Est repellat dolorem optio porro rem eos molestiae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 2, 4, 12, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(159, 'Fugit sint dolores voluptatum exercitationem sed.', 'Voluptas est.', '6 Crore', '3', '1', '150 Sq. Yd.', 'Voluptatem voluptatem provident qui ea natus. Facilis facilis ducimus fugiat earum ipsum minima quia. Alias eos qui nisi quo aliquid. Quis sed sequi et praesentium autem accusantium tenetur harum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 2, 2, 13, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(160, 'Sed quod recusandae.', 'Dolorem pariatur.', '2 Crore', '4', '3', '3000 Sq. Yd.', 'Voluptas et esse numquam iste aliquid non qui mollitia. Sequi eligendi perferendis voluptatem ipsum nostrum. Maiores ut nisi aliquam ut id. Eius sequi magni optio.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 3, 2, 7, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(161, 'Est illo amet.', 'Quia molestiae et.', '4 Crore', '7', '1', '600 Sq. Yd.', 'Quis labore itaque nihil nulla sit ut quidem. Voluptatem voluptatem maiores eos culpa dicta voluptatem praesentium. Molestias culpa et recusandae accusantium. Incidunt eius voluptatem est labore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 3, 3, 10, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(162, 'Inventore laudantium quo ullam architecto.', 'Vel pariatur.', '2.5 Crore', '5', '2', '600 Sq. Yd.', 'Velit illum dolor eos doloremque labore. Sint et rerum voluptatem aut omnis. Consequatur ut sint sapiente id fugit animi tempore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 2, 2, 8, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(163, 'Aperiam quasi est tempora illum.', 'Asperiores aspernatur.', '3.5 Crore', '3', '4', '300 Sq. Yd.', 'Distinctio dolores est vel qui sint iusto soluta. Architecto nostrum debitis modi quo aut totam libero. Quo soluta at qui ullam. Mollitia vitae culpa beatae mollitia et. Ut pariatur amet ullam iste.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 1, 1, 9, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(164, 'Sapiente quisquam eligendi et enim.', 'Autem ex magnam.', '15 Crore', '7', '4', '120 Sq. Yd.', 'Rem nihil veniam quia dolores reiciendis ut. Nulla excepturi est accusantium earum perspiciatis qui. Quis neque sit corporis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 2, 1, 6, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(165, 'Id a ipsa repellat molestias.', 'Non velit doloribus.', '6 Crore', '6', '4', '300 Sq. Yd.', 'Quas porro sed quo dignissimos dicta eum. Iusto qui quis adipisci. Est voluptatum voluptatem architecto sed consectetur ducimus est ut. Sit aut id eius dicta nostrum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 1, 3, 5, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(166, 'Ut nesciunt beatae dicta iusto.', 'Aperiam omnis et.', '2 Crore', '5', '2', '200 Sq. Yd.', 'Consectetur est quis autem ex dicta deleniti voluptatum voluptas. Consequatur natus accusantium dolorem laudantium dignissimos alias. Neque atque expedita est fuga iusto libero.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:20', 1, 0, 3, 5, 1, 2, '2018-12-13 14:24:20', '2018-12-13 14:24:20'),
(167, 'Maxime incidunt illum ut laborum in.', 'Et officia ducimus.', '4 Crore', '3', '1', '300 Sq. Yd.', 'Et ullam quo qui exercitationem quaerat ea. Beatae omnis ut quas doloremque rerum. Labore optio atque harum ut dignissimos eum expedita quia. Veritatis animi vero autem accusamus est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 3, 1, 12, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(168, 'Vero cupiditate molestiae corrupti a.', 'Voluptatem consectetur.', '3.5 Crore', '4', '3', '150 Sq. Yd.', 'Quisquam fuga laudantium fugit. Ea quo nobis ut exercitationem ad doloremque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 3, 13, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(169, 'Velit dicta qui.', 'Nihil facilis voluptas.', '5.6 Crore', '3', '1', '1000 Sq. Yd.', 'Tempore beatae non aspernatur at libero. Non natus expedita cumque facere et modi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 5, 9, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(170, 'Assumenda quaerat similique mollitia pariatur aliquam.', 'Ipsa sapiente dolorum.', '15 Crore', '6', '4', '600 Sq. Yd.', 'Quibusdam molestias provident nam expedita a fugiat. Quam quo ab vel beatae magnam deleniti. Nobis voluptatem nam quaerat et enim.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 2, 2, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(171, 'Distinctio modi id error.', 'Expedita est ratione.', '4 Crore', '4', '2', '1000 Sq. Yd.', 'Expedita voluptatem sunt consequuntur harum nihil. Explicabo dicta voluptatem maiores earum voluptatem corporis reiciendis. Ea exercitationem placeat illum inventore non. A dolor eos ea in aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 5, 16, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(172, 'Deserunt similique sed.', 'Sunt omnis autem.', '3 Crore', '3', '3', '300 Sq. Yd.', 'Nam facilis pariatur saepe et maiores ab repellat. Eum officia debitis unde sed beatae est. Commodi nisi dicta labore sapiente est qui sit debitis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 1, 2, 4, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(173, 'Non eos et porro magni.', 'Vero incidunt.', '8 Crore', '3', '2', '600 Sq. Yd.', 'Blanditiis eos vel accusamus iusto necessitatibus aspernatur voluptas. Voluptatum harum odit sit sed sed eos consequatur. A quis maiores adipisci in voluptatibus aliquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 4, 13, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(174, 'Molestiae laboriosam dolorem reprehenderit iste in.', 'Omnis qui impedit.', '5 Crore', '5', '5', '300 Sq. Yd.', 'Eos ea ipsa labore earum. Et aut asperiores officiis ut. Amet culpa deserunt voluptatem dolores explicabo quia soluta. Sed amet quae explicabo magnam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 5, 8, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(175, 'Enim soluta qui ducimus velit qui.', 'Animi eos.', '4 Crore', '3', '2', '1000 Sq. Yd.', 'Recusandae quo ipsam autem non est. Quod tempora ut corrupti placeat et. Maxime fuga sed consequatur voluptatibus amet sapiente vel cum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 3, 13, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(176, 'Sequi dolores cumque voluptas corrupti porro.', 'Reprehenderit tempora repellendus.', '5 Crore', '6', '5', '200 Sq. Yd.', 'Omnis nisi quis reprehenderit nostrum ut eaque. Ea temporibus voluptatem ad recusandae. Repellendus excepturi aut delectus nobis blanditiis reprehenderit voluptatem cupiditate.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 3, 2, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(177, 'Dicta est consequatur eaque minima cumque.', 'Quia omnis accusamus.', '2 Crore', '7', '2', '150 Sq. Yd.', 'Dolor labore incidunt iure cum. Maiores deleniti sequi sunt laudantium omnis. Culpa natus delectus doloribus molestias dolores dolor quibusdam. Temporibus ut eaque molestias provident.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 3, 5, 16, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(178, 'Aut non ipsum est esse rerum.', 'Aspernatur quia similique.', '3.5 Crore', '3', '1', '300 Sq. Yd.', 'Fugiat nobis est laborum ipsa aut quia. Velit ut beatae rerum odit. Dolor quibusdam ipsam ab fuga eum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 4, 5, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(179, 'Unde maxime similique eum.', 'Velit voluptatem aliquam.', '5.6 Crore', '7', '1', '1500 Sq. Yd.', 'Sunt ut nulla fuga excepturi. Quasi ipsum magnam mollitia et ab explicabo. Tenetur dolorum qui tenetur incidunt sed. Sunt sed voluptas non quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 3, 9, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(180, 'Blanditiis excepturi nobis id in.', 'Culpa modi.', '3.5 Crore', '4', '4', '120 Sq. Yd.', 'Dicta inventore et corrupti impedit voluptatem ipsam officiis est. Officia culpa voluptatem commodi qui. Alias quidem in eos recusandae facilis reiciendis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 3, 4, 15, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(181, 'Et inventore dicta minus repudiandae qui.', 'Eveniet facere animi.', '15 Crore', '5', '1', '120 Sq. Yd.', 'Voluptatem dolorem dicta ea adipisci dolorem dicta fugiat. Quia sapiente nobis facilis. Expedita tempora aut et aut aperiam velit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 2, 6, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(182, 'Soluta impedit delectus neque autem.', 'Velit et.', '15 Crore', '7', '1', '300 Sq. Yd.', 'Eligendi dignissimos commodi enim veritatis impedit. Minima quia est nihil qui magnam eum ad. Est quas dolorum impedit eveniet. Ut voluptatibus ut omnis optio voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 3, 3, 16, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(183, 'Itaque ea et omnis sapiente.', 'Quis molestiae.', '3.5 Crore', '7', '2', '1500 Sq. Yd.', 'Quia tenetur unde minima aut. Voluptas ut laborum sit earum. Sit aut vel omnis officiis vel a provident incidunt. Necessitatibus nam perferendis ratione quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 1, 3, 1, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(184, 'In ut incidunt commodi quia aspernatur.', 'Aut provident distinctio.', '3.5 Crore', '4', '3', '200 Sq. Yd.', 'At ab consectetur dolorem laudantium qui nemo. In quo iusto soluta esse qui ipsum. Mollitia dolorem voluptatem cum et tempora inventore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 1, 5, 15, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(185, 'Placeat minima commodi eveniet qui.', 'Quae voluptatem reiciendis.', '12 Crore', '6', '4', '150 Sq. Yd.', 'Ducimus architecto eos alias autem eius. Et aut et consectetur necessitatibus. Et atque est vel rerum ut commodi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:21', 1, 0, 2, 1, 6, 2, '2018-12-13 14:24:21', '2018-12-13 14:24:21'),
(186, 'Molestiae autem aut voluptatum non ea.', 'Consequatur similique rerum.', '6 Crore', '5', '4', '3000 Sq. Yd.', 'Repellendus et et odit dolorem. Voluptatem recusandae in animi temporibus voluptatem debitis possimus magnam. Repellendus dignissimos omnis natus maiores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 4, 2, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(187, 'Exercitationem perspiciatis eos dolore est.', 'Assumenda et.', '7 Crore', '6', '5', '300 Sq. Yd.', 'Deserunt vel et libero a. Non accusantium similique facilis est fuga. Accusamus dignissimos modi et amet.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 2, 11, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(188, 'Vero quas dolores eveniet.', 'Eos laudantium.', '9 Crore', '4', '1', '120 Sq. Yd.', 'Alias quia omnis fuga fugiat. Et in sit ex earum. Vitae quas laborum vel eos veniam facilis aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 5, 10, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(189, 'Deleniti aut nemo praesentium dicta.', 'Architecto alias.', '3.5 Crore', '7', '2', '200 Sq. Yd.', 'Nihil maxime excepturi ut enim fugiat qui. Neque atque tenetur et cumque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 1, 2, 7, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(190, 'Voluptas labore pariatur.', 'Explicabo voluptatum sunt.', '3.5 Crore', '6', '4', '300 Sq. Yd.', 'Inventore exercitationem consectetur quia vel vitae qui nihil. Odio sint temporibus minima sunt aperiam doloribus. Facere in aut id possimus odit fugiat minima.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 1, 3, 15, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(191, 'Laborum temporibus ab dolorem.', 'Blanditiis quis.', '7 Crore', '6', '5', '3000 Sq. Yd.', 'Laboriosam alias cupiditate tenetur fugit necessitatibus voluptas. Rerum quasi dolorem et voluptas qui qui voluptatem. Quia et consequatur ut minus repellat delectus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 2, 10, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(192, 'Modi rerum repudiandae.', 'Voluptate sunt.', '8 Crore', '4', '5', '300 Sq. Yd.', 'Repellendus minus repellendus soluta eius. Voluptas beatae distinctio tempore ea est. Excepturi voluptas praesentium veniam mollitia earum quas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 1, 3, 14, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(193, 'Aliquam consequatur vel iure libero.', 'Sapiente dolor voluptatem.', '5 Crore', '3', '3', '150 Sq. Yd.', 'Et illum delectus voluptatem. Aut non quidem nisi ut vel veniam qui. Officiis libero aliquid eos corporis autem et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 5, 2, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(194, 'Tempore explicabo fuga totam.', 'Dolorem est.', '15 Crore', '6', '3', '1000 Sq. Yd.', 'Voluptatibus soluta et qui. Voluptas cumque voluptas et voluptatum mollitia molestiae aut sit. Fugit sed illo accusantium id officia impedit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 2, 12, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(195, 'Ut dolores dicta quaerat.', 'Cupiditate qui.', '15 Crore', '5', '2', '3000 Sq. Yd.', 'Totam iste facilis vel quod ea magnam. Nesciunt nobis est tempore tenetur qui. Ut aspernatur dolore illo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 1, 3, 5, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(196, 'Et labore nihil sit impedit.', 'Est rem velit.', '2 Crore', '5', '5', '1000 Sq. Yd.', 'Quae ipsa ex aspernatur qui. Magnam praesentium veritatis fugiat dignissimos et. Incidunt aperiam est quis praesentium et minima. Tenetur non consequatur qui error.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 4, 4, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(197, 'Ullam dolor molestiae.', 'Nam enim.', '3.5 Crore', '7', '5', '150 Sq. Yd.', 'Eligendi qui vitae fugit aliquid ab omnis assumenda. Aperiam impedit debitis quisquam asperiores. Nostrum dolor impedit maxime modi sit sunt ut dolorem. Fuga odit saepe sequi et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 4, 10, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(198, 'Fugiat autem rerum est dolores.', 'Modi eum ipsum.', '3.5 Crore', '5', '2', '600 Sq. Yd.', 'Reprehenderit nesciunt autem occaecati quae. Et culpa autem occaecati magni quas vero quam. Quisquam hic ex blanditiis sed quae a vel et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 4, 4, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(199, 'Laudantium voluptas excepturi unde.', 'Asperiores dolorum.', '4 Crore', '5', '3', '300 Sq. Yd.', 'Tempore eos beatae non ullam non. Qui sit provident molestiae aperiam provident distinctio laudantium. Ab magnam et incidunt iste qui. Amet quaerat saepe cupiditate distinctio quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 2, 5, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(200, 'Reiciendis vitae tenetur cumque quod.', 'Minima eligendi laboriosam.', '5 Crore', '3', '3', '150 Sq. Yd.', 'Illo est nam qui omnis maiores asperiores aspernatur. Qui et sint quis quos corrupti aut facilis. Magni et maiores eos et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 3, 8, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(201, 'Repellat odio et aut quas.', 'Esse quidem.', '12 Crore', '4', '1', '150 Sq. Yd.', 'Explicabo debitis autem officia sint occaecati possimus. Nulla dolore dolores id eius qui dolorem. Sequi sit quo consequatur libero id tempore aut accusantium.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 4, 16, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(202, 'Ipsa quia at optio.', 'Sit et sint.', '7 Crore', '5', '1', '3000 Sq. Yd.', 'Expedita inventore aut incidunt vel recusandae. Magnam a similique dolore voluptatem odit debitis. Et hic dolor fugiat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 1, 12, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(203, 'Corrupti harum aspernatur velit.', 'Voluptate at consequatur.', '5 Crore', '4', '4', '1500 Sq. Yd.', 'Vel cumque sapiente quia praesentium laboriosam vel. Cumque porro commodi quasi expedita sit. Minus consequatur qui rerum inventore et qui rerum. Corrupti alias necessitatibus perferendis corrupti.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 3, 4, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(204, 'Debitis delectus ut.', 'Eos ut.', '2 Crore', '5', '2', '120 Sq. Yd.', 'Atque eaque ex ut ut. Sunt alias necessitatibus quidem pariatur aut. Quisquam dicta tempora et occaecati. Et voluptate aperiam saepe natus. Numquam expedita amet non provident eum nisi voluptatibus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 4, 15, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(205, 'Incidunt tempora unde.', 'Illo molestias in.', '9 Crore', '6', '4', '120 Sq. Yd.', 'Et qui ea et et. Numquam accusantium earum corporis dolor iusto quasi delectus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 1, 1, 8, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(206, 'Sit illo ut.', 'Et aut.', '3 Crore', '3', '4', '200 Sq. Yd.', 'Voluptas sequi laudantium quia velit saepe ea. Eum fuga nisi quia doloribus. Cumque consectetur eaque voluptatem maiores aspernatur. Ex adipisci qui quibusdam ad impedit nesciunt.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 2, 5, 7, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(207, 'Dolores earum dignissimos.', 'Animi eos nulla.', '3 Crore', '7', '4', '150 Sq. Yd.', 'Voluptas aliquam similique labore quaerat perspiciatis sed. Doloribus nam accusamus velit non eligendi ab error. Cupiditate expedita dolor eos animi consectetur. Ea sint eveniet aut corporis ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 5, 1, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(208, 'Nihil voluptatem doloribus a maiores.', 'Aut veniam.', '6 Crore', '4', '4', '3000 Sq. Yd.', 'Dicta laudantium est pariatur in iure dicta. Quos non sint quidem quod explicabo. Rerum ad est veritatis consequatur laudantium. Sit et aliquid quisquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 1, 5, 3, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(209, 'Minima deserunt officia.', 'Doloremque sed odit.', '6.5 Crore', '7', '2', '600 Sq. Yd.', 'Et cum ratione cupiditate voluptates est non corrupti. Consectetur ex animi sunt minus velit est. Aspernatur impedit consequatur porro est illum et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:22', 1, 0, 3, 2, 15, 2, '2018-12-13 14:24:22', '2018-12-13 14:24:22'),
(210, 'Harum maxime est eligendi magni provident.', 'Aut iure.', '6.5 Crore', '7', '1', '120 Sq. Yd.', 'Placeat dolorem quia aut maxime beatae a. Pariatur et rem expedita sed beatae libero. Velit et odit repellendus ut iure ipsa. Et fuga molestiae hic libero.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 1, 15, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(211, 'Esse aut ut laboriosam consequuntur.', 'Nesciunt et labore.', '9 Crore', '4', '2', '1500 Sq. Yd.', 'Voluptas itaque facilis sit ipsum animi consequatur ea maiores. Sint beatae non non ut inventore molestias nam ad.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 3, 5, 4, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(212, 'Repellendus velit dolores quas id.', 'Est minus.', '6 Crore', '7', '1', '1500 Sq. Yd.', 'Placeat quam ex ut optio. Eum et possimus eos ipsam quam. Ab dolorum nam hic illo culpa illum. Molestiae occaecati iste atque unde molestiae. Sit pariatur culpa aut et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 3, 3, 11, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(213, 'Vitae molestias dolorem consequatur provident.', 'Ipsum facilis.', '3.5 Crore', '6', '2', '120 Sq. Yd.', 'Odit ipsa aut nam. Corporis debitis dignissimos porro dicta est. Et rerum iusto dolorem sit blanditiis placeat cum. Perspiciatis error labore quidem officiis numquam impedit ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 2, 16, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(214, 'Ut et tenetur.', 'Illum fugit fugiat.', '9 Crore', '5', '1', '1500 Sq. Yd.', 'Voluptate voluptates facere accusantium omnis et ipsum. Nesciunt ex et ut. Ab rerum aut velit amet voluptatem vel illum repudiandae. Dignissimos et neque voluptas fuga rem et autem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 3, 1, 5, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(215, 'Quidem rerum enim iste.', 'Fugiat dolor modi.', '12 Crore', '5', '5', '150 Sq. Yd.', 'Consectetur sunt odit at aliquam aut ut aut. Fugit vel laborum ullam quaerat cupiditate aut repudiandae. Placeat omnis ab excepturi ex ratione. Quos qui maxime ullam rem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 2, 5, 14, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(216, 'Et rerum quo.', 'Quibusdam facilis occaecati.', '8 Crore', '3', '2', '600 Sq. Yd.', 'Eius et animi voluptatibus distinctio velit omnis qui. Qui quam velit qui natus. Aliquam rerum doloremque nemo minus officiis ullam. Qui pariatur non totam et labore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 3, 13, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(217, 'Occaecati illum id commodi nam incidunt.', 'Voluptatem consequatur.', '15 Crore', '7', '5', '3000 Sq. Yd.', 'Quidem alias sunt fuga incidunt. Iste quas voluptate sunt laudantium cumque rem velit. Sapiente temporibus ipsum est alias. Praesentium quia ut qui commodi. In et nihil dolores ea.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 3, 2, 2, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(218, 'At modi voluptas ducimus cumque explicabo.', 'Aperiam ut.', '2 Crore', '7', '1', '1000 Sq. Yd.', 'Aspernatur rerum velit molestiae. Nesciunt qui in porro a omnis. Voluptatibus odio natus non quidem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 3, 14, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(219, 'Nostrum eveniet et in.', 'Ducimus non.', '9 Crore', '3', '3', '1500 Sq. Yd.', 'Atque ipsam quis quidem sit provident at. Ut vitae dolorum ut ut ad iste incidunt sit. Reprehenderit et non aut incidunt magnam. Eos id dolores voluptatibus consectetur officia officia nemo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 3, 4, 3, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(220, 'Eveniet tenetur eveniet eligendi neque.', 'Quaerat nihil.', '6.5 Crore', '3', '2', '3000 Sq. Yd.', 'Non velit ab est aliquam modi. Repudiandae alias cumque aut fuga quod. Voluptate velit saepe odio voluptatem consequatur nostrum sint. Atque qui ex ipsa a numquam omnis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 3, 3, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(221, 'Facere iste est labore.', 'Sunt fugit.', '2 Crore', '5', '2', '150 Sq. Yd.', 'Sit et quasi labore. Aut id sapiente maxime quisquam corporis labore rerum. Qui consequuntur sed neque odio. Quidem laboriosam et eveniet illo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 1, 11, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(222, 'Accusamus vel odio recusandae.', 'Molestiae quia.', '2 Crore', '6', '4', '120 Sq. Yd.', 'Et et voluptates quia aut voluptate libero earum. Est est corrupti voluptas eius vero. Nam eos sed ab natus placeat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 4, 11, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(223, 'Esse unde odio nemo et aut.', 'Harum saepe ut.', '6.5 Crore', '6', '3', '120 Sq. Yd.', 'Voluptates aut et atque corporis. Laborum alias placeat harum est tempore sed. Nisi qui autem in est dolore. Natus est aperiam distinctio sunt facere ut velit aperiam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 1, 2, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(224, 'Numquam a perspiciatis voluptas.', 'Omnis quis.', '7 Crore', '3', '4', '3000 Sq. Yd.', 'Est non sed autem. Dolorem id distinctio quos harum sunt inventore deleniti. Et facilis qui officia modi molestiae aut sequi. Perspiciatis in ea et quos fugit et veniam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 2, 3, 1, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(225, 'Id deleniti voluptatem et.', 'Inventore recusandae ipsam.', '2.5 Crore', '6', '5', '1000 Sq. Yd.', 'Ut iure et sit iusto voluptatem nobis optio. Necessitatibus dignissimos quibusdam dolore sapiente impedit. Voluptas eum ut similique cupiditate sequi omnis quia. At omnis voluptatem laboriosam cum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 2, 4, 13, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(226, 'Id in culpa.', 'Quo repellendus natus.', '3.5 Crore', '3', '5', '600 Sq. Yd.', 'Reiciendis quia nam quisquam excepturi deleniti in delectus. Velit libero doloremque repellendus necessitatibus et at. Occaecati qui et deserunt quam magnam totam. Ipsum deleniti explicabo rerum et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 3, 1, 7, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(227, 'Et et modi facilis.', 'Accusantium aut natus.', '3 Crore', '7', '2', '300 Sq. Yd.', 'Est aut sunt suscipit veniam perferendis. Amet et nisi unde sint tempore excepturi. Et ad unde itaque ut. Illo vitae sit nisi eligendi quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:23', 1, 0, 1, 4, 12, 2, '2018-12-13 14:24:23', '2018-12-13 14:24:23'),
(228, 'Voluptatibus earum atque omnis mollitia.', 'Voluptatum impedit quia.', '4 Crore', '4', '3', '150 Sq. Yd.', 'Vitae at qui perferendis ab rerum saepe quia. Consectetur quo voluptate fuga vitae. Dicta iusto enim quibusdam vel veritatis odio suscipit. Et molestiae et vero maiores cupiditate commodi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 4, 1, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(229, 'Ipsum sapiente qui.', 'Eligendi veritatis earum.', '12 Crore', '4', '1', '150 Sq. Yd.', 'Expedita amet sit aliquid in vitae. Nihil itaque et earum. Incidunt vel repudiandae aut ipsa dolore dolor. Et nemo atque debitis consequatur quis perspiciatis pariatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 5, 3, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(230, 'Nihil culpa necessitatibus officia.', 'Earum nisi odio.', '6 Crore', '7', '2', '200 Sq. Yd.', 'Et ad aut commodi commodi autem laboriosam quidem. Sunt vel vero sed voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 2, 9, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(231, 'Aut voluptatem aliquam.', 'Aperiam velit ab.', '6.5 Crore', '3', '2', '300 Sq. Yd.', 'Eligendi blanditiis voluptas doloribus hic autem. Et voluptatibus ut neque quis. Quibusdam exercitationem architecto saepe vitae sit officia nobis. Architecto recusandae inventore nulla et molestias.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 3, 14, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(232, 'Dignissimos aut quasi sed.', 'Aut ut id.', '2 Crore', '3', '4', '150 Sq. Yd.', 'Laboriosam quia modi sequi blanditiis quia sit. Facere ut molestias voluptates est. Ut sequi eos quos ducimus ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 3, 5, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(233, 'Omnis facere a ipsum.', 'Quis voluptatibus.', '8 Crore', '4', '3', '300 Sq. Yd.', 'Consectetur aut voluptas voluptatibus repellendus ut quia. Voluptatem fugit ea ex. Voluptatibus nobis aut sapiente corporis officia. Consequatur possimus natus esse.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 3, 14, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(234, 'Quod quos voluptates minus amet.', 'Accusamus aut ducimus.', '4 Crore', '5', '1', '120 Sq. Yd.', 'Sint ab et repudiandae quidem blanditiis unde est doloribus. Error maiores odio optio dolore a. Qui dignissimos molestias et facere. Fugiat aut ea aut aut eos delectus nostrum iusto.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 5, 1, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(235, 'Explicabo et eveniet doloremque deserunt.', 'Eveniet porro.', '3.5 Crore', '5', '4', '1000 Sq. Yd.', 'Est ut vitae et et ut totam aliquam. Quisquam culpa ut sapiente ab et magnam. Dolor et quis ut quidem nesciunt non.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 2, 4, 3, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(236, 'Itaque earum dolorum omnis officia.', 'Ducimus impedit doloribus.', '12 Crore', '4', '1', '3000 Sq. Yd.', 'Laboriosam ut ut pariatur omnis. Perferendis qui ut accusamus libero sit provident qui recusandae. Vero voluptatem reiciendis aut accusamus deleniti. Pariatur quas culpa vel nihil error quibusdam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 5, 16, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(237, 'Pariatur architecto maxime cupiditate sed.', 'Perspiciatis minus fuga.', '6 Crore', '7', '1', '200 Sq. Yd.', 'Ipsum distinctio recusandae quibusdam natus at. Numquam deserunt voluptatem quasi suscipit et tempore. Reprehenderit quisquam vitae at deleniti modi nemo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 3, 16, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(238, 'Cumque est vitae.', 'Perferendis suscipit.', '12 Crore', '3', '3', '1000 Sq. Yd.', 'In nulla optio et ea. Vitae aliquid molestias culpa sunt. Ut voluptatem aliquam numquam doloribus. Perspiciatis ipsam eligendi doloremque nihil dicta.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 1, 4, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(239, 'Velit rem aut.', 'Inventore magni perspiciatis.', '5 Crore', '5', '3', '300 Sq. Yd.', 'Officiis aut neque nemo dolores ullam commodi. Eos ab dolore pariatur aut asperiores itaque. Eaque ut commodi et quas ex. At amet voluptate tempora omnis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 3, 12, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(240, 'Id eos ut beatae dolorem.', 'Eum eligendi.', '3 Crore', '5', '1', '1500 Sq. Yd.', 'Aspernatur quia iusto minus et quidem non. Corporis sit et id.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 2, 3, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(241, 'Ducimus dolorem consequuntur sunt.', 'Aliquid saepe.', '7 Crore', '5', '2', '120 Sq. Yd.', 'Quaerat aut dicta aliquid magnam et enim. Quis voluptatum itaque nobis. Soluta tempora architecto eius. Voluptatem delectus occaecati accusantium aperiam nam dolores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 2, 15, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(242, 'Et dolores accusamus eligendi soluta iusto.', 'Fugit dolores.', '6 Crore', '7', '5', '3000 Sq. Yd.', 'Provident et ut mollitia aut quis aut mollitia et. Quia sed natus molestiae. Fuga delectus aut rerum voluptatum. Ipsam amet voluptatum molestiae quidem. Quae quidem enim id ex eaque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 1, 7, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(243, 'Sed sed officia et et maxime.', 'Dolor eaque suscipit.', '7 Crore', '7', '2', '3000 Sq. Yd.', 'Nihil rerum quia eius cum aut quis. Veritatis odit libero natus a. Autem quibusdam dolore ut minus. Dolores nam error quibusdam architecto qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 1, 7, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(244, 'Fugit facilis et id.', 'Commodi iusto architecto.', '2.5 Crore', '4', '4', '1500 Sq. Yd.', 'Et ex dolores et in. Nisi qui vel repellat quia id amet. Sit perspiciatis porro repudiandae et quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 1, 12, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(245, 'Perspiciatis cumque autem tempore velit.', 'Autem est.', '9 Crore', '7', '5', '3000 Sq. Yd.', 'Blanditiis inventore facilis quia et. Magnam ab eius minus qui. Beatae pariatur vero perferendis perferendis mollitia non. Est repellat non omnis ipsa ut facilis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 5, 6, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(246, 'Eius quia quas.', 'Nobis ut enim.', '6.5 Crore', '3', '2', '150 Sq. Yd.', 'Qui culpa sapiente ut quas eligendi ut. Placeat illo harum qui cum impedit et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 2, 4, 10, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(247, 'Accusamus sed ut.', 'Qui nam.', '3.5 Crore', '7', '5', '150 Sq. Yd.', 'Animi veritatis qui dignissimos sunt maxime. Aut est qui consequatur illo. Est amet voluptatibus quia numquam ut. Dolor adipisci delectus et ab itaque ullam reiciendis placeat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 3, 1, 1, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(248, 'Animi inventore facilis placeat.', 'Repellendus ut optio.', '7 Crore', '4', '3', '1000 Sq. Yd.', 'Omnis soluta sit est rerum sint dolore ut. Non nihil ipsa dolor expedita enim. Aut ad iure veniam beatae qui dolorem minima. Facilis illo saepe labore ipsa exercitationem qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 1, 1, 9, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(249, 'Est optio quia blanditiis ut.', 'Earum architecto.', '15 Crore', '5', '5', '1000 Sq. Yd.', 'Dolor at beatae reprehenderit officia accusantium asperiores. Rerum facere unde dolorem pariatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 2, 2, 16, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(250, 'Sed nostrum eligendi.', 'Laboriosam itaque ut.', '7 Crore', '6', '5', '200 Sq. Yd.', 'Non sapiente enim expedita tenetur natus. Ex suscipit sed rerum ad eos est voluptatem. Sit reprehenderit possimus vitae et ipsa aperiam eveniet amet.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:24', 1, 0, 2, 4, 6, 2, '2018-12-13 14:24:24', '2018-12-13 14:24:24'),
(251, 'Commodi perferendis iste qui.', 'Animi debitis molestiae.', '6.5 Crore', '5', '3', '150 Sq. Yd.', 'Aliquam maiores veniam culpa debitis odio. Repellat enim quaerat aperiam asperiores necessitatibus dolor. Doloribus neque sed dolores magni.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 2, 5, 12, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(252, 'Esse reiciendis voluptatem optio.', 'Aut qui.', '5 Crore', '4', '2', '120 Sq. Yd.', 'Aut et rerum dolor quo non. Error sed delectus eligendi nisi omnis est. Incidunt illo quaerat excepturi quam voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 2, 5, 5, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(253, 'Et et est quia.', 'A voluptatem ut.', '8 Crore', '7', '5', '150 Sq. Yd.', 'Ea architecto ex adipisci aliquid. Consequatur quisquam et voluptates. Fuga consectetur laudantium qui expedita veniam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 2, 8, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(254, 'Architecto eos incidunt vel dignissimos.', 'Tenetur voluptates.', '12 Crore', '4', '2', '3000 Sq. Yd.', 'Ratione quas labore adipisci debitis praesentium tempore. Est ut quidem eaque officiis dolores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 2, 1, 2, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(255, 'Quasi veritatis qui sed neque.', 'Ullam laudantium rerum.', '3 Crore', '7', '3', '120 Sq. Yd.', 'Tempora iusto voluptatem omnis et hic id pariatur. Inventore et impedit facere omnis. Voluptatibus dolor provident veritatis ad reiciendis. Praesentium minima earum culpa ducimus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 5, 9, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(256, 'Quod voluptatibus nihil.', 'Doloribus ullam.', '6 Crore', '4', '4', '3000 Sq. Yd.', 'Aut molestiae laborum distinctio ipsam omnis rem quaerat maxime. Atque est a sed provident quasi qui quia. Hic omnis reiciendis itaque facilis odit dicta sit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 5, 1, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(257, 'Officia voluptas possimus voluptatum.', 'Nihil qui ut.', '2.5 Crore', '4', '5', '1500 Sq. Yd.', 'Omnis quidem sequi eligendi expedita ratione iure eaque. Odio aut hic iusto ipsam omnis rerum. Mollitia non explicabo consequatur ratione. Atque minus numquam deleniti ducimus doloribus tenetur id.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 2, 9, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(258, 'Qui rerum veniam illum rerum.', 'Ducimus dolorem quidem.', '7 Crore', '5', '4', '3000 Sq. Yd.', 'Repellendus ut ad harum sed error. Reprehenderit totam omnis in et asperiores dicta. Est beatae rerum qui optio autem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 3, 2, 1, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(259, 'Quod optio quia.', 'Eveniet voluptates voluptate.', '6 Crore', '3', '5', '1500 Sq. Yd.', 'Qui ut quam ut sed. Saepe at optio consequatur recusandae laboriosam autem aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 3, 9, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(260, 'Impedit eaque et modi et assumenda.', 'Molestias provident velit.', '8 Crore', '5', '3', '600 Sq. Yd.', 'Ratione omnis deleniti sapiente. Ut numquam officia qui architecto facere eveniet illo beatae. Ipsam sed voluptates tempore. Iste sint et dolores rem est quasi neque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 1, 3, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(261, 'Sit deserunt molestiae voluptatum voluptas.', 'Incidunt dolor quaerat.', '3.5 Crore', '4', '1', '300 Sq. Yd.', 'Et et molestias ut quia ut. Fugiat omnis eius facilis qui nulla aspernatur. Ea voluptatibus sint est omnis veniam eum quia. Et officia omnis porro eos dolore sint quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 4, 9, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(262, 'Dolorem dolor molestias ipsum.', 'Voluptatum dolorem.', '5 Crore', '4', '1', '120 Sq. Yd.', 'Suscipit sapiente vero blanditiis rem ut eligendi dolores. Id adipisci rerum dignissimos. Illum enim et aut rerum soluta dolor cumque ex.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 2, 5, 14, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(263, 'Quis voluptatibus sapiente.', 'Ducimus deleniti suscipit.', '3.5 Crore', '3', '5', '120 Sq. Yd.', 'Vero nemo nostrum est aut. In repellat aut temporibus quibusdam reprehenderit similique ullam quis. Minima officiis et ducimus in non aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 2, 5, 7, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25');
INSERT INTO `property` (`id`, `title`, `slogan`, `price`, `beds`, `baths`, `area`, `details`, `img`, `exp_date`, `status`, `featured`, `purpose_id`, `type_id`, `area_id`, `user_id`, `created_at`, `updated_at`) VALUES
(264, 'Fugit culpa eius dolorem dolorem.', 'Quod quod.', '3.5 Crore', '5', '5', '600 Sq. Yd.', 'Asperiores dolorem fuga enim. Consequatur corrupti quam consequatur. Beatae voluptatem non quisquam rerum. Sit maiores in tempora ut repellat corporis ratione.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 3, 6, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(265, 'Sed illo qui explicabo.', 'Delectus impedit.', '4 Crore', '6', '2', '120 Sq. Yd.', 'Fugiat consequatur tempora vel debitis. Ea rerum consequatur et qui eveniet voluptas incidunt. Suscipit hic ex aliquam omnis voluptates tenetur. Nesciunt omnis eveniet aperiam dolores voluptatem nam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 2, 4, 4, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(266, 'Quibusdam architecto harum.', 'Voluptate deserunt assumenda.', '2.5 Crore', '7', '2', '600 Sq. Yd.', 'Nihil consectetur deleniti et sed soluta harum. Eius doloribus voluptatem eum et repellendus unde dolores. Quaerat et temporibus praesentium eaque natus atque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 2, 9, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(267, 'Enim dicta aut fugit.', 'Totam dolorum perferendis.', '8 Crore', '3', '3', '300 Sq. Yd.', 'Dolores iusto omnis suscipit vel ut consequatur. Itaque sunt quaerat dolor rem eum enim quas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 1, 15, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(268, 'Accusamus voluptas culpa quia deleniti.', 'Perspiciatis repudiandae alias.', '4 Crore', '7', '1', '1500 Sq. Yd.', 'Rem porro voluptatibus non suscipit magni. Exercitationem neque reiciendis tenetur rerum dolores iste. Aliquam qui doloremque in dolorum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 3, 6, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(269, 'Non non quasi aperiam non.', 'Tempora eos.', '2 Crore', '5', '3', '300 Sq. Yd.', 'Voluptatem aut nostrum ea facilis. Qui velit quaerat vel et. Praesentium impedit est dolorem doloremque suscipit provident. Aut autem culpa dolores magni.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 1, 4, 12, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(270, 'Soluta dolore adipisci quia.', 'Totam molestiae.', '6.5 Crore', '4', '4', '1500 Sq. Yd.', 'Libero voluptatibus labore quia ipsum aliquid et in. Quod ipsum itaque et accusantium totam alias aspernatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 3, 2, 7, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(271, 'Sunt natus eligendi sed.', 'Cupiditate ea.', '5 Crore', '7', '5', '300 Sq. Yd.', 'Officia dolor debitis et est sed velit neque magni. Ipsam est labore porro eius. Aut ad beatae dolores hic eum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 3, 3, 4, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(272, 'Velit ipsam cum quae.', 'Recusandae suscipit quasi.', '3.5 Crore', '5', '3', '120 Sq. Yd.', 'Labore perferendis aspernatur sed dolor quae qui. Fugit atque non vel aut sed et. Rerum maiores laborum suscipit autem est quam quam. Qui dolor similique at porro dicta velit culpa.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:25', 1, 0, 2, 4, 10, 2, '2018-12-13 14:24:25', '2018-12-13 14:24:25'),
(273, 'Dolores voluptatem sint aliquam.', 'Consequatur nihil et.', '9 Crore', '5', '4', '150 Sq. Yd.', 'Est enim itaque ab. Harum et et consequuntur nihil. Molestiae voluptatem a non aspernatur rerum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 1, 1, 10, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(274, 'Doloribus voluptatum neque vero atque nulla.', 'Vel sed ea.', '8 Crore', '3', '3', '3000 Sq. Yd.', 'At mollitia ad aut laboriosam totam reiciendis. Dignissimos sunt et eius eius sapiente repellendus. Rerum earum reiciendis aut officia et animi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 1, 14, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(275, 'Aut nulla eos enim id.', 'Laboriosam quas natus.', '8 Crore', '5', '2', '200 Sq. Yd.', 'Quasi commodi exercitationem vitae et. Ut et vitae omnis alias iure sed minus. Explicabo nostrum quam non pariatur quaerat sit. Nihil officiis aut consequatur est repellendus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 2, 5, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(276, 'Temporibus expedita blanditiis.', 'Repellat ducimus possimus.', '3.5 Crore', '3', '2', '200 Sq. Yd.', 'Et quia laboriosam et soluta sed magnam. Aperiam consectetur ipsa fuga sapiente aliquid est. Iste voluptatem voluptas autem esse laborum quidem voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 1, 15, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(277, 'Laudantium quod fuga tenetur.', 'Voluptates architecto eum.', '8 Crore', '3', '4', '1500 Sq. Yd.', 'Qui deleniti cumque quidem. Consequatur rem iste adipisci quia. Alias accusantium illum sit sunt. Eveniet aut ipsum velit vero ex.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 1, 3, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(278, 'Placeat nam et id ipsam vel.', 'Libero eligendi.', '3 Crore', '5', '1', '200 Sq. Yd.', 'Est assumenda asperiores maiores fugiat consequatur. Quis neque id ducimus non nihil blanditiis. Eum dignissimos ullam fugiat incidunt eum tempora earum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 3, 13, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(279, 'Et sit odio et est perferendis.', 'Consequuntur et.', '12 Crore', '5', '2', '120 Sq. Yd.', 'Quae officia commodi deserunt sint. Velit ut dolorem sunt ea non possimus corporis. Voluptatem autem qui voluptas expedita quo. Autem corporis consectetur quos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 3, 1, 16, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(280, 'Officia rem et excepturi.', 'Distinctio iste.', '9 Crore', '5', '2', '3000 Sq. Yd.', 'Magnam non dolor quos suscipit aut et. Quae voluptatem ipsam dolores at qui. Dolore illo omnis mollitia fugiat nisi perspiciatis eum. Exercitationem eos blanditiis sequi sunt.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 3, 1, 13, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(281, 'Recusandae amet eos.', 'Pariatur qui quisquam.', '3 Crore', '6', '1', '3000 Sq. Yd.', 'Tempora itaque laboriosam eos non qui. Sit recusandae ducimus voluptate velit. Omnis sit nostrum molestiae qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 1, 5, 14, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(282, 'Itaque deleniti aut quia.', 'Tempore culpa in.', '12 Crore', '5', '4', '3000 Sq. Yd.', 'Perferendis doloremque tempora magni facilis aliquam maxime. Illo atque vel aliquid est odit tempore animi. Est neque perspiciatis quisquam ut. Soluta qui ipsum qui sunt ipsum nobis ut molestiae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 3, 1, 7, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(283, 'Nisi id id.', 'Laborum eum.', '7 Crore', '7', '1', '300 Sq. Yd.', 'Labore expedita iusto consectetur beatae dolorem voluptatibus at. Laudantium nulla itaque in. Et voluptas dolores dolore quo. Optio fugiat non voluptas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 3, 4, 9, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(284, 'Reprehenderit voluptate ratione rerum consequatur.', 'Et officia animi.', '5.6 Crore', '3', '2', '200 Sq. Yd.', 'Animi eveniet maiores consectetur ea omnis. Voluptatum eaque provident iusto ut quia perspiciatis. Voluptatibus veniam aut necessitatibus tempore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 3, 2, 6, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(285, 'Qui quia velit eius.', 'Doloribus quod.', '8 Crore', '6', '3', '1500 Sq. Yd.', 'Ipsa modi eum accusantium libero sed voluptatibus molestiae modi. Veniam qui incidunt necessitatibus est neque ut. Consequatur velit vitae quia et dignissimos id dignissimos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 2, 8, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(286, 'Qui porro in.', 'Maiores harum.', '6.5 Crore', '4', '1', '150 Sq. Yd.', 'Et nam qui consequatur deleniti saepe repellendus. Rem facilis voluptatem nostrum odio dolor incidunt est. Corrupti dolores dignissimos laborum neque ut quisquam ipsum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 1, 8, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(287, 'Doloremque non et amet.', 'Quia et.', '5 Crore', '4', '1', '1500 Sq. Yd.', 'Quo distinctio totam culpa et harum asperiores. Possimus quibusdam sint voluptas saepe reiciendis provident explicabo animi. Distinctio necessitatibus cupiditate eius ipsa et ratione non.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 5, 10, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(288, 'Esse minus doloremque molestiae qui.', 'Autem cupiditate asperiores.', '5.6 Crore', '3', '2', '300 Sq. Yd.', 'Impedit asperiores repellendus repellendus eos omnis. Laboriosam quia aut ex eum facere officiis aut vitae. Quia necessitatibus ea provident modi qui harum deleniti.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 1, 15, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(289, 'Ut animi non suscipit quis.', 'At nobis.', '7 Crore', '4', '4', '1500 Sq. Yd.', 'Et esse iusto ut ut autem aut enim sed. Sunt beatae ut vel molestiae ut eos. Ab eius voluptatem unde dicta molestias fuga unde qui. Sapiente officiis et ex delectus ut labore rerum quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 3, 4, 6, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(290, 'Mollitia adipisci illum distinctio necessitatibus doloribus.', 'Facere est.', '4 Crore', '7', '1', '120 Sq. Yd.', 'Explicabo dolor hic reprehenderit rerum. Minus exercitationem et consequatur quia ab illo. At occaecati dolorum facilis corporis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 3, 4, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(291, 'Cum aperiam et sapiente ut veniam.', 'Provident commodi ut.', '2 Crore', '5', '5', '1500 Sq. Yd.', 'Ut reiciendis rerum cumque eius incidunt est. Earum non eaque eum dolor. Labore odio eligendi aspernatur ipsa iste est non. Quae repellat sunt corrupti voluptatem ea nam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 1, 4, 4, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(292, 'Et nemo ea reiciendis amet quos.', 'Et rerum.', '12 Crore', '6', '5', '1500 Sq. Yd.', 'Nam id enim fugit placeat consequatur. Vitae fuga qui fugit voluptate in nulla molestiae. In autem iste temporibus eos minima explicabo culpa.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 4, 5, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(293, 'Laborum laboriosam perferendis est.', 'Nostrum qui.', '6.5 Crore', '7', '5', '1000 Sq. Yd.', 'In eveniet quam nesciunt voluptas voluptatibus in quam. Ab rem reprehenderit et quis. Recusandae cumque quam natus officiis provident consequatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 1, 2, 13, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(294, 'Architecto ipsa sunt aperiam.', 'Sed et.', '9 Crore', '6', '1', '600 Sq. Yd.', 'Vel et corrupti explicabo. Laudantium officiis saepe blanditiis eius praesentium pariatur a cum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 4, 3, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(295, 'Quam incidunt reprehenderit praesentium.', 'Sint amet veniam.', '5 Crore', '5', '1', '1000 Sq. Yd.', 'Quibusdam nesciunt recusandae cum amet neque ea. Autem qui vitae itaque magnam qui commodi omnis. Odio sint vel omnis expedita cupiditate a.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 2, 2, 9, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(296, 'Repellendus doloremque iste tempore in.', 'Ut nesciunt.', '15 Crore', '4', '4', '200 Sq. Yd.', 'Non iure deleniti doloribus. Sint dolore totam saepe sunt quas. Excepturi quia perferendis nisi voluptatem dolores ea unde.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 1, 4, 3, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(297, 'Deserunt molestiae laudantium consequatur.', 'Consequatur maiores.', '2 Crore', '4', '1', '3000 Sq. Yd.', 'Ipsa maxime nemo culpa quo. Libero et architecto facilis enim. Quis non explicabo voluptatem fugiat maiores omnis. Nihil ut aspernatur doloribus minima sit consequatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:26', 1, 0, 1, 2, 13, 2, '2018-12-13 14:24:26', '2018-12-13 14:24:26'),
(298, 'Id vitae rerum.', 'A non.', '3 Crore', '7', '2', '150 Sq. Yd.', 'Magni omnis quaerat reprehenderit et ullam expedita. Laboriosam labore voluptatem fugit consequatur. Est quo odio numquam nulla eaque incidunt alias. Tempora sunt esse eaque rerum doloribus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 4, 10, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(299, 'Officiis soluta similique.', 'Quasi at ut.', '7 Crore', '4', '5', '300 Sq. Yd.', 'Placeat laudantium quia eaque expedita autem et. Quidem laboriosam ullam nobis nulla sint aut occaecati. Et voluptas ut sed dolores. Excepturi vel ea illum quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 1, 14, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(300, 'Incidunt sint consequuntur aperiam.', 'Modi est id.', '8 Crore', '6', '3', '1000 Sq. Yd.', 'Officia iure eos aut sint officia. Fugit autem et temporibus odio molestiae accusantium modi labore. Hic aut ipsam repellendus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 3, 11, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(301, 'Nam voluptatem et quaerat impedit.', 'Sit et.', '4 Crore', '5', '5', '1500 Sq. Yd.', 'Ut ea voluptatem accusamus ut. Quia odit qui eaque doloribus. Et quaerat quo id qui accusamus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 2, 11, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(302, 'Hic pariatur tempore.', 'Rerum culpa in.', '2 Crore', '5', '3', '1000 Sq. Yd.', 'Tempore ratione ut sequi quidem minus quia. Et amet laudantium et nihil aliquam incidunt alias. Expedita maiores similique dignissimos nulla.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 3, 1, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(303, 'Aliquam et quo quia.', 'Totam itaque.', '5 Crore', '6', '1', '300 Sq. Yd.', 'Quia mollitia autem unde corrupti. Temporibus dolorem a quos totam ad. Sint sit omnis aut dolor. Recusandae iusto odio adipisci ad omnis explicabo dolores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 2, 5, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(304, 'Quidem deserunt dicta ipsa labore.', 'Dolorum ut.', '4 Crore', '3', '2', '3000 Sq. Yd.', 'Accusantium architecto laudantium saepe earum nobis quis ullam. Nihil ab voluptatem ipsam. Omnis enim vel sunt sint modi. A tenetur ex dolores aliquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 2, 14, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(305, 'Accusantium accusamus optio minima unde aut.', 'Corrupti dolor.', '5.6 Crore', '5', '1', '1500 Sq. Yd.', 'Inventore vero quidem recusandae possimus nihil. Optio tenetur aut sunt ullam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 4, 1, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(306, 'Ut qui rerum.', 'Rerum quibusdam.', '3.5 Crore', '7', '5', '200 Sq. Yd.', 'Voluptas eos perferendis sint inventore est eum deserunt. Velit voluptatum maiores accusantium nostrum ea aut quod. Excepturi sunt est quae odit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 1, 8, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(307, 'Cum quisquam quos mollitia nostrum.', 'Deserunt facilis consequatur.', '3 Crore', '4', '3', '300 Sq. Yd.', 'Aut rerum voluptas molestiae praesentium et tempora cupiditate. Delectus ratione occaecati aspernatur dolorum. Enim repudiandae magni omnis dolores recusandae. Cumque rerum qui esse voluptatibus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 5, 5, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(308, 'Porro sit rerum autem.', 'Molestiae hic recusandae.', '3.5 Crore', '5', '2', '120 Sq. Yd.', 'Nihil repellat veniam sit. Iusto non minima fugiat et et quod. Est optio sint aut ut natus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 5, 13, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(309, 'Eaque reprehenderit consequatur voluptatibus.', 'Maxime velit.', '6.5 Crore', '5', '5', '150 Sq. Yd.', 'Architecto aliquam doloremque aut eius. Asperiores totam nihil quam quidem. Voluptatem velit cumque ratione cupiditate eius. Qui dolores et consequatur. Modi ea cupiditate ab aperiam rerum qui nobis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 4, 7, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(310, 'Delectus sed voluptatem corporis quia perspiciatis.', 'Voluptatem est.', '9 Crore', '4', '4', '1500 Sq. Yd.', 'Possimus beatae enim pariatur. Temporibus explicabo perspiciatis et modi. Perspiciatis et quidem architecto qui eveniet.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 4, 2, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(311, 'Veniam officia alias mollitia quo.', 'Minus debitis aperiam.', '6 Crore', '3', '2', '3000 Sq. Yd.', 'Id qui voluptatem qui et aut. Vel quis incidunt quas sunt quod. Sint debitis in eius aut recusandae. Excepturi eius neque autem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 1, 6, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(312, 'At voluptates eum neque.', 'Et et.', '5 Crore', '7', '3', '3000 Sq. Yd.', 'Esse quidem maxime totam aut illum. Illo magni occaecati eum consequuntur. Dolorem quam nemo non laborum cumque ex velit. Enim qui ut nam. Pariatur ut dignissimos dolore id autem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 2, 12, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(313, 'Hic pariatur incidunt consequuntur.', 'Deserunt at aperiam.', '5.6 Crore', '7', '5', '600 Sq. Yd.', 'Labore enim voluptatem possimus qui neque omnis. Similique quia vel dolorem quaerat. Et magnam aut autem accusamus et nihil. Quae qui dolores facere dolorem dicta sit doloribus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 1, 1, 12, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(314, 'Iure porro amet voluptate.', 'Numquam minima temporibus.', '15 Crore', '6', '5', '600 Sq. Yd.', 'Consequatur iste quia et consequatur dolore minus. Consequatur esse non ipsam et rem. Voluptas dignissimos sit odit a voluptatibus natus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 1, 10, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(315, 'Aut quasi culpa vel.', 'Voluptatum fuga ut.', '5 Crore', '4', '5', '1000 Sq. Yd.', 'Iste nemo voluptas dicta ut non fuga. Quis placeat repudiandae sit sint. Quas error cumque numquam aut dignissimos illum illo. Praesentium quod beatae dolore voluptatem eum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 4, 11, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(316, 'Consequuntur ab at quia.', 'Animi beatae labore.', '15 Crore', '6', '1', '600 Sq. Yd.', 'Magnam ut culpa ea eveniet. Delectus aut corporis aut iure ducimus illo consequatur. Minus illo dicta aperiam voluptas quam. Voluptatem illum qui provident fugiat numquam iure minima.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 2, 16, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(317, 'Odio voluptatem veniam repellendus sequi nam.', 'Beatae ipsa.', '3.5 Crore', '7', '5', '200 Sq. Yd.', 'Beatae ut magni quibusdam voluptatem. Incidunt quas voluptas illum sunt illum qui perspiciatis vel. Incidunt accusamus harum vero est. Doloribus voluptatem omnis eos aliquid natus minima odio et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 4, 13, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(318, 'Quae dolor asperiores.', 'Aspernatur cum tempore.', '7 Crore', '3', '1', '200 Sq. Yd.', 'Ipsa perferendis magnam expedita quidem reiciendis eveniet. Reiciendis non rerum nulla amet laboriosam voluptatem minima. Magni debitis rem et dicta quia quis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 2, 8, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(319, 'Necessitatibus et impedit accusantium occaecati iure.', 'Perspiciatis aliquid.', '3.5 Crore', '3', '2', '3000 Sq. Yd.', 'Aliquam magni ullam nihil. Veritatis voluptatum in praesentium et labore dicta blanditiis. Unde voluptatem aut dolorum rerum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 2, 1, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(320, 'At a autem ut voluptatem.', 'Eos ut aut.', '8 Crore', '6', '1', '3000 Sq. Yd.', 'Sunt sed at et officiis. Dolor deserunt porro nobis ducimus. Error voluptatem aliquid in repudiandae nobis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 2, 5, 3, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(321, 'Quia odit quisquam.', 'Sed non.', '12 Crore', '3', '5', '150 Sq. Yd.', 'Vero beatae molestias nemo libero ut ut eum in. Distinctio saepe quia voluptate aut qui ut vel. Dolor ut aut commodi possimus occaecati qui. Eum explicabo aperiam est et harum sint dolore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 2, 10, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(322, 'Mollitia ut nam molestias.', 'Non et.', '6.5 Crore', '6', '3', '1000 Sq. Yd.', 'Autem pariatur quo architecto vel repellendus reiciendis corrupti. Aut quis aliquid et explicabo. Porro cum sed minus ipsa et culpa id et. Rerum beatae totam et ut quos. Ad autem eos et quis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:27', 1, 0, 3, 3, 14, 2, '2018-12-13 14:24:27', '2018-12-13 14:24:27'),
(323, 'Id laboriosam magni.', 'Sint repudiandae.', '12 Crore', '4', '1', '1000 Sq. Yd.', 'Quia nesciunt eum perspiciatis nostrum suscipit. Quae velit ut ea eos perferendis maxime. Ratione inventore laboriosam beatae assumenda at ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 3, 3, 15, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(324, 'Sint corrupti magni.', 'Earum recusandae aut.', '12 Crore', '6', '3', '1000 Sq. Yd.', 'Adipisci aliquam non inventore. Aut quibusdam voluptatem animi et repellat. Atque earum vel quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 2, 1, 8, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(325, 'Repellat eveniet aut rerum cumque.', 'Eos dolorum illum.', '3.5 Crore', '6', '3', '150 Sq. Yd.', 'Sunt fuga pariatur aut facilis quia inventore. Quaerat sed nisi similique non nostrum voluptas. Magnam quas quibusdam quo aut quia quod aut. Et quibusdam dolorem molestias facilis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 5, 8, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(326, 'Reiciendis vel quisquam maiores quis.', 'Tempora dolor.', '7 Crore', '7', '5', '150 Sq. Yd.', 'Labore culpa perferendis iste nulla. Dicta rerum a dolorem adipisci veritatis libero quod nobis. Eius unde voluptas culpa repellendus fuga.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 2, 3, 9, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(327, 'Omnis impedit vero sunt.', 'Dolores id nisi.', '3 Crore', '3', '4', '150 Sq. Yd.', 'Quaerat et voluptatum ut dolore maiores tenetur. Minus illo quasi odio minus consequatur rerum eum et. Rerum modi ut at tempore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 5, 12, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(328, 'Maiores quis voluptates.', 'Ea quia.', '7 Crore', '7', '2', '120 Sq. Yd.', 'Magni maxime qui dolorum est amet eaque. Et deleniti ducimus aut perferendis qui nobis laudantium. Voluptas quaerat non sit. Sit magnam adipisci hic corporis magnam sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 3, 4, 7, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(329, 'Minus omnis voluptatem.', 'Maxime omnis nostrum.', '15 Crore', '3', '3', '300 Sq. Yd.', 'Ut ut veritatis quasi maiores consectetur molestiae ea. Asperiores qui voluptas et ea dolores at labore. Modi ipsam excepturi ratione aliquid deleniti earum. Sint possimus rerum enim tenetur eius.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 2, 1, 9, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(330, 'Reiciendis suscipit voluptas eligendi.', 'Molestiae aperiam veritatis.', '3.5 Crore', '4', '2', '200 Sq. Yd.', 'At veniam accusantium et enim eos repudiandae. Est accusantium nemo omnis et omnis enim dolore. Cum fugit est aspernatur exercitationem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 1, 12, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(331, 'Laboriosam sit eos.', 'Delectus aliquam voluptatem.', '5.6 Crore', '6', '4', '1000 Sq. Yd.', 'Necessitatibus modi sunt laudantium. Deserunt voluptate ut rerum maxime. Minima blanditiis itaque fugiat voluptas libero non. Dolore deleniti dolores possimus autem voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 1, 16, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(332, 'Et tempore reprehenderit officia temporibus.', 'Nulla quo voluptates.', '2.5 Crore', '5', '1', '1500 Sq. Yd.', 'Omnis delectus quia ea adipisci. Unde modi qui qui aut qui. Quia quaerat est sed excepturi eveniet eius harum. Non eveniet quos molestiae illum ullam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 1, 13, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(333, 'Enim sit odit recusandae itaque.', 'Temporibus sed.', '3.5 Crore', '6', '3', '1000 Sq. Yd.', 'Aut sint itaque sed voluptates expedita magni. Adipisci corrupti illo laboriosam provident.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 2, 2, 3, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(334, 'Fugiat aut et et dolores.', 'Iste blanditiis et.', '2 Crore', '3', '3', '600 Sq. Yd.', 'Ea corrupti ipsa molestias et. Quos inventore nulla et quia et quam. Excepturi repellendus deserunt minus veniam omnis nihil qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 3, 2, 3, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(335, 'Nihil ad nam nesciunt.', 'Voluptatem nostrum quo.', '7 Crore', '6', '1', '3000 Sq. Yd.', 'Consequatur enim nam tenetur error saepe est dignissimos. Mollitia blanditiis enim provident vitae eos reprehenderit. Doloribus aut et voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 2, 5, 16, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(336, 'Ut totam molestiae rem.', 'Cupiditate placeat molestias.', '2 Crore', '3', '5', '1500 Sq. Yd.', 'Totam odio sed aut veniam et rerum. Recusandae in non nihil eos error officia ipsum. Sed laudantium sint reprehenderit sit. Error suscipit alias amet omnis itaque necessitatibus quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 3, 7, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(337, 'Nisi praesentium velit nisi.', 'Et ipsum.', '2 Crore', '3', '5', '120 Sq. Yd.', 'Sit quos saepe velit sapiente voluptas accusamus expedita. Non atque accusamus aut autem qui cum excepturi. Aspernatur distinctio consectetur ratione error aut ratione corrupti voluptate.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 1, 10, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(338, 'Nemo dolor sed ipsa nam dolore.', 'Modi maiores.', '6 Crore', '7', '2', '1500 Sq. Yd.', 'Temporibus omnis in expedita nesciunt vel. Natus voluptates architecto et debitis rem. Voluptates autem repellendus quae repudiandae eaque culpa ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 2, 5, 12, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(339, 'Voluptas vel nam enim ipsa.', 'Qui fuga ut.', '2 Crore', '4', '3', '200 Sq. Yd.', 'Laudantium voluptas dolor recusandae pariatur. Pariatur et excepturi quia est. Iure totam autem incidunt numquam possimus molestiae consequatur ab.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 2, 16, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(340, 'Sed odio expedita accusamus consequatur.', 'Nisi quia non.', '3.5 Crore', '6', '5', '300 Sq. Yd.', 'Aliquam aliquam non minus quibusdam omnis ut. Magnam consectetur aliquid dolorem laboriosam animi. Explicabo eligendi distinctio quia tempora ipsam sed magni.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 2, 1, 7, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(341, 'In illo aut officia.', 'Blanditiis aut.', '5 Crore', '3', '4', '200 Sq. Yd.', 'Voluptas alias voluptatum ut quia. Est minima voluptas et repellendus. Sunt autem ipsa sint deleniti eum officiis. Est incidunt non fugit inventore.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 1, 11, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(342, 'Dolorum est placeat praesentium.', 'Quam mollitia nostrum.', '5.6 Crore', '6', '5', '150 Sq. Yd.', 'Nulla praesentium aut et aperiam. Excepturi quia vel aliquam quibusdam qui similique repudiandae. Et beatae ut est eum eveniet.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 3, 1, 11, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(343, 'Dicta pariatur asperiores incidunt quod.', 'Voluptatem omnis et.', '15 Crore', '5', '2', '150 Sq. Yd.', 'Illum numquam ad dolore non. Numquam voluptatem maiores at. Rem occaecati quae eum ex ut deleniti fugit quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 4, 8, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(344, 'Et qui quasi culpa.', 'Voluptatem eos.', '12 Crore', '6', '4', '300 Sq. Yd.', 'Illo maxime facilis asperiores autem. Voluptas minus aliquam error quia voluptas iusto quas. Quia nesciunt ad iusto quo natus dolor consequatur temporibus. Doloremque qui quos ex amet blanditiis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 1, 13, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(345, 'Eveniet recusandae sint culpa dolore.', 'Voluptatem rerum.', '2.5 Crore', '5', '3', '300 Sq. Yd.', 'Maxime eius non iusto dolores non. Et earum consequatur repellendus sequi dolore vero.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 5, 13, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(346, 'Delectus velit consectetur consequuntur.', 'Et quod.', '3 Crore', '3', '1', '3000 Sq. Yd.', 'Error qui omnis est sunt. Laborum consectetur totam facilis eum perspiciatis aspernatur voluptatem. Nulla excepturi veritatis iure reiciendis dolore velit. Ipsa sapiente ea cumque rerum nesciunt.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 3, 3, 6, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(347, 'Sequi alias quasi omnis.', 'Aspernatur distinctio.', '5 Crore', '4', '3', '1500 Sq. Yd.', 'Voluptatibus aut voluptas et neque. Recusandae et eos omnis placeat architecto est et. Facere pariatur nisi itaque quae maiores. Quis aut modi aut consequatur. Soluta sit et iure expedita ad.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:28', 1, 0, 1, 3, 10, 2, '2018-12-13 14:24:28', '2018-12-13 14:24:28'),
(348, 'Quod impedit dicta error nam est.', 'Et similique.', '2 Crore', '3', '4', '200 Sq. Yd.', 'Dignissimos quod quam et error nisi quasi. Et sunt a eaque iusto ut. Explicabo dolores ea similique id cum. Eum adipisci facilis saepe repudiandae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 1, 15, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(349, 'Corporis ex et molestiae.', 'Similique labore.', '6 Crore', '5', '2', '300 Sq. Yd.', 'Voluptatem eum sequi quos ut quia qui. Omnis laboriosam esse aut voluptas. Enim consequatur dolor laborum enim ut quo non. Iure quis dignissimos nisi nostrum nihil sit iusto nobis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 4, 3, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(350, 'Omnis ex ut tempora consequatur non.', 'Eveniet cupiditate.', '7 Crore', '3', '1', '150 Sq. Yd.', 'Sunt accusantium impedit cumque accusantium. Perspiciatis rerum sunt sed fuga iste est ipsa. Error et sint eos et dolore velit quam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 1, 11, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(351, 'Omnis voluptate assumenda.', 'Quia amet.', '9 Crore', '5', '2', '1000 Sq. Yd.', 'Autem ea quod aut ad. Fugit ipsam dolorum necessitatibus. Voluptate dolor rerum at doloremque voluptates. Sint sapiente ad quis ut nisi aut laboriosam. Sunt doloremque dolorum occaecati eos sed et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 1, 8, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(352, 'Quia itaque deleniti dicta qui.', 'Consectetur in.', '8 Crore', '6', '3', '600 Sq. Yd.', 'Et qui et dolores ut et. Architecto sint nihil enim unde optio quo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 4, 12, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(353, 'Doloribus recusandae dolores dicta.', 'Fuga voluptatum hic.', '2.5 Crore', '4', '5', '3000 Sq. Yd.', 'Et ut reprehenderit atque dolor. Sit animi nemo assumenda nobis sapiente. Vero ducimus esse et. Praesentium et est blanditiis magni. Est nostrum consectetur et maiores saepe.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 3, 2, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(354, 'Vitae rerum dignissimos voluptas.', 'Provident mollitia.', '3.5 Crore', '3', '4', '200 Sq. Yd.', 'Officiis non ipsa enim quis a quo et. Natus sit nihil ut et eos occaecati nesciunt. Temporibus ullam vero veniam. Reiciendis veniam vel omnis deserunt eum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 1, 2, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(355, 'Possimus ipsum qui voluptas.', 'Tenetur veritatis.', '2.5 Crore', '4', '5', '120 Sq. Yd.', 'Autem a rem voluptas dolor molestiae corrupti non. Et voluptatem veritatis et. Quia omnis deleniti distinctio sint.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 2, 15, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(356, 'Sequi natus soluta dolores.', 'Aliquam a fugiat.', '12 Crore', '4', '1', '600 Sq. Yd.', 'Veritatis dignissimos asperiores maxime et et fugit et. Minima neque voluptatem et nihil. Dolorum facilis molestiae impedit mollitia quia vero commodi. Eveniet vel enim temporibus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 2, 4, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(357, 'Non neque consequatur veniam.', 'Recusandae quisquam.', '9 Crore', '5', '5', '600 Sq. Yd.', 'Exercitationem voluptas eos ut quisquam quam inventore. Sunt et consequatur in magni ut. Voluptatem mollitia quasi inventore atque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 5, 14, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(358, 'Modi quia deleniti.', 'Mollitia ullam autem.', '2 Crore', '7', '1', '1000 Sq. Yd.', 'Officia ut eaque nulla voluptatem. Harum ut sint vero cum expedita. Nisi inventore consequatur reiciendis molestiae. Illum recusandae nam rerum magni occaecati labore veritatis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 1, 8, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(359, 'Quaerat quaerat voluptate eos consectetur.', 'Quidem a.', '6.5 Crore', '5', '3', '120 Sq. Yd.', 'Distinctio est et voluptatem in ab sint. Fugit reiciendis nihil officia fugit repellendus dolorum et delectus. Esse modi aut aliquam quod molestiae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 1, 6, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(360, 'Eos quis maiores.', 'Quaerat reprehenderit aperiam.', '15 Crore', '5', '5', '1000 Sq. Yd.', 'Et quasi beatae soluta aut consectetur. Eum architecto dolore dignissimos. Iste quia impedit ullam veniam. Aut dolorem iusto voluptas exercitationem consequatur inventore debitis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 4, 15, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(361, 'Et cupiditate error autem autem.', 'Ex non.', '6 Crore', '5', '1', '3000 Sq. Yd.', 'Vitae qui sint neque corporis adipisci. Rerum vel cum sit deleniti qui. Deleniti facere aut non voluptatum laborum deserunt maiores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 1, 7, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(362, 'Praesentium exercitationem veniam dignissimos inventore.', 'Voluptas ipsum.', '5.6 Crore', '5', '3', '150 Sq. Yd.', 'Autem beatae dolor quo deleniti consequatur. Consequuntur quos corrupti magni in. Fugiat eos dicta vel mollitia. Illo maiores ut ut culpa tempore illum at sit. Non odio facilis sunt neque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 4, 16, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(363, 'Animi laborum pariatur deserunt similique.', 'Non quis.', '12 Crore', '6', '3', '1000 Sq. Yd.', 'Sequi iste labore aut aperiam numquam similique. Ipsam officia totam aspernatur eos aut enim ipsum exercitationem. Amet rem delectus voluptates iure ab. Ducimus dolorem eveniet illum corporis vitae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 3, 8, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(364, 'Sunt consequatur commodi excepturi dolorem perspiciatis.', 'Voluptates et aut.', '5.6 Crore', '3', '1', '150 Sq. Yd.', 'Eos labore maiores omnis non. Aut dolorum qui neque ut qui praesentium id.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 4, 16, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(365, 'Recusandae est amet eum.', 'Consequatur quis.', '4 Crore', '6', '4', '300 Sq. Yd.', 'Itaque vitae quis ea. Reiciendis sit nemo quam in debitis dolores sit. Sed aut quia impedit in nesciunt veniam et dolor. Ratione velit nam voluptate possimus et nostrum occaecati atque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 4, 11, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(366, 'Voluptatibus eum ea.', 'Incidunt beatae dolor.', '2 Crore', '4', '5', '120 Sq. Yd.', 'Natus recusandae id sunt dolores quaerat mollitia. Qui numquam et illum in corrupti facilis. Sint aperiam qui esse et quo distinctio. Eum minima necessitatibus consequuntur mollitia iure sit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 1, 13, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(367, 'Iure ipsum aperiam laborum sunt ad.', 'Occaecati minus.', '5 Crore', '5', '5', '200 Sq. Yd.', 'Qui ut qui quia enim qui iure. Voluptatum libero et rerum. Qui aut rerum vel autem repellat temporibus et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 2, 7, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(368, 'Sunt quam ea.', 'Qui et maiores.', '12 Crore', '4', '1', '3000 Sq. Yd.', 'Vero voluptatem fugit a nostrum et molestias dolorum. Natus qui dicta molestiae consectetur quam blanditiis est. Fugit eius reiciendis totam voluptatum modi sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 4, 6, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(369, 'Quisquam et possimus omnis mollitia vero.', 'Soluta repudiandae.', '4 Crore', '6', '1', '3000 Sq. Yd.', 'Voluptatum est aut voluptatem et voluptatibus mollitia. Porro placeat sed tempore sapiente nostrum quasi qui. Et laudantium atque suscipit non.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 4, 16, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(370, 'Suscipit id doloribus molestias.', 'Nulla dolor est.', '2.5 Crore', '3', '4', '120 Sq. Yd.', 'Accusantium delectus quae ipsam dolore occaecati. Qui dignissimos hic quam nisi quas eos. Odio laudantium consequatur eos amet ipsa vel.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 2, 13, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(371, 'Atque ratione repudiandae animi quidem corporis.', 'Magnam assumenda.', '6 Crore', '6', '4', '200 Sq. Yd.', 'Commodi quo doloribus ea. Aut distinctio quasi odit dolorum. A itaque enim sint blanditiis quisquam qui eveniet. Necessitatibus error tempore vitae sed et recusandae quis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 5, 2, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(372, 'Quia esse non quibusdam.', 'Et nam.', '4 Crore', '4', '2', '1500 Sq. Yd.', 'Inventore similique sit id. Voluptates est quo sint rem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 1, 2, 1, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(373, 'Qui vel reiciendis.', 'Perspiciatis maiores.', '5.6 Crore', '3', '3', '600 Sq. Yd.', 'Tempore magni dolorum est a non. Sit officia adipisci esse et fugiat reprehenderit dolorum iusto. Id iure non quisquam vel voluptatem eum. Vel officiis eveniet accusantium aut minus repellendus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 3, 1, 2, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(374, 'Sit facere est.', 'Quibusdam cumque blanditiis.', '15 Crore', '6', '1', '300 Sq. Yd.', 'Ipsam est est cumque quasi quas qui numquam. Mollitia incidunt molestiae facilis iure ut. Perferendis eum et dolor libero ipsum odit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:29', 1, 0, 2, 4, 7, 2, '2018-12-13 14:24:29', '2018-12-13 14:24:29'),
(375, 'Et est a qui.', 'Inventore optio.', '3.5 Crore', '5', '4', '200 Sq. Yd.', 'Quia maxime consequatur eum aliquid non ducimus saepe. Cupiditate eos perferendis quos voluptatem delectus. Veniam nesciunt et consequuntur rerum impedit fugit ad omnis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 4, 12, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(376, 'Molestiae est est impedit voluptas.', 'Sit nesciunt corrupti.', '4 Crore', '7', '1', '1000 Sq. Yd.', 'Consequatur laudantium saepe id culpa. Qui animi sed voluptas ut consequatur quia asperiores. Tempore consequatur iste aut ex iste est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 1, 15, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(377, 'Exercitationem neque sequi molestiae blanditiis eaque.', 'Beatae ratione aut.', '7 Crore', '6', '5', '300 Sq. Yd.', 'Deleniti velit aliquid nam ipsa ex alias repellendus. In ea qui minus porro fuga quaerat corrupti. Omnis ea voluptatum culpa blanditiis a ipsa illo.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 2, 11, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(378, 'Reiciendis id consequuntur dignissimos quo.', 'Dignissimos enim pariatur.', '3 Crore', '3', '4', '3000 Sq. Yd.', 'Sed porro suscipit doloremque tempore incidunt et. Veritatis qui veritatis et occaecati qui suscipit vel nostrum. Expedita expedita velit dolorem et. Assumenda aut sunt officia in officiis ea.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 1, 1, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(379, 'Molestiae earum et.', 'Dolores aliquam.', '9 Crore', '5', '3', '120 Sq. Yd.', 'Aliquam incidunt et eius. Quo consequatur possimus deserunt totam ducimus dolores. A eius dolor totam sed id rerum in ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 4, 12, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(380, 'Id minus qui aut.', 'Ex totam qui.', '3 Crore', '6', '5', '200 Sq. Yd.', 'Alias porro amet cumque provident. Quisquam repellat a aliquam inventore est. Ex asperiores qui odio aspernatur consequatur recusandae alias sunt.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 4, 11, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(381, 'Dolores ab iusto iure incidunt magni.', 'Et optio iusto.', '2 Crore', '4', '3', '600 Sq. Yd.', 'Consequatur voluptate commodi aliquam sequi laboriosam. Eos sint sapiente porro voluptatem. Rerum veritatis sit est doloribus. Consequuntur provident in accusantium cum molestiae nam commodi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 2, 4, 13, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(382, 'Placeat sed ut velit.', 'Sit et.', '12 Crore', '7', '4', '1000 Sq. Yd.', 'Ipsam reiciendis id ab quo in sit. Ad optio alias in non mollitia. Vitae fugiat accusamus sit. Quidem ut velit cumque dolorem molestias dolore sint. Adipisci neque itaque ab.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 5, 2, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(383, 'Deserunt assumenda et voluptatibus ut.', 'Eos molestiae.', '15 Crore', '5', '3', '150 Sq. Yd.', 'Aut placeat veniam enim molestiae laborum et et. Nemo temporibus qui illo sed architecto rerum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 2, 2, 9, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(384, 'Qui qui ea harum.', 'Quo doloribus eius.', '6.5 Crore', '5', '3', '1500 Sq. Yd.', 'Incidunt quis repellat ratione et expedita. Praesentium illo rem fugiat aut hic. Provident omnis numquam ad quia repellendus minus iste. Voluptates illum est perferendis architecto qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 2, 3, 12, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(385, 'Dolore occaecati cumque iure atque.', 'Accusamus maiores.', '2.5 Crore', '7', '3', '3000 Sq. Yd.', 'Laboriosam libero molestiae quos voluptatem ipsam. Deserunt nemo quos voluptas id. Sint minima saepe aut aliquid reiciendis quis fuga.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 4, 16, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(386, 'Sed aspernatur quae.', 'Dolor qui similique.', '5.6 Crore', '3', '4', '120 Sq. Yd.', 'Enim sed voluptatem laudantium debitis. Tempore fugit et mollitia id nesciunt voluptate voluptas. In sint aperiam eum omnis repellat quam. Non sed nisi culpa a ut architecto.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 5, 3, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(387, 'Doloremque hic quia itaque est.', 'Voluptatem qui commodi.', '2.5 Crore', '6', '2', '150 Sq. Yd.', 'Quisquam explicabo aspernatur voluptas. Repudiandae ut ut omnis impedit illo quae. Voluptatem numquam repellat error officia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 2, 8, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(388, 'Quod quam possimus nobis quia.', 'Et qui.', '15 Crore', '3', '5', '600 Sq. Yd.', 'Officia omnis modi neque unde molestias. Et ut et excepturi qui. Sed velit officiis est in impedit adipisci. Ipsam voluptatum tenetur autem cupiditate sunt et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 2, 12, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(389, 'Quis expedita et suscipit suscipit blanditiis.', 'Labore doloribus odio.', '9 Crore', '3', '5', '600 Sq. Yd.', 'Velit exercitationem nemo et maxime. Deleniti praesentium quidem aut a ut. Eos repellat iusto autem nostrum ad non. Id minima aperiam non saepe.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 4, 14, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(390, 'Totam corrupti perspiciatis quod.', 'Cumque autem.', '4 Crore', '7', '1', '150 Sq. Yd.', 'Dolor modi explicabo suscipit sint. Rerum voluptatibus nesciunt facere nam vel. Autem maxime voluptate cumque et est. Sit omnis animi repudiandae magni.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 1, 8, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(391, 'Et aut consequatur laudantium ut.', 'Natus id.', '3 Crore', '5', '2', '1500 Sq. Yd.', 'Nihil adipisci et totam. Alias in vitae rerum placeat. Aut libero eum voluptates a. Repellendus corrupti quos quasi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 2, 3, 5, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(392, 'Natus soluta et nihil similique praesentium.', 'Excepturi est.', '3.5 Crore', '7', '4', '3000 Sq. Yd.', 'Impedit sed est beatae dignissimos exercitationem non. Enim voluptatem dolores iste non dolorem quisquam. Et vero quis velit ad accusantium id laudantium. Ea sit sunt id aut iure qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 2, 3, 1, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30');
INSERT INTO `property` (`id`, `title`, `slogan`, `price`, `beds`, `baths`, `area`, `details`, `img`, `exp_date`, `status`, `featured`, `purpose_id`, `type_id`, `area_id`, `user_id`, `created_at`, `updated_at`) VALUES
(393, 'Molestiae accusantium et voluptatum.', 'Rerum non ex.', '5.6 Crore', '7', '2', '1000 Sq. Yd.', 'Delectus quia ut officia et. Id est corrupti nobis et. Consectetur rem laborum veritatis quisquam quibusdam veritatis doloribus. Et animi aut sit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 4, 9, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(394, 'Inventore dolor placeat amet rerum.', 'Et et dolorum.', '9 Crore', '7', '2', '3000 Sq. Yd.', 'Dolores voluptatibus autem quo eum excepturi. Quis quo nulla incidunt voluptatem. Ipsam ea architecto et omnis architecto dolorem quisquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 3, 16, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(395, 'Nulla perspiciatis accusantium sapiente sunt neque.', 'Quis non.', '3.5 Crore', '7', '2', '300 Sq. Yd.', 'Voluptates est velit asperiores aspernatur est excepturi unde. Voluptatum sint accusantium nobis aliquam id. Consequatur dolores ea totam aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 5, 2, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(396, 'Quia aperiam accusantium rem consequatur.', 'Ut velit.', '5 Crore', '4', '2', '200 Sq. Yd.', 'Similique non distinctio laudantium non autem consequatur hic. Dolorum sequi deleniti quia enim eius sed. Est vero assumenda magni est voluptatem rerum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 2, 3, 7, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(397, 'Hic quidem et.', 'Ut voluptatem mollitia.', '9 Crore', '6', '3', '120 Sq. Yd.', 'Qui molestiae nemo impedit tempore. Quod sed reprehenderit voluptatem et. Excepturi aspernatur unde eaque optio laudantium ea sit. Est asperiores necessitatibus ut similique tempora dolorem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 2, 11, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(398, 'Eveniet dicta dolorum autem voluptatem.', 'Asperiores quia voluptas.', '9 Crore', '5', '4', '3000 Sq. Yd.', 'Officia nihil et dolores vel nihil et. Explicabo numquam et ut eius quaerat. Quibusdam quisquam velit deserunt eum eum. Aspernatur non perferendis eligendi qui eos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 3, 4, 4, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(399, 'Eum debitis explicabo rerum veritatis velit.', 'Possimus atque.', '6.5 Crore', '4', '1', '200 Sq. Yd.', 'Quia temporibus ut impedit. Sapiente vero dignissimos minus itaque et non. Ut labore expedita ab ratione. Dolorem eius magni sint enim.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 2, 2, 6, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(400, 'Voluptatum omnis reprehenderit doloremque deleniti.', 'Sit ut.', '3 Crore', '6', '3', '1000 Sq. Yd.', 'Sunt velit ut ullam accusantium odit est et porro. Ab reprehenderit atque corporis suscipit est. Dicta libero et est et provident ipsum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 1, 14, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(401, 'Et dolorem quaerat vero eius.', 'Aut placeat beatae.', '4 Crore', '3', '3', '150 Sq. Yd.', 'Officiis explicabo placeat doloremque. Repudiandae tempora voluptatibus sint eaque molestias eius enim voluptatum. Assumenda optio voluptates rerum consequatur quaerat eos quisquam vel.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:30', 1, 0, 1, 3, 5, 2, '2018-12-13 14:24:30', '2018-12-13 14:24:30'),
(402, 'Architecto non dicta quia.', 'Eum vero.', '5.6 Crore', '4', '4', '300 Sq. Yd.', 'Non quae est ipsa modi. Quibusdam est dicta occaecati. Aut maxime doloribus inventore ut et eveniet.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 1, 5, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(403, 'Placeat unde ea ut dolorum.', 'Qui quia.', '15 Crore', '7', '5', '3000 Sq. Yd.', 'Asperiores adipisci est aperiam dolor non. Quos ut fuga iste incidunt omnis. Corrupti omnis ab quidem quia. A impedit dolorem est totam consectetur adipisci molestiae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 4, 16, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(404, 'Quo dolorem in consequuntur sed.', 'Molestiae molestias accusamus.', '4 Crore', '6', '1', '200 Sq. Yd.', 'Rerum voluptates veniam aut iste minus voluptatem earum. Quos nostrum dolorem accusantium explicabo aliquid molestiae id. Provident id libero maxime animi ea pariatur incidunt magnam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 2, 16, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(405, 'Debitis necessitatibus nisi aut quia harum.', 'Quasi aut rerum.', '3 Crore', '4', '5', '1500 Sq. Yd.', 'Nostrum ut est quibusdam quisquam facere facere. Nulla repellendus mollitia qui quam quas sed id.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 3, 5, 1, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(406, 'Molestias facere ullam repellat blanditiis consequatur.', 'Architecto nihil.', '12 Crore', '5', '5', '3000 Sq. Yd.', 'Laborum reiciendis rem maxime natus. Voluptates sunt et iure ab commodi. Autem deleniti architecto dolor nihil deleniti facere. Et sint dicta molestias.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 1, 11, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(407, 'Molestiae nobis ullam rerum sapiente et.', 'Ea rem velit.', '7 Crore', '6', '2', '600 Sq. Yd.', 'Ex quae et eveniet blanditiis repellendus dicta. Adipisci animi voluptates eaque rerum non. Accusantium ipsa animi delectus recusandae. Nemo blanditiis est quia vel necessitatibus esse et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 2, 5, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(408, 'Dolor quia deserunt aut officiis.', 'Voluptatum dolorum sed.', '2.5 Crore', '4', '2', '1000 Sq. Yd.', 'Amet dolores natus veritatis corporis rerum error modi. Optio cupiditate facilis fugit laborum. Et ex similique aliquid consequatur illo fuga. Necessitatibus quasi modi optio totam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 1, 5, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(409, 'Officia asperiores minima quisquam molestias tempora.', 'Sunt quia.', '6 Crore', '4', '4', '1000 Sq. Yd.', 'Suscipit possimus maiores rem. Velit voluptatibus in eum molestiae. Non quod aut a quidem quae tempore optio minima.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 5, 10, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(410, 'Laudantium rerum quo.', 'Excepturi excepturi.', '6 Crore', '6', '2', '150 Sq. Yd.', 'Sequi inventore qui aliquid inventore earum. Quia expedita itaque reprehenderit amet non minus. Sint maiores esse architecto omnis voluptas. Et et provident ut est molestiae sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 3, 1, 13, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(411, 'Sed ad ut odit ex laboriosam.', 'Nisi nobis nulla.', '3.5 Crore', '5', '2', '150 Sq. Yd.', 'Asperiores consequatur ut eos. Molestiae sit sequi quam voluptate maiores. Aut aut earum est deleniti. Aperiam sapiente voluptates libero eum suscipit voluptatem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 1, 13, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(412, 'Fuga odit beatae aliquam.', 'Nihil alias voluptate.', '2 Crore', '4', '3', '200 Sq. Yd.', 'Et praesentium enim ipsam ducimus qui eos. Praesentium qui deleniti natus suscipit ab debitis labore. Id dolore sapiente consectetur voluptatem neque aut. Ab quidem voluptate tenetur qui quod.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 4, 11, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(413, 'Saepe in veniam.', 'Eos distinctio.', '12 Crore', '6', '4', '3000 Sq. Yd.', 'Voluptatum molestiae voluptas totam non est ut sint. Ullam sit voluptatem autem soluta aut doloribus nesciunt. Dolores dolorem maiores architecto totam enim.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 1, 9, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(414, 'Libero praesentium saepe temporibus.', 'A laboriosam.', '2.5 Crore', '3', '1', '1000 Sq. Yd.', 'In adipisci deleniti ut qui eligendi deserunt enim qui. Impedit adipisci possimus occaecati ut id. Voluptatem excepturi labore rerum sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 1, 11, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(415, 'Voluptas quae et aspernatur.', 'Facilis ut est.', '2 Crore', '5', '1', '300 Sq. Yd.', 'Nisi modi dolorum et porro. Ut dolores quia sit voluptatibus quas deserunt autem sed. Et rerum ducimus vel eos. Quis ea est nam voluptatem sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 2, 11, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(416, 'Excepturi ipsa commodi voluptate saepe velit.', 'Voluptatem hic.', '3 Crore', '7', '4', '300 Sq. Yd.', 'Et necessitatibus in placeat et dolor incidunt dolores vero. Assumenda reprehenderit distinctio facere fugit iusto accusamus. Et reprehenderit et perferendis molestiae velit qui eius.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 1, 16, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(417, 'Sapiente adipisci at debitis.', 'Optio itaque quibusdam.', '15 Crore', '7', '5', '600 Sq. Yd.', 'Dolore excepturi ratione reiciendis et repudiandae ducimus. Reiciendis molestiae illum excepturi reiciendis facere ea. Perspiciatis fuga delectus beatae laborum similique ullam aliquam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 3, 4, 2, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(418, 'Quisquam amet non.', 'Asperiores libero delectus.', '9 Crore', '3', '2', '150 Sq. Yd.', 'Reiciendis incidunt tenetur et non quas. Quam sed sed quibusdam harum reiciendis ipsam veniam. Dolorem nemo eveniet ex delectus ut. Error nemo ut voluptates iure.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 3, 13, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(419, 'Doloremque libero beatae.', 'Aspernatur molestiae.', '8 Crore', '4', '1', '150 Sq. Yd.', 'Similique est eum voluptatum cumque. Et quaerat repellendus dolores aliquam tempore quia commodi. Maiores sit inventore eum aut. Aliquid molestiae nesciunt nemo in odit ducimus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 2, 4, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(420, 'Magni voluptatem sed necessitatibus quibusdam.', 'Officiis vero animi.', '15 Crore', '5', '4', '1500 Sq. Yd.', 'Optio dignissimos optio quo asperiores. Consequuntur ipsam inventore ut quia. Autem ullam atque id consequatur. Et exercitationem voluptatum repudiandae distinctio et qui esse.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 2, 7, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(421, 'Illo consectetur temporibus omnis.', 'Repellat reiciendis.', '2.5 Crore', '6', '4', '1500 Sq. Yd.', 'In ut dolorum necessitatibus. Ut dignissimos quas illo voluptatem ut saepe. Et delectus quasi velit exercitationem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 4, 1, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(422, 'Quisquam omnis aperiam non maxime.', 'Quia suscipit.', '4 Crore', '7', '5', '3000 Sq. Yd.', 'Doloremque doloribus officiis esse accusantium sint. Consequatur et tempore quas ea ullam. Ipsam quis ipsam perferendis et officiis ut quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 1, 5, 11, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(423, 'Adipisci dolor consequatur sit.', 'Soluta pariatur.', '4 Crore', '7', '1', '150 Sq. Yd.', 'Atque id qui est corporis. Porro et dolorum expedita quibusdam expedita in. Impedit tempore odit cumque magnam maxime. Quos perferendis deleniti dicta dolorum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:31', 1, 0, 2, 5, 10, 2, '2018-12-13 14:24:31', '2018-12-13 14:24:31'),
(424, 'Ea minus est et magnam similique.', 'Sapiente culpa.', '15 Crore', '6', '4', '300 Sq. Yd.', 'Eligendi nemo soluta placeat ad laborum. Dolor aliquid vitae dignissimos consequatur. Quasi qui non ut aliquam est et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 5, 11, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(425, 'Et accusantium numquam perspiciatis.', 'Asperiores dolorum.', '3 Crore', '3', '1', '1000 Sq. Yd.', 'Quasi aut repellendus doloremque rerum et. Totam et officia cumque veniam. Ex maxime vitae in reiciendis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 5, 15, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(426, 'Quasi et eveniet rerum sunt.', 'Dolor aliquid autem.', '15 Crore', '5', '3', '120 Sq. Yd.', 'Quaerat voluptatem maiores molestiae qui molestiae. Maiores nobis sequi perspiciatis perferendis unde. Est delectus id mollitia. Laudantium ipsa iusto vel illum molestias modi.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 1, 1, 7, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(427, 'Est recusandae vero aut.', 'Officia maiores.', '9 Crore', '4', '1', '150 Sq. Yd.', 'Eum eius et reprehenderit quam consectetur non. Nesciunt est itaque vel et nesciunt nam. Consequuntur ratione quidem harum id sunt ut impedit. Quae ut et ut aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 3, 2, 4, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(428, 'Laudantium aut quibusdam ut.', 'Id suscipit quia.', '3.5 Crore', '6', '2', '1000 Sq. Yd.', 'Perspiciatis commodi eligendi nam placeat et. Nam eaque et voluptatem aperiam neque non. Temporibus reiciendis inventore aspernatur ut quis facere.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 5, 1, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(429, 'Asperiores delectus animi eum.', 'Dolorem quod.', '12 Crore', '5', '4', '600 Sq. Yd.', 'Voluptatem sit voluptates et voluptas voluptatem consequatur. Similique placeat aut optio error velit expedita. Libero voluptatem unde enim ex. Iste dolor ut eos laudantium nihil consequatur eum et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 1, 7, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(430, 'Perspiciatis esse voluptatem eius libero.', 'Corporis commodi.', '3 Crore', '7', '5', '1000 Sq. Yd.', 'Eveniet sunt debitis vero et quia. Repellat animi dicta aut recusandae. Fuga numquam quo voluptatibus iure magni est quaerat.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 2, 3, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(431, 'Unde sint illum.', 'Voluptatum voluptatem voluptas.', '2 Crore', '5', '3', '3000 Sq. Yd.', 'Enim non sit et eum perspiciatis. Voluptatum voluptatibus officia qui aut. Sunt eos sunt libero eius.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 1, 2, 8, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(432, 'Dolores ut odio voluptas quia temporibus.', 'Necessitatibus adipisci.', '2 Crore', '6', '3', '150 Sq. Yd.', 'Adipisci repellendus consectetur ratione. Omnis qui itaque enim. Voluptatibus qui deserunt ut voluptates. Ratione est quaerat quaerat enim voluptas voluptas. Qui ratione saepe qui ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 3, 13, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(433, 'Ut autem et est.', 'Sed vitae nisi.', '4 Crore', '4', '5', '1500 Sq. Yd.', 'Commodi ut similique corrupti aut officia accusantium. Sunt molestias rerum quaerat at ex. Error totam dolores omnis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 2, 10, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(434, 'Corrupti nam ipsum velit eligendi.', 'Sunt nesciunt.', '12 Crore', '6', '2', '3000 Sq. Yd.', 'Quas quibusdam at inventore at modi cumque. Quo perferendis fuga sunt et nobis. Maxime qui sit alias eius id.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 3, 3, 5, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(435, 'Inventore quo libero quas alias.', 'Iure animi.', '5 Crore', '6', '5', '1000 Sq. Yd.', 'Officiis quidem sunt sit doloremque. Magni expedita vel fuga ut est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 1, 15, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(436, 'Qui sed et sint recusandae.', 'Qui dolores.', '3.5 Crore', '7', '2', '3000 Sq. Yd.', 'Sint cum est ratione sapiente laudantium. Et itaque debitis numquam. Doloribus omnis quidem corporis ea nisi aut. Quia neque quia minima.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 3, 2, 7, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(437, 'In modi officiis qui.', 'Sapiente odio.', '4 Crore', '5', '5', '3000 Sq. Yd.', 'Reprehenderit nam est quos quia sapiente rem sunt sapiente. Dicta sit reiciendis eum omnis blanditiis atque. Voluptates maxime corporis quos magnam et quo. Animi quaerat nihil ab eum quas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 4, 14, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(438, 'Voluptatum deserunt vel eveniet et adipisci.', 'Doloremque itaque ex.', '2.5 Crore', '5', '5', '200 Sq. Yd.', 'Nihil labore blanditiis assumenda et voluptate vel. Quia mollitia quo odit ratione asperiores. Velit et doloribus porro perferendis aspernatur aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 1, 2, 9, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(439, 'Aperiam qui nemo.', 'Alias debitis.', '15 Crore', '3', '3', '600 Sq. Yd.', 'Non atque occaecati porro. Nihil tempore ab quam rem ut. Iure voluptates velit cupiditate quas cumque quisquam. Vero commodi et voluptas facere officiis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 3, 1, 16, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(440, 'Consectetur deleniti deleniti vel.', 'Est quos quae.', '2 Crore', '7', '2', '1500 Sq. Yd.', 'Beatae quia iure qui autem sunt. Aut et et corrupti id eaque rem porro ut. Occaecati aliquid ducimus quisquam placeat vitae beatae veniam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 3, 5, 7, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(441, 'Assumenda iure aliquid placeat.', 'Nobis vel voluptas.', '3 Crore', '6', '5', '600 Sq. Yd.', 'Iure non doloribus iusto repellendus odit assumenda sunt. Non ut et ut illum. Non sit ab dicta distinctio temporibus consectetur consectetur cupiditate.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 1, 12, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(442, 'Repellendus dolorem veritatis consequatur est.', 'Aperiam aperiam sed.', '12 Crore', '3', '5', '600 Sq. Yd.', 'Velit ab rerum inventore labore ab. Sit voluptatem laborum aut autem et. Quis omnis earum aut consequatur perferendis sunt nisi. Eius quo ipsa ea aut molestiae.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 1, 4, 10, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(443, 'Aut sint dolores rem doloremque sapiente.', 'Dolore dolorem.', '7 Crore', '5', '2', '200 Sq. Yd.', 'Ea doloremque et magni iusto voluptatem autem. Velit et reiciendis quos qui ut occaecati. Laudantium sit non et dolore eligendi et qui pariatur. Est sit autem nisi ipsum sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 4, 4, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(444, 'Dolorum ratione ipsum soluta.', 'Aut fuga.', '5 Crore', '5', '5', '150 Sq. Yd.', 'Corporis dolores cumque molestias et dolor. Quidem est dignissimos dolorum delectus aut eos. Eum maiores dolores laboriosam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 3, 5, 2, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(445, 'Nihil eos delectus occaecati.', 'Error soluta.', '6 Crore', '7', '4', '120 Sq. Yd.', 'Voluptatum aut iure consequatur rem quas. Velit nihil voluptatem rerum. Repudiandae aut sed ipsum sint. Sunt quasi molestiae beatae dolores necessitatibus est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 4, 2, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(446, 'Voluptatem incidunt nostrum rerum quis.', 'Labore explicabo.', '4 Crore', '6', '5', '150 Sq. Yd.', 'Laborum aut sunt distinctio occaecati totam. Doloribus consequatur sapiente sint optio asperiores. Iure quod et illo sint sapiente.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 1, 3, 7, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(447, 'Et delectus repudiandae sed consequatur pariatur.', 'Voluptatibus qui.', '3 Crore', '7', '3', '1500 Sq. Yd.', 'Sit animi iste et et. Praesentium illo non saepe accusamus et enim cum unde. Deserunt dolor laboriosam consequuntur similique debitis ipsa sunt.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 2, 2, 5, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(448, 'Et rerum et.', 'Ab doloremque quo.', '4 Crore', '4', '4', '120 Sq. Yd.', 'Et nihil et vel. Ipsa dolorem ex tenetur dolorum. Minima at non voluptas sint vitae aut laudantium. Voluptatum iste aperiam veritatis voluptatem molestiae ad.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 3, 2, 16, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(449, 'Quo vero sint aliquid.', 'Unde possimus.', '2 Crore', '3', '1', '300 Sq. Yd.', 'Aut deleniti neque vero quis et. Omnis est quidem tempora quasi autem et. Cumque non labore voluptatibus nesciunt cum aut. Modi ut aut consequatur harum quis sit nihil.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:32', 1, 0, 1, 2, 6, 2, '2018-12-13 14:24:32', '2018-12-13 14:24:32'),
(450, 'Sed ab ad quas.', 'Neque et aliquam.', '3 Crore', '3', '4', '300 Sq. Yd.', 'Non nam est saepe harum rerum rem vitae quia. Ex ullam et consequatur molestiae. Est laboriosam dolor ut omnis.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 2, 13, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(451, 'Amet perferendis impedit quo.', 'Beatae aut.', '3 Crore', '7', '4', '150 Sq. Yd.', 'Eos mollitia vero sunt quo. Nihil quo a distinctio. Consequuntur quasi accusamus est sed qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 3, 3, 1, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(452, 'Hic sit voluptatem repellat error et.', 'Earum libero veniam.', '6.5 Crore', '4', '2', '3000 Sq. Yd.', 'Id commodi animi similique. Iure veniam ratione qui et. Ut rerum excepturi suscipit saepe et quos. Dolorem minus enim debitis nihil ad.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 5, 7, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(453, 'Error ea iure id voluptatem.', 'Debitis pariatur.', '3 Crore', '7', '4', '1000 Sq. Yd.', 'Reprehenderit rerum nihil soluta quia qui voluptate aut. Qui blanditiis et nulla dolore a ut cum. Eos non nihil aut est quaerat sapiente. In aut vel sit ea accusamus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 1, 10, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(454, 'Error natus dolorum velit.', 'Voluptas amet.', '3.5 Crore', '7', '5', '120 Sq. Yd.', 'Expedita harum recusandae eos. Expedita earum qui sint. Veritatis eaque itaque magni animi vel nemo quo. Id ratione sed ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 4, 1, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(455, 'Aspernatur cumque nostrum perferendis a officia.', 'Aspernatur aliquid.', '3.5 Crore', '6', '1', '1000 Sq. Yd.', 'Et natus quidem qui sint consequatur omnis quidem. Saepe odio veniam ipsa numquam quo provident.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 1, 7, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(456, 'Sequi unde veritatis.', 'Ad quae ratione.', '2 Crore', '4', '5', '150 Sq. Yd.', 'Dolor odit beatae magnam culpa quibusdam quo. Quas sed vero veritatis accusamus. Sapiente deserunt ea nostrum hic animi necessitatibus. Ex beatae animi consequatur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 3, 4, 7, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(457, 'Eveniet blanditiis debitis in reprehenderit.', 'Ipsa aut voluptatem.', '12 Crore', '3', '2', '600 Sq. Yd.', 'Reiciendis sint ea sunt cumque minus et consequatur inventore. Voluptatum ratione tempore omnis quo nemo. Ratione at ea omnis perspiciatis qui id. Veritatis sit et magni dolorem saepe voluptas.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 3, 1, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(458, 'Molestiae minus omnis laudantium.', 'Excepturi sit omnis.', '5.6 Crore', '3', '1', '1000 Sq. Yd.', 'Perferendis et et qui ad facilis aut rem sit. Est porro atque ut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 4, 11, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(459, 'Dicta atque dolores molestiae quo similique.', 'Quam quasi.', '15 Crore', '7', '2', '600 Sq. Yd.', 'Non nemo suscipit illum voluptate aspernatur ex et. Autem et aperiam voluptatem ut eos necessitatibus. Quo ut in dolorem perspiciatis et doloribus. Sint alias dolore impedit incidunt.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 2, 1, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(460, 'Numquam officiis molestiae nihil.', 'Vitae velit.', '8 Crore', '4', '5', '3000 Sq. Yd.', 'Molestias totam quo deserunt quisquam in. Repudiandae ad maiores labore cum sed delectus. Est modi quia quibusdam. Fuga eaque quas ut dolor maiores. Cum aperiam expedita pariatur architecto.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 2, 14, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(461, 'Earum quasi eos magni.', 'Sunt reiciendis perferendis.', '7 Crore', '3', '1', '3000 Sq. Yd.', 'Magni a maxime occaecati aut aspernatur ipsa veritatis. Dicta minima est accusantium non commodi facilis. Cumque ut ducimus ipsum ex.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 3, 1, 1, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(462, 'Nihil praesentium sed aut.', 'Voluptate totam soluta.', '6 Crore', '3', '5', '1500 Sq. Yd.', 'Asperiores velit earum et non possimus non. Ea optio mollitia est illo sit consectetur enim. Molestiae sit nihil est consequuntur labore qui.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 5, 13, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(463, 'Omnis harum suscipit distinctio eum error.', 'Sint et.', '5.6 Crore', '5', '3', '1500 Sq. Yd.', 'Molestiae quod non laudantium amet voluptatem. Et occaecati voluptates dolores sit. Et recusandae corporis eligendi ullam non architecto.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 5, 4, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(464, 'Odio blanditiis in nesciunt nulla.', 'Aliquid dolore dolorum.', '7 Crore', '7', '2', '150 Sq. Yd.', 'Quam vitae ad asperiores et quia assumenda ipsa. Sed asperiores placeat libero.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 1, 15, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(465, 'Eos consequuntur dignissimos aut temporibus.', 'Velit deleniti.', '8 Crore', '4', '3', '600 Sq. Yd.', 'Quisquam sint consequatur dolorum exercitationem. Nihil illum non sit quas. Id qui dignissimos optio consequatur sequi. Voluptatem omnis quasi velit quaerat dolorum dolores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 4, 12, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(466, 'Corrupti qui delectus rerum non et.', 'Iusto a laudantium.', '15 Crore', '7', '2', '600 Sq. Yd.', 'Esse ea dolorem reiciendis dolores earum voluptatem. Accusamus est nihil labore sit rem non commodi. Labore at et facere repellendus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 4, 12, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(467, 'Asperiores quas et.', 'Qui beatae.', '2 Crore', '3', '1', '300 Sq. Yd.', 'Quia est quis aut et eius et est. Quae expedita quos voluptatibus est illo enim est.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 3, 2, 15, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(468, 'Esse non laborum amet recusandae est.', 'Sed quia ut.', '2.5 Crore', '7', '2', '1500 Sq. Yd.', 'Qui alias provident error aut sed possimus quasi tenetur. Similique maxime sed necessitatibus voluptatem harum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 3, 4, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(469, 'Sunt omnis ut dicta.', 'Repellat odio.', '2 Crore', '5', '1', '200 Sq. Yd.', 'Harum et dolore minus voluptatem. Aut et corrupti quod nobis. Et neque mollitia rem sint itaque enim. Sed dignissimos quibusdam officiis est possimus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 5, 13, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(470, 'Odit ut eaque fugit.', 'Fuga temporibus.', '6.5 Crore', '3', '2', '150 Sq. Yd.', 'Molestias expedita amet illo architecto eum aliquam quasi. Ullam nobis quasi sint in possimus. Ut alias tempora nam et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 3, 5, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(471, 'Excepturi quos qui beatae et.', 'Sunt velit quibusdam.', '7 Crore', '4', '5', '3000 Sq. Yd.', 'Ut qui soluta ex quod nisi impedit. Inventore molestiae dolorem libero. Consequuntur et eligendi ut amet expedita.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 2, 5, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(472, 'Repellendus dolor unde ab adipisci.', 'Quia dolor est.', '8 Crore', '7', '2', '120 Sq. Yd.', 'Eius aut nemo aut et nobis provident ipsam. Aperiam saepe vel facere dolorem est enim iusto. Hic sit at doloribus voluptate quis quam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 2, 3, 6, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(473, 'Explicabo possimus cum qui odio.', 'Deleniti eos.', '4 Crore', '7', '4', '3000 Sq. Yd.', 'Consequatur dignissimos et omnis impedit totam. Adipisci molestiae porro rerum velit. Id dolor et excepturi suscipit et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:33', 1, 0, 1, 2, 16, 2, '2018-12-13 14:24:33', '2018-12-13 14:24:33'),
(474, 'Earum earum sed mollitia.', 'Numquam eaque repellendus.', '4 Crore', '3', '1', '200 Sq. Yd.', 'Optio ab quo praesentium tenetur eos atque necessitatibus dolores. Quo et pariatur ipsam impedit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 4, 15, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(475, 'Deserunt iste aut harum.', 'Qui consectetur.', '3 Crore', '4', '3', '600 Sq. Yd.', 'Laboriosam porro aperiam occaecati autem. Quia delectus labore inventore. Officiis unde cum enim hic.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 1, 14, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(476, 'Occaecati et qui.', 'Dolorem eum quis.', '7 Crore', '3', '2', '200 Sq. Yd.', 'Quos rerum est impedit excepturi. Hic repudiandae autem corporis exercitationem quia ullam consequatur. Enim repellendus incidunt voluptatem est laborum occaecati.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 1, 5, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(477, 'Commodi dolorum quod est.', 'Omnis beatae quos.', '2 Crore', '6', '3', '150 Sq. Yd.', 'Repudiandae tempore possimus aliquid in. Sapiente debitis sed maiores corporis ad similique cupiditate. Vel eligendi distinctio autem necessitatibus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 1, 9, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(478, 'Amet quia sapiente quisquam id voluptatem.', 'Quaerat reprehenderit molestias.', '6.5 Crore', '7', '1', '1500 Sq. Yd.', 'Quos et est id sit et magnam molestiae provident. Sapiente ut et minima quia cum. Ullam repellendus inventore maxime quia ut fuga sunt dolorum. Totam culpa rem quidem natus earum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 1, 8, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(479, 'Porro sunt blanditiis.', 'Voluptates quia eius.', '3 Crore', '6', '4', '1500 Sq. Yd.', 'Fugiat modi sed tempore aut nihil quia. Dolores fuga doloremque sunt accusamus eligendi fugiat. Omnis tempore nobis cumque dolorem.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 5, 4, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(480, 'Cum molestiae quis dignissimos.', 'Quo similique.', '2.5 Crore', '7', '2', '1500 Sq. Yd.', 'Tempore reiciendis dolores consequatur dolorem blanditiis incidunt. Debitis tempore consequuntur harum. Quia aut et voluptatibus aut sed aspernatur saepe mollitia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 2, 13, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(481, 'Aliquam illo velit voluptates ducimus.', 'Laboriosam neque non.', '15 Crore', '5', '1', '1500 Sq. Yd.', 'Sit vel deleniti dolor quis. Repellendus aut possimus ipsam dignissimos. Culpa libero sed maxime.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 4, 1, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(482, 'Iusto ut esse sint et esse.', 'Molestias sed officiis.', '12 Crore', '5', '2', '150 Sq. Yd.', 'Modi est ad id dolorem mollitia eaque. Quis dolorum commodi omnis sit id in atque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 3, 5, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(483, 'Autem maxime reiciendis fuga ut dignissimos.', 'Occaecati quo placeat.', '5.6 Crore', '5', '1', '600 Sq. Yd.', 'Ut non et cum eum nobis excepturi libero. Occaecati temporibus tenetur illo accusamus et quisquam. Dolor ex ut ea cupiditate earum.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 1, 11, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(484, 'Numquam quas voluptas voluptatum.', 'Consequatur itaque et.', '6 Crore', '6', '1', '1000 Sq. Yd.', 'Ex fugiat earum repudiandae vel quia vel quaerat. Distinctio ipsum quia laborum corporis est. Quis nihil asperiores quia sit deserunt esse ipsa. Est nisi modi sequi magnam quibusdam omnis asperiores.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 1, 9, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(485, 'Consequatur aliquam soluta ipsa sit ratione.', 'Delectus voluptatem.', '3.5 Crore', '7', '2', '150 Sq. Yd.', 'Recusandae ab eum enim et. Ullam labore dolores voluptatibus dolores quidem quam. Nulla magni molestiae autem nisi suscipit.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 3, 8, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(486, 'Fugit est doloribus deserunt alias.', 'Nulla inventore culpa.', '2 Crore', '6', '3', '600 Sq. Yd.', 'Autem atque consectetur est delectus fuga. Dolores ullam neque quo rerum. Quia repudiandae accusamus molestias minus dolore a.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 1, 14, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(487, 'Cupiditate laboriosam architecto.', 'Enim quaerat consectetur.', '7 Crore', '4', '1', '1000 Sq. Yd.', 'Doloribus ea qui consequuntur. Rem facere dolor quo voluptatem eum. Pariatur omnis corporis dignissimos voluptatem soluta.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 4, 5, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(488, 'Ex voluptatem rerum.', 'Quae iure.', '6.5 Crore', '5', '3', '300 Sq. Yd.', 'Adipisci necessitatibus ea aut. Incidunt quaerat explicabo veniam nihil. Esse necessitatibus libero et sed.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 2, 7, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(489, 'Odio corrupti dolor eos pariatur possimus.', 'Qui amet voluptatem.', '2.5 Crore', '4', '2', '1000 Sq. Yd.', 'Ab et rerum fugiat assumenda omnis eveniet nostrum. Debitis nesciunt et ad nobis provident. Unde perferendis quisquam aut ut dolores id nihil dignissimos.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 3, 3, 13, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(490, 'Sint earum voluptatem aspernatur quis itaque.', 'Magni sed.', '8 Crore', '4', '2', '1500 Sq. Yd.', 'Quibusdam distinctio ea ipsum amet enim nihil. Qui nisi quos cum necessitatibus. Nulla totam recusandae quod enim.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 4, 12, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(491, 'Est voluptatum sed reprehenderit.', 'Et perspiciatis qui.', '5.6 Crore', '7', '1', '1500 Sq. Yd.', 'Eaque ut itaque libero vel modi. Dolores ea tempora eos ex eum. Ratione molestiae atque in magnam repellat excepturi. Blanditiis in consequatur tenetur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 3, 2, 3, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(492, 'Minus sunt mollitia repellendus perferendis in.', 'Voluptatibus voluptas quae.', '12 Crore', '3', '1', '200 Sq. Yd.', 'Incidunt sequi nulla vero est itaque. Molestiae velit exercitationem odio quaerat magnam facere. Occaecati ratione quam ad qui asperiores deserunt. Earum qui soluta doloribus consequuntur.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 1, 2, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(493, 'Sit qui quaerat dolor.', 'Sapiente ratione consequatur.', '3 Crore', '4', '3', '1500 Sq. Yd.', 'Eos quod non ratione. Voluptatem voluptatem eius dolorem dolor aperiam enim nostrum. Debitis voluptatum quo odit consequuntur qui rerum et.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 4, 2, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(494, 'Architecto laborum vero laudantium rerum officiis.', 'Porro dolores.', '5.6 Crore', '4', '3', '200 Sq. Yd.', 'Aut et eum quia facere in dolorem. Et debitis officia nulla rem. Quidem asperiores voluptates voluptate qui sed non at. Amet assumenda beatae hic qui quia.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 2, 16, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(495, 'Iusto natus sed voluptatem atque.', 'Et vel illo.', '12 Crore', '6', '5', '1000 Sq. Yd.', 'Qui distinctio fuga repellat veniam necessitatibus voluptatum expedita non. Et minus sint vero aliquam omnis rem temporibus. Excepturi ipsam maiores rerum repellendus facilis ex.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 3, 1, 12, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(496, 'Dolorum recusandae natus ipsam.', 'Et consequuntur.', '5.6 Crore', '4', '4', '300 Sq. Yd.', 'Aut velit pariatur molestiae veritatis minus cupiditate. Quia ut facere ut tempore. Ex minus consequatur dolorem nobis eum. Nemo rerum rerum a minus.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 4, 9, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(497, 'Id aliquam vel voluptatibus.', 'Sapiente rerum.', '5 Crore', '5', '2', '200 Sq. Yd.', 'Ipsam id velit autem rerum sed qui inventore rerum. Quae nesciunt qui officia. Et sint quod ullam molestias commodi quia. Voluptatem et fugit facere quam.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 1, 1, 12, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(498, 'Magnam voluptatem nulla voluptatem in ea.', 'Eos repudiandae.', '7 Crore', '4', '4', '300 Sq. Yd.', 'Non ad nesciunt adipisci veniam. Ut qui numquam odio itaque impedit quo aliquam. Quia nihil numquam temporibus totam repellendus ut. Libero voluptates ut nulla beatae recusandae et aut.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 3, 5, 3, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(499, 'Asperiores officia dolores velit.', 'Voluptas dolor voluptatem.', '2 Crore', '5', '4', '300 Sq. Yd.', 'Libero sunt laboriosam voluptatem odio voluptatibus ratione quasi similique. Minima et fuga qui aliquam. Necessitatibus est et maxime in minima aliquam in.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:34', 1, 0, 2, 3, 16, 2, '2018-12-13 14:24:34', '2018-12-13 14:24:34'),
(500, 'Vel est voluptate sequi molestiae dolorem.', 'Qui corporis similique.', '7 Crore', '5', '2', '3000 Sq. Yd.', 'Aut velit velit excepturi voluptate error et vel. Error iste voluptatem at velit est. Id reiciendis labore eius. Similique sunt eos velit neque.', 'https://source.unsplash.com/1600x900/?home', '2018-12-13 14:24:35', 1, 0, 1, 1, 6, 2, '2018-12-13 14:24:35', '2018-12-13 14:24:35'),
(501, 'Web Developer', NULL, '234234', '4', '4', '100 Sq Yards', 'ssdfsdf', NULL, '2019-01-18 08:13:51', 1, 0, 3, 1, 14, 1, '2018-12-19 08:13:51', '2018-12-19 08:13:51'),
(502, '10 Rooms Flat for Sale Near Punjab Colony', NULL, '10 Crore', '10', '4', '200 Sq Yards', NULL, '10-rooms-flat-for-sale-near-punjab-colony_0_1545345390.jpg', '2018-12-27 17:36:30', 1, 0, 3, 1, 2, 1, '2018-12-20 17:36:30', '2018-12-20 17:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `property_projects`
--

CREATE TABLE `property_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `type_id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property_projects`
--

INSERT INTO `property_projects` (`id`, `title`, `price`, `locality`, `img`, `details`, `status`, `type_id`, `area_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'new flat', '10000000', 'DHA phase 5', 'web-developer_0_1546778087.jpg', 'sererwer', 1, 1, 2, 1, '2019-01-06 07:34:47', '2019-01-06 07:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `property_purposes`
--

CREATE TABLE `property_purposes` (
  `id` int(10) UNSIGNED NOT NULL,
  `purpose` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property_purposes`
--

INSERT INTO `property_purposes` (`id`, `purpose`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sale', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(2, 'Buy', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(3, 'Rent', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `property_types`
--

CREATE TABLE `property_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property_types`
--

INSERT INTO `property_types` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Comercial', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(2, 'Apartment', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(3, 'Office', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(4, 'Industrial', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(5, 'residential', 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '', '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(2, 'manager', 'Manager', '', '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(3, 'user', 'User', '', '2018-12-13 14:24:09', '2018-12-13 14:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 3),
(3, 3),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sub_types`
--

CREATE TABLE `sub_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_types`
--

INSERT INTO `sub_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Student', NULL, NULL),
(2, 'Individual', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_min` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `phone`, `img`, `type_id`, `email`, `password`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Hassan', 'hassanraza', '03332671710', '', 2, 'admin@admin.com', '$2y$10$nb6UDg6iwS.YzabGw0lAjOfLsCVsxU00gAmr/PagV5w6sFRJo99WC', 'NV0zHPutxoK0NiO5w5dcaECL9UuEB8x9sm9esujTQMSvprpO2fk4kcQ0mOVW', 1, '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(2, 'Imran', 'imrankhan', '03332671710', '', 6, 'user@user.com', '$2y$10$pZNwo3QtxTJ9/bG8vHHL9.DkQS/sxhdPIdYcp53YlRN/QUypIImQu', NULL, 1, '2018-12-13 14:24:10', '2018-12-13 14:24:10'),
(3, 'Kamran', 'kamran786', '03332671710', NULL, 1, 'karmran786@gmail.com', '$2y$10$6lmIFT87o5o/P2pe72kufOhZqM2rAIc.PY2b02F5BejY/VJy3Xyk.', NULL, 1, '2018-12-13 15:10:00', '2018-12-13 15:10:00'),
(4, 'kami', 'kmaran', '03332671716', NULL, 1, 'kamran@gmail.com', '$2y$10$EPdWfyusw1k17u1mndL1xOra8aE/yk19VJxx9KqJVahIpJpoH22iW', NULL, 1, '2018-12-20 18:22:14', '2018-12-20 18:22:14'),
(5, 'asif', 'asifali', '123566688', NULL, 1, 'asif@gmail.com', '$2y$10$Dtc/4X6dyyf.SDRRNuV//.7KXkC63xEfVLtshAj42dITpRo2F9Rbq', 'XQd3CqzgN0LnYpKHyn8gJgTxiwGcT9wtu4eB3iNvZA7e8tJMjNifuCuTFCD9', 1, '2018-12-20 18:30:47', '2018-12-20 18:30:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Individual', 1, '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(2, 'Builder', 1, '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(3, 'Real Estate or Business', 1, '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(4, 'Architect', 1, '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(5, 'Interior Designer', 1, '2018-12-13 14:24:09', '2018-12-13 14:24:09'),
(6, 'Legal Service Advisor', 1, '2018-12-13 14:24:09', '2018-12-13 14:24:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `areas_city_id_index` (`city_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_purpose_id_index` (`purpose_id`),
  ADD KEY `property_type_id_index` (`type_id`),
  ADD KEY `property_area_id_index` (`area_id`),
  ADD KEY `property_user_id_index` (`user_id`);

--
-- Indexes for table `property_projects`
--
ALTER TABLE `property_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_projects_type_id_index` (`type_id`),
  ADD KEY `property_projects_area_id_index` (`area_id`),
  ADD KEY `property_projects_user_id_index` (`user_id`);

--
-- Indexes for table `property_purposes`
--
ALTER TABLE `property_purposes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_types`
--
ALTER TABLE `property_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sub_types`
--
ALTER TABLE `sub_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_type_id_index` (`type_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_types`
--
ALTER TABLE `project_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=503;

--
-- AUTO_INCREMENT for table `property_projects`
--
ALTER TABLE `property_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `property_purposes`
--
ALTER TABLE `property_purposes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `property_types`
--
ALTER TABLE `property_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_types`
--
ALTER TABLE `sub_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `property`
--
ALTER TABLE `property`
  ADD CONSTRAINT `property_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `property_purpose_id_foreign` FOREIGN KEY (`purpose_id`) REFERENCES `property_purposes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `property_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `property_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `property_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `property_projects`
--
ALTER TABLE `property_projects`
  ADD CONSTRAINT `property_projects_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `property_projects_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `project_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `property_projects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `user_types` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
