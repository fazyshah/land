<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('price')->nullable();
            $table->string('locality')->nullable();
            $table->string('img')->nullable();
            $table->text('details')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('type_id')->unsigned()->index();
            $table->foreign('type_id')->references('id')->on('project_types')->onDelete('cascade');
            $table->integer('area_id')->unsigned()->index();
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_projects');
    }
}
