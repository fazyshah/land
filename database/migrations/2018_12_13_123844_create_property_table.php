<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slogan')->nullable();
            $table->string('price')->nullable();
            $table->string('beds')->nullable();
            $table->string('baths')->nullable();
            $table->string('area')->nullable();
            $table->text('details')->nullable();
            $table->string('img')->nullable();
            $table->timestamp('exp_date');
            $table->boolean('status')->default(true);
            $table->boolean('featured')->default(false);
            $table->integer('purpose_id')->unsigned()->index();
            $table->foreign('purpose_id')->references('id')->on('property_purposes')->onDelete('cascade');
            $table->integer('type_id')->unsigned()->index();
            $table->foreign('type_id')->references('id')->on('property_types')->onDelete('cascade');
            $table->integer('area_id')->unsigned()->index();
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property');
    }
}
