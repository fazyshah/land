<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('user_types')->insert([
            [   'type' => 'Individual',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Builder',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Real Estate or Business',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Architect',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Interior Designer',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Legal Service Advisor',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);

        DB::table('sub_types')->insert([
            [   'name' => 'Student',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Individual',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);

        DB::table('roles')->insert([
            [   'name' => 'admin',
                'display_name' => 'Admin',
                'description' => '',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [   'name' => 'manager',
                'display_name' => 'Manager',
                'description' => '',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ],

            [   'name' => 'user',
                'display_name' => 'User',
                'description' => '',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);


        DB::table('users')->insert([
            'name' => 'Hassan',
            'username' => 'hassanraza',
            'phone' => '03332671710',
            'img' => '',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456'),
            'type_id' => $faker->numberBetween($min = 1, $max = 6),
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('role_user')->insert([
            'user_id'=> 1,
            'role_id'=> 1
        ]);

        DB::table('permissions')->insert([
            'name' => 'home',
            'display_name' => 'Home',
            'description' => 'Home Page',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 1
        ]);

        DB::table('users')->insert([
            'name' => 'Imran',
            'username' => 'imrankhan',
            'phone' => '03332671710',
            'img' => '',
            'email' => 'user@user.com',
            'password' => bcrypt('123456'),
            'type_id' => $faker->numberBetween($min = 1, $max = 6),
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('role_user')->insert([
            'user_id'=> 2,
            'role_id'=> 3
        ]);
    }
}