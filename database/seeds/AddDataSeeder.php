<?php

use Illuminate\Database\Seeder;

class AddDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('property_types')->insert([
            [   'type' => 'Comercial',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Apartment',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Office',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Industrial',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'residential',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);

        DB::table('property_purposes')->insert([
            [   'purpose' => 'For Sale',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'purpose' => 'To Buy',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'purpose' => 'For Rent',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);

        DB::table('cities')->insert([
            [   'name' => 'Karachi',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Lahore',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Islamabad',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'type' => 'Hyderabad',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);

        DB::table('areas')->insert([
            [   'name' => 'Gulshan e Iqbal',
                'city_id' => 1,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Clifton',
                'city_id' => 1,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'DHA Phase 5',
                'city_id' => 1,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Touheed Commercial',
                'city_id' => 1,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Gulistan e Johar',
                'city_id' => 1,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Gulburg',
                'city_id' => 2,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Bahria Town Lahore',
                'city_id' => 2,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Purana lahore',
                'city_id' => 2,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Anarkali',
                'city_id' => 2,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Blue Area',
                'city_id' => 3,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Sector I-10',
                'city_id' => 3,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Sector G9',
                'city_id' => 3,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Markaz I-10/2',
                'city_id' => 3,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Qasimabad',
                'city_id' => 4,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Latifabad',
                'city_id' => 4,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [   'name' => 'Haider Chowk',
                'city_id' => 4,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],

        ]);

        for ($i = 0; $i < 500; $i++) {
            DB::table('property')->insert([
                'title'   => $faker->sentence($nbWords = 4, $variableNbWords = true),
                'slogan' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'price' => $faker->randomElement($array = array ('2', '3','4','5', '6', '7', '8', '9', '3.5', '12', '5.6', '2.5', '15', '6.5')).' Crore',
                'beds' => $faker->numberBetween($min = 3, $max = 7),
                'baths'   => $faker->numberBetween($min = 1, $max = 5),
                'area'   => $faker->randomElement($array = array ('120', '150','200','300', '600', '1000', '1500', '3000')).' Sq. Yd.',
                'details'   => $faker->text($maxNbChars = 200),
                'img'   => 'https://source.unsplash.com/1600x900/?home',
                'exp_date'   => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'status' => 1,
                'user_id' => 2,
                'area_id'   => $faker->numberBetween($min = 1, $max = 16),
                'purpose_id'   => $faker->numberBetween($min = 1, $max = 3),
                'type_id'   => $faker->numberBetween($min = 1, $max = 5),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        }
    }
}