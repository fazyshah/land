<?php
//namespace App\Http;

//class Helpers {
//
//}

    if (!function_exists('cart')) {
        function cart() {
            return session()->get('cart');
        }
    }

    if (! function_exists('user')) {
        function user($parameter = null) {
            if(!\Auth::check()) {
                return null;
            }
            if($parameter) {
                return \Auth::user()->$parameter;
            }
            return \Auth::user();
        }
    }

    // if (!function_exists('role')) {
    //     function role() {
    //         return auth()->user()->roles[0]->name;
    //     }
    // }

    function isActiveRoute($route, $output = "is-active") {
        if (Route::currentRouteName() == $route) return $output;
    }

    function areActiveRoutes(Array $routes, $output = "is-active") {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }
    }