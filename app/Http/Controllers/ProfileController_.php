<?php

namespace App\Http\Controllers;

use App\Models\Images;
use App\Models\Profile;
use App\Models\Schedule;
use App\Models\States;
use App\Models\Job;
use App\Models\Seeking;
use App\Models\Smoke;
use App\Models\RelationStatus;
use App\Models\Children;
use App\Models\Drink;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class ProfileController extends Controller
{
    public function step2()
    {
        if (Auth::check() && Auth::user()->profile->status >= 2) {
            return redirect('register/step3');
        }
            $seeking = Seeking::pluck('name', 'id');
            $smoke = Smoke::pluck('name', 'id');
            $relationship = RelationStatus::pluck('name', 'id');
            $children = Children::pluck('name', 'id');
            $drink = Drink::pluck('name', 'id');

        return view('auth.register_step2', compact('seeking', 'smoke', 'relationship', 'children', 'drink'));
    }

    public function Step2Store(Request $request)
    {
        // Save Profile Step 2
        $profile = Profile::where('user_id', Auth::id())->firstOrFail();
        $profile->seeking_id            = Input::get('seeking');
        $profile->religion              = Input::get('religion');
        $profile->pets                  = Input::get('pets');
        $profile->smoke_id              = Input::get('smoke');
        $profile->children_id           = Input::get('relationship');
        $profile->drink_id              = Input::get('children');
        $profile->relationship_id       = Input::get('drink');
        $profile->about_yourself        = Input::get('about_yourself');
        $profile->looking_for           = Input::get('looking_for');
        $profile->hobbies_interests     = Input::get('hobbies_interests');
        $profile->status                = 2;
        $profile->save();

        // redirect
        Session::flash('message', 'Step 2 saved successfully');
        return Redirect::to('register/step3');
    }

    public function step3()
    {
        if (Auth::check() && Auth::user()->profile->status >= 3) {
            //Auth::logout();
            return Redirect::to('register/step4');
        }

        return view('auth.register_step3');
    }

    public function Step3Store(Request $request)
    {
        // Save Profile Step 3
        $profile = Profile::where('user_id', Auth::id())->firstOrFail();
        $profile->package               = Input::get('package');
        $profile->status                = 3;
        $profile->save();

        // redirect
        Session::flash('message', 'Step 3 saved successfully');
        return Redirect::to('register/step4');

    }

    public function step4()
    {
        // Save Profile Step 4
        $profile = Profile::where('user_id', Auth::id())->firstOrFail();
        $profile->status                = 4;
        $profile->save();

        if(Auth::user()->profile->verified == false){
            auth()->logout();
            $errors = 'Congratulations.. You are registered successfully. Now An admin is verifying your identify and making sure that you\'re a member of the medical community. Please allow 24 hours. You will receive an email confirmation once complete.';
            return Redirect::to('/')->withErrors($errors);
        } else {
            return Redirect::to('/home');
        }
    }

    public function search()
    {
        //$users = User::whereId(1)->with('profile')->get();
        //return $users;

        $users = profile::with('user')->paginate(15);

        $states = States::pluck('name', 'id');
        $schedule = Schedule::pluck('name', 'id');
        $job = Job::pluck('name', 'id');
        $seeking = Seeking::pluck('name', 'id');
        $smoke = Smoke::pluck('name', 'id');
        $relationship = RelationStatus::pluck('name', 'id');
        $children = Children::pluck('name', 'id');
        $drink = Drink::pluck('name', 'id');

        return view('search.search', compact('users', 'states', 'schedule', 'job', 'seeking', 'smoke', 'relationship', 'children', 'drink'));
    }
    public function searchResult(Request $request)
    {
        //dd($request->all());
        $agefrom = Input::get('agefrom');
        $ageto = Input::get('ageto');
        $seeking = Input::get('seeking');
        $schedule = Input::get('schedule');
        $job = Input::get('job');
        $children = Input::get('children');
        $smoke = Input::get('smoke');
        $drink = Input::get('drink');

        $users = Profile::whereBetween('age', [$agefrom, $ageto])
            ->orWhere('schedule_id', $schedule)
            ->orWhere('seeking_id', $seeking)
            ->orWhere('job_id', $job)
            ->orWhere('children_id', $children)
            ->orWhere('smoke_id', $smoke)
            ->orWhere('drink_id', $drink)
            ->paginate(15);
        //dd($users);

        $states = States::pluck('name', 'id');
        $schedule = Schedule::pluck('name', 'id');
        $job = Job::pluck('name', 'id');
        $seeking = Seeking::pluck('name', 'id');
        $smoke = Smoke::pluck('name', 'id');
        $relationship = RelationStatus::pluck('name', 'id');
        $children = Children::pluck('name', 'id');
        $drink = Drink::pluck('name', 'id');

        return view('search.search', compact('users', 'states', 'schedule', 'job', 'seeking', 'smoke', 'relationship', 'children', 'drink'));
    }

    public function UserProfile($id)
    {
        $user = Profile::whereUserId($id)->with('images')->first();

        return view('profile.profile_view', compact('user'));
    }
    public function myAccount()
    {
        $user = User::whereId(Auth::id())->with('profile','profile.state')->first();
        //return $user;

        return view('profile.my_account', compact('user'));
    }

    public function editProfile()
    {
        $user = User::whereId(Auth::id())->with('profile', 'images', 'profile.state')->first();
        //return $user;

        $states = States::pluck('name', 'id');
        $schedule = Schedule::pluck('name', 'id');
        $job = Job::pluck('name', 'id');
        $seeking = Seeking::pluck('name', 'id');
        $smoke = Smoke::pluck('name', 'id');
        $relationship = RelationStatus::pluck('name', 'id');
        $children = Children::pluck('name', 'id');
        $drink = Drink::pluck('name', 'id');

        return view('profile.edit_profile', compact('user', 'states', 'schedule', 'job', 'seeking', 'smoke', 'relationship', 'children', 'drink'));
    }

    public function updateProfile(Request $request)
    {
        // validate
        $rules = array(
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,id,'.Auth::id(),
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('edit_profile')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $user = User::find(Auth::id());
            $user->username     = Input::get('username');
            $user->email        = Input::get('email');
            $user->save();

            // Save Profile
            $profile = Profile::find($user->profile->id);
            $profile->license_number        = Input::get('license_number');
            $profile->schedule_id           = Input::get('schedule');
            $profile->job_id                = Input::get('job');
            $profile->age                   = Input::get('age');
            $profile->sex                   = Input::get('sex');
            $profile->city                  = Input::get('city');
            $profile->state_id              = Input::get('state');
            $profile->zip                   = Input::get('zip');
            $profile->seeking_id            = Input::get('seeking');
            $profile->religion              = Input::get('religion');
            $profile->pets                  = Input::get('pets');
            $profile->smoke_id              = Input::get('smoke');
            $profile->children_id           = Input::get('relationship');
            $profile->drink_id              = Input::get('children');
            $profile->relationship_id       = Input::get('drink');
            $profile->about_yourself        = Input::get('about_yourself');
            $profile->looking_for           = Input::get('looking_for');
            $profile->hobbies_interests     = Input::get('hobbies_interests');
            if ($request->hasFile('photo')) {
                $file            = $request->file('photo');
                $destinationPath = public_path('img/profiles');
                $filename        = time() . '.' . $file->getClientOriginalExtension();
                $uploadSuccess   = $file->move($destinationPath, $filename);
                $profile->photo  = $filename;
            }
            $profile->save();

            // redirect
            Session::flash('message', 'Profile updated successfully');
            return Redirect::to('my_account');
        }
    }

    public function ChangePass(Request $request)
    {
        // validate
        $rules = array(
            'password' => 'required|min:6|confirmed',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $user = User::find(Auth::id());
            $user->password        = bcrypt(Input::get('password'));
            $user->save();

            // redirect
            Session::flash('message', 'Password Changed successfully');
            return Redirect::back();
        }
    }

    public function addImage(Request $request)
    {
        // validate
        $rules = array(
            'photos' => 'required',
            'photos.*' => 'image'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator);
        } else {
            if ($request->hasFile('photos')) {

                $photos = Input::file('photos');
                //dd($photos);
                foreach($photos as $photo){

                    // Save Photo one by one
                    $image = new Images();
                    $image->user_id = Auth::id();

                    $destinationPath = public_path('img/profiles/more');
                    $filename        = uniqid('img_'.Auth::id()) . '.' . $photo->getClientOriginalExtension();
                    $uploadSuccess   = $photo->move($destinationPath, $filename);
                    $image->photo  = $filename;
                    $image->save();
                }
            }
        }

        // redirect
        Session::flash('message', 'Photos added successfully');
        return Redirect::to('my_account');

    }

    public function SupportPage()
    {
        return view('support');
    }

    public function upgradePkg()
    {
        return view('auth.register_step3');
    }

}