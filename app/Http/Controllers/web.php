<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', ['uses' => 'MainController@home', 'as' => 'home']);
Route::get('friends', ['uses' => 'MainController@friends', 'as' => 'friends']);
Route::get('family', ['uses' => 'MainController@family', 'as' => 'family']);
Route::get('my-posts', ['uses' => 'MainController@myPosts', 'as' => 'my-posts']);
Route::get('complete-profile', ['uses' => 'MainController@completeProfile', 'as' => 'complete-profile']);

Route::get('states/{parent_id}', ['uses' => 'MainController@states', 'as' => 'states']);
Route::get('cities/{parent_id}', ['uses' => 'MainController@cities', 'as' => 'cities']);


Route::group(['prefix' => 'register', 'middleware' => ['auth']], function () {
    Route::post('step2store', 'ProfileController@Step2Store')->name('step2store');
    Route::get('step2', 'ProfileController@Step2')->name('step2');

    Route::get('step3', 'ProfileController@Step3')->name('step3');
    Route::post('step3store', 'ProfileController@Step3Store')->name('step3store');

    Route::get('step4', 'ProfileController@Step4')->name('step4');
});

Route::group(['namespace' => 'Backend', 'prefix' => '{role}', 'middleware' => ['auth', 'routeperm']], function () { //'middleware'=>'role:admin'

    // Route::resource('users', 'UserController');
    // Route::resource('user-roles', 'RoleController');
    // Route::resource('user-permissions', 'PermissionsController');
    // Route::get('change-role-perm', ['uses' => 'PermissionsController@changeRolePerm', 'as' => 'change.role.perm']);
    // Route::get('roles-and-permissions', ['uses' => 'PermissionsController@rolesPermissions', 'as' => 'roles.permissions']);
    // Route::resource('categories', 'CategoryController');

    //Error Page
    Route::get('404', function () {return view('errors.404');})->name('404');

});
