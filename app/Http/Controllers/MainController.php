<?php

namespace App\Http\Controllers;

use App\Careers;
use App\ContactForm;
use Illuminate\Http\Request;
use App\City;
use App\Area;
use App\Accomodation;
use App\Images;
use App\Property;
use App\User;
use App\PropertyPurpose;
use App\PropertyType;
use App\ProjectType;
use App\PropertyProject;
use Session;

class MainController extends Controller
{
    public function home()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $featured_properties = Property::whereFeatured(true)->take(10)->get();
        $new_pprojects = PropertyProject::orderBy('id', 'desc')->take(3)->get();
        // dd($cities);
        return view('home', compact('cities', 'featured_properties', 'new_pprojects'));
    }

    public function search(Request $request)
    {
        $data = $request->all();
        // dd($data);

        $keyword = $data['keyword'];
        $city = $data['city'];
        //dd($city);
        // dd($city);
        //dd($keyword);
        $min=$data['min'];
        $max=$data['max'];


        if (!empty($city) && !empty($keyword)) {

            $searches=Property::with('propertyArea')->whereBetween('price',[$min,$max])->whereHas('propertyArea', function ($query) use ($city, $keyword) {
                   $query->where('city_id','=',$city)->where('name', 'LIKE', '%' . $keyword . '%');
              })->paginate(10);
            //dd($searches);

//            $searches = Property::whereBetween('price',[$min,$max])->
//                with('propertyArea', 'propertyUser', 'propertyType',
//                    'propertyPurpose')
//                ->whereHas('propertyArea', function ($query) use ($city, $keyword) {
//                    $query->where('city_id','=',$city)->where('name', 'LIKE', '%' . $keyword . '%');
//                })->
//                where('title', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('slogan', 'LIKE', '%' . $keyword . '%')
//
//                ->orWhere('beds', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('baths', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('area', 'LIKE', '%' . $keyword . '%')
//
//                ->WhereHas('propertyUser', function ($query) use ($keyword) {
//                    $query->where('name', 'LIKE', '%' . $keyword . '%')
//                        ->orWhere('username', 'LIKE', '%' . $keyword . '%')
//                        ->orWhere('phone', 'LIKE', '%' . $keyword . '%')
//                        ->orWhere('email', 'LIKE', '%' . $keyword . '%');
//                })
//                ->WhereHas('propertyType', function ($query) use ($keyword) {
//                     $query->where('type', 'LIKE', '%' . $keyword . '%');
//                })
//                ->WhereHas('propertyPurpose', function ($query) use ($keyword) {
//                    $query->where('purpose', 'LIKE', '%' . $keyword . '%');
//                })->orderBy('created_at', 'desc')->paginate(10);



            //dd($searches);
//        } elseif (empty($city) && !empty($keyword)) {
//            $searches = Property::where('title', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('slogan', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('price', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('beds', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('baths', 'LIKE', '%' . $keyword . '%')
//                ->orWhere('area', 'LIKE', '%' . $keyword . '%')
//                ->with('propertyArea', 'propertyArea.areaCity')
//                ->orWhereHas('propertyArea', function ($query) use ($keyword) {
//                    $query->where('name', 'LIKE', '%' . $keyword . '%');
//                })
//                ->orWhereHas('propertyArea.areaCity', function ($query) use ($keyword) {
//                    $query->where('name', 'LIKE', '%' . $keyword . '%');
//                })
//                ->orWhereHas('propertyUser', function ($query) use ($keyword) {
//                    $query->where('name', 'LIKE', '%' . $keyword . '%')
//                        ->orWhere('username', 'LIKE', '%' . $keyword . '%')
//                        ->orWhere('phone', 'LIKE', '%' . $keyword . '%')
//                        ->orWhere('email', 'LIKE', '%' . $keyword . '%');
//                })
//                ->orWhereHas('propertyType', function ($query) use ($keyword) {
//                    $query->where('type', 'LIKE', '%' . $keyword . '%');
//                })
//                ->orWhereHas('propertyPurpose', function ($query) use ($keyword) {
//                    $query->where('purpose', 'LIKE', '%' . $keyword . '%');
//                })->orderBy('created_at', 'desc')->paginate(10);
//
////        } elseif (!empty($city)) {
////            $searches = Property::with('propertyArea', 'propertyArea.areaCity')
////                ->WhereHas('propertyArea.areaCity', function ($query) use ($city) {
////                    $query->whereId($city);
////                })->orderBy('created_at', 'desc')->paginate(10);
////        }
//
//            // if(isset($keyword)) {
//            //   $searches = Property::where('name', 'LIKE', '%'.$keyword.'%')
//            //   ->orWhere('slogan', 'LIKE', '%'.$keyword.'%')
//            //   ->orWhere('price', 'LIKE', '%'.$keyword.'%')
//            //   ->orWhere('beds', 'LIKE', '%'.$keyword.'%')
//            //   ->orWhere('baths', 'LIKE', '%'.$keyword.'%')
//            //   ->orWhere('area', 'LIKE', '%'.$keyword.'%')
//            //   ->orWhere('phone', 'LIKE', '%'.$keyword.'%')
//            //   ->orWhere('email', 'LIKE', '%'.$keyword.'%')
//            //   ->with(['propertyArea'=>function($query){
//            //       $query->select('name');
//            //   }])
//            //   ->with(['propertyArea.areaCity'=>function($query){
//            //       $query->select('name');
//            //   }])
//            //   ->orWhereHas('propertyArea', function ($query) use ($keyword) {
//            //       $query->where('name', 'LIKE', '%'.$keyword.'%');
//            //   })
//            //   ->orWhereHas('propertyArea.areaCity', function ($query) use ($keyword) {
//            //       $query->where('name', 'LIKE', '%'.$keyword.'%');
//            //   })->get();
//            // }else{
//            //   $searches = Property::with(['propertyArea'=>function($query){
//            //       $query->select('name');
//            //   }])
//            //   ->with(['propertyArea.areaCity'=>function($query){
//            //       $query->select('name');
//            //   }])
//            //   ->WhereHas('propertyArea.areaCity', function ($query) use ($city) {
//            //       $query->whereId($city);
//            //   })->get();
//            // }
//
//            // dd($searches);
        }
        return view('search', compact('searches'));
    }

    // Projects Listings
    public function Projects()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $projects = PropertyProject::with('projectArea', 'projectArea.areaCity')
            ->orderBy('created_at', 'desc')->paginate(10);
        return view('project-listing', compact('projects','cities'));
    }

    // Add Projects
    public function addProject()
    {
        $ptypes = ProjectType::orderBy('type', 'asc')->select('type', 'id')->get();
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        return view('add-project', compact('ptypes', 'cities'));
    }

    // SAve Project
    public function saveProject(Request $request)
    {
        $data = $request->all();
        // dd($data);

        if ($data['images']) {
            $allImages = array();
            foreach ($data['images'] as $key => $image) {
                $destinationPath = public_path('web_asset/images/projects');
                $filename = str_slug($data['title'], '-') . '_' . $key . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $filename);
                $allImages[] = $filename;
            }
            $allImages = implode('|', $allImages);
        }

        $property = PropertyProject::create([
            'title' => $data['title'],
            'area_id' => $data['area'],
            'price' => $data['price'],
            'locality' => $data['locality'],
            'type_id' => $data['ptype'],
            'img' => isset($allImages) ? $allImages : null,
            'details' => isset($data['details']) ? $data['details'] : null,
            'user_id' => 1,
            'status' => 1,
        ]);
        return redirect()->route('view.property', ['id' => $property->id]);
    }

    // View Project
    public function viewProject($id)
    {
        // dd($id);
        $project = PropertyProject::whereId($id)
            ->with('projectArea', 'projectArea.areaCity')->first();
        return view('view-project', compact('project'));
    }


    public function addAccomodation()
    {

        $ppurposes = PropertyPurpose::orderBy('purpose', 'asc')->select('purpose', 'id')->get();
        $ptypes = PropertyType::orderBy('type', 'asc')->select('type', 'id')->get();
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        return view('add-accomodation', compact('ppurposes', 'ptypes', 'cities'));
    }

    public function saveAccomodation(Request $request)
    {
        $x = Accomodation::create([
            'title' => $request['title'],
            'details' => $request['details'],
            'city' => $request['city'],
            'location' => $request['location'],
            'room_size' => $request['room_size'],
            'price' => $request['price']
        ]);

        if ($request->hasFile('images')) {
            $multifile = $request->images;
            $path = 'uploads';
            //$pathUpdate = '';
            foreach ($multifile as $file) {
                $ref_id = $x->id;
                $name = $file->getClientOriginalName();
                $rand_num = rand(11111, 99999);
                $name = $rand_num . '-' . $name;
                $pathUpdate = $file->move('uploads', $name);

                $image_path = $pathUpdate;
                $img_products = Images::create([
                    'tbl_name' => 'Accomodation',
                    'ref_id' => $ref_id,
                    'img_src' => $image_path,
                ]);
            }


            Session::flash('message', 'Accomodation Added Successfully!');
            return redirect()->back();

        } else {
            return "problem please select images";
        }


    }

    public function accomodationDetail($id)
    {
        $accomodation = Accomodation::find($id);
        return view('shared-accomodation-detail')->with('accomodation', $accomodation);
    }

    public function ShowAccomodation()
    {
        $accomodations = Accomodation::all();
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $imgscr='web_asset/images/';
        return view('shared-accomodation', compact('cities','imgscr','accomodations'));

        //return view('shared-accomodation')->with('accomodations', $accomodations);
    }


    // Add Properties
    public function addProperty()
    {
        $ppurposes = PropertyPurpose::orderBy('purpose', 'asc')->select('purpose', 'id')->get();
        $ptypes = PropertyType::orderBy('type', 'asc')->select('type', 'id')->get();
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        return view('add-property', compact('ppurposes', 'ptypes', 'cities'));
    }

    // SAve Properties
    public function saveProperty(Request $request)
    {
        $data = $request->all();
        // dd($data);

        if ($data['images']) {
            $allImages = array();
            foreach ($data['images'] as $key => $image) {
                $destinationPath = public_path('web_asset/images/properties');
                $filename = str_slug($data['title'], '-') . '_' . $key . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $filename);
                $allImages[] = $filename;
            }
            $allImages = implode('|', $allImages);
        }

        $property = Property::create([
            'title' => $data['title'],
            'area_id' => $data['area'],
            'price' => $data['price'],
            'baths' => $data['baths'],
            'beds' => $data['beds'],
            'area' => $data['area_size'],
            'purpose_id' => $data['ppurpose'],
            'type_id' => $data['ptype'],
            'img' => isset($allImages) ? $allImages : null,
            'exp_date' => isset($data['validity']) ? \Carbon\Carbon::now()->addDays($data['validity']) : \Carbon\Carbon::now()->addDays(7),
            'details' => isset($data['details']) ? $data['details'] : null,
            'user_id' => 1,
            'status' => 1,
            'featured' => isset($data['featured']) ? $data['featured'] : null,
        ]);
        return redirect()->route('view.property', ['id' => $property->id]);
    }

    // View Properties
    public function viewProperty($id)
    {
        // dd($id);
        $property = Property::whereId($id)
            ->with('propertyArea', 'propertyArea.areaCity')->first();
        $featured_properties = Property::whereFeatured(true)->take(10)->get();

        $legal = User::where('type_id', 6)->get();

        return view('view-property', compact('property', 'featured_properties', 'legal'));


    }

    public function areaAjax($id)
    {
        $area = Area::whereCityId($id)->orderBy('name', 'asc')->pluck('name', 'id');
        return response()->json($area, 200);
    }

    // Buy Properties
    public function buyProperty()
    {
        $properties = Property::wherePurposeId(2)
            ->with('propertyArea', 'propertyArea.areaCity')
            ->orderBy('created_at', 'desc')->paginate(10);
        return view('buy-properties', compact('properties'));
    }

    // Rent Properties
    public function rentProperty()
    {
        $properties = Property::wherePurposeId(3)
            ->with('propertyArea', 'propertyArea.areaCity')
            ->orderBy('created_at', 'desc')->paginate(10);
        return view('rent-properties', compact('properties'));
    }

    // Sell Properties
    public function sellProperty()
    {
        $properties = Property::wherePurposeId(1)
            ->with('propertyArea', 'propertyArea.areaCity')
            ->orderBy('created_at', 'desc')->paginate(10);
        return view('sell-properties', compact('properties'));
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function users()
    {
        $users = User::orderBy('name', 'asc')->get();
        // dd($cities);
        return view('admin.users', compact('users'));
    }

    public function userTypes()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        // dd($cities);
        return view('home', compact('cities'));
    }

    public function userRoles()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        // dd($cities);
        return view('home', compact('cities'));
    }

    public function cities()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        // dd($cities);
        return view('home', compact('cities'));
    }

    public function areas()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        // dd($cities);
        return view('home', compact('cities'));
    }

    public function properties()
    {
        $properties = Property::with('propertyArea')->orderBy('created_at', 'desc')->get();
        // dd($cities);
        return view('admin.properties', compact('properties'));
    }

    public function propertyTypes()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        // dd($cities);
        return view('home', compact('cities'));
    }

    public function propertyPurposes()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        // dd($cities);
        return view('home', compact('cities'));
    }


    public function contactFormSubmit(Request $request)
    {
        $contact = new ContactForm();
        $contact->name = $request['name'];
        $contact->email = $request['email'];
        $contact->number = $request['pn'];
        $contact->message = $request['msg'];
        $contact->save();
        return view('contact-us');
    }


    public function legalAdvisorListing()
    {
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $legaladvisors=User::where('type_id',6)->paginate(10);
        $imgscr='web_asset/images/';
        return view('legal-advisor-listing', compact('cities','imgscr','legaladvisors'));
    }


    public function careerformsubmit(Request $request)
    {
        //dd($request);
        $careers = new Careers();
        //$file = $request->file('myfile');
        //dd($request['position']);
        //dd($file);
        //Move Uploaded File
        //$destinationPath = 'uploads';
        //$filename=$file->getClientOriginalName();
        //$file->move($destinationPath,$file->getClientOriginalName());
        $file = $request->file('myfile');

        $name = "/uploads/" . time() . '.' . $file->getClientOriginalExtension();

        $request->file('myfile')->move("uploads", $name);
        $careers->first_name = $request['first_name'];
        $careers->last_name = $request['last_name'];
        $careers->email = $request['email'];
        $careers->position = $request['position'][0];
        $careers->filename = $name;
        $careers->save();
        //

        return view('careers', ['imgscr' => 'web_asset/images/']);
    }


    public function TrendsView(){
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $imgscr='web_asset/images/';
        return view('trends', compact('cities','imgscr'));
    }

    public function getAuctionPage(){
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $imgscr='web_asset/images/';
        return view('auction-page', compact('cities','imgscr'));
    }


    public function getSocietyMaps(){
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $imgscr='web_asset/images/';
        return view('society-maps', compact('cities','imgscr'));
    }

    public function getAgentDirectory(){
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $imgscr='web_asset/images/';
        return view('agent-directory', compact('cities','imgscr'));
    }

    public function askanexpert(){
        $cities = City::orderBy('name', 'asc')->pluck('name', 'id');
        $imgscr='web_asset/images/';
        return view('ask-an-expert', compact('cities','imgscr'));
    }
}

