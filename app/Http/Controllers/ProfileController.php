<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Countries;
use App\User;
use App\Profile;
use App\Community;
use App\CommunityMembers;
use App\Social;


class ProfileController extends Controller
{
    public function step1(){
      if (user()->status >= 1) {
        return redirect()->route('cp.step2');
      }
      $countries = Countries::orderBy('name','asc')->pluck('name','id');
        return view('step-1', compact('countries'));
    }

    public function step2(){
      if (user()->status >= 2) {
        return redirect()->route('cp.step3');
      }elseif (user()->status < 1) {
        return redirect()->route('cp.step1');
      }
      $countries = Countries::orderBy('name','asc')->pluck('name','id');
        return view('step-2', compact('countries'));
    }

    public function step3(){
      if (user()->status >= 3) {
        return redirect()->route('cp.step4');
      }elseif (user()->status < 2) {
        return redirect()->route('cp.step2');
      }
      $countries = Countries::orderBy('name','asc')->pluck('name','id');
        return view('step-3', compact('countries'));
    }

    public function step4(){
      if (user()->status >= 4) {
        return redirect()->route('cp.step5');
      }elseif (user()->status < 3) {
        return redirect()->route('cp.step3');
      }
      dd('step44');
      $countries = Countries::orderBy('name','asc')->pluck('name','id');
        return view('step-4', compact('countries'));
    }

    public function step5(){
      if (user()->status >= 5) {
        return redirect()->route('home');
      }elseif (user()->status < 4) {
        return redirect()->route('cp.step4');
      }
      dd('step55');
      $countries = Countries::orderBy('name','asc')->pluck('name','id');
        return view('step-4', compact('countries'));
    }



    public function Step1Store(Request $request)
    {
        $data = $request->all();
        // dd($data);

        // store
        $user = User::find(Auth::id());
        $user->status = 1;

        // Save Profile
        $profile = Profile::whereUserId(user('id'))->firstOrFail();
        $profile->nick_name   = $data['nickname'];
        $profile->gender      = $data['gender'];
        $profile->dob         = $data['dob'];
        $profile->contact     = $data['contact'];
        $profile->bio_data    = $data['bio_data'];

        if ($request->hasFile('profile_img')) {
          $file            = $request->file('profile_img');
          $destinationPath = public_path('assets/img/profiles');
          $filename        = user('id').time() . '.' . $file->getClientOriginalExtension();
          $uploadSuccess   = $file->move($destinationPath, $filename);
          $profile->profile_img  = $filename;
        }
        if ($request->hasFile('cover_img')) {
          $file            = $request->file('cover_img');
          $destinationPath = public_path('assets/img/covers');
          $filename        = user('id').time() . '.' . $file->getClientOriginalExtension();
          $uploadSuccess   = $file->move($destinationPath, $filename);
          $profile->cover_img  = $filename;
        }
        $profile->save();

        // Save Profile
        $social = Social::whereUserId(user('id'))->firstOrFail();
        $social->google     = $data['google'];
        $social->facebook   = $data['facebook'];
        $social->linkedin   = $data['linkedin'];
        $social->twitter    = $data['twitter'];
        $social->web        = $data['web'];
        $social->save();

        // Save User status to Step 1
        $user->save();

        // redirect
        return redirect()->route('cp.step2');
    }
    public function Step2Store(Request $request)
    {
        $data = $request->all();
        // dd($data);

        $user = User::find(Auth::id());
        $user->status = 2;

        if (isset($data['location']) && $data['location'] != 'new_loc') {
          // Assigin Community
          $member = new CommunityMembers;
          $member->community_id = $data['location'];
          $member->user_id = user('id');
          $member->is_moderator = 0;
          $member->votes = 0;
          $member->save();
        }elseif (isset($data['newlocation']) && $data['location'] == 'new_loc') {
          // Save Community

          $comunity = new Community;
          $comunity->ctype_id = 1;
          $comunity->table_id = $data['city'];
          $comunity->name = $data['newlocation'];
          $comunity->taglines = isset($data['taglines']) ? $data['taglines'] : null;

          if ($request->hasFile('dp')) {
            $file            = $request->file('dp');
            $destinationPath = public_path('assets/img/communities');
            $filename        = user('id').time() . '.' . $file->getClientOriginalExtension();
            $uploadSuccess   = $file->move($destinationPath, $filename);
            $profile->dp  = $filename;
          }

          if ($request->hasFile('cover')) {
            $file            = $request->file('cover');
            $destinationPath = public_path('assets/img/communities/covers');
            $filename        = user('id').time() . '.' . $file->getClientOriginalExtension();
            $uploadSuccess   = $file->move($destinationPath, $filename);
            $profile->cover  = $filename;
          }

          $comunity->desc = $data['about'];
          $comunity->creator_id = user('id');
          $comunity->moderator_id = user('id');
          $comunity->location = null;
          $comunity->save();

          $member = new CommunityMembers;
          $member->community_id = $comunity->id;
          $member->user_id = user('id');
          $member->is_moderator = 1;
          $member->votes = 0;
          $member->save();

        }

        // Save User status to Step 2
        $user->save();

        // redirect
        return redirect()->route('cp.step3');
    }
    public function Step3Store(Request $request)
    {
        // Save Profile Step 3
        $user = User::where('id', Auth::id())->firstOrFail();
        $user->status = 3;
        $user->save();

        // redirect
        return redirect()->route('cp.step4');
    }
    public function Step4Store(Request $request)
    {
        // Save Profile Step 4
        $user = User::where('id', Auth::id())->firstOrFail();
        $user->status = 4;
        $user->save();

        // redirect
        return redirect()->route('cp.step5');
    }
    public function Step5Store(Request $request)
    {
        // Save Profile Step 5
        $user = User::where('id', Auth::id())->firstOrFail();
        $user->status = 5;
        $user->save();

        // redirect
        return redirect()->route('home');
    }

}