<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSetting extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'slogan', 'contact1', 'contact2', 'email', 'Address', 'year', 'full_url', 'url', 'small_logo', 'big_logo'
    ];
}