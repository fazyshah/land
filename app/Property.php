<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
	protected $table = 'property';

    protected $fillable = [
		'title', 'slogan', 'price', 'beds', 'baths', 'area', 'details', 'img', 'exp_date', 'area_id', 'purpose_id', 'type_id', 'user_id', 'status'

 	];

	public function propertyArea() {
        return $this->belongsTo('App\Area', 'area_id', 'id');
    }

    public function propertyUser() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function propertyType() {
        return $this->hasOne('App\PropertyType', 'id', 'type_id');
    }
    public function propertyPurpose() {
        return $this->hasOne('App\PropertyPurpose', 'id', 'purpose_id');
    }
}
