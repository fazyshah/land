<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyProject extends Model
{
    protected $fillable = [
		'title', 'locality', 'price', 'details', 'img', 'area_id', 'type_id', 'user_id', 'status'

 	];

    public function projectType() {
        return $this->hasOne('App\ProjectType', 'id', 'type_id');
    }

    public function projectArea() {
        return $this->belongsTo('App\Area', 'area_id', 'id');
    }

    public function projectUser() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
