<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Careers extends Model
{
    //
    protected $table='careers';
    protected $fillable=['first_name','last_name','email','position','filename'];
}
