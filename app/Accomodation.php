<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accomodation extends Model
{
    protected $table = 'accomodation';

    protected $fillable = [
		'title','details', 'city', 'location', 'room_size', 'price', 'created_at'
 	];
}
