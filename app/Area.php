<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = [
		'name', 'city_id'
	];

	// public function userPostComment() {
 //        return $this->belongsTo('App\User', 'user_id', 'id');
 //    }

    public function areaCity() {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function areaProperties() {
        return $this->hasMany('App\Property', 'id', 'area_id')->orderBy('name', 'asc');
    }
}
