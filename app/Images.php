<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'images';

    protected $fillable = [
		'tbl_name','ref_id', 'img_src','created_at'
 	];
}
