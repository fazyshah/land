new WOW().init();

$(function() {
    //Slim Scroller
    


    $('[data-fancybox="video-file"]').fancybox({
         iframe : {
           css : {
             width : '75%',
             height : '100%'
           }
         }
       }); 

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })



    
   

        $('.log').click( function(){
           if ( $( '.log-form .inner-log' ).hasClass("disp") ) {
               $('.log-form .inner-log').removeClass('disp');
                $('.overlay-bg-2').removeClass('active-overlay');
           } else {    
               $('.log-form .inner-log').addClass('disp');
                $('.overlay-bg-2').addClass('active-overlay');
         }
     });

     $('.overlay-bg-2').click(function(){
      $('.log-form .inner-log').removeClass('disp');
     if ( $( '.log-form .inner-log' ).hasClass("disp") ) {
       $('.overlay-bg-2').addClass('active-overlay');
     }else {
       $('.overlay-bg-2').removeClass('active-overlay');
     }
   });
   
   


   $('.test').on('click', function(){
        $('.--client-logos [class^="col-md-"]:nth-child(n+9)').fadeIn();
        $(this).fadeOut();
    });

   $('.test').on('click', function(){
        $('.--client-test [class^="col-md-"]:nth-child(n+9)').fadeIn();
        $(this).fadeOut();
    });
    
    
    

    $(".shakira li").hover(function() {
            $(this).hasClass("act") || ($(this).hasClass("loc-dubai") && ($(".shakira li").removeClass("act"), 
                $(".regional-sec").addClass("dubai").removeClass("uk").removeClass("sg"),
                $(this).addClass("act")), $(this).hasClass("loc-uk") && ($(".shakira li").removeClass("act"), 
                $(".regional-sec").addClass("uk").removeClass("dubai").removeClass("sg"), 
                $(this).addClass("act")), 
                $(this).hasClass("loc-sg") && ($(".shakira li").removeClass("act"), 
                    $(".regional-sec").addClass("sg").removeClass("uk").removeClass("dubai"), 
                    $(this).addClass("act")))
    })

    $(".colr").on('click', function() {
        $('.new').slideToggle('show');
    });
        
    
    $(".fltr").on('click', function(e) {
        $('.filter').slideToggle('show');
        
    });

    $("#example-two").on("click", function() {
      var el = $(this);
      if (el.text() == el.data("text-swap")) {
        el.text(el.data("text-original"));
      } else {
        el.data("text-original", el.text());
        el.text(el.data("text-swap"));
      }
    });

    $(".drop").on('click', function(e) {
        e.stopPropagation();
        $(this).children('ul').slideToggle();
        $(this).toggleClass('rotate');
        
    });

    $(".call-form").on('click', function() {
        $('.popup-form .inner').addClass('show');
    });

    $(".call-form-email").on('click', function() {
        $('.popup-form-email .inner').addClass('show');
    });

  



    $(".accordion li h5").click(function () {
      var current_li = $(this).parent();
      $(".accordion li div").each(function(i,el) {          
        if($(el).parent().is(current_li)) {             
          $(el).prev().toggleClass("plus");
          $(el).slideToggle();              
        } else{
          $(el).prev().removeClass("plus");
          $(el).slideUp();
        }
      });
    });
    $('.accordion li > div').hide();
    $('.accordion li h5').first().addClass("plus");
    $('.accordion li > div').first().show().addClass("plus");






   

    $('[data-targetit]').on('click',function () {
            $(this).siblings().removeClass('current');
            $(this).addClass('current');
            var target = $(this).data('targetit');
            $('.'+target).siblings('[class^="tabs"]').removeClass('current');
            $('.'+target).addClass('current');
        });
        
        $('[data-targetit]').on('click',function () {
            $(this).siblings().removeClass('current');
            $(this).addClass('current');
            var target = $(this).data('targetit');
            $('.'+target).siblings('[class^="tabs-"]').removeClass('visible');
            $('.'+target).addClass('visible');
            $('.slick-slider').slick('setPosition', 0);
            $('.matchheight').matchHeight();
        });

    



    /*Form Validate*/
    $(".jform").validate();

     
      

    $('.form_validate').validate({
                rules : {
                    pass : {
                        minlength : 1
                    },
                    cn : {
                        required :true
                    },
                    ln : {
                        required :true,
                    },
                    adr1 : {
                        required :true,

                    },
                    cp : {
                        required :true,
                        
                    },
                    ce : {
                        required :true,
                        
                    },
                   
                    cc : {
                        minlength : 13,
                        maxlength : 17,
                        required : true
                    },
                    date : {
                        required : true
                    },
                    zip : {
                        required: true,
                        minlength: 5,
                        maxlength: 5,
                        digits: true
                    },
                    cvn : {
                        required : true,
                        minlength : 3,
                        maxlength : 4
                        
                    }
                },
                 messages: {
                    cn: "Please enter your first name",
                    ln: "Please enter your last name",
                    adr1: "Please enter your address",

                    date: "Please enter your card expiry date",

                    ce: {
                        required: "Please enter your email address",
                        email: "Please enter a valid email address."
                    },
                    
                    agree: "You must agree to our terms and conditions",
                    cp: {
                        number: "Please enter a valid number",
                        required: "Please enter your phone number"
                    },

                    zip: {
                        required: "Please enter your Postal Code!",
                        minlength: "Your Postal Code must be 5 numbers!",
                        maxlength: "Your Postal Code must be 5 numbers!",
                        digits: "Your Postal Code must be 5 numbers!"
                    },

                    
                    
                    cvn: {
                        required: "Please enter your card verification Number",
                        minlength: "Your card verification Number must be 3 numbers!",
                        maxlength: "Your card verification Number must be 4 numbers!"
                    },
                    cc: {
                        required: "Please enter your card Number",
                        number: "Please enter a valid card Number."
                    }
                    
      }

     });

 
 



    $(".count-slider").slick({
        dots: !0,
        arrows: !1,
        infinite: !0,
        speed: 600,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 420,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    }), s = {
        1: {
            slider: ".about-slider"
        },
        2: {
            slider: ".testimonial-slider"
        },
        3: {
            slider: ".pricing-slider"
        },
        4: {
            slider: ""
        }
    }, $.each(s, function() {
        $(this.slider).slick({
            dots: false,
            arrows: !1,
            infinite: !1,
            autoplay: true,
            speed: 600,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        })
    }), $(".call-form").click(function() {
        $(".popup-form").fadeIn(), $(".overlay-bg").fadeIn()
    }), $(".close-popup").click(function() {
        $(".popup-form").fadeOut(), $(".overlay-bg").fadeOut()
    }),

    $(".call-form-email").click(function() {
        $(".popup-form-email").fadeIn(), $(".overlay-bg").fadeIn()
    }), $(".close-popup").click(function() {
        $(".popup-form-email").fadeOut(), $(".overlay-bg").fadeOut()
    }),



     $(".close-sticky").click(function() {
        $(".sticky-cta").fadeOut()
    }), $(".matchheight").matchHeight(), $(".mobile-nav-btn").click(function() {
        $(".mobile-nav-btn, .mobile-nav, .app-container, header.ph").toggleClass("active")
    }), 
    // $("nav.pn li").each(function() {
    //     $(this).removeClass("current"), $(this).find("a").attr("href") === window.location.pathname && $(this).addClass("current")
    // }),
     $("[data-targetit]").on("click", function() {
        $(this).siblings().removeClass("current"), $(this).addClass("current");
        var s = $(this).data("targetit");
        $("." + s).siblings('[class^="tabs-"]').removeClass("visible"), $("." + s).addClass("visible"), $(".slick-slider").slick("setPosition", 0), $(".matchheight").matchHeight()
    }), $(".homepage-hero-slider").slick({
        dots: !1,
        arrow: !0,
        infinite: !0,
        speed: 300,
        slidesToShow: 1,
        autoplay: !0,
        autoplaySpeed: 3e3,
        adaptiveHeight: !0
    }), $(".testi-slider").slick({
        dots: !0,
        arrows: false,
        infinite: !0,
        speed: 2e3,
        slidesToShow: 1,
        autoplay: !0,
        autoplaySpeed: 3e3
    }),

    $(".main-slider").slick({
        arrows: false,
        dots: true,
        autoplay: true,
        slidesToShow:1,
        slidesToScroll: 1,
        autoplaySpeed: 4000
    });

    $(".ecom-slider").slick({
        arrows: false,
        dots: true,
        autoplay: true,
        slidesToShow:1,
        slidesToScroll: 1,
        autoplaySpeed: 4000
    });


    $(".premium-agency-slider").slick({
        arrows: true,
        dots: false,
        autoplay: true,
        slidesToShow:8,
        slidesToScroll: 1,
        autoplaySpeed: 4000,
        responsive: [
            
            {
              breakpoint: 991,
              settings: {
                vertical: false,
                slidesToShow: 6
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            }
        ]
    });

     $(".side-advertisement-slider").slick({
        arrows: false,
        dots: false,
        autoplay: true,
        slidesToShow:11,
        slidesToScroll: 1,
        vertical: true,
        responsive: [
            
            {
              breakpoint: 991,
              settings: {
                vertical: false,
                slidesToShow: 6
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            }
        ]
        
    });
     $(".side-advertisement-slider-2").slick({
        arrows: false,
        dots: false,
        autoplay: true,
        slidesToShow:15,
        slidesToScroll: 1,
        vertical: true,

        responsive: [
            
            {
              breakpoint: 991,
              settings: {
                vertical: false,
                slidesToShow: 6
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            }
        ]
        
    });
     $(".auction-slider").slick({
        arrows: true,
        dots: false,
        autoplay: true,
        slidesToShow:3,
        slidesToScroll: 1,
        speed: 1200,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1
              }
            }
        ]
    });
    

    ////// testimonials slider end

    ////// category slider
    $(".about-category-slider").slick({
        arrows: false,
        dots: false,
        autoplay: true,
        adaptiveHeight: true,
        responsive: [
            {
              breakpoint: 10000,
              settings: "unslick"
            },
            {
              breakpoint: 767,
              settings: {
                unslick: true,
                dots: true
              }
            }
        ]
    });

   

    $('.big-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            autoplay: false,
            autoplaySpeed: 3000,
            asNavFor: '.small-slider',
            responsive: [
                {
                  breakpoint: 767,
                  settings: {
                    autoplay:true
              }
            }
            ]
      });
      $('.small-slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.big-slider',
            dots: false,
            autoplay: false,
            arrows: true,
            autoplaySpeed: 3000,
            focusOnSelect: true
      });

      $('.big-slider-2').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            dots: false,
            mouseover: false,
            autoplay: false,
            autoplaySpeed: 3000,
            asNavFor: '.small-slider-2',
            responsive: [
                {
                  breakpoint: 767,
                  settings: {
                    autoplay:true
              }
            }
            ]
      });
      $('.small-slider-2').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.big-slider-2',
            dots: false,
            autoplay: false,
            arrows: true,
            autoplaySpeed: 3000,
            focusOnSelect: true,
            vertical: true
      });


      $(".box-det-slider").slick({
          arrows: false,
          dots: false,
          autoplay: true,
          slidesToShow: 4,
          responsive: [
              
              {
                breakpoint: 991,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
          ]
          
      });

      $(".flash-slider").slick({
        arrows: false,
        dots: false,
        autoplay: true,
        responsive: [
            {
              breakpoint: 2000,
              settings: "unslick"
            },
            {
              breakpoint: 991,
              settings: {
                unslick: true,
                slidesToShow: 3,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 767,
              settings: {
                unslick: true,
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 535,
              settings: {
                unslick: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                
              }
            }
        ]
    });

 
  ////// clients slider
    $(".shared-slider").slick({
        arrows: false,
        dots: false,
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1
              }
            }
        ]
        
    });

     $(".shared-slider-four-block").slick({
        arrows: false,
        dots: false,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1
              }
            }
        ]
        
    });

    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 500;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "View more +";
    var lesstext = "View less -";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a  href="" class="morelink btn-detail float-right">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });


    
 $(".portfoliopg-slider").slick();
    var s = {
        1: {
            slider: ".logo-features-slider"
        },
        2: {
            slider: ".logo-ser-slick"
        },
        3: {
            slider: ".process-slick"
        },
        4: {
            slider: ".thankyou-slick"
        },
        5: {
            slider: ".left-list"
        },
        6: {
            slider: ".right-list"
        }
    };
    $.each(s, function() {
        $(this.slider).slick({
            arrows: !1,
            dots: !0,
            autoplay: !0,
            infinite: !0,
            speed: 600,
            responsive: [{
                breakpoint: 9999,
                settings: "unslick"
            }, {
                breakpoint: 767,
                settings: {
                    unslick: !0
                }
            }]
        })
    })
});


$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    // var msg = "";
    // if (ratingValue > 1) {
    //     msg = "Thanks! You rated this " + ratingValue + " stars.";
    // }
    // else {
    //     msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    // }
    // responseMessage(msg);
    
  });

  
  
  
});