/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
$(function () {
    'use strict';
    var count = 0;
    // Initialize the jQuery File Upload widget:
    $('#add-listing-form').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'imageUpload',
        
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        process:[
            {
                acceptFileTypes: /^image\/(gif|jpeg|jpg|png)$/,
                maxFileSize: 15242880, // 5MB
                action: 'load',
            },
            {
                action: 'resize',
                maxWidth: 1920,
                maxHeight: 1080,
                minWidth: 800,
                minHeight: 600
            },
            {
                action: 'save'
            }
        ],
        add: function (e, data) {
            $('.spinner').show();
            // $(".se-pre-con").show();
            $('.submit').prop('disabled', true);
            $('#add-listing-form').addClass('fileupload-processing');
            var uploadFile = data.files[0];
            var ext = uploadFile.name.split('.').pop().toLowerCase();
            var goUpload = true;
            
            if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                goUpload = false;
                $('.submit').prop('disabled', false);
                $('.spinner').hide();     
            }
            
            
            if (uploadFile.size > 15242880) { // 5MB
                goUpload = false;
            }

            if (goUpload == true) {
                count++; 
                var $this = $(this);
                data.process(function () {
                    return $this.fileupload('process', data);
                }).done(function() {
                    data.submit();
                });
            }
        },
        done: function (e, data) {
            var folder = data.result;
            $('#add-listing-form').append('<input type="hidden" name="folder[]" value="'+ folder +'" />');
            
            $("#files").prop('required',false);
            // $(".se-pre-con").hide();
            count--;
            if(count == 0){
                $("#files").prop('required',false);
                if($('#submit-error-msg').css('display') == 'none')
                {
                    $('.submit').prop('disabled', false);
                }
                $('.spinner').hide();
            }
        },
    });
});

$(function () {
    'use strict';
    var count = 0;
    // Initialize the jQuery File Upload widget:
    $('#edit-listing-form').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/file_process',
        
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        process:[
            {
                acceptFileTypes: /^image\/(gif|jpeg|jpg|png)$/,
                maxFileSize: 15000000, // 5MB
                action: 'load',
            },
            {
                action: 'resize',
                maxWidth: 1920,
                maxHeight: 1080,
                minWidth: 800,
                minHeight: 600
            },
            {
                action: 'save'
            }
        ],
        add: function (e, data) {
            count++;   
            $('.spinner').show();
            // $(".se-pre-con").show();
            $('.update').prop('disabled', true);
            $('#edit-listing-form').addClass('fileupload-processing');
            var uploadFile = data.files[0];
            var goUpload = true;
            
            if (uploadFile.size > 15000000) { // 5MB
                goUpload = false;
            }
            if (goUpload == true) {
                var $this = $(this);
                data.process(function () {
                    return $this.fileupload('process', data);
                }).done(function() {
                    data.submit();
                });
            }
        },
        done: function (e, data) {
            var folder = data.result.files[0].folder_name;
            $('#edit-listing-form').append('<input type="hidden" name="folder[]" value="'+ folder +'" />');
            
            $("#files").prop('required',false);
            // $(".se-pre-con").hide();
            count--;
            console.log(count);
            if(count == 0){
                $('.update').prop('disabled', false);
                $('.spinner').hide();
            }
        },
    });
});