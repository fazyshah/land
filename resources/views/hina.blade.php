@extends('layout.master')
@section('title', 'advertise')

@section('content')




<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Advertise</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Rokkitt:100,200,300,400,500,600,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <!-- <script type="text/javascript" src="../assets/js/ZoomifyImageViewerFree-min.js"></script>  -->
           <!--    <script type="text/javascript"> Z.showImage("myContainer", "../assets/images/a"); </script>  -->


        
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="../assets/css/style.css">
        
    </head>
<body class="advertise">

<header class="primary" style="background: url(../assets/images/topnav-bg.jpg);">
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <a href="/">
                <img class="ptpx-3" src="../assets/images/logo.png" alt="*">
                </a>
            </div>
            <div class="col-lg-10 nopadd-left hidden-md-down">
                <nav class="primary  float-left">
                    <ul class="list-left unstyled">
                        <li><span class="icon-home1 fc-white pr-1"></span><a href="../ask-an-expert">Ask An Expert</a></li>
                        <li><a href="../blog">Blog</a></li>
                        <li><a href="../society-maps">Landtrack Maps</a></li>
                        <li><a href="../home-partners">Home Partners</a></li>
                        <li><a href="../legal-advisor-listing">Legal Services</a></li>
                        <li><a href="../auction-page">Auction</a></li>


                                                
                    </ul>
                    <ul class="login-panel inline fc-white">
                            <li><a href="#">Language <span class="icon-caret-down"></span></a></li>
                            <li><a href="../sign-up"><span class="icon-profile"></span>SignUp</a></li>
                            
                            <li class="log-form">
                                <div class="overlay-bg-2"></div>
                                <a class="log" href="javascript:;"><span class="icon-user2"></span>Login
                                </a>
                                <div class="inner-log">
                                    <div class="bui">
                                      <form class="hlogin-form" >
                                            <input  type="email" id="emailaddress" name="emailaddress" placeholder="Email*">
                                            <input class="mtpx-15" type="password" placeholder="Password*">
                                            
                                            <div class="dow">
                                                <input class=" btn-orange w-100 mtpx-15" type="submit"   value="Login">  
                                            </div>
                                        
                                      </form>
                                      <div class="grid-block --type-two-blocks ptpx-8 pbpx-20">
                                          <div class="item ta-left">
                                              <label class="fc-lgrey  fs-small"><input type="checkbox" value="">Remember Me</label>
                                          </div>
                                          <div class="item  ta-right">
                                              <a class=" fs-small" href="javascript:;">Forgot Password</a>
                                          </div>
                                      </div>
                                      <div class="butons">
                                          <a class="btn-fb w-100 tt-uppercase" href="javascript:;"><span class="icon-facebook icu"></span>Sign in With Facebook</a>

                                          <a class="btn-google  w-100 tt-uppercase mtpx-10" href="javascript:;"><span class="icon-google-plus icu-g"></span>Sign in With gmail</a>
                                          <p class="fs-medium fc-secondary ptpx-15 pbpx-5">Not a Member Yet?</p>
                                          <a class="fw-medium a3" href="../sign-up">join us now</a>
                                      </div>
                                    </div>  
                                </div>
                            </li>
                        </ul>    
                </nav>
                
                <nav class="secondary float-left">
                    <ul class="inline tt-uppercase float-left">
                        <li><a href="../buy">Buy</a></li>
                        <li><a href="../buy">Sell</a></li>
                        <li><a href="../buy">Rent</a></li>
                        <li><a href="../shared-accomodation">Shared Accomodation</a></li>
                        <li><a href="../project-listing">New Projects<!-- <span class="icon-caret-down"></span> --></a></li>
                        <li><a href="../agent-directory">Agent Directory</a></li>
                        <li><a href="javascript:;">LT Store</a></li>
                        <li><a href="../trends">Property Trends</a></li>
                        
                    </ul>
                    <a class="btn-secondary fs-small" href="../add-property"><span class="icon-plus"></span>Add Property</a>
                </nav>
                
            </div>
        </div>
    </div>
    
</header>


<div class="mobile-nav-btn ">
    <span class="lines"></span>
</div>

<div class="mobile-nav">
    <!-- <a href="/" class="mob-nav-logo ">
        <img src="../assets/images/logo.png" alt=""/>
    </a> -->
    <nav class="pl">
        <ul class="unstyled">
            <li><a href="../buy">Buy</a></li>
            <li><a href="../buy">Sell</a></li>
            <li><a href="../buy">Rent</a></li>
            <li><a href="../shared-accomodation">Shared Accomodation</a></li>
            <li><a href="javascript:;">New Projects<span class="icon-caret-down"></span></a></li>
            <li><a href="../agent-directory">Agent Directory</a></li>
            <li><a href="javascript:;">LT Store</a></li>
            <li><a href="../trends">Property Trends</a></li>
            <li><a href="">Ask An Expert</a></li>
            <li><a href="../blog">Blog</a></li>
            <li><a href="../society-maps">Landtrack Maps</a></li>
            <li><a href="../home-partners">Home Partners</a></li>
            <li><a href="../legal-advisor-listing">Legal Services</a></li>
            <li><a href="../auction-page">Auction</a></li>
            
        </ul>
    </nav>
</div>



<section class="second-banner buy sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb ff-primary">
                  <li><a href="/">Home&nbsp;</a></li>
                  <li>&nbsp;Advertise</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="glance">
                    <p class="heading-main fs-large fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">land track at a Glance</p>
                    <ul class="unstyled inline grid-block --type-five-blocks ">
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="../assets/images/glance/img1.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">Over 3.5 million monthly visits</p>
                               </div>
                           </div>
                       </li>
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="../assets/images/glance/img2.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">More than 15 million monthly pageviews</p>
                               </div>
                           </div>
                       </li>
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="../assets/images/glance/img3.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">Registered members exceeding 700,000</p>
                               </div>
                           </div>
                       </li> 
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="../assets/images/glance/img4.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">More than 200,000 new properties <br>added every <br>month</p>
                               </div>
                           </div>
                       </li> 
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="../assets/images/glance/img5.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">Over 3.5 million monthly visits</p>
                               </div>
                           </div>
                       </li> 
                    </ul>
                </div>

                <div class="glance two mtpx-20">
                    <p class="heading-main fs-large fw-semi-bold d-small-2 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Why Advertise With land track?</p>
                    <p class="fs-13 ">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>

                    <p class="heading-main fs-large fw-semi-bold abou-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft mtpx-40">AGENCIES</p>
                    <p class="fs-13 ">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                    <p class="heading-main fs-large fw-semi-bold abou-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft  mtpx-40">DEVELOPERS</p>
                    <p class="fs-13 mbpx-40">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                    <a class="glance-btn" href="javascript:;"><span class="icon-icon-49"></span>0800-1234 (45678)</a>
                </div>
            </div>
            <div class="col-lg-3">
                
                <div class="advertise-sect   bg-white">
  <div class="head">
    <p class="fs-17 fc-primary fw-semi-bold tt-uppercase"><span class="icon-icon-48"></span>Advertise</p>
  </div>
  <ul class="unstyled acco">
    <li class="drop"><a class="fw-semi-bold" href="javascript:;">Banner Advertising</a>
      <ul class="unstyled dotted">
        <li><a href="javascript:;">Leaderboard</a></li>
        <li><a href="javascript:;">Site Wide Right Banner</a></li>
        <li><a href="javascript:;">Splash Banner</a></li>
        <li><a href="javascript:;">Middle Banner Home</a></li>
        <li><a href="javascript:;">Middle Banner Search</a></li>
        <li><a href="javascript:;">Middle Banner Category</a></li>
        <li><a href="javascript:;">Wallpaper Takeover</a></li>
      </ul>
    </li>
    <li class="drop"><a class="fw-semi-bold" href="javascript:;">Property Advertising</a>
      <ul class="unstyled dotted">
        <li><a href="javascript:;">October (24)</a></li>
        <li><a href="javascript:;">September (52)</a></li>
      </ul>
    </li>
    <li class="drop"><a class="fw-semi-bold" href="javascript:;">Email Advertising</a>
      <ul class="unstyled dotted">
        <li><a href="javascript:;">October (24)</a></li>
        <li><a href="javascript:;">September (52)</a></li>
      </ul>
    </li>
    <li class="drop"><a class="fw-semi-bold" href="javascript:;">Developer Advertising</a>
      <ul class="unstyled dotted">
        <li><a href="javascript:;">October (24)</a></li>
        <li><a href="javascript:;">September (52)</a></li>
        <li><a href="javascript:;">August (57)</a></li>
      </ul>
    </li>
    
    
  </ul>
</div>                
            </div>
        </div>
        
    </div>
</section>






<footer class="primary" data-img="url(../assets/images/footer-bg.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p class="head fc-secondary fw-bold">Landtrack contact</p>
                <ul class="unstyled first">
                    <li><a href="javascript:;"><span class="icon-map"></span>123, Main Road, Your City, NY 1234</a></li>
                    <li><a href="tel:03112296605"><span class="icon-mobile1"></span>+92 0311-2296605</a></li>
                    <li><a href="mailto:info@landtrack.com"><span class="icon-envelope"></span>info@landtrack.com</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <p class="head fc-secondary fw-bold">Company</p>
                <ul class="unstyled second">
                    <li><a href="../about-us">About Us</a></li>
                    <li><a href="../blog">Blog</a></li>
                    <li><a href="#">Expo</a></li>
                    <li><a href="../privacy-policy">Privacy Policy</a></li>
                    <li><a href="../terms-of-use">Terms Of Use</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <p class="head fc-secondary fw-bold">Connect</p>
                <ul class="unstyled second">
                    <!-- <li><a href="../#">News</a></li> -->
                    
                    <li><a href="../contact-us">Contact Us</a></li>
                    <li><a href="../careers">Work With Us</a></li>
                    <li><a href="../help-and-support">Help & Support</a></li>
                    <li><a href="../advertise">Advertise On Landtrack</a></li>
                    <li><a href="../ask-an-expert">Ask An Expert</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <p class="head fc-secondary fw-bold">Follow Us</p>
                <ul class="unstyled inline social">
                    <li><a href=""><span class="icon-facebook"></span></a></li>
                    <li><a href=""><span class="icon-google-plus1"></span></a></li>
                    <li><a href=""><span class="icon-twitter"></span></a></li>
                </ul>
                <div class="app-btns">
                    <a href="https://www.apple.com/lae/ios/app-store/"><img src="../assets/images/app-btn.png" alt="*"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.google.android.gms&hl=en"><img src="../assets/images/android-btn.png" alt="*"></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<footer class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <ul class="unstyled inline">
                    <li><a class="fc-white fs-small" href="../buy">Lahore Property</a></li>
                    <li><a class="fc-white fs-small" href="../buy">Karachi Property</a></li>
                    <li><a class="fc-white fs-small" href="../buy">Islamabad Property</a></li>
                </ul>
                <ul class="unstyled inline">
                    <li><a class="fc-white fs-small" href="../">Landtrack.com - Pakistan Property Portal</a></li>
                    <!-- <li><a class="fc-white fs-small" href="">Home Finance</a></li>  -->                
                </ul>
                <p class="fs-small ptpx-10">Copyright © <script>document.write(new Date().getFullYear())</script> LandTrack All Rights Reserved</p> 
            </div>
            <div class="offset-lg-6 col-lg-2 nopadd-left">
                <p class="pbpx-10">Official Home Partners</p>
                <img src="../assets/images/home-partner-1.jpg" alt="*">
            </div>
        </div>
        
    </div>
</footer>



<div class="overlay-bg"></div>
<div class="popup-form">
    <div class="inner">
        <h5 class="td-underline">Contact Details</h5>

        <div class="ta-left ptpx-20">
            <p class="fc-grey">Phone : &nbsp;&nbsp;<a class="fc-secondary ta-left td-underline" href="tel:123456789">123456789</a></p>
        </div>
        
        <a class="close-popup" href="javascript:;">X</a>
    </div>
</div>


<div class="popup-form-email">
    <div class="inner">
        <h5 class="td-underline">Email Details</h5>

        <div class="ta-left ptpx-20">
            <p class="fc-grey">Email : &nbsp;&nbsp;<a class="fc-secondary ta-left td-underline" href="mailto:info@landtrack.com">info@landtrack.com</a></p>
        </div>
        
        <a class="close-popup" href="javascript:;">X</a>
    </div>
</div>



@endsection