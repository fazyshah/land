@extends('layout.master')
@section('title', 'Advertise')

@section('content')




<section class="second-banner buy sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb ff-primary">
                  <li><a href="/">Home&nbsp;</a></li>
                  <li>&nbsp;Advertise</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="glance">
                    <p class="heading-main fs-large fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">land track at a Glance</p>
                    <ul class="unstyled inline grid-block --type-five-blocks ">
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="{{$imgscr}}glance/img1.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">Over 3.5 million monthly visits</p>
                               </div>
                           </div>
                       </li>
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="{{$imgscr}}glance/img2.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">More than 15 million monthly pageviews</p>
                               </div>
                           </div>
                       </li>
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="{{$imgscr}}glance/img3.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">Registered members exceeding 700,000</p>
                               </div>
                           </div>
                       </li> 
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="{{$imgscr}}glance/img4.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">More than 200,000 new properties <br>added every <br>month</p>
                               </div>
                           </div>
                       </li> 
                       <li class="item">
                           <div class="glance-box">
                               <figure>
                                   <img class="w-100" src="{{$imgscr}}glance/img5.jpg" alt="*">
                               </figure>
                               <div class="content">
                                   <p class="fw-semi-bold">Over 3.5 million monthly visits</p>
                               </div>
                           </div>
                       </li> 
                    </ul>
                </div>

                <div class="glance two mtpx-20">
                    <p class="heading-main fs-large fw-semi-bold d-small-2 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Why Advertise With land track?</p>
                    <p class="fs-13 ">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>

                    <p class="heading-main fs-large fw-semi-bold abou-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft mtpx-40">AGENCIES</p>
                    <p class="fs-13 ">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                    <p class="heading-main fs-large fw-semi-bold abou-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft  mtpx-40">DEVELOPERS</p>
                    <p class="fs-13 mbpx-40">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                    <a class="glance-btn" href="javascript:;"><span class="icon-icon-49"></span>0800-1234 (45678)</a>
                </div>
            </div>
            <div class="col-lg-3">
                
             </div>

            @include('layout./partials.side-advertisement-new')

        </div>

                
   
          </div>  </div>  </div>  </div>  </div>  </div>
            </div>  </div>  </div>  </div>  </div>
              </div>  </div>  </
                </div>  </div>  </div>
                 </div>  </div>  </div>
                </div>  </div>  </div>  </div>
                  </div>  </div>  </div>
           
          </div>  </div>  </div>  </div>  </div>  </div>
            </div>  </div>  </div>  </div>  </div>
              </div>  </div>  </
                </div>  </div>  </div>
                 </div>  </div>  </div>
                </div>  </div>  </div>  </div>
                  </div>  </div>  </div>       
 

@endsection