@extends('layout.master')
@section('title', 'aution')

@section('content')

<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner " style="background: url({{$imgscr}}banner-1.jpg);">
    
</section>
</div>
<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 hidden-md-down">
                    
                </div>
                <div class="col-lg-8  col-md-10 col-sm-12">
                    
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="fs-medium fc-white fw-medium">FIND YOUR NEXT INVESTMENT HOME NOW</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            {{ Form::open(['class' => 'form-1', 'route' => 'search','method' => 'get']) }}

                                            <div class="control-group w-20 d-inline-block">
                                                {{ Form::select('city', $cities, old('city'), ['placeholder' => 'Select City', 'required']) }}

                                                {{-- <select name="" id="">
                                                    <option value="">Select City</option>
                                                    <option value="">asdasda</option>
                                                </select> --}}
                                            </div>
                                            <div class="control-group w-62  d-inline-block mar">
                                                <input name="keyword" type="text" placeholder="Type Location, area or keyword">
                                            </div>
                                            <div class="control-group  w-17  d-inline-block ">
                                                <button class="btn-submit" type="submit" value="">SEARCH</button>

                                            </div>
                                            <div class="filter-btn">
                                                <a class=" fs-small fw-semi-bold fc-black tt-uppercase" href="http://onlinekidstoy.com/landtrack.com/#"><span class="icon-plus-circle "></span>Add Filter</a>
                                            </div>

                                            {{ Form::close() }}

                                        </div>
                                        <div  class="tabs-two hidden">
                                            4565454654 asdasd a456s4d6as4d56
                                        </div>
                                        <div  class="tabs-three hidden">
                                            ashdajsdh adsjkask absdb
                                        </div>
                                        <div  class="tabs-four hidden">
                                            zeeshan
                                        </div>
                                    </div>

                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>
</div>



<section class=" sec-padding bg-cream" >
    <div class="container">
        <h4 class="fs-28 fc-secondary ta-center fw-semi-bold lh-xmedium ls-medium">The Nation's Leading Online <span class="d-block fc-primary">Real Estate Marketplace</span></h4>
        <p class="w-45 m-auto ta-center fs-17 fc-grey ptpx-20">We are focused exclusively on the sale of residential bank-owned and foreclosure properties.</p>
        <div class="border-dashed"></div>

        <h6 class="fw-semi-bold fc-nblack pbpx-15 ptpx-25">RECOMMENDED FOR YOU</h6>
        <div class="row auction-slider">
                <div class="col-md-4">
                        <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                            <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                            <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                            </a>
                        </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-2.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-3.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
        </div>

        <h6 class="fw-semi-bold fc-nblack pbpx-15 ptpx-35">NEWLY ADDED FORECLOSURE PROPERTIES <a class="btn-orange small float-right" href="javascript:;">View All</a></h6>
        <div class="row auction-slider">
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
        </div>

        <h6 class="fw-semi-bold fc-nblack pbpx-15 ptpx-35">NEWLY ADDED BANK OWNED PROPERTIES <a class="btn-orange small float-right" href="javascript:;">View All</a></h6>
        <div class="row auction-slider">
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="auction-box" style="background: url({{$imgscr}}auction/img-1.jpg);">
                        <a href=""><img class="float-right" src="{{$imgscr}}star.png" alt="*"></a>
                        <a href="../auction-page-detail">
                            <div class="text">
                                <p class="fs-13 fc-white">Starting Bid: TBD</p>
                                <p class="fs-13 fc-white">Live Auction  |  Nov 07, 10:00am</p>
                                <p class="fs-13 fc-white">1960 CEYLON ST, AURORA, CO, 80011</p>
                            </div>
                        </a>
                       </div>
            </div>
        </div>
 </div>
</div>



@endsection
