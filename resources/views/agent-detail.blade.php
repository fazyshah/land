@extends('layout.master')
@section('title', 'agent-detail')

@section('content')





<section class="second-banner buy sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <ul class="breadcrumb mbpx-5">
          <li><a href="#">Home&nbsp;</a></li>
          <li><a href="#">&nbsp;Gujar Khan&nbsp;</a></li>
          <li>&nbsp;Saroya Real Estate</li>
        </ul>
        
        <div class="row legal-detail one">
            <div class="col-lg-6 col-md-6 ">
              <p class="fs-17 fw-semi-bold fc-nblack ptpx-18">The Property Magnates (TPM) <span class="fs-13 fc-grey fw-medium ml-3">Lahore, Pakistan</span></p>
              
            </div>
            <div class="col-lg-6 col-md-6 ta-right">
              <ul class="unstyled inline main">
                <li class="ttc">
                  <a href="javascript:;"><p class="fs-13 fc-secondary fw-semi-bold"><span class="icon-star fc-yellow mr-2 fs-large lh-normal"></span> Titanium Agency</p></a> 
                </li>
                <li class="ttc"><a href="javascript:;"><p class="fb"><span class="icon-bookmark1"></span> &nbsp;Save to Facebook</p></a></li>
                <li class="ttc">
                    <ul class="unstyled inline ">
                        <li><a href=""><span class="icon-external-link"></span></a></li>
                        <li><a href=""><span class="icon-heart1"></span></a></li>
                        <li><a href=""><span class="icon-printer-text"></span></a></li>
                    </ul>
                </li>
              </ul>
            </div>
        </div>
        <div class="row">
          <div class="smaller-agent-banner" style="background: url({{$imgscr}}agent-banner/banner1.png);">
            <div class="col-md-8  offset-md-2 pro d-table">
              <div class="d-table-cell  va-middle">
                  <h3 class="fs-30 fc-white">The Property Magnates (TPM)</h3>
                  <h6 class="fc-white ptpx-10">Lahore, Pakistan</h6>  
              </div>
              
            </div>
            
          </div>
        </div>
        <div class="row legal-detail two ">
            <div class="col-lg-9 col-md-8">
              <div class="left">
                <h6 class="heading-main fw-semi-bold small-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft prpx-0">PROPERTY DETAILS</h6>


                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <table class="prop-table ag-d w-100 mbpx-10 mtpx-10">
                        <tr>
                            <td class="w-50"><span class="icon-icon-67"></span>Properties </td>
                            <td class="w-50"><b>18</b> </td>
                        </tr>
                        <tr>
                            <td class="w-50"><span class="icon-icon-68"></span>Service Areas</td>
                            <td class="w-50"><b>Abbottabad</b></td>
                        </tr>
                    </table>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <table class="prop-table ag-d w-100 mbpx-10 mtpx-10">
                        <tr>
                            <td class="w-50"><span class="icon-icon-69"></span>Property Type: </td>
                            <td class="w-50"><b>Houses</b></td>
                        </tr>
                        <tr>
                            <td class="w-50"><span class="icon-icon-70"></span>Properties for</td>
                            <td class="w-50"><b>Sale</b> </td>
                        </tr>
                    </table>
                  </div>
                  <div class="col-md-12 ptpx-40">
                      <h6 class="heading-main fw-semi-bold abou lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft prpx-0">Description</h6>
                      <p class="fs-13 fc-nblack fw-medium lh-large pbpx-20">Buying and selling real estate signifies a time of change. Whether making an investment for capital gains or for personal or business use, the process can be challenging for any consumer. For people who have never set foot in dealings, investments and buying or selling property and are unsure of what to expect in the market it can be a daunting task , sometimes even the more experienced people fail to make their investment count because they are unaware of the shifting landscape and modern requirements. This is where our Estate Agency can make a difference and provide you the most beneficial and profitable deal. Trust, honesty and transparency are pillars of our dealings with the clients and are well versed in all kind of real estate needs. Please contact us with your real estate requirements and let us take care of everything else.</p>

                      <h6 class="heading-main fw-semi-bold abou lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft prpx-0 mtpx-40">Also Serving in</h6>
                        <div class="col-md-6 col-sm-12 nopadd-left">
                          <table class="prop-table ag-d w-100 mbpx-10 mtpx-10">
                              <tr>
                                  <td class="w-50"><span class="icon-icon-67"></span>Properties </td>
                                  <td class="w-50"><b>18</b> </td>
                              </tr>
                              <tr>
                                  <td class="w-50"><span class="icon-icon-68"></span>Service Areas</td>
                                  <td class="w-50"><b>Abbottabad</b></td>
                              </tr>
                          </table>
                        </div>
                        <div class="col-md-6 col-sm-12  nopadd-right">
                          <table class="prop-table ag-d w-100 mbpx-10 mtpx-10">
                              <tr>
                                  <td class="w-50"><span class="icon-icon-69"></span>Property Type: </td>
                                  <td class="w-50"><b>Houses</b></td>
                              </tr>
                              <tr>
                                  <td class="w-50"><span class="icon-icon-70"></span>Properties for</td>
                                  <td class="w-50"><b>Sale</b> </td>
                              </tr>
                          </table>
                        </div>
                  </div>
                  <div class="col-md-4">
                    <a class="btn-detail w-100 ptpx-8 pbpx-8 fs-13 mtpx-20" href="../buy">View All property for Sale</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 border-l pbpx-30   col-md-4">
              <div class="right-contact">
                  <div class="head">
                      <p class="fs-medium ta-center fc-white fw-medium ff-primary">Contact for More information</p>
                  </div>
                  <div class="bottom">
                      <form action="" class="jform validate right-form ">
                          <div class="control-group">
                              
                              <input type="text" placeholder="Name" name="cn" class="required" minlength="2" maxlength="60">
                          </div>
                          <div class="control-group">
                              
                              <input type="text" name="em" class="required email" placeholder="Email" maxlength="60">
                          </div>
                          <div class="control-group clearfix">                                            
                                  <input type="text" name="pn" id="phone" class="number required" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                             
                          </div>
                          <div class="control-group">
                              <textarea name="msg" class="required" placeholder="Message" minlength="2" maxlength="1000"></textarea>
                          </div> 
                          <div class="control-group">
                              
                              <input type="submit" value="Call" class="fw-normal tt-uppercase w-49 btn-detail orange call-form">
                              <input type="submit" value="Send Email" class="fw-normal tt-uppercase w-49 btn-detail blue">
                          </div>
                          <div class="control-group last">
                              <div class="input">
                                  <input type="checkbox" id="informed">
                              </div>
                              <div class="lbl">
                                  <label class="fs-xsmall fw-light " for="informed">Keep me Informed about similar properties By Submitting this Form.I agree to <a class="fc-secondary fw-semi-bold" href="">Terms Of Use</a></label>
                              </div>
                              
                          </div>                        
                      </form>
                  </div>
              </div>
              <div class="estate-links mtpx-20">
                    <p class="usef fw-bold tt-uppercase fc-primary fs-large">Agent Information</p>
                    <ul class="unstyled inline grid-block --type-two-blocks ">
                        <li>
                            <img class="ptpx-15" src="{{$imgscr}}agents-logo/saroya.jpg" alt="*">
                        </li>
                        <li class="brdr-left">
                            <ul class="unstyled insider">
                                <li><a href="javascript:;"><span class="icon-person"><sup class="icon-dot-single"></sup></span>Shaikh Asif</a></li>
                                <li><a href="javascript:;"><span class="icon-phone_in_talk"></span>0210602168</a></li>
                                <li><a href="javascript:;"><span class="icon-messages"></span>Chat Now</a></li>
                                <li><a class="btn-detail green" href="javascript:;">View Profile</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row mtpx-40">
          <h6 class="heading-main fw-semi-bold main-2 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft"><span class="fc-lgrey">all properties listed by</span> The Property Magnates</h6>
          
          <div class="shared-slider-four-block ptpx-10">
              <div class="item matchheight">
                  <div class="prop-new-box">
                      <div class="top" style="background: url('{{$imgscr}}property/prop1.jpg');">
                          <div class="ico">
                              <a class="btn-s" href="javascript:;">Featured</a>
                              <a class="btn-s orange" href="javascript:;">For Rent</a>
                              <ul class="unstyled inline">
                                  <li><a href="javascript:;"><img src="{{$imgscr}}hot.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}star.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}verified.png" alt=""></a></li>
                              </ul>
                          </div>
                          <div class="profile">
                              <div class="left">
                                  <img src="{{$imgscr}}user/img-1.png" alt="*">
                                  <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                              </div>
                              <div class="float-right ta-center">
                                  <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                  <p class="fc-white fs-small">2 month</p>
                              </div>
                          </div>
                      </div>
                      
                      <div class="content cord">
                          <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                          <p class="fc-primary fw-medium fs-small fs-italic lh-large"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                          <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                          <img class="con-img" src="{{$imgscr}}agents-logo/saroya.jpg" alt="*">
                      </div>
                      
                      <div class="bottom">
                          <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                              <li>800 Sqft</li>
                              <li>2 Beds</li>
                              <li>2 Baths</li>
                          </ul>
                      </div>
                      <div class="butons bord grid-block --type-two-blocks">
                          <a class="btn-orange fs-13 w-100  call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                          <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                      </div>
                  </div>
              </div>
              <div class="item matchheight">
                  <div class="prop-new-box">
                      <div class="top" style="background: url('{{$imgscr}}property/prop1.jpg');">
                          <div class="ico">
                              <a class="btn-s" href="javascript:;">Featured</a>
                              <a class="btn-s orange" href="javascript:;">For Rent</a>
                              <ul class="unstyled inline">
                                  <li><a href="javascript:;"><img src="{{$imgscr}}hot.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}star.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}verified.png" alt=""></a></li>
                              </ul>
                          </div>
                          <div class="profile">
                              <div class="left">
                                  <img src="{{$imgscr}}user/img-1.png" alt="*">
                                  <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                              </div>
                              <div class="float-right ta-center">
                                  <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                  <p class="fc-white fs-small">2 month</p>
                              </div>
                          </div>
                      </div>
                      <div class="content cord">
                          <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                          <p class="fc-primary fw-medium fs-small fs-italic lh-large"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                          <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                          <img class="con-img" src="{{$imgscr}}agents-logo/saroya.jpg" alt="*">
                      </div>
                      <div class="bottom">
                          <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                              <li>800 Sqft</li>
                              <li>2 Beds</li>
                              <li>2 Baths</li>
                          </ul>
                      </div>
                      <div class="butons bord grid-block --type-two-blocks">
                          <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                          <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                      </div>
                  </div>
              </div>
              <div class="item matchheight">
                  <div class="prop-new-box">
                      <div class="top" style="background: url('{{$imgscr}}property/prop1.jpg');">
                          <div class="ico">
                              <a class="btn-s" href="javascript:;">Featured</a>
                              <a class="btn-s orange" href="javascript:;">For Rent</a>
                              <ul class="unstyled inline">
                                  <li><a href="javascript:;"><img src="{{$imgscr}}hot.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}star.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}verified.png" alt=""></a></li>
                              </ul>
                          </div>
                          <div class="profile">
                              <div class="left">
                                  <img src="{{$imgscr}}user/img-1.png" alt="*">
                                  <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                              </div>
                              <div class="float-right ta-center">
                                  <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                  <p class="fc-white fs-small">2 month</p>
                              </div>
                          </div>
                      </div>
                      <div class="content cord">
                          <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                          <p class="fc-primary fw-medium fs-small fs-italic lh-large"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                          <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                          <img class="con-img" src="{{$imgscr}}agents-logo/saroya.jpg" alt="*">
                      </div>
                      <div class="bottom">
                          <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                              <li>800 Sqft</li>
                              <li>2 Beds</li>
                              <li>2 Baths</li>
                          </ul>
                      </div>
                      <div class="butons bord grid-block --type-two-blocks">
                          <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                          <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                      </div>
                  </div>
              </div>
              <div class="item matchheight">
                  <div class="prop-new-box">
                      <div class="top" style="background: url('{{$imgscr}}property/prop1.jpg');">
                          <div class="ico">
                              <a class="btn-s" href="javascript:;">Featured</a>
                              <a class="btn-s orange" href="javascript:;">For Rent</a>
                              <ul class="unstyled inline">
                                  <li><a href="javascript:;"><img src="{{$imgscr}}hot.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}star.png" alt=""></a></li>
                                  <li><a href="javascript:;"><img src="{{$imgscr}}verified.png" alt=""></a></li>
                              </ul>
                          </div>
                          <div class="profile">
                              <div class="left">
                                  <img src="{{$imgscr}}user/img-1.png" alt="*">
                                  <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                              </div>
                              <div class="float-right ta-center">
                                  <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                  <p class="fc-white fs-small">2 month</p>
                              </div>
                          </div>
                      </div>
                      <div class="content cord">
                          <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                          <p class="fc-primary fw-medium fs-small fs-italic lh-large"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                          <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                          <img class="con-img" src="{{$imgscr}}agents-logo/saroya.jpg" alt="*">
                      </div>
                      <div class="bottom">
                          <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                              <li>800 Sqft</li>
                              <li>2 Beds</li>
                              <li>2 Baths</li>
                          </ul>
                      </div>
                      <div class="butons bord grid-block --type-two-blocks">
                          <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                          <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                      </div>
                  </div>
              </div>
              
          </div>
  
            </div>
              </div>
              
          </div>
                  </div>
              </div>
              
          </div>

@endsection