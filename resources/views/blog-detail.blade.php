@extends('layout.master')
@section('title', 'auction-page')

@section('content')






<section class=" sec-padding --small bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-10 mbpx-5">
          <li><a href="/">Home </a></li>
          <li> Blog Detail Page</li>
        </ul>

        <div class="row">
          <div class="col-lg-9">
            <div class="row blog-bbox-detail">
                <div class="col-md-12">
                    <div class="date-tag">
                       <p class="fc-white fs-small lh-nmedium">Sep<span class="d-block">17</span></p>
                    </div>
                  <figure>
                    <img src="main-blog/big-1.jpg" alt="*">
                  </figure>
                  <div class="text-side">
                    
                    <h6 class=" fc-secondary fw-semi-bold">How to Define a Living Room with Dining Area</h6>
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="ptpx-12 fs-13 fc-grey lh-large">A large number of interior designer major emphasis is on designing living room of the house in the most appeasing of manners as it is the core of one’s home. In most of the cases, every single room of the house is interconnected with the living room in a direct or indirect way. Being the most prominent and the most extensively used room, one need to give extra significance to its design and decoration. In truth, there aren’t any bounding or restrictions associated with designing one’s room and it all comes down to the individual’s sense of taste at the end. There is no such thing as overdoing or underdoing a living room, as whatever appeases the eye of the homeowner will definitely appeal others too. Although, each and every unique individual have their own sense of taste and perspective and quite often the designing ideas of two individuals belonging from the same background differs a great deal. In such cases, the moderation is the order of the day and the expert at Aenzay Interiors are well equipped with knowledge and skills to understand the designing needs and wants of the client in the best way.</p>
                    
                    
                  </div>
                  <figure class="ptpx-30">
                    <img src="main-blog/big-1.jpg" alt="*">
                  </figure>
                  <div class="text-side">
                    <p class="ptpx-5 fs-13 fc-grey lh-large">A large number of interior designer major emphasis is on designing living room of the house in the most appeasing of manners as it is the core of one’s home. In most of the cases, every single room of the house is interconnected with the living room in a direct or indirect way. Being the most prominent and the most extensively used room, one need to give extra significance to its design and decoration. In truth, there aren’t any bounding or restrictions associated with designing one’s room and it all comes down to the individual’s sense of taste at the end. There is no such thing as overdoing or underdoing a living room, as whatever appeases the eye of the homeowner will definitely appeal others too. Although, each and every unique individual have their own sense of taste and perspective and quite often the designing ideas of two individuals belonging from the same background differs a great deal. In such cases, the moderation is the order of the day and the expert at Aenzay Interiors are well equipped with knowledge and skills to understand the designing needs and wants of the client in the best way.</p>
                  </div>

                  <figure class="ptpx-30">
                    <img src="main-blog/big-1.jpg" alt="*">
                  </figure>
                  <div class="text-side pbpx-20">
                    <p class="ptpx-5 fs-13 fc-grey lh-large">A large number of interior designer major emphasis is on designing living room of the house in the most appeasing of manners as it is the core of one’s home. In most of the cases, every single room of the house is interconnected with the living room in a direct or indirect way. Being the most prominent and the most extensively used room, one need to give extra significance to its design and decoration. In truth, there aren’t any bounding or restrictions associated with designing one’s room and it all comes down to the individual’s sense of taste at the end. There is no such thing as overdoing or underdoing a living room, as whatever appeases the eye of the homeowner will definitely appeal others too. Although, each and every unique individual have their own sense of taste and perspective and quite often the designing ideas of two individuals belonging from the same background differs a great deal. In such cases, the moderation is the order of the day and the expert at Aenzay Interiors are well equipped with knowledge and skills to understand the designing needs and wants of the client in the best way.</p>
                  </div>
                </div>
                
            </div>
            <div class="row social-buttons">
              <div class="col-md-12">
                <ul class="unstyled inline">
                  <li><a class="button-soc facebook" href="">Facebook</a></li>
                  <li><a class="button-soc twitter" href="">Twitter</a></li>
                  <li><a class="button-soc print" href="">Print</a></li>
                  <li><a class="button-soc email" href="">Email</a></li>
                  <li><a class="button-soc more" href="">More</a></li>

                  
                </ul>
              </div>
            </div>
            
          </div>

          <div class="col-lg-3 mbpx-20">
           
          </div>
          <div >
           
          </div>
          

        </div>

        <div class="row ptpx-25">
          <div class="col-lg-12">
            <h6 class="heading-main  fw-semi-bold abou-3  ff-primary lh-normal  fc-nblack tt-uppercase wow slideInLeft">Related Blogs</h6>
            <div class="row">
              <div class="col-md-4">
                
                <div class="related-blogs-box">
                  <div class="date-tag">
                       <p class="fc-white fs-medium lh-nmedium">Sep 17</p>
                    </div>
                  <figure>
                    <img src="related-blogs/img-1.jpg" alt="*">
                    
                  </figure>
                  <div class="text-side">
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="fc-lgrey lh-medium ptpx-7">Canyon Views Islamabad offers a new style of homes with breathtaking views,</p>
                    
                  </div>
                  <div class="butons ">
                        <a class="btn-blue fs-13 w-100" href="../blog-detail">Read More <span class="ml-2 icon-keyboard_arrow_right h6"></span></a>
                    </div>
                </div>
              </div>
              <div class="col-md-4">
                
                <div class="related-blogs-box">
                  <div class="date-tag">
                       <p class="fc-white fs-medium lh-nmedium">Nov 11</p>
                    </div>
                  <figure>
                    <img src="related-blogs/img-1.jpg" alt="*">
                    
                  </figure>
                  <div class="text-side">
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="fc-lgrey lh-medium ptpx-7">Canyon Views Islamabad offers a new style of homes with breathtaking views,</p>
                    
                  </div>
                  <div class="butons ">
                        <a class="btn-blue fs-13 w-100" href="../blog-detail">Read More <span class="ml-2 icon-keyboard_arrow_right h6"></span></a>
                    </div>
                </div>
              </div>
              <div class="col-md-4">
                
                <div class="related-blogs-box">
                  <div class="date-tag">
                       <p class="fc-white fs-medium lh-nmedium">13 Oct</p>
                    </div>
                  <figure>
                    <img src="related-blogs/img-1.jpg" alt="*">
                    
                  </figure>
                  <div class="text-side">
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="fc-lgrey lh-medium ptpx-7">Canyon Views Islamabad offers a new style of homes with breathtaking views,</p>
                    
                  </div>
                  <div class="butons ">
                        <a class="btn-blue fs-13 w-100" href="../blog-detail">Read More <span class="ml-2 icon-keyboard_arrow_right h6"></span></a>
                    </div>
                </div>
              </div>
            </div>
          </div>
           </div>
        </div>


   </div>
</div>



@endsection