@extends('layout.master')
@section('title', 'agent-directory')

@section('content')




<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>

</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class=" fc-white fw-medium">Property for Sale in Pakistan</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            {{ Form::open(['class' => 'form-1', 'route' => 'search']) }}

                                            <div class="control-group w-20 d-inline-block">
                                                {{ Form::select('city', $cities, old('city'), ['placeholder' => 'Select City', 'required']) }}

                                                {{-- <select name="" id="">
                                                    <option value="">Select City</option>
                                                    <option value="">asdasda</option>
                                                </select> --}}
                                            </div>
                                            <div class="control-group w-62  d-inline-block mar">
                                                <input name="keyword" type="text" placeholder="Type Location, area or keyword">
                                            </div>
                                            <div class="control-group  w-17  d-inline-block ">
                                                <button class="btn-submit" type="submit" value="">SEARCH</button>

                                            </div>
                                            <div class="filter-btn">
                                                <a class=" fs-small fw-semi-bold fc-black tt-uppercase" href="http://onlinekidstoy.com/landtrack.com/#"><span class="icon-plus-circle "></span>Add Filter</a>
                                            </div>

                                            {{ Form::close() }}

                                        </div>
                                        <div  class="tabs-two hidden">
                                            4565454654 asdasd a456s4d6as4d56
                                        </div>
                                        <div  class="tabs-three hidden">
                                            ashdajsdh adsjkask absdb
                                        </div>
                                        <div  class="tabs-four hidden">
                                            zeeshan
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class="premium-agencies sec-padding --small bg-cream" >
    <div class="container">
        <h4 class=" fc-secondary ta-center mbpx-20">Premium Agencies</h4>
        <div class="pre-box">
            <div class="premium-agency-slider">
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-1.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-2.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-3.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-4.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-5.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-6.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-7.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-8.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-9.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-10.jpg" alt="*"></a>
                    </figure>
                </div>
            </div>

            <p class="fs-large fc-lgrey ta-center">Gwadar Iqra Associates(Karachi)</p>
            <p class="fs-medium fc-lgrey ta-center">209 Properties For Sale</p>
        </div>

        <h4 class=" fc-secondary ta-center mbpx-20 mtpx-30">Featured Agencies</h4>
        <div class="pre-box two">
            <div class="premium-agency-slider two">
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-1.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-2.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-3.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-4.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-5.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-6.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-7.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-8.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-9.jpg" alt="*"></a>
                    </figure>
                </div>
                <div class="item">
                    <figure>
                        <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-10.jpg" alt="*"></a>
                    </figure>
                </div>
            </div>
        </div>
        
        <h4 class=" fc-secondary ta-center mbpx-20 mtpx-30">Browse Estate Agents by City</h4>
        <div class="pre-box three">
            <form class="form-1" action="">
                <div class="control-group w-20 d-inline-block">
                    <select name="" id="">
                        <option value="">Select City</option>
                        <option value="">asdasda</option>
                    </select>
                </div>
                <div class="control-group w-62  d-inline-block mar">
                    <input type="text" placeholder="Type Location, area or keyword">
                </div>
                <div class="control-group  w-17  d-inline-block ">
                    <button class="btn-submit" type="submit" value="">SEARCH</button>
                </div>
            </form>

            <p class="fs-small fc-grey ptpx-5 pbpx-30">Showing <span class="fw-semi-bold fc-nblack">20</span> of <span class="fw-semi-bold fc-nblack">38</span> Agencies in Karachi</p>

            <p class="heading-main fs-large fw-semi-bold small-2 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Premium agencies in karachi</p>

            <ul class="paik unstyled inline mtpx-30">
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-9.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-10.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-11.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-12.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-13.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-14.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-15.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-16.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-17.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-18.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-19.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-20.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-21.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-22.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-23.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-24.jpg" alt="*"></a>
                </li>
            </ul>
            
            <div class="mtpx-40">
            <p class="heading-main fs-large fw-semi-bold small-2 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">featured agencies in karachi</p>

            <ul class="paik unstyled inline mtpx-30">
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-9.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-10.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-11.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-12.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-13.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-14.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-15.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-16.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-17.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-18.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-19.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-20.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-21.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-22.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-23.jpg" alt="*"></a>
                </li>
                <li>
                    <a href="../agent-detail"><img  src="{{$imgscr}}agencies/premium/agency-24.jpg" alt="*"></a>
                </li>
            </ul>
            </div>
        </div>

        
    </div>

        
    
            </div>
        </div>

  </div>
</div>



@endsection
