@extends('layout.master')
@section('title', 'lt-store')

@section('content')




<div class="bg-cream ptpx-20 pbpx-20">
    
    <section class="e-com">
        <div class="container">
            <div class="row">
                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                    <form class="form-1 " action="">
                        <div class="control-group w-26 d-inline-block">
                            <select name="" id="category">
                                <option value="">Select Category</option>
                                <option value="">asdasda</option>
                            </select>
                        </div>
                        <div class="control-group w-61  d-inline-block mar">
                            <select class="two" name="" id="product">
                                <option value="">Search Product</option>
                                <option value="">asdasda</option>
                            </select>
                        </div>
                        <div class="control-group  w-12  d-inline-block">
                            <button class="btn-submit" type="submit" value="">SEARCH</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4 nopadd-left">
                    <div class="cart-btn">
                        <a class="btn-cart" href="javascript:;"><span class="icon-cart cart-1"></span>10 <span class="icon-keyboard_arrow_down cart-2"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ecom-main bg-cover bg-center mtpx-10" style="background: url({{$imgscr}}ecommerce/banner.png);background-repeat:no-repeat;">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 nopadd-right">
                    <div class="advertise-sect blue   bg-white">
                      <div class="head">
                        <p class="  tt-uppercase"><span class="icon-category"></span>Category</p>
                      </div>
                      <ul class="unstyled ">
                        <li class=""><a href="javascript:;">Design and Planning</a> </li> 
                        <li class=""><a href="javascript:;">Roof, Foundation</a> </li> 
                        <li class=""><a  href="javascript:;">Siding, Windows</a> </li>
                        <li class=""><a  href="javascript:;">Demolition</a> </li>
                        <li class=""><a  href="javascript:;">Insulation</a> </li>
                        <li class=""><a  href="javascript:;">Fine Carpentry</a> </li>
                        <li class=""><a  href="javascript:;">Interior Painting</a> </li>
                        <li class=""><a  href="javascript:;">Flooring</a> </li>
                        <li class=""><a  href="javascript:;">Major Auxiliary Building</a> </li>
                        <li class=""><a  href="javascript:;"></a> </li>

                      </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6  col-sm-8  col-nopadd">
                    <div class="ecom-slider">
                        <div class="item">
                            <img src="{{$imgscr}}ecommerce/slide-1.jpg" alt="*">
                        </div>
                        <div class="item">
                            <img src="{{$imgscr}}ecommerce/slide-2.jpg" alt="*">
                        </div>
                        <div class="item">
                            <img src="{{$imgscr}}ecommerce/slide-3.jpg" alt="*">
                        </div>
                        <div class="item">
                            <img src="{{$imgscr}}ecommerce/slide-4.jpg" alt="*">
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-3 nopadd-left hidden-md-down">
                    <div class="advertise-sect fpro bg-white">
                      <div class="head">
                        <p class="fc-secondary fw-semi-bold tt-uppercase"><span class="fc-primary icon-icon-5"></span>Featured Products</p>
                      </div>
                      <ul class="unstyled fpro-detail">
                          <li class="fp-box">
                              <figure>
                                  <img src="{{$imgscr}}ecommerce/box-1.jpg" alt="*">
                              </figure>
                              <div class="content">
                                  <p class="fs-13 fc-grey fw-semi-bold">Product Name</p>
                                  <p class="fs-13 fc-nblack fw-semi-bold">30,000 <span class="fs-xsmall fw-medium">PKR</span></p>
                                  <img src="{{$imgscr}}rating.png" alt="*">
                                  <a class="fc-primary td-underline" href="javascript:;">Add To Cart</a>
                              </div>
                          </li>
                          <li class="fp-box">
                              <figure>
                                  <img src="{{$imgscr}}ecommerce/box-2.jpg" alt="*">
                              </figure>
                              <div class="content">
                                  <p class="fs-13 fc-grey fw-semi-bold">Product Name</p>
                                  <p class="fs-13 fc-nblack fw-semi-bold">30,000 <span class="fs-xsmall fw-medium">PKR</span></p>
                                  <img src="{{$imgscr}}rating.png" alt="*">
                                  <a class="fc-primary td-underline" href="javascript:;">Add To Cart</a>
                              </div>
                          </li>
                          <li class="fp-box">
                              <figure>
                                  <img src="{{$imgscr}}ecommerce/box-3.jpg" alt="*">
                              </figure>
                              <div class="content">
                                  <p class="fs-13 fc-grey fw-semi-bold">Product Name</p>
                                  <p class="fs-13 fc-nblack fw-semi-bold">30,000 <span class="fs-xsmall fw-medium">PKR</span></p>
                                  <img src="{{$imgscr}}rating.png" alt="*">
                                  <a class="fc-primary td-underline" href="javascript:;">Add To Cart</a>
                              </div>
                          </li>
                          <li class="fp-box">
                              <figure>
                                  <img src="{{$imgscr}}ecommerce/box-4.jpg" alt="*">
                              </figure>
                              <div class="content">
                                  <p class="fs-13 fc-grey fw-semi-bold">Product Name</p>
                                  <p class="fs-13 fc-nblack fw-semi-bold">30,000 <span class="fs-xsmall fw-medium">PKR</span></p>
                                  <img src="{{$imgscr}}rating.png" alt="*">
                                  <a class="fc-primary td-underline" href="javascript:;">Add To Cart</a>
                              </div>
                          </li>
                          
                      </ul>
                      
                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <section class="sec-padding new-deals pbpx-10">
        <div class="container">
            <h4 class="ta-center fc-secondary ">Flash Deals</h4>
            <div class="grid-block --type-four-blocks --style-offsets ptpx-25 pbpx-10 --grid-ipad-full flash-slider" >
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-1.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-2.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-3.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-4.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
            </div>


            <h4 class="ta-center fc-secondary ptpx-40">Featured Brands</h4>
            <div class="grid-block --type-four-blocks --style-offsets ptpx-25 pbpx-10 --grid-ipad-full flash-slider" >
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-1.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-2.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-3.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-4.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
            </div>



            <h4 class="ta-center fc-secondary ptpx-40">Just For You</h4>
            <div class="grid-block --type-four-blocks --style-offsets --style-bottom-offsets --bottom-offsets-small ptpx-25 pbpx-10 --grid-ipad-full flash-slider" >
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-1.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-2.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-3.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-4.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-1.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-2.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-3.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-4.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
            </div>
 

  



@endsection