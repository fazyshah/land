
@extends('layout.master')
@section('title', 'partner-profile')

@section('content')



<div class="super-banner">
<div class="main-slider">
<section class="hero-banner small" style="background: url(banner-1.jpg);">
    
    
</section>

</div>




</div>



<section class="second-banner buy sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <ul class="breadcrumb mbpx-0">
          <li><a href="#">Home</a></li>
          <li><a href="#">Partner</a></li>
          <li>Profile</li>
        </ul>
        <div class="row bg-white detail-shared">
            <div class="col-md-12 col-nopadd">
                <div class="first">
                    <h6 class="fs-medium fw-semi-bold tt-uppercase fc-secondary d-inline-block"> Profilez</h6>
                    <ul class="unstyled inline ">
                        <!-- <li><a href=""><span class="icon-external-link"></span></a></li> -->
                        <li><a href=""><span class="icon-heart1"></span> &nbsp; Save Partner</a></li>
                        <!-- <li><a href=""><span class="icon-printer-text"></span></a></li> -->
                    </ul>
                </div>
                <div class="second-part pbpx-15 ptpx-15">
                    <div class="row">
                        
                        <div class="col-md-9 matchheight">
                            
                            <div class="row ">
                                <div class="tip">
                                    <div class="col-md-10 ">
                                        <div class="big-slider-2 ">
                                            
                                            <div>
                                                <img src="profile/slide1.jpg" alt="*">
                                            </div>
                                            <div>
                                                <img src="profile/slide1.jpg" alt="*">
                                            </div>
                                            <div>
                                                <img src="profile/slide3.jpg" alt="*">
                                            </div>
                                            <div>
                                                <img src="profile/slide4.jpg" alt="*">
                                            </div>
                                            <div>
                                                <img src="profile/slide5.jpg" alt="*">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-nopadd ptpx-20">
                                        <div class="small-slider-2  ptpx-5">
                                            <div>
                                                <img src="profile/slide1.jpg" alt="*">
                                            </div>
                                            <div>
                                                <img src="profile/slide1.jpg" alt="*">
                                            </div>
                                            <div>
                                                <img src="rofile/slide3.jpg" alt="*">
                                            </div>
                                            <div>
                                                <img src="profile/slide4.jpg" alt="*">
                                            </div>
                                            <div class="">
                                                <img src="profile/slide5.jpg" alt="*">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                       <div class="col-lg-3 col-md-4 col-sm-6 matchheight nopadd-right">
                            <div class="profilez-box">
                                <div class="head">
                                    <p class="fs-17 fc-primary fw-semi-bold tt-uppercase"><span class="icon-icon-57"></span>Profilez</p>
                                </div>
                                <figure class="bix">
                                    <img class="m-auto" src="profile/logo1.jpg" alt="*">
                                </figure>
                                <p class="fc-lgrey bottom"><span class="icon-icon-55 fc-primary"></span>31-Sarwar Road Commercial <br>Avenue Cantt, Lahore</p>
                                <div class="butns">
                                    <p class="fs-11 ta-center fc-black">Please quote landtrack.com when calling us</p>
                                    <div class="ta-center mtpx-10">
                                        <a class="btn-detail one mr-2" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                        <a class="btn-detail two" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            
        </div>
      
        <div class="row ptpx-20">
               
            <div class="col-lg-9 col-sm-12 nopadd-left">
               <div class="roomate-desc ptpx-30 pbpx-30 bg-white ">
                   <h6 class="heading-main fw-semi-bold abou-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Overview</h6>
                   <div class="content ptpx-10">
                         <p class="fs-13">PROFILEZ is a pioneer in introducing UPVC Windows and door system in Pakistan. Local fabrication facility with automated machinery puts us in unique positionn of customizing the product to match the varying needs of the market. All the profiles and accessories are imported. We have the capacity to produce over hundred windows per day.<br><br>PROFILEZ has been providing custom made products for housing and commercial projects in Pakistan for the past 20 years. We have been fabricating and providing windows and doors with other glass solutions.<br><br>PROFILEZ UPVC windows and doors are designed to withstand the harshest of weather conditions prevailing in our region. Our profile offers excellent heat and sound insulation and are free from electrolytic corrosion in saline climates. The advanced profile design, with systematically reinforced frames and steel with multiple locking systems offers a trusted level of security. Whatever your choice of windows, PROFILEZ always has the best solution.
                        </p>
                    </div>
                    <br>
                    <div class="mtpx-10 map">
                    <h6 class="heading-main fw-semi-bold abou-6 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Map</h6>
                   <div class="content ">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3619.766976243651!2d67.05781811431557!3d24.871807084048626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33e923adec49d%3A0xcc2dbd936f1c5627!2sTariq+Rd%2C+PECHS%2C+Karachi%2C+Karachi+City%2C+Sindh%2C+Pakistan!5e0!3m2!1sen!2s!4v1542707994823" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                         
                    </div>
                    </div>
                       
                    
                        
                    
                   
               </div>
            </div>
            <div class="col-lg-3 col-sm-12 ">
                <div class="lawyer-box bg-white">
                    <div class="head con">
                        <p class="fs-17 fc-primary fw-semi-bold tt-uppercase"><span class="icon-icon-58"></span>CONTACT PROFILEZ</p>
                    </div>
                    <div class="cont">
                        <form class="profile-form" action="">
                            <div class="control-group">
                                <label for="Name">Name <span class="fc-red fs-medium">*</span></label>
                                <input type="text" placeholder="" required>
                            </div>
                            <div class="control-group">
                                <label for="Email">Email Address<span class="fc-red fs-medium">*</span></label>
                                <input type="email" name="em" class="required" placeholder="Enter Your Email" />
                            </div>
                            <div class="control-group">
                               <label for="number">Cell Number 1<span class="fc-red fs-medium">*</span></label>
                               <input type="number" name="pn3" placeholder="0123-456789123" required>
                           </div>
                           <div class="control-group">
                               <label for="number">Phone Number 1<span class="fc-red fs-medium">*</span></label>
                               <input type="number" name="pn4" placeholder="0123-456789123" required>
                           </div>
                           <div class="control-group">
                               <label for="Name">Message<span class="fc-red fs-medium">*</span></label>
                               <textarea name="Descrition" placeholder="" id="" cols="30" rows="10"></textarea>
                           </div>
                           <div class="control-group">
                                <label for="Name">Security Code<span class="fc-red fs-medium">*</span></label>
                                <div class="grid-block --type-two-blocks --style-offsets">
                                    <div class="item">
                                        <input class="w-50" type="text"> 
                                    </div>
                                    <div class="item pl-5">
                                        <input class="" type="text"> 
                                    </div>
                                </div>
                           </div>
                           <div class="control-group">
                               <p class="fs-11 fc-lgrey ptpx-10">By submitting this form <br>I agree to <a class="fc-primary td-underline" href="../terms-of-use">Terms of Use</a></p>

                           </div>
                           <div class="control-group buto mtpx-10">
                               <input class="btn-new-form w-100" type="submit" value="Send Email" placeholder="">
                               <span class="icon-keyboard_arrow_right"></span>
                           </div>
                        </form>
                    </div>
                    <div class="social-icons">
                        <ul class="unstyled inline social">
                            <li><a href=""><span class="icon-facebook"></span></a></li>
                            <li><a href=""><span class="icon-google-plus1"></span></a></li>
                            <li><a href=""><span class="icon-twitter"></span></a></li>
                            <li><a href=""><span class="icon-youtube"></span></a></li>
                            <li><a href=""><span class="icon-pinterest"></span></a></li>
                        </ul>
                    </div>
                </div>

                
            </div>
            
          </div>
            </div>
        </div>


   </div>
</div>



@endsection