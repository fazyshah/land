@extends('layout.master')
@section('title', 'auction-page-details')

@section('content')



<section class=" sec-padding --small bg-cream" >
    <div class="container">
        <ul class="breadcrumb mbpx-0">
          <li><a href="#">Auction</a></li>
          <li>1428 MABEL DR</li>
        </ul>
        <div class="row">
            <div class="col-lg-9">
                <div class="auc-detail">
                    <div class="first">
                        <p class=""><span class="icon-bookmark1"></span> Save to Facebook</p>
                        <ul class="unstyled inline ">
                            <li><a href=""><span class="icon-external-link"></span></a></li>
                            <li><a href=""><span class="icon-heart1"></span></a></li>
                            <li><a href=""><span class="icon-printer-text"></span></a></li>
                        </ul>
                    </div>
                    <div class="auc-detail-box">
                        <div class="row">
                            <div class="col-md-5">
                                <figure class="below-image">
                                    <img class="w-100" src="auction/big-1.jpg" alt="*">
                                    <div class="shade">
                                        <ul class="unstyled inline">
                                            <li><a href=""><span class="icon-camera3 h6" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                            
                                           
                                            <li><a href=""><span class="icon-icon-56 h6" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                            <li ><a href=""><span class="icon-icon-55 h6" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                </figure>
                            </div>
                            <div class="col-md-4 ">
                                <div class="text">
                                    <h6 class="tt-uppercase fc-secondary pbpx-5">1428 MABEL DR</h6>
                                    <p class="fs-13 fc-grey">WARRIOR, AL 35180, Jefferson Country</p>
                                    <ul class="unstyled inline lister">
                                      <li><a href="javascript:;">Two Familes</a></li>
                                      <li><a href="javascript:;">Occupancy Status is Unknown</a></li>
                                    </ul>
                                    <div class="disturb ptpx-20">
                                        <p class="fs-17 fc-grey ">Do Not Disturb Occupant. <span class="d-block fc-lgrey">It is a criminal offense to trespass on this property.</span></p>
                                    </div>
                                    <div class="ptpx-20">
                                        <a class="btn-detail" href=""><span class="icon-star-o"></span> &nbsp;Save Asset</a>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-3">
                                <div class="text two">
                                    <div class="ta-center green-head"><p class="fs-11">Active - Scheduled for Auction</p></div>
                                    <p class="fc-grey fs-11 est ta-right ptpx-10">Est. Credit Bid <span class="icon-icon-53 fc-secondary"></span></p>
                                    <p class="fs-large ta-right fc-grey">Not Disclosed</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="auc-detail pro  mtpx-20 ">
                    <h6 class="heading-main  fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">PROPERTY DETAILS</h6>
                    <table class="prop-table w-100 mbpx-40">
                        <tr>
                            <td class="w-25">Property Type: </td>
                            <td class="w-75">Single Family </td>
                        </tr>
                        <tr>
                            <td class="w-25">Lot Size (acres):</td>
                            <td class="w-75">4.5 </td>
                        </tr>
                        <tr>
                            <td class="w-25">File No: </td>
                            <td class="w-75">18-014960 </td>
                        </tr>
                        <tr>
                            <td class="w-25">APN: </td>
                            <td class="w-75">03 00 12 4 002 002.000</td>
                        </tr>
                        <tr>
                            <td class="w-25">Event Item #: </td>
                            <td class="w-75">E11878-1000 </td>
                        </tr>
                        <tr>
                            <td class="w-25">Property ID: </td>
                            <td class="w-75">2587500 </td>
                        </tr>
                    </table>

                    <h6 class="heading-main  fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">EVENT DETAILS</h6>
                    <table class="w-100 event-table mbpx-40">
                        <tr>
                            <td class="ta-center w-8"><span class="icon-icon-7 ico"></span></td>
                            <td class="w-92">Wednesday, Feb 20, 2019</td>
                        </tr>
                        <tr>
                            <td class="ta-center w-8"><span class="icon-icon-54 ico"></span></td>
                            <td class="w-92">Auction Starts: 11:00 am</td>
                        </tr>
                        <tr>
                            <td class="ta-center w-8"><span class="icon-icon-45 ico"></span></td>
                            <td class="w-92"><span class="fc-primary">Jefferson County Courthouse - Birmingham - Main Entrance</span></td>
                        </tr>
                        <tr>
                            <td class=" w-8 no-bg"></td>
                            <td class="w-92">716 Richard Arrington Jr Blvd N, Birmingham, AL 35203</td>
                        </tr>
                        <tr>
                            <td class="no-bg w-8"></td>
                            <td class="w-92"><span class="fc-primary">Full Event Details</span></td>
                        </tr>
                        <tr>
                            <td class="no-bg w-8"></td>
                            <td class="w-92"><span class="fc-primary">What to bring to a live Auction?</span></td>
                        </tr>

                    </table>

                    <h6 class="heading-main  fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">ADDITIONAL CONTACTS</h6>
                    <table class="w-100 prop-table sec">
                        <tr>
                            <td class="w-38">Customer Service Support:</td>
                            <td class="w-62">(800) 280-2832</td>
                        </tr>
                        <tr>
                            <td class="w-38">Contact (for Borrowers/Homeowners):</td>
                            <td class="w-62">Shapiro and Ingle LLP <br>(704) 333-8107</td>
                        </tr>
                        

                    </table>
                </div>

                <div class="auc-detail pro-2 ">
                    <h6 class="tt-uppercase fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase">My Saved Assets</h6>
                    <p class="fc-grey ptpx-8 pbpx-15">To view saved assets Log in or create a <span>FREE</span> account.</p>

                    <a class="btn-detail" href="../login"><span class="icon-person_outline"></span>&nbsp;Log In</a>
                </div>

                
            </div>
            <div class="col-lg-3 hidden-md-down">
                <?php include '../inc/documents.php';?>
                
                <?php include '../inc/side-advert-small.php';?>
                
            </div>
            
        </div>
        

        <div class="auction-box-cont">
            <h6 class="heading-main mtpx-50 fw-semi-bold d-small lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft"><span class="fc-lgrey">not the right fit?</span> View Similar properties</h6>
            <div class="row auction-slider ptpx-10">    
                <div class="col-md-4">
                    <div class="tup">
                        <div class="main-div">
                            <div class="auction-box" style="background: url(auction/img-1.jpg);">
                                <a href=""><img class="float-right" src="star.png" alt="*"></a>
                                <ul class="unstyled inline auc-list">
                                    <li>2,064 Sq Ft</li>
                                    <li>387 Hickory circle Union grove, Al 35175</li>
                                </ul>

                            </div>
                            <div class="auction-box-detail">
                                <div class="grid-block --type-two-blocks ">
                                    <div class="item">
                                        <p class="fs-medium fc-nblack">Auction Dates</p>
                                        <p class="fc-lgrey lh-medium ptpx-8">Oct19, 1:30pm <span class="d-block">Forclosure SSale, Live</span></p>
                                    </div>
                                    <div class="item">
                                        <p class="fs-medium fc-secondary ta-right">Opening Bid</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4">
                    <div class="tup">
                        <div class="main-div">
                            <div class="auction-box" style="background: url(auction/img-1.jpg);">
                                <a href=""><img class="float-right" src="star.png" alt="*"></a>
                                <ul class="unstyled inline auc-list">
                                    <li>2,064 Sq Ft</li>
                                    <li>387 Hickory circle Union grove, Al 35175</li>
                                </ul>

                            </div>
                            <div class="auction-box-detail">
                                <div class="grid-block --type-two-blocks ">
                                    <div class="item">
                                        <p class="fs-medium fc-nblack">Auction Dates</p>
                                        <p class="fc-lgrey lh-medium ptpx-8">Oct19, 1:30pm <span class="d-block">Forclosure SSale, Live</span></p>
                                    </div>
                                    <div class="item">
                                        <p class="fs-medium fc-secondary ta-right">Opening Bid</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4">
                    <div class="tup">
                        <div class="main-div">
                            <div class="auction-box" style="background: url(auction/img-1.jpg);">
                                <a href=""><img class="float-right" src="star.png" alt="*"></a>
                                <ul class="unstyled inline auc-list">
                                    <li>2,064 Sq Ft</li>
                                    <li>387 Hickory circle Union grove, Al 35175</li>
                                </ul>

                            </div>
                            <div class="auction-box-detail">
                                <div class="grid-block --type-two-blocks ">
                                    <div class="item">
                                        <p class="fs-medium fc-nblack">Auction Dates</p>
                                        <p class="fc-lgrey lh-medium ptpx-8">Oct19, 1:30pm <span class="d-block">Forclosure SSale, Live</span></p>
                                    </div>
                                    <div class="item">
                                        <p class="fs-medium fc-secondary ta-right">Opening Bid</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4">
                    <div class="tup">
                        <div class="main-div">
                            <div class="auction-box" style="background: url(auction/img-1.jpg);">
                                <a href=""><img class="float-right" src="star.png" alt="*"></a>
                                <ul class="unstyled inline auc-list">
                                    <li>2,064 Sq Ft</li>
                                    <li>387 Hickory circle Union grove, Al 35175</li>
                                </ul>

                            </div>
                            <div class="auction-box-detail">
                                <div class="grid-block --type-two-blocks ">
                                    <div class="item">
                                        <p class="fs-medium fc-nblack">Auction Dates</p>
                                        <p class="fc-lgrey lh-medium ptpx-8">Oct19, 1:30pm <span class="d-block">Forclosure SSale, Live</span></p>
                                    </div>
                                    <div class="item">
                                        <p class="fs-medium fc-secondary ta-right">Opening Bid</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                   
            </div>
        </div>
        
        
        
    </div>
    
      
    </div>
    
 </div>
</div>



@endsection