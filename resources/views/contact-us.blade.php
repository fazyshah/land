@extends('layout.master')
@section('title', 'contact-us')

@section('content')



    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3619.7668337027344!2d67.05781811447774!3d24.87181195092907!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33e923adec49d%3A0xcc2dbd936f1c5627!2sTariq+Rd%2C+PECHS%2C+Karachi%2C+Karachi+City%2C+Sindh%2C+Pakistan!5e0!3m2!1sen!2s!4v1542728047757" width="100%" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
    




<section class=" sec-padding --small bg-cream contactbg" style="background: url(contactbg.png);">
    <div class="container">
        <h4 class="ta-center fs-28 fw-semi-bold fc-secondary">Contact Us</h4>
        
        <div class="grid-block --type-three-blocks --style-offsets ptpx-30 --grid-ipad-half --grid-mobile-full">
            <div class="item matchheight">
                <div class="contact-box">
                    <div class="head">
                        <p class="fs-17 tt-uppercase fc-secondary fw-semi-bold"><span class="icon-icon-60 fc-primary"></span> Get in touch</p>
                    </div>
                    <div class="content">
                        <p class="fs-13 fc-grey lh-large">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit.</p>
                        <ul class="unstyled det">
                            <li><span class="icon-phone-call"></span> 0000-22328 (1121) <br> (+92) 12 3456 0000 <i class="fc-primary fs-small">(9 am To 6 pm)</i></li>
                            <li><span class="icon-envelope-o"></span> info@landtrack.com</li>
                        </ul>
                        <ul class="unstyled det2">
                            <li><a class="btn facebook" href="javascript:;"><span class="icon-facebook"></span> &nbsp;Sign in With Facebook</a></li>
                            <li><a class="btn twitter" href="javascript:;"><span class="icon-twitter"></span> &nbsp;Sign in With twitter</a></li>
                            <li><a class="btn google" href="javascript:;"><span class="icon-google-plus"></span> &nbsp;Sign in With Gmail</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="item matchheight">
                <div class="contact-box">
                    <div class="head">
                        <p class="fs-17 tt-uppercase fc-secondary fw-semi-bold"><span class="icon-icon-59 fc-primary offic"></span> OFFICES</p>
                    </div>
                    <div class="content">
                        <p class="fs-13 fc-grey lh-large">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br><br>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat deserunt mollit.</p>
                        <ul class="unstyled det off">
                            <li><span class="icon-icon-45"></span> 23-D/1, Main Khayaban-e-Jami, Phase II, DHA Karachi</li>
                        </ul>
                        
                    </div>
                </div>
            </div>
            <div class="item matchheight">
                <div class="contact-box">
                    <div class="head">
                        <p class="fs-17 tt-uppercase fc-secondary fw-semi-bold"><span class="icon-paper-plane-o fc-primary minfo"></span> for more information</p>
                    </div>
                    <div class="content det3">
                        <form action="submit/contact/form" method="post" class="jform validate c-form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                            <div class="control-group">
                                
                                <input type="text" placeholder="Name" name="name" class="required" minlength="2" maxlength="60">
                            </div>
                            <div class="control-group">
                                
                                <input type="text" name="email" class="required email" placeholder="Email" maxlength="60">
                            </div>
                            <div class="control-group clearfix">                                            
                                    <input type="text" name="pn" id="phone" class="number required" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                               
                            </div>
                            <div class="control-group">
                                <textarea name="msg" class="required" placeholder="Message" minlength="2" maxlength="1000"></textarea>
                            </div> 
                            <div class="control-group buto mtpx-10">
                               <input class="btn-new-form blue w-100" type="submit" value="Send Email" placeholder="">
                               <span class="blue icon-paperplane"></span>
                           </div>
                            <div class="control-group last">
                                <div class="input">
                                    <input type="checkbox" id="informed">
                                </div>
                                <div class="lbl">
                                    <label class="fs-small fw-light fc-lgrey" for="informed">Keep me Informed about similar properties By Submitting this Form.I agree to <a class="fc-primary fw-semi-bold td-underline" href="">Terms Of Use</a></label>
                                </div>
                                
                            </div>                        
                        </form>
                    </div>
                </div>
                 
          </div>
            </div>
        </div>


   </div>
</div>



@endsection
