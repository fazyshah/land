@extends('layout.master')
@section('title', 'trend-detail')

@section('content')

<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>

</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 hidden-md-down">
                    
                </div>
                <div class="col-lg-8  col-md-10 col-sm-12">
                    
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="fs-medium fc-white fw-medium tt-uppercase">Find most searched locations in Pakistan</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-1 " action="">
                                            <div class="control-group w-20 d-inline-block">
                                                <select name="" id="">
                                                    <option value="">Select City</option>
                                                    <option value="">asdasda</option>
                                                </select>
                                            </div>
                                            <div class="control-group w-60  d-inline-block mar">
                                                <input type="text" placeholder="Type Location, area or keyword">
                                            </div>
                                            <div class="control-group  w-18  d-inline-block ml-1">
                                                <button class="btn-submit" type="submit" value="">SEARCH</button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class=" sec-padding bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-0">
          <li><a href="/">Home </a></li>
          <li><a href="/trends">Trends </a></li>
          <li><a href="/">Lahore </a></li>
          <li> Buying Trends For Properties in DHA Lahore</li>
        </ul>

        <div class="row">
            <div class="col-lg-12">
                <div class="about-us bg-white">
                    <ul class="unstyled inline tabbing-links">
                        <li class="current w-20 ta-center" data-targetit="tabs-overview">
                        Buying Trends</li><li class=" w-20 ta-center" data-targetit="tabs-board">
                        Rental Trends</li>
                    </ul>
                </div>
                <div class="tabs-overview  hidden visible trend-detail bg-white ">
                    <div class="head">
                        <h6 class="fc-black fw-semi-bold">Buying Trends for Properties in DHA Defence</h6>
                    </div>
                    <div class="bottom">
                        <div class="left">
                            <ul class="unstyled d-block">
                                <li><a href="">Sep 2018</a></li>
                                <li><a href="">Aug 2018</a></li>
                                <li><a href="">Jul 2018</a></li>
                                <li><a href="">Jun 2018</a></li>
                                <li><a href="">May 2018</a></li>
                                <li><a href="">Apr 2018</a></li>
                                <li><a href="">Mar 2018</a></li>
                                <li><a href="">Feb 2018</a></li>
                                <li><a href="">Jan 2018</a></li>
                                <li><a href="">Dec 2017</a></li>
                                <li><a href="">Nov 2017</a></li>
                                <li><a href="">Oct 2017</a></li>
                            </ul>
                        </div>
                        <div class="right">
                             <table class="table table-hover">
                                 <thead>
                                   <tr>
                                     <th class="w-5">RANK</th>
                                     <th class="w-20">LOCALITY</th>
                                     <th class="w-28">PERCENTAGE OF TOTAL SEARCHES (%)</th>
                                     <th class="w-15">TRENDS</th>
                                     <th class="w-22">PERFORMANCE</th>
                                   </tr>
                                 </thead>
                                 <tbody>
                                   <tr>
                                     <td>1</td>
                                     <td>DHA Phase</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">18%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td>No change</td>
                                   </tr>
                                   <tr>
                                     <td>2</td>
                                     <td>DHA Phase 6</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="95" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">17.6%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td class="ptpx-20"><a href=""><p>1&nbsp;<span class="icon-caret-up"></span></p></a></td>
                                   </tr>
                                   <tr>
                                     <td>3</td>
                                     <td>DHA Phase 7</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="85" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">16.4%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td>No change</td>
                                   </tr>
                                   <tr>
                                     <td>4</td>
                                     <td>DHA Phase 9 Prism</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="82" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">8.6%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td>No change</td>
                                   </tr>
                                   <tr>
                                     <td>5</td>
                                     <td>DHA Phase 5</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="80" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">5.7%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td>No change</td>
                                   </tr>
                                   <tr>
                                     <td>6</td>
                                     <td>DHA 9 Town</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="85" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">3.2%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td class="ptpx-20"><a href=""><p>1&nbsp;<span class="icon-caret-up"></span></p></a></td>
                                   </tr>
                                   <tr>
                                     <td>7</td>
                                     <td>DHA Phase 3</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="48" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">1.9%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td>No change</td>
                                   </tr>
                                   <tr>
                                     <td>8</td>
                                     <td>DHA Phase 4</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="42" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">1.4%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td class="ptpx-20"><a href=""><p>1&nbsp;<span class="icon-caret-up"></span></p></a></td>
                                   </tr>
                                   <tr>
                                     <td>9</td>
                                     <td>DHA Phase 11 - Halloki Gardens</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">0.8%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td>No change</td>
                                   </tr>
                                   <tr>
                                     <td>10</td>
                                     <td>Defence Raya</td>
                                     <td>
                                         <div class="barWrapper">
                                             <div class="progress">
                                                 <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" > 
                                                 </div>
                                                 <span class="fc-white fs-medium">0%</span>
                                             </div>
                                         </div>
                                     </td>
                                     <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                     <td>No change</td>
                                   </tr>
                               </tbody>
                           </table>
                        </div>
                    </div>
                </div>

                <div class="tabs-board  hidden trend-detail bg-white ">
                    <div class="head">
                        <h6 class="fc-black fw-semi-bold">Rental Trends for Properties in DHA Defence</h6>
                    </div>
                    <div class="bottom">
                        <div class="left">
                            <ul class="unstyled d-block">
                                <li><a href="">Sep 2018</a></li>
                                <li><a href="">Aug 2018</a></li>
                                <li><a href="">Jul 2018</a></li>
                                <li><a href="">Jun 2018</a></li>
                                <li><a href="">May 2018</a></li>
                                <li><a href="">Apr 2018</a></li>
                                <li><a href="">Mar 2018</a></li>
                                <li><a href="">Feb 2018</a></li>
                                <li><a href="">Jan 2018</a></li>
                                <li><a href="">Dec 2017</a></li>
                                <li><a href="">Nov 2017</a></li>
                                <li><a href="">Oct 2017</a></li>
                            </ul>
                        </div>
                        <div class="right">
                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th class="w-5">RANK</th>
                                    <th class="w-20">LOCALITY</th>
                                    <th class="w-28">PERCENTAGE OF TOTAL SEARCHES (%)</th>
                                    <th class="w-15">TRENDS</th>
                                    <th class="w-22">PERFORMANCE</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>1</td>
                                    <td>DHA Phase</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" > 
                                                </div>
                                                <span class="fc-white fs-medium">18%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td>No change</td>
                                  </tr>
                                  <tr>
                                    <td>2</td>
                                    <td>DHA Phase 6</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="95" > 
                                                </div>
                                                <span class="fc-white fs-medium">17.6%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td class="ptpx-20"><a href=""><p>1&nbsp;<span class="icon-caret-up"></span></p></a></td>
                                  </tr>
                                  <tr>
                                    <td>3</td>
                                    <td>DHA Phase 7</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="85" > 
                                                </div>
                                                <span class="fc-white fs-medium">16.4%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td>No change</td>
                                  </tr>
                                  <tr>
                                    <td>4</td>
                                    <td>DHA Phase 9 Prism</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="82" > 
                                                </div>
                                                <span class="fc-white fs-medium">8.6%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td>No change</td>
                                  </tr>
                                  <tr>
                                    <td>5</td>
                                    <td>DHA Phase 5</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="80" > 
                                                </div>
                                                <span class="fc-white fs-medium">5.7%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td>No change</td>
                                  </tr>
                                  <tr>
                                    <td>6</td>
                                    <td>DHA 9 Town</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="85" > 
                                                </div>
                                                <span class="fc-white fs-medium">3.2%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td class="ptpx-20"><a href=""><p>1&nbsp;<span class="icon-caret-up"></span></p></a></td>
                                  </tr>
                                  <tr>
                                    <td>7</td>
                                    <td>DHA Phase 3</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="48" > 
                                                </div>
                                                <span class="fc-white fs-medium">1.9%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td>No change</td>
                                  </tr>
                                  <tr>
                                    <td>8</td>
                                    <td>DHA Phase 4</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="42" > 
                                                </div>
                                                <span class="fc-white fs-medium">1.4%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td class="ptpx-20"><a href=""><p>1&nbsp;<span class="icon-caret-up"></span></p></a></td>
                                  </tr>
                                  <tr>
                                    <td>9</td>
                                    <td>DHA Phase 11 - Halloki Gardens</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" > 
                                                </div>
                                                <span class="fc-white fs-medium">0.8%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td>No change</td>
                                  </tr>
                                  <tr>
                                    <td>10</td>
                                    <td>Defence Raya</td>
                                    <td>
                                        <div class="barWrapper">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" > 
                                                </div>
                                                <span class="fc-white fs-medium">0%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img class="m-auto" src="{{$imgscr}}trends/graph-small.jpg" alt="*"></td>
                                    <td>No change</td>
                                  </tr>
                              </tbody>
                          </table>
                        </div>
                    </div>
                </div>

            </div>
     
            
          </div>
            </div>
        </div>


   </div>
</div>



@endsection


    </body>

<script>
    $(function () { 
      $('[data-toggle="tooltop"]').tooltip({trigger: 'manual'}).tooltip('show');
    });  

    // $( window ).scroll(function() {   
     // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
      $(".progress-bar").each(function(){
        each_bar_width = $(this).attr('aria-valuenow');
        $(this).width(each_bar_width + '%');
      });
           
     //  }  
    // });


</script>    