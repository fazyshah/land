@extends('layout.master')
@section('title', 'society-maps')

@section('content')

<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>

</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 hidden-md-down">
                    
                </div>
                <div class="col-lg-8  col-md-10 col-sm-12">
                    
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="fs-medium fc-white fw-medium tt-uppercase">Find approved maps of societies and projects in Pakistan</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            {{ Form::open(['class' => 'form-1', 'route' => 'search']) }}

                                            <div class="control-group w-20 d-inline-block">
                                                {{ Form::select('city', $cities, old('city'), ['placeholder' => 'Select City', 'required']) }}

                                                {{-- <select name="" id="">
                                                    <option value="">Select City</option>
                                                    <option value="">asdasda</option>
                                                </select> --}}
                                            </div>
                                            <div class="control-group w-62  d-inline-block mar">
                                                <input name="keyword" type="text" placeholder="Type Location, area or keyword">
                                            </div>
                                            <div class="control-group  w-17  d-inline-block ">
                                                <button class="btn-submit" type="submit" value="">SEARCH</button>

                                            </div>
                                            <div class="filter-btn">
                                                <a class=" fs-small fw-semi-bold fc-black tt-uppercase" href="http://onlinekidstoy.com/landtrack.com/#"><span class="icon-plus-circle "></span>Add Filter</a>
                                            </div>

                                            {{ Form::close() }}

                                        </div>
                                        <div  class="tabs-two hidden">
                                            4565454654 asdasd a456s4d6as4d56
                                        </div>
                                        <div  class="tabs-three hidden">
                                            ashdajsdh adsjkask absdb
                                        </div>
                                        <div  class="tabs-four hidden">
                                            zeeshan
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class=" sec-padding --small bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-0 mbpx-0">
          <li><a href="/">Home </a></li>
          <li><a href="/trends">Trends </a></li>
          <li><a href="/">Lahore </a></li>
          <li> Buying Trends For Properties in DHA Lahore</li>
        </ul>

        <div class="row">
            <h6 class="ta-center fw-semi-bold fc-secondary mbpx-20 ptpx-15">Featured Maps</h6>
            <div class="grid-block --type-four-blocks --style-offsets --style-bottom-offsets  --bottom-offsets-small --grid-ipad-half --grid-mobile-full">
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map1.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">Divine Gardens</p>
                            <p class="fc-black">Lahore</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map2.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">Peninsula Commercial Area</p>
                            <p class="fc-black">Karachi</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map3.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">AWT Phase 2 - Block E-1</p>
                            <p class="fc-black">Lahore</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map4.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">AWT Phase 2 - Block A</p>
                            <p class="fc-black">Lahore</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map5.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">Goverment Teacher Hous...</p>
                            <p class="fc-black">Karachi</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map6.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">Bahria Town Phase 8 - Block K</p>
                            <p class="fc-black">Rawalpindi</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map7.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">Bahria Town Phase 8 - Sect...</p>
                            <p class="fc-black">Rawalpindi</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="map-box">
                        <figure>
                            <img class="w-100" src="{{$imgscr}}society-maps/map8.jpg" alt="*">
                        </figure>
                        <div class="content ta-center">
                            <p class="fs-medium fc-secondary fw-semi-bold">Shadman City Phase 3</p>
                            <p class="fc-black">Bahawalpur</p>
                        </div>
                        <div class="butons">
                            <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>

        <div class="row">
            <h6 class="ta-center fw-semi-bold fc-secondary mbpx-20 ptpx-15">Browse Maps by Location</h6>
            <div class="map-by-location">
                <div class="head">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <div class="middle-form wow fadeInUp">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form class="form-1 " action="">
                                                    <div class="control-group w-20 d-inline-block">
                                                        <select name="" id="">
                                                            <option value="">Select City</option>
                                                            <option value="">asdasda</option>
                                                        </select>
                                                    </div>
                                                    <div class="control-group w-60  d-inline-block mar">
                                                        <select class="two" name="" id="">
                                                            <option value="">Haji Park Housing Scheme Map, Lahore</option>
                                                            <option value="">asdasda</option>
                                                        </select>
                                                    </div>
                                                    <div class="control-group  w-18  d-inline-block ml-1">
                                                        <button class="btn-submit" type="submit" value="">SEARCH</button>
                                                    </div>
                                                </form>
                                            </div>
                                            
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <div class="total-map">
                                <ul class="unstyled inline tabbing-links">
                                    <li class="current " data-targetit="tabs-one">Haji Park Housing Scheme Map</li>
                                    <li data-targetit="tabs-two">Commute and nearby</li>
                                </ul>
                                <div class="main-maps">
                                    <div class="tabs-one  hidden visible">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d924234.6301497458!2d66.59496105984819!3d25.193389485801205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33e06651d4bbf%3A0x9cf92f44555a0c23!2sKarachi%2C+Karachi+City%2C+Sindh%2C+Pakistan!5e0!3m2!1sen!2s!4v1542886418516" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>

                                    <div class="tabs-two  hidden">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d435518.68178291846!2d74.07127001381937!3d31.483220875157073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39190483e58107d9%3A0xc23abe6ccc7e2462!2sLahore%2C+Punjab%2C+Pakistan!5e0!3m2!1sen!2s!4v1542886434399" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
  

                        
  </div>  </div>  </div>  </div>  </div>  </div>  </div>  </div>  </div>
    </div>  </div>  </div>  </div>  </div>  </div>  </div>
  </div>

@endsection
