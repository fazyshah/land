@extends('layout.master')
@section('title', 'trend')

@section('content')

<
<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>

</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 hidden-md-down">
                    
                </div>
                <div class="col-lg-8  col-md-10 col-sm-12">
                    
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="fs-medium fc-white fw-medium">FIND YOUR NEXT INVESTMENT HOME NOW</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        {{ Form::open(['class' => 'form-1', 'route' => 'search']) }}

                                        <div class="control-group w-20 d-inline-block">
                                            {{ Form::select('city', $cities, old('city'), ['placeholder' => 'Select City', 'required']) }}

                                            {{-- <select name="" id="">
                                                <option value="">Select City</option>
                                                <option value="">asdasda</option>
                                            </select> --}}
                                        </div>
                                        <div class="control-group w-62  d-inline-block mar">
                                            <input name="keyword" type="text" placeholder="Type Location, area or keyword">
                                        </div>
                                        <div class="control-group  w-17  d-inline-block ">
                                            <button class="btn-submit" type="submit" value="">SEARCH</button>

                                        </div>
                                        <div class="filter-btn">
                                            <a class=" fs-small fw-semi-bold fc-black tt-uppercase" href="/"><span class="icon-plus-circle "></span>Add Filter</a>
                                        </div>

                                        {{ Form::close() }}
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class="second-banner buy sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <ul class="breadcrumb mbpx-0">
          <li><a href="#">Home&nbsp;</a></li>
          <li>&nbsp;Trends</li>
        </ul>
        <h4 class="ta-center fc-secondary mbpx-20">Popular Trends for Renting Properties in Sep 2018</h4>
        <div class="row trend">
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-1.jpg);">
                <p class="text wow fadeInLeft">Karachi</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-2.jpg);">
                <p class="text wow fadeInLeft">Islamabad</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-3.jpg);">
                <p class="text wow fadeInLeft">Lahore</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>  
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-4.jpg);">
                <p class="text wow fadeInLeft">Rawalpindi</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-5.jpg);">
                <p class="text wow fadeInLeft">Gujranwala</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-6.jpg);">
                <p class="text wow fadeInLeft">Faisalabad</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-7.jpg);">
                <p class="text wow fadeInLeft">Peshawar</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="trend-box" style="background:url({{$imgscr}}trends/trend-8.jpg);">
                <p class="text wow fadeInLeft">Multan</p>
                <a class="detail-btn wow fadeInRight" href="../trends-cities-list" >View Details</a>
            </div>
          </div>

  
        
    
          </div>

             </div>
  
  </div>
            
          </div>
            </div>
        </div>


   </div>
</div>



@endsection
