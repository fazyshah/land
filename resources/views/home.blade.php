@extends('layout.master')
@section('title', 'Home')

@section('content')
<div class="super-banner">
<div class="main-slider">
<section class="hero-banner" style="background: url(web_asset/images/banner-1.jpg);">


</section>
<section class="hero-banner" style="background: url(web_asset/images/banner-2.jpg);">
</section>
<section class="hero-banner" style="background: url(web_asset/images/banner-3.jpg);">
</section>
</div>

<section class="form-section">

        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 hidden-md-down">

                </div>
                <div class="col-lg-8  col-md-10 col-sm-12">
                    <h2 class="fw-light ta-center fc-white lh-normal wow fadeInDown pbpx-20 ff-primary">Your Partners For <span class="d-block tt-uppercase fw-bold ls-large">Reliable Properties</span></h2>
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-5">
                                        <p class="fs-medium fc-white fw-medium">Property for Sale in Pakistan</p>
                                    </div>
                                    <div class="col-md-7">
                                        <ul class="unstyled inline tabbing-links">
                                            <li class="current" data-targetit="tabs-one"><a href="javascript:;">BUY</a></li><li data-targetit="tabs-two"><a href="javascript:;">RENT</a></li><li data-targetit="tabs-three"><a href="javascript:;">AGENTS</a></li><li  data-targetit="tabs-four"><a href="javascript:;">PROJECTS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            {{ Form::open(['class' => 'form-1', 'route' => 'search','method' => 'get']) }}

                                                <div class="control-group w-20 d-inline-block">
                                                    {{ Form::select('city', $cities, old('city'), ['placeholder' => 'Select City', 'required']) }}

                                                    {{-- <select name="" id="">
                                                        <option value="">Select City</option>
                                                        <option value="">asdasda</option>
                                                    </select> --}}
                                                </div>
                                                <div class="control-group w-62  d-inline-block mar">
                                                    <input name="keyword" type="text" placeholder="Type Location, area or keyword">
                                                </div>
                                                <div class="control-group  w-17  d-inline-block ">
                                                    <button class="btn-submit" type="submit" value="">SEARCH</button>

                                                </div>
                                                <div class="filter-btn" onclick="showfilters();">
                                                    <a class=" fs-small fw-semi-bold fc-black tt-uppercase" href="#"><span class="icon-plus-circle "></span>Add Filter</a>
                                                </div>

                                            <div class="filters" style="display: none;">
                                                <div class="form-group col-md-2">
                                                    <input name="min" required type="number" placeholder="min">
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <input name="max" required type="number" placeholder="max">
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <select name="property-type">
                                                        <option>Property Type</option>
                                                        <option value="1">
                                                            Commercial
                                                        </option>
                                                        <option value="2">
                                                            Apartment
                                                        </option>
                                                        <option value="3">
                                                            Office
                                                        </option>
                                                        <option value="4">
                                                            Industrial
                                                        </option>
                                                        <option value="5">
                                                            Residential
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            {{ Form::close() }}

                                        </div>
                                        <div  class="tabs-two hidden">
                                            4565454654 asdasd a456s4d6as4d56
                                        </div>
                                        <div  class="tabs-three hidden">
                                            ashdajsdh adsjkask absdb
                                        </div>
                                        <div  class="tabs-four hidden">
                                            zeeshan
                                        </div>
                                    </div>

                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

</section>


</div>



<section class="second-banner sec-padding --small bg-cream" >
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <!-- SHARED PROJECTS -->
                <h6 class="heading-main fw-semi-bold main lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Looking for SHARED ACCOMODATION</h6>

                <div class="shared-slider">
                    <div class="item matchheight">
                        <div class="prop-new-box">
                            <div class="top" style="background: url{{url('web_asset/images/property/prop1.jpg')}};">
                                <div class="ico">
                                    <a class="btn-s" href="javascript:;">Featured</a>
                                    <a class="btn-s orange" href="javascript:;">For Rent</a>
                                    <ul class="unstyled inline">
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/hot.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/star.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/verified.png') }}" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="profile">
                                    <div class="left">
                                        <img src="{{ asset('web_asset/images/user/img-1.png') }}" alt="*">
                                        <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                                    </div>
                                    <div class="float-right ta-center">
                                        <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                        <p class="fc-white fs-small">2 month</p>
                                    </div>
                                </div>
                            </div>

                            <a href="shared-accomodation-detail">
                            <div class="content">
                                <p class="fs-medium fc-nblack fw-semi-bold"></p>
                                <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span></p>
                                <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                            </div>
                            </a>

                            <div class="bottom">
                                <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                    <li>800 Sqft</li>
                                    <li>2 Beds</li>
                                    <li>2 Baths</li>
                                </ul>
                            </div>
                            <div class="butons bord grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                    </div>
                    <div class="item matchheight">
                        <div class="prop-new-box">
                            <div class="top" style="background: url{{url('web_asset/images/property/prop1.jpg')}};">
                                <div class="ico">
                                    <a class="btn-s" href="javascript:;">Featured</a>
                                    <a class="btn-s orange" href="javascript:;">For Rent</a>
                                    <ul class="unstyled inline">
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/hot.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/star.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/verified.png') }}" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="profile">
                                    <div class="left">
                                        <img src="{{ asset('web_asset/images/user/img-1.png') }}" alt="*">
                                        <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                                    </div>
                                    <div class="float-right ta-center">
                                        <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                        <p class="fc-white fs-small">2 month</p>
                                    </div>
                                </div>
                            </div>
                            <a href="shared-accomodation-detail">
                            <div class="content">
                                <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                                <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                            </div>
                            </a>
                            <div class="bottom">
                                <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                    <li>800 Sqft</li>
                                    <li>2 Beds</li>
                                    <li>2 Baths</li>
                                </ul>
                            </div>
                            <div class="butons bord grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                    </div>
                    <div class="item matchheight">
                        <div class="prop-new-box">
                            <div class="top" style="background: url{{url('web_asset/images/property/prop1.jpg')}};">
                                <div class="ico">
                                    <a class="btn-s" href="javascript:;">Featured</a>
                                    <a class="btn-s orange" href="javascript:;">For Rent</a>
                                    <ul class="unstyled inline">
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/hot.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/star.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/verified.png') }}" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="profile">
                                    <div class="left">
                                        <img src="{{ asset('web_asset/images/user/img-1.png') }}" alt="*">
                                        <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                                    </div>
                                    <div class="float-right ta-center">
                                        <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                        <p class="fc-white fs-small">2 month</p>
                                    </div>
                                </div>
                            </div>
                            <a href="shared-accomodation-detail">
                            <div class="content">
                                <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                                <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                            </div>
                            </a>
                            <div class="bottom">
                                <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                    <li>800 Sqft</li>
                                    <li>2 Beds</li>
                                    <li>2 Baths</li>
                                </ul>
                            </div>
                            <div class="butons bord grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                    </div>
                    <div class="item matchheight">
                        <div class="prop-new-box">
                            <div class="top" style="background: url{{url('web_asset/images/property/prop1.jpg')}};">
                                <div class="ico">
                                    <a class="btn-s" href="javascript:;">Featured</a>
                                    <a class="btn-s orange" href="javascript:;">For Rent</a>
                                    <ul class="unstyled inline">
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/hot.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/star.png') }}" alt=""></a></li>
                                        <li><a href="javascript:;"><img src="{{ asset('web_asset/images/verified.png') }}" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="profile">
                                    <div class="left">
                                        <img src="{{ asset('web_asset/images/user/img-1.png') }}" alt="*">
                                        <p class="fc-white fs-medium lh-medium">Usama Bilal <span class="d-block fs-small">xyz Real Estate</span></p>
                                    </div>
                                    <div class="float-right ta-center">
                                        <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                        <p class="fc-white fs-small">2 month</p>
                                    </div>
                                </div>
                            </div>
                            <a href="shared-accomodation-detail">
                            <div class="content">
                                <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                                <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                            </div>
                            </a>
                            <div class="bottom">
                                <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                    <li>800 Sqft</li>
                                    <li>2 Beds</li>
                                    <li>2 Baths</li>
                                </ul>
                            </div>
                            <div class="butons bord grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                    </div>
                </div>


                <figure class="adver-shadow mtpx-40">
                    <img class="w-100" src="{{ asset('web_asset/images/advertisement/advert-1.jpg') }}" alt="advertisement">
                </figure>


                <!-- NEW PROJECTS -->
                <h6 class="heading-main small fw-semi-bold ff-primary lh-normal  fc-nblack tt-uppercase wow slideInLeft">New Projects</h6>

                <div class="grid-block --type-three-blocks --style-offsets --grid-ipad-full --grid-mobile-full ">

                    @forelse ($new_pprojects as $npproject)
                        <div class="item matchHeight">
                            <div class="prop-new-box-2">
                                @php
                                    if (preg_match("/\|/", $npproject->img)){
                                        $fp_img = explode('|', $npproject->img);
                                        $fp_img = $npproject[0];
                                    }elseif(!empty($npproject->img)){
                                        $fp_img = $npproject->img;
                                    }else{
                                        $fp_img = 'no-image.png';
                                    }
                                @endphp

                                <div class="top" style="background: url({{ asset('web_asset/images/projects/'.$fp_img) }});">

                                    <div class="text">
                                        <p class="fc-white fs-medium fw-medium">{{$npproject->title}} <span class="fs-13 d-block ls-normal">{{$npproject->projectArea->name.', '.$npproject->projectArea->areaCity->name}}</span></p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>{{$npproject->price}}</p>
                                    <ul class="unstyled fs-small lh-xmedium">
                                        <li><strong>City: </strong>{{$npproject->projectArea->areaCity->name}}</li>
                                        <li><strong>Locality: </strong>{{ $npproject->locality }}</li>
                                        <li><strong>Type: </strong>{{$npproject->projectType->type}}</li>
                                    </ul>
                                </div>
                                <div class="butons ">
                                    <a class="btn-blue fs-13 w-100" href="{{route('view.project', ['id' => $npproject->id])}}">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>No New Projects found...!!</p>
                    @endforelse


                </div>

                <figure class="adver-shadow mtpx-30">
                    <img class="w-100" src="{{ asset('web_asset/images/advertisement/advert-1.jpg') }}" alt="advertisement">
                </figure>

                <!-- FEATURED PROJECTS -->
                <h6 class="heading-main small-2 fw-semi-bold ff-primary lh-normal  fc-nblack tt-uppercase wow slideInLeft">FEATURED Property</h6>

                <div class="shared-slider">
                    @forelse ($featured_properties as $fproperty)
                        <div class="item matchheight">
                            <div class="prop-new-box">
                                @php
                                    if (preg_match("/\|/", $fproperty->img)){
                                        $fp_img = explode('|', $fproperty->img);
                                        $fp_img = $fproperty[0];
                                    }elseif(!empty($fproperty->img)){
                                        $fp_img = $fproperty->img;
                                    }else{
                                        $fp_img = 'no-image.png';
                                    }
                                @endphp

                                <div class="top" style="background: url({{ asset('web_asset/images/properties/'.$fp_img) }});">

                                    <div class="ico">
                                        <a class="btn-s" href="javascript:;">Featured</a>
                                        <a class="btn-s orange" href="javascript:;">{{$fproperty->propertyPurpose->purpose}}</a>
                                        <ul class="unstyled inline">
                                            <li><a href="javascript:;"><img src="{{ asset('web_asset/images/hot.png') }}" alt=""></a></li>
                                            <li><a href="javascript:;"><img src="{{ asset('web_asset/images/star.png') }}" alt=""></a></li>
                                            <li><a href="javascript:;"><img src="{{ asset('web_asset/images/verified.png') }}" alt=""></a></li>
                                        </ul>
                                    </div>
                                    <div class="profile">
                                        <div class="left">
                                            <img src="{{ asset('web_asset/images/user/img-1.png') }}" alt="*">
                                            <p class="fc-white fs-medium lh-medium">{{$fproperty->propertyUser->name}} <span class="d-block fs-small">xyz Real Estate</span></p>
                                        </div>
                                        <div class="float-right ta-center">
                                            <span class="icon-calendar3 fc-white d-block fs-large"></span>
                                            <p class="fc-white fs-small">{{ \Carbon\Carbon::parse($fproperty['created_at'])->diffForHumans() }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="content">
                                    <p class="fs-medium fc-nblack fw-semi-bold">{{$fproperty->title}}</p>
                                    <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>{{$fproperty->propertyArea->name.', '.$fproperty->propertyArea->areaCity->name}}</p>
                                    <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>{{$fproperty->price}}</p>
                                </div>
                                <div class="bottom">
                                    <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                        <li>{{$fproperty->area}}</li>
                                        <li>{{$fproperty->beds}} Beds</li>
                                        <li>{{$fproperty->baths}} Baths</li>
                                    </ul>
                                </div>
                                <div class="butons bord grid-block --type-two-blocks">
                                    <a class="btn-orange fs-13 w-100" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>No Fetured Properties found...!!</p>
                    @endforelse
                </div>

                <figure class="adver-shadow mtpx-30">
                    <img class="w-100" src="{{ asset('web_asset/images/advertisement/advert-1.jpg') }}" alt="advertisement">
                </figure>



                <!-- FEATURED PROJECTS -->
                <div class="row">
                    <div class="col-md-6 nopadd-right">

                        <h6 class="heading-main  fw-semi-bold v-small  ff-primary lh-normal  fc-nblack tt-uppercase wow slideInLeft">FEATURED Agencies</h6>

                        <div class="featured-agency">
                            <figure>
                                <a href="agent-detail">
                                <img class="" src="{{ asset('web_asset/images/partners-agency/agency-1.jpg') }}" alt="*">
                                </a>
                            </figure>
                            <figure>
                                <a href="agent-detail">
                                <img class="" src="{{ asset('web_asset/images/partners-agency/agency-2.jpg') }}" alt="*">
                                </a>
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/agency-3.jpg') }}" alt="*">
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/agency-4.jpg') }}" alt="*">
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/agency-5.jpg') }}" alt="*">
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/agency-6.jpg') }}" alt="*">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-6  nopadd-right">

                        <h6 class="heading-main v-small fw-semi-bold  ff-primary lh-normal  fc-nblack tt-uppercase wow slideInRight">Platinum Partners</h6>

                        <div class="featured-agency">
                            <figure>
                                <a href="partner-profile">
                                <img class="" src="{{ asset('web_asset/images/partners-agency/partner-1.jpg') }}" alt="*">
                                </a>
                            </figure>
                            <figure>
                                <a href="partner-profile">
                                <img class="" src="{{ asset('web_asset/images/partners-agency/partner-2.jpg') }}" alt="*">
                                </a>
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/partner-3.jpg') }}" alt="*">
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/partner-4.jpg') }}" alt="*">
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/partner-5.jpg') }}" alt="*">
                            </figure>
                            <figure>
                                <img class="" src="{{ asset('web_asset/images/partners-agency/partner-6.jpg') }}" alt="*">
                            </figure>

                        </div>
                    </div>
                </div>


                <figure class="adver-shadow mtpx-20 mbpx-20">
                    <img class="w-100" src="{{ asset('web_asset/images/advertisement/advert-1.jpg') }}" alt="advertisement">
                </figure>

            </div>

            @include('layout./partials.side-advertisement-new')

        </div>
    </div>
</section>


<section class="find-property-city hidden-md-down">
    <div class="container">
        <h6 class="ff-primary hafter lh-normal  fc-nblack tt-uppercase wow slideInLeft">Find Property By City</h6>
        <div class="row ptpx-20">
            <div class="col-lg-6  nopadd-right" >
                <ul class="unstyled first">
                    <li class="wow slideInLeft" data-wow-duration="0.6s" data-wow-delay="0.2s">
                        <a href="#">
                            <img src="{{ asset('web_asset/images/big-1.jpg') }}" alt="*">
                            <p class="ff-primary tt-uppercase wow fadeInLeft">Properties In Karachi</p>
                            <a class="detail-btn wow fadeInRight" href="buy">View Details</a>
                        </a>
                    </li>
                    <li class="wow slideInLeft" data-wow-duration="0.8s" data-wow-delay="0.4s">
                        <a href="#">
                            <img src="{{ asset('web_asset/images/big-2.jpg') }}" alt="*">
                            <p class="ff-primary tt-uppercase wow fadeInLeft">Properties In Gwadar</p>
                            <a class="detail-btn wow fadeInRight" href="buy">View Details</a>
                        </a>
                    </li>
                </ul>

            </div>
            <div class="col-lg-6  nopadd-left">
                <ul class="unstyled second">
                    <li class="wow slideInRight" data-wow-duration="0.6s" data-wow-delay="0.2s">
                        <a href="#">
                            <img src="{{ asset('web_asset/images/big-3.jpg') }}" alt="*">
                            <p class="ff-primary tt-uppercase wow fadeInLeft">Properties<br>In Lahore</p>
                            <a class="detail-btn wow fadeInRight" href="buy">View Details</a>
                        </a>
                    </li>
                    <li  class="wow slideInRight" data-wow-duration="0.8s" data-wow-delay="0.4s">
                        <a href="#">
                            <img src="{{ asset('web_asset/images/big-4.jpg') }}" alt="*">
                            <p class="ff-primary tt-uppercase wow fadeInLeft">Properties<br>In Islamabad</p>
                            <a class="detail-btn wow fadeInRight" href="buy">View Details</a>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

      <section class="blogs hidden-md-down bg-cream" data-img="url(assets/images/blog-bg.jpg)">
    <div class="container">
        <div class="mains">
            <h6 class="our-blog fc-white ff-primary fw-semi-bold">Our Blog</h6>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="grid-block --type-three-blocks --style-offsets main-left">
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>                                
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                        <div class="blog-box">
                            <p class="head">12<br>Aug</p>
                            <figure>
                               
                            </figure>
                            <div class="content">
                                <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                                
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="right">
                    <h6 class="right-head fc-white fw-medium ff-primary"><span class="icon-icon1"></span>JOIN OUR DISCUSSION FORUM</h6>
                    
                    <div class="ptpx-20">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Buying Property  (9029)</p>
                        <p class="fs-13 fc-white lh-medium">Need to buy property but not sure what to do and where to go? Post your query here and let us take care of the rest.</p>
                        </a>
                    </div>
                    
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Renting Property  (444)</p>
                        <p class="fs-13 fc-white lh-medium">Whether you’re looking to rent property or find tenants, post all of your rent related queries in this section for expert advice.</p>
                        </a>
                    </div>
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Selling Properties  (2036)</p>
                        <p class="fs-13 fc-white lh-medium">Selling your property in Pakistan but not sure how to proceed. Post your concerns here and let the experts do the rest.</p>
                        </a>
                    </div>
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Projects & Developments  (1447)</p>
                        <p class="fs-13 fc-white lh-medium">Learn about the various existing and upcoming projects & developments in Pakistan.</p>
                        </a>
                    </div>
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Maintenance & Safety  (102)</p>
                        <p class="fs-13 fc-white lh-medium">Voice your concerns about property maintenance & safety in this section and find out what our experts have to say about your issues.</p>
                        </a>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

        </div>


    </div>
</section>
    <script>
        function showfilters() {
            $(".filters").show();
        }
    </script>
@endsection
