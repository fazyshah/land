@extends('layout.master')
@section('title', 'careers')

@section('content')


<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner" style="background: url({{$imgscr}}banner-1.jpg);">
    
</section>
</div>
</div>



<section class=" sec-padding --small bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-0 mbpx-5">
          <li><a href="/">Home </a></li>
          <li> Careers</li>
        </ul>

        <div class="row">
            <h4 class="fc-secondary ta-center fw-medium">Careers</h4>
                <div class="col-lg-12 detail career bg-white ">
                    <h6 class=" heading-main abou fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Company Overview</h6>
                    <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                    <h6 class=" heading-main small-4 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft mtpx-50">WHAT’S IT LIKE TO WORK FOR US?</h6>
                    <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                    <h6 class=" heading-main small-3 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft  mtpx-50">WHAT THE FUTURE HOLDS</h6>
                    <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                    <h6 class=" heading-main abou-3 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft  mtpx-50">How To Apply</h6>
                    <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>

                        

                    <div class="col-lg-7 col-nopadd">
                        <p class="tt-uppercase fc-secondary fs-large lh-large fw-semi-bold ptpx-30 pbpx-10">apply now</p>
                        <form class="career-form" action="submit/careers/form" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                            <div class="col-md-6 nopadd-left">
                               <div class="control-group">
                                   <label for="">First Name<sup class="fc-red">*</sup></label>
                                   <input name="first_name" type="text" placeholder="First Name" required>
                               </div> 
                            </div>
                            <div class="col-md-6 nopadd-left">
                               <div class="control-group">
                                   <label for="">Last Name<sup class="fc-red">*</sup></label>
                                   <input name="last_name" type="text" placeholder="Last Name" required>
                               </div> 
                            </div>
                            <div class="col-md-12 nopadd-left">
                               <div class="control-group">
                                   <label for="">Email<sup class="fc-red">*</sup></label>
                                   <input name="email" type="email" placeholder="Email" required>
                               </div> 
                            </div>
                            <div class="col-md-12 nopadd-left">
                               <div class="control-group">
                                   <label for="">Position<sup class="fc-red">*</sup></label>
                                   <select name="position[]" id="">
                                       <option value="0">Select Your Position</option>
                                       <option value="1">IT Manager</option>
                                       <option value="2">Architecture</option>
                                       <option value="3">Civil Engineer</option>
                                   </select>
                               </div> 
                            </div>
                            <div class="col-md-6 nopadd-left">
                                <div class="control-group">
                                   <label for="">Upload Resume<sup class="fc-red">*</sup></label><br>
                                   <div class="upload-btn-wrapper">
                                     <button class="btn">Browse File</button>
                                     <input type="file" name="myfile" />
                                   </div>
                               </div>
                            </div>
                            <div class="col-md-6 nopadd-left">
                                 <div class="control-group ta-right ">
                                    <br>
                                    <input class="btn-secondary" type="submit" value="Submit">
                                </div>
                            </div>
                            
                            
                        </form>
                    </div>
                        
          </div>
            </div>
        </div>


   </div>
</div>



@endsection
