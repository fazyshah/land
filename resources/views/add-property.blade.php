@extends('layout.master')
@section('title', 'Add New property')

@section('content')

<section class="second-banner buy sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <div class="row">
            <div class="col-lg-9 nopadd-right">
                <div class="architect-2 bg-white">
                    <div class="head">
                        <p class="fs-17 tt-uppercase fw-bold fc-dblue">Add a Property</p>
                    </div>
                    {{ Form::open(['class' => 'architect-form-2', 'route' => 'save.property', 'files' => true]) }}
                        <div class="row body1">
                            <div class="col-md-6">
                                <div class="control-group check">
                                    <label class="d-block mbpx-10" for="purpose">Purpose <span class="fc-red fs-medium">*</span></label>

                                    <ul class="unstyled inline">
                                        @foreach($ppurposes as $key => $ppurpose)
                                        <li><input class="styled-checkbox" name="ppurpose" id="ppurpose_{{$ppurpose->id}}" type="checkbox" value="{{$ppurpose->id}}"><label for="ppurpose_{{$ppurpose->id}}">{{$ppurpose->purpose}}</label></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group check">
                                    <label class="d-block mbpx-10" for="purpose">Property Type<span class="fc-red fs-medium">*</span></label>

                                    <ul class="unstyled inline">
                                        @foreach($ptypes as $ptype)
                                        <li><input class="styled-checkbox" name="ptype" id="ptype_{{$ptype->id}}" type="checkbox" value="{{$ptype->id}}"><label for="ptype_{{$ptype->id}}">{{$ptype->type}}</label></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="row body2 ptpx-0">

                            <div class="col-md-4">
                                <div class="control-group">
                                    <label for="city">City<span class="fc-red fs-medium">*</span></label>
                                    {{ Form::select('city', $cities, old('city'), ['class' => 'type_fld', 'id' => 'city','placeholder' => 'Select City', 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="area">Location<span class="fc-red fs-medium">*</span></label>
                                    {{ Form::select('area', ['' => ''], old('area'), ['class' => 'type_fld', 'id' => 'area','placeholder' => 'Select Area', 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="control-group">
                                    <label for="featured">Featured</label>
                                    <select class="type_fld" name="featured" id="featured">
                                        <option value="" disabled="" selected="" hidden="">Select</option>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="title">Property Title<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" name="title" id="title" placeholder="Property Title" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="price">All Inclusive Price (PKR)<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" name="price" id="price" placeholder="1000.00000" required>
                                    <p class="fs-small fc-primary ta-right">Price must contain numbers only</p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="beds">Bed Rooms<span class="fc-red fs-medium">*</span></label>
                                    <input type="number" name="beds" id="beds" placeholder="5" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="baths">Bath Rooms<span class="fc-red fs-medium">*</span></label>
                                    <input type="number" name="baths" id="baths" placeholder="3" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="area_size">Land Area<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" name="area_size" id="area_size" placeholder="400 Sq. Yards" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="units">Units<span class="fc-red fs-medium">*</span></label>
                                    <select class="type_fld" name="units" id="units">
                                        <option value="" disabled="" selected="" hidden=""></option>
                                        <option value="1">01</option>
                                        <option value="2">02</option>
                                        <option value="3">03</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="images">Property Image<span class="fc-red fs-medium">*</span></label>
                                    <input class="form-control" id="images" type="file" name="images[]" required multiple>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="details">Description:</label>
                                    <textarea name="details" placeholder="Description" id="details" cols="30" rows="10"></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="validity">Expires After</label>
                                    <select class="type_fld" name="validity" id="validity">
                                        <option value="" disabled="" selected="" hidden="">Select Validity Period</option>
                                        <option value="7">7 Days</option>
                                        <option value="15">15 Days</option>
                                        <option value="30">30 Days</option>
                                    </select>
                                </div>
                            </div>

                            {{-- <div class="col-md-2 nopadd-right">
                                <div class="control-group">
                                    <label id="#bb" class="btn-file second"> View More
                                        <input type="file" id="File" size="60">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="control-group">
                                    <p class="warning fs-small fc-lgrey ptpx-25"><span class="fc-red icon-icon-warning"></span> Press CTRL key while selecting images to upload multi images at once</p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="control-group check">
                                    <p class="fs-17 fc-secondary fw-semi-bold tt-uppercase ptpx-20">Membership Status</p>

                                    <ul class="unstyled inline ptpx-15">
                                        <li><input class="styled-checkbox" id="styled-checkbox-existing" type="checkbox" value="existing" checked><label for="styled-checkbox-existing">Existing Member </label></li>
                                        <li class="ml-10"><input class="styled-checkbox" id="styled-checkbox-new" type="checkbox" value="new-member"><label for="styled-checkbox-new">New Member </label></li>

                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="">User Name<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="">Password<span class="fc-red fs-medium">*</span></label>
                                    <input type="password" placeholder="******" required>
                                </div>
                            </div> --}}

                        </div>




                        <div class="row body2">
                            <div class="col-md-8  last">
                                <div class="control-group">
                                    <input type="checkbox" id="basket-1" value="0" required><label for="basket-1" class="basket fc-dblue fs-small tt-uppercase fw-bold"> &nbsp; I Agree</label>
                                    <p class="fs-small clicking">By clicking <span>Register</span> you agree to the <a class="fc-primary" href="../terms-of-use">Terms and Conditions</a> <br>set out by this site, including our Cookie Use.</p>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="control-group buto">
                                    <input class="btn-new-form" type="submit" value="Submit Property" placeholder="" >
                                    <span class="icon-keyboard_arrow_right"></span>

                                </div>
                            </div>
                        </div>

                    {{ Form::close() }}
                </div>
            </div>
            @include('layout./partials.side-advertisement-new')
        </div>

    </div>
</section>

@endsection

@section('script')
    <script>
        $(document).ready(function() {

            // the selector will match all input controls of type :checkbox
            // and attach a click event handler
            $("input:checkbox").on('click', function() {
              // in the handler, 'this' refers to the box clicked on
              var $box = $(this);
              if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
              } else {
                $box.prop("checked", false);
              }
            });

            $('select[name="city"]').on('change', function(){
                var cityId = $(this).val();
                console.log(cityId);

                if(cityId) {
                    $.ajax({
                        url: '/area-ajax/'+cityId,
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },
                        success:function(data) {
                            $('select[name="area"]').empty();
                            $('select[name="area"]').attr("placeholder", "Select Location/Area");
                            $('select[name="area"]').append('<option value="new_loc">Add New Location/ Area</option>');
                            $.each(data, function(key, value){
                                $('select[name="area"]').append('<option value="'+ key +'">' + value + '</option>');
                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                        }
                    });
                } else {
                    $('select[name="area"]').empty();
                }

            });

        });

    </script>
@endsection