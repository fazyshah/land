@extends('layout.master')
@section('title', 'agent-directory-listing')

@section('content')



<section class=" sec-padding --small bg-cream" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb ff-primary  border-b">
                  <li><a href="/">Home&nbsp;</a></li>
                  <li><a href="javascript:;">&nbsp;Agent Directory </a></li>
                  <li>&nbsp;Gujar Khan</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9 ptpx-15">
                <h6 class="heading-main main-2 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">6 Property Agents in Gujar Khan</h6>
                <div class="row">
                    <div class="col-md-4">
                        <a href="javascript:;">
                        <div class="agent-directory-box">
                            <div class="head">
                                <figure>
                                    <img class="m-auto" src="{{$imgscr}}agent-directory/agent-1.jpg" alt="*">
                                </figure>
                            </div>
                            <div class="content">
                                <p class="fs-medium fw-semi-bold fc-black lh-nmedium">Saroya Real Estate <span class="d-block">Saroya Real Estate - Gujar Khan</span></p>
                                <p class="fs-13 fc-lgrey ptpx-5 ">Total Number Of Properties : 18</p>
                            </div>
                            <a href="javascript:;"><p class="like fs-small fc-secondary fw-light tt-uppercase ta-center fw-semi-bold"><span class="icon-heart-o mr-2 fw-semi-bold fs-default"></span>Save agent</p></a>
                            <div class="butons grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100 call-form-email" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="javascript:;">
                        <div class="agent-directory-box">
                            <div class="head">
                                <figure>
                                    <img class="m-auto" src="{{$imgscr}}agent-directory/agent-2.jpg" alt="*">
                                </figure>
                            </div>
                            <div class="content">
                                <p class="fs-medium fw-semi-bold fc-black lh-nmedium">Saroya Real Estate <span class="d-block">Saroya Real Estate - Gujar Khan</span></p>
                                <p class="fs-13 fc-lgrey ptpx-5 ">Total Number Of Properties : 18</p>
                            </div>
                            <a href="javascript:;"><p class="like fs-small fc-secondary fw-light tt-uppercase ta-center fw-semi-bold"><span class="icon-heart-o mr-2 fw-semi-bold fs-default"></span>Save agent</p></a>
                            <div class="butons grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100 call-form-email" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="javascript:;">
                        <div class="agent-directory-box">
                            <div class="head">
                                <figure>
                                    <img class="m-auto" src="{{$imgscr}}agent-directory/agent-3.jpg" alt="*">
                                </figure>
                            </div>
                            <div class="content">
                                <p class="fs-medium fw-semi-bold fc-black lh-nmedium">Saroya Real Estate <span class="d-block">Saroya Real Estate - Gujar Khan</span></p>
                                <p class="fs-13 fc-lgrey ptpx-5 ">Total Number Of Properties : 18</p>
                            </div>
                            <a href="javascript:;"><p class="like fs-small fc-secondary fw-light tt-uppercase ta-center fw-semi-bold"><span class="icon-heart-o mr-2 fw-semi-bold fs-default"></span>Save agent</p></a>
                            <div class="butons grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100 call-form-email" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="javascript:;">
                        <div class="agent-directory-box">
                            <div class="head">
                                <figure>
                                    <img class="m-auto" src="{{$imgscr}}agent-directory/agent-1.jpg" alt="*">
                                </figure>
                            </div>
                            <div class="content">
                                <p class="fs-medium fw-semi-bold fc-black lh-nmedium">Saroya Real Estate <span class="d-block">Saroya Real Estate - Gujar Khan</span></p>
                                <p class="fs-13 fc-lgrey ptpx-5 ">Total Number Of Properties : 18</p>
                            </div>
                            <a href="javascript:;"><p class="like fs-small fc-secondary fw-light tt-uppercase ta-center fw-semi-bold"><span class="icon-heart-o mr-2 fw-semi-bold fs-default"></span>Save agent</p></a>
                            <div class="butons grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100 call-form-email" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="javascript:;">
                        <div class="agent-directory-box">
                            <div class="head">
                                <figure>
                                    <img class="m-auto" src="{{$imgscr}}agent-directory/agent-2.jpg" alt="*">
                                </figure>
                            </div>
                            <div class="content">
                                <p class="fs-medium fw-semi-bold fc-black lh-nmedium">Saroya Real Estate <span class="d-block">Saroya Real Estate - Gujar Khan</span></p>
                                <p class="fs-13 fc-lgrey ptpx-5 ">Total Number Of Properties : 18</p>
                            </div>
                            <a href="javascript:;"><p class="like fs-small fc-secondary fw-light tt-uppercase ta-center fw-semi-bold"><span class="icon-heart-o mr-2 fw-semi-bold fs-default"></span>Save agent</p></a>
                            <div class="butons grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100 call-form-email" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="javascript:;">
                        <div class="agent-directory-box">
                            <div class="head">
                                <figure>
                                    <img class="m-auto" src="{{$imgscr}}agent-directory/agent-3.jpg" alt="*">
                                </figure>
                            </div>
                            <div class="content">
                                <p class="fs-medium fw-semi-bold fc-black lh-nmedium">Saroya Real Estate <span class="d-block">Saroya Real Estate - Gujar Khan</span></p>
                                <p class="fs-13 fc-lgrey ptpx-5 ">Total Number Of Properties : 18</p>
                            </div>
                            <a href="javascript:;"><p class="like fs-small fc-secondary fw-light tt-uppercase ta-center fw-semi-bold"><span class="icon-heart-o mr-2 fw-semi-bold fs-default"></span>Save agent</p></a>
                            <div class="butons grid-block --type-two-blocks">
                                <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                <a class="btn-blue fs-13 w-100 call-form-email" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                            </div>
                        </div>
                        </a>
                    </div>


                </div>
           </div>

            @include('layout./partials.side-advertisement-new')

        </div>
 </div>

         </div> </div>
         
          </div>
           </div>
            </div>
             </div> </div>
              </div> </div> </div>

 
        
    
@endsection