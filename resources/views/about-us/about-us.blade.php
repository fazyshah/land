<?php
 $meta = 'About Us';
 $desc = '';
 $pageclass = 'about-page';
 $imgsrc = '../assets/images/';

?>


<?php include '../inc/inner-header.php';?>
<div class="super-banner">
<div class="main-slider">
<section class="hero-banner small" style="background: url(<?php echo $imgsrc ?>banner-1.jpg);">
    
</section>
</div>
</div>



<section class=" sec-padding bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-0">
          <li><a href="/">Home </a></li>
          <li> About Us</li>
        </ul>

        <div class="row">
            <h4 class="fc-secondary ta-center fw-medium pbpx-15">About Us</h4>

            <div class="col-lg-12">
                <div class="about-us bg-white">
                    <ul class="unstyled inline tabbing-links">
                        <li class="current" data-targetit="tabs-overview">
                        OVERVIEW</li><li data-targetit="tabs-board">
                        BOARD OF DIRECTORS</li><li data-targetit="tabs-investors">INVESTORS</li>
                    </ul>
                </div>

                <div class="detail bg-white ">
                    <div  class="tabs-overview  hidden visible">
                        <h6 class=" heading-main abou fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">WHAT IS land track?</h6>
                        <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                        <h6 class=" heading-main abou fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft mtpx-50">How did it all start?</h6>
                        <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                        <h6 class=" heading-main abou-1 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft  mtpx-50">What do we do?</h6>
                        <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                        <h6 class=" heading-main abou-2 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft  mtpx-50">land track Expo's</h6>
                        <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>

                        <h6 class=" heading-main d-small fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft  mtpx-50">Exclusive marketing by landtrack.com</h6>
                        <p class="fs-13 fc-nblack">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                    </div>
                    <div class="tabs-board hidden">
                        asdasdadasdasdas
                    </div>
                    <div class="tabs-investors hidden">
                        asdasdasd
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>



<?php include '../inc/inner-footer.php';?>
        
    </body>
</html>