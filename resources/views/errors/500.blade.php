
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.png">
    <title>Chopal</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="assets/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/errors.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="error-header"> </div>
<div class="container ">
    <section class="error-container text-center">
        <h1 class="animated fadeInDown">500</h1>
        <div class="error-divider animated fadeInUp">
            <h2>ooops!!</h2>
            <p class="description">SOMETHING WENT WRONG.</p>
        </div>

        @if(auth()->check())
            <a href="{{ route('home') }}" class="return-btn"><i class="fa fa-home"></i> GO TO HOME</a>
        }
        }
        @else
            <a href="{{ route('login') }}" class="return-btn"><i class="fa fa-key"></i> Login</a>
        @endif
    </section>
</div>
</body>
</html>
