<@extends('layout.master')
@section('title', 'shared-accomodation-listing')
@section('content')


<div class="super-banner">
<div class="main-slider">
<section class="hero-banner" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>
<section class="hero-banner" style="background: url({{$imgscr}}banner-2.jpg);">
</section>
<section class="hero-banner" style="background: url({{$imgscr}}banner-3.jpg);">
</section>
</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h2 class="fw-light ta-center fc-white lh-normal wow fadeInDown pbpx-20 ff-primary">Your Partners For <span class="d-block tt-uppercase fw-bold ls-large">Reliable Properties</span></h2>
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p class=" fc-white fw-medium">Property for Sale in Pakistan</p>
                                    </div>
                                    <div class="col-md-8">
                                        <ul class="unstyled inline tabbing-links">
                                            <li class="current" data-targetit="tabs-one"><a href="javascript:;">BUY</a></li><li data-targetit="tabs-two"><a href="javascript:;">RENT</a></li><li  data-targetit="tabs-three"><a href="javascript:;">SHARED</a></li><li data-targetit="tabs-five"><a href="javascript:;">AGENTS</a></li><li  data-targetit="tabs-four"><a href="javascript:;">PROJECTS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            <form class="form-1 " action="">
                                                <div class="control-group w-20 d-inline-block">
                                                    <select name="" id="">
                                                        <option value="">Select City</option>
                                                        <option value="">asdasda</option>
                                                    </select>
                                                </div>
                                                <div class="control-group w-62  d-inline-block mar">
                                                    <input type="text" placeholder="Type Location, area or keyword">
                                                </div>
                                                <div class="control-group  w-17  d-inline-block ">
                                                    <button class="btn-submit" type="submit" value="">SEARCH</button>

                                                </div>
                                                
                                                <a class="fs-small fc-black tt-uppercase" href="#"><span class="icon-plus-circle "></span>Add Filter</a>
                                            </form>

                                        </div>
                                        <div  class="tabs-two hidden">
                                            4565454654 asdasd a456s4d6as4d56
                                        </div>
                                        <div  class="tabs-three hidden">
                                            ashdajsdh adsjkask absdb 
                                        </div>
                                        <div  class="tabs-four hidden">
                                            zeeshan
                                        </div>
                                        <div  class="tabs-five hidden">
                                            Creative Laws
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class="second-banner sec-padding --small ptpx-20" >
    <div class="container">
        <ul class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li><a href="#">Buy</a></li>
          <li>Homes</li>
        </ul>
        <div class="row">
            <div class="col-lg-12 pbpx-20">
                <div class="posts-search ">
                    <ul class="unstyled">
                        <li>
                            
                                <p class=" fs-medium fc-primary ta-left">Showing 1 to 25 of 32,625 Adds</p>
                            
                                <p class="fc-primary ta-right">Sort by: 
                                    <select name="" id="">
                                        <option value="0">Posted newest first</option>
                                        <option value="1"></option>
                                    </select>
                                </p>
                        </li>
                        <li>
                            <p class="fc-black fs-small">Get an alert with the newest ads for "pakistani" in Toronto (GTA)</p>
                            <p class="fc-black fs-small ta-right">Register for <span class="fc-primary">LandTrack</span> Alerts  [?] 
                                <a class="btn-sign" href="sign-up">Sign Up<span class="icon-caret-right"></span></a>
                            </p>
                        </li>
                       
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 nopadd-right">
                    <div class=" shred-box-2">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}sa-2/img-1.jpg" alt="*">
                                        <ul class="unstyled inline">
                                            <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                            </li>
                                            <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                        </ul>
                                </div>
                            </div>
                            <div class="col-md-7 nopadd-left">
                                <div class="content">
                                    <div class="main">
                                        <div class=" top-text d-inline-block"><p class=" fs-medium  fc-secondary fw-semi-bold ">University of Toronto - Fall Student Housing</p> <!-- <p class="fs-24 fc-primary d-inline float-right"><span class="fs-medium">PKR</span> 10,000</p> --></div>
                                        <p class="fs-13 ">Hi, I am looking for a room partner on monthly rent. I am a Digital Marketing Manager and looking for a good single paying guest.</p>
                                        <ul class="fs-13 ">
                                            <li>Rent Rs 6000</li>
                                            <li>Adv Rs 5000</li>
                                            <li>Room Location in front of Karachi Air Port</li>
                                        </ul>
                                        <div class="ptpx-20 foot">
                                            <a class="btn-detail one " href="">Gallery</a>
                                            <a class="btn-detail one green" href="">Location</a>
                                            <a class="btn-detail two orange" href="">Features &amp; Amenities</a>
                                        </div>
                                        <div class="right top-right">
                                            <!-- <p class="added" style="background: url('{{$imgscr}}big-sprite.png') no-repeat -24px -1px;">Added: &nbsp;1 week ago </p> -->
                                            <ul class="tag unstyled inline">
                                                <li><p class="fs-small fc-lgrey">Added: 1 week ago</p></li>
                                                <li><span class="tag-1"></span></li> 
                                            </ul>
                                            <h4 class="price fw-semi-bold fc-primary ff-primary"><p class=" fs-large d-inline-block">PKR</p> 10,000</h4>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <figure class="adver-shadow">
                        <img class="w-100" src="{{$imgscr}}advertisement/advert-1.jpg" alt="advertisement">
                    </figure>
                    <div class=" shred-box-2">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}sa-2/img-2.jpg" alt="*">
                                        <ul class="unstyled inline">
                                            <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                            </li>
                                            <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                        </ul>
                                </div>
                            </div>
                            <div class="col-md-7 nopadd-left">
                                <div class="content">
                                    <div class="main">
                                        <div class=" top-text d-inline-block"><p class=" fs-medium  fc-secondary fw-semi-bold ">University of Toronto - Fall Student Housing</p> <!-- <p class="fs-24 fc-primary d-inline float-right"><span class="fs-medium">PKR</span> 10,000</p> --></div>
                                        <p class="fs-13 ">Hi, I am looking for a room partner on monthly rent. I am a Digital Marketing Manager and looking for a good single paying guest.</p>
                                        <ul class="fs-13 ">
                                            <li>Rent Rs 6000</li>
                                            <li>Adv Rs 5000</li>
                                            <li>Room Location in front of Karachi Air Port</li>
                                        </ul>
                                        <div class="ptpx-20 foot">
                                            <a class="btn-detail one " href="">Gallery</a>
                                            <a class="btn-detail one green" href="">Location</a>
                                            <a class="btn-detail two orange" href="">Features &amp; Amenities</a>
                                        </div>
                                        <div class="right top-right">
                                            <!-- <p class="added" style="background: url('{{$imgscr}}big-sprite.png') no-repeat -24px -1px;">Added: &nbsp;1 week ago </p> -->
                                            <ul class="tag unstyled inline">
                                                <li><p class="fs-small fc-lgrey">Added: 1 week ago</p></li>
                                                <li><span class="tag-1"></span></li> 
                                            </ul>
                                            <h4 class="price fw-semi-bold fc-primary ff-primary"><p class=" fs-large d-inline-block">PKR</p> 10,000</h4>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" shred-box-2">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}sa-2/img-3.jpg" alt="*">
                                        <ul class="unstyled inline">
                                            <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                            </li>
                                            <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                        </ul>
                                </div>
                            </div>
                            <div class="col-md-7 nopadd-left">
                                <div class="content">
                                    <div class="main">
                                        <div class=" top-text d-inline-block"><p class=" fs-medium  fc-secondary fw-semi-bold ">University of Toronto - Fall Student Housing</p> <!-- <p class="fs-24 fc-primary d-inline float-right"><span class="fs-medium">PKR</span> 10,000</p> --></div>
                                        <p class="fs-13 ">Hi, I am looking for a room partner on monthly rent. I am a Digital Marketing Manager and looking for a good single paying guest.</p>
                                        <ul class="fs-13 ">
                                            <li>Rent Rs 6000</li>
                                            <li>Adv Rs 5000</li>
                                            <li>Room Location in front of Karachi Air Port</li>
                                        </ul>
                                        <div class="ptpx-20 foot">
                                            <a class="btn-detail one " href="">Gallery</a>
                                            <a class="btn-detail one green" href="">Location</a>
                                            <a class="btn-detail two orange" href="">Features &amp; Amenities</a>
                                        </div>
                                        <div class="right top-right">
                                            <!-- <p class="added" style="background: url('{{$imgscr}}big-sprite.png') no-repeat -24px -1px;">Added: &nbsp;1 week ago </p> -->
                                            <ul class="tag unstyled inline">
                                                <li><p class="fs-small fc-lgrey">Added: 1 week ago</p></li>
                                                <li><span class="tag-1"></span></li> 
                                            </ul>
                                            <h4 class="price fw-semi-bold fc-primary ff-primary"><p class=" fs-large d-inline-block">PKR</p> 10,000</h4>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" shred-box-2">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}sa-2/img-4.jpg" alt="*">
                                        <ul class="unstyled inline">
                                            <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                            </li>
                                            <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                        </ul>
                                </div>
                            </div>
                            <div class="col-md-7 nopadd-left">
                                <div class="content">
                                    <div class="main">
                                        <div class=" top-text d-inline-block"><p class=" fs-medium  fc-secondary fw-semi-bold ">University of Toronto - Fall Student Housing</p> <!-- <p class="fs-24 fc-primary d-inline float-right"><span class="fs-medium">PKR</span> 10,000</p> --></div>
                                        <p class="fs-13">Hi, I am looking for a room partner on monthly rent. I am a Digital Marketing Manager and looking for a good single paying guest.</p>
                                        <ul class="fs-13">
                                            <li>Rent Rs 6000</li>
                                            <li>Adv Rs 5000</li>
                                            <li>Room Location in front of Karachi Air Port</li>
                                        </ul>
                                        <div class="ptpx-20 foot">
                                            <a class="btn-detail one " href="">Gallery</a>
                                            <a class="btn-detail one green" href="">Location</a>
                                            <a class="btn-detail two orange" href="">Features &amp; Amenities</a>
                                        </div>
                                        <div class="right top-right">
                                            <!-- <p class="added" style="background: url('{{$imgscr}}big-sprite.png') no-repeat -24px -1px;">Added: &nbsp;1 week ago </p> -->
                                            <ul class="tag unstyled inline">
                                                <li><p class="fs-small fc-lgrey">Added: 1 week ago</p></li>
                                                <li><span class="tag-1"></span></li> 
                                            </ul>
                                            <h4 class="price fw-semi-bold fc-primary ff-primary"><p class=" fs-large d-inline-block">PKR</p> 10,000</h4>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" shred-box-2">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}sa-2/img-1.jpg" alt="*">
                                        <ul class="unstyled inline">
                                            <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                            </li>
                                            <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                            <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                        </ul>
                                </div>
                            </div>
                            <div class="col-md-7 nopadd-left">
                                <div class="content">
                                    <div class="main">
                                        <div class=" top-text d-inline-block"><p class=" fs-medium  fc-secondary fw-semi-bold ">University of Toronto - Fall Student Housing</p> <!-- <p class="fs-24 fc-primary d-inline float-right"><span class="fs-medium">PKR</span> 10,000</p> --></div>
                                        <p class="fs-13 ">Hi, I am looking for a room partner on monthly rent. I am a Digital Marketing Manager and looking for a good single paying guest.</p>
                                        <ul class="fs-13 ">
                                            <li>Rent Rs 6000</li>
                                            <li>Adv Rs 5000</li>
                                            <li>Room Location in front of Karachi Air Port</li>
                                        </ul>
                                        <div class="ptpx-20 foot">
                                            <a class="btn-detail one " href="">Gallery</a>
                                            <a class="btn-detail one green" href="">Location</a>
                                            <a class="btn-detail two orange" href="">Features &amp; Amenities</a>
                                        </div>
                                        <div class="right top-right">
                                            <!-- <p class="added" style="background: url('{{$imgscr}}big-sprite.png') no-repeat -24px -1px;">Added: &nbsp;1 week ago </p> -->
                                            <ul class="tag unstyled inline">
                                                <li><p class="fs-small fc-lgrey">Added: 1 week ago</p></li>
                                                <li><span class="tag-1"></span></li> 
                                            </ul>
                                            <h4 class="price fw-semi-bold fc-primary ff-primary"><p class=" fs-large d-inline-block">PKR</p> 10,000</h4>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
             @include('layout./partials.side-advertisement-new')

 
            
        </div>
   
    


@endsection