@extends('layout.master')
@section('title', 'help-and-support')

@section('content')





<section class=" sec-padding --small bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-0 mbpx-10">
          <li><a href="/">Home </a></li>
          <li> Help And Support</li>
        </ul>
          
          <div class="row  ">
            
            <div class="col-lg-9 ">
              <div class="bg-white  terms help">
                <h6 class="heading-main fw-semi-bold d-small-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft mtpx-15 mbpx-0" >Land Track Help & Support</h6>
                <div class="accordion">
                  <ul class="unstyled">
                    <li>
                      <h5 class="fs-medium tt-uppercase fw-semi-bold"> 1.  How to add a new user under your agency account?</h5>
                      <div class="fs-13 fc-dgrey">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
                    </li>
                    <li>
                      <h5 class="fs-medium tt-uppercase fw-semi-bold">2.  How to create and manage teams? </h5>
                      <div class="fs-13 fc-dgrey">Nulla facilisi. Proin sodales dolor in odio lacinia, ut venenatis massa lobortis. Morbi congue dignissim nisi gravida consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed egestas diam. Nunc ut mauris tempus, rutrum massa vel, pellentesque velit. Nullam eget diam sit amet diam pretium scelerisque. Nunc sed odio nisi. Nunc odio est, rhoncus vitae risus a, sagittis ultrices mauris. Fusce scelerisque posuere pulvinar.</div>
                    </li>
                    
                    <li>
                      <h5 class="fs-medium tt-uppercase fw-semi-bold">3.  How to manage users under your agency account?</h5>
                      <div class="fs-13 fc-dgrey">Nulla facilisi. Proin sodales dolor in odio lacinia, ut venenatis massa lobortis. Morbi congue dignissim nisi gravida consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed egestas diam. Nunc ut mauris tempus, rutrum massa vel, pellentesque velit. Nullam eget diam sit amet diam pretium scelerisque. Nunc sed odio nisi. Nunc odio est, rhoncus vitae risus a, sagittis ultrices mauris. Fusce scelerisque posuere pulvinar.</div>
                    </li>
                    <li>
                      <h5 class="fs-medium tt-uppercase fw-semi-bold">4.  What are teams?</h5>
                      <div class="fs-13 fc-dgrey">Nulla facilisi. Proin sodales dolor in odio lacinia, ut venenatis massa lobortis. Morbi congue dignissim nisi gravida consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed egestas diam. Nunc ut mauris tempus, rutrum massa vel, pellentesque velit. Nullam eget diam sit amet diam pretium scelerisque. Nunc sed odio nisi. Nunc odio est, rhoncus vitae risus a, sagittis ultrices mauris. Fusce scelerisque posuere pulvinar.</div>
                      
                    </li>
                    <li>
                      <h5 class="fs-medium tt-uppercase fw-semi-bold">5.  What are user privileges and how you can define them for different users?</h5>
                      <div class="fs-13 fc-dgrey">Nulla facilisi. Proin sodales dolor in odio lacinia, ut venenatis massa lobortis. Morbi congue dignissim nisi gravida consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed egestas diam. Nunc ut mauris tempus, rutrum massa vel, pellentesque velit. Nullam eget diam sit amet diam pretium scelerisque. Nunc sed odio nisi. Nunc odio est, rhoncus vitae risus a, sagittis ultrices mauris. Fusce scelerisque posuere pulvinar.</div>
                    </li>

                    <li>
                      <h5 class="fs-medium tt-uppercase fw-semi-bold">6.  What are users (agency staff)?</h5>
                      <div class="fs-13 fc-dgrey">Nulla facilisi. Proin sodales dolor in odio lacinia, ut venenatis massa lobortis. Morbi congue dignissim nisi gravida consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed egestas diam. Nunc ut mauris tempus, rutrum massa vel, pellentesque velit. Nullam eget diam sit amet diam pretium scelerisque. Nunc sed odio nisi. Nunc odio est, rhoncus vitae risus a, sagittis ultrices mauris. Fusce scelerisque posuere pulvinar.</div>
                    </li>
                    
                  </ul>
                </div>
              </div>
              
              <figure class="adver-shadow mtpx-20">
                  <img class="w-100" src="{{$imgscr}}advertisement/advert-1.jpg" alt="advertisement">
              </figure>
            </div>
            <div class="col-lg-3">
              <div class="advertise-sect doc bg-white">
                <div class="head">
                  <p class="fs-17 fc-primary fw-semi-bold tt-uppercase"><span class="icon-icon-50"></span>HELP CENTER</p>
                </div>
                <ul class="unstyled acco">
                  <li class="drop"><a class="fw-semi-bold" href="javascript:;">Banner Advertising</a>
                    <ul class="unstyled dotted">
                      <li><a href="javascript:;">Leaderboard</a></li>
                      <li><a href="javascript:;">Site Wide Right Banner</a></li>
                      <li><a href="javascript:;">Splash Banner</a></li>
                      <li><a href="javascript:;">Middle Banner Home</a></li>
                      <li><a href="javascript:;">Middle Banner Search</a></li>
                      <li><a href="javascript:;">Middle Banner Category</a></li>
                      <li><a href="javascript:;">Wallpaper Takeover</a></li>
                    </ul>
                  </li>
                  <li class="drop"><a class="fw-semi-bold" href="javascript:;">Property Advertising</a>
                    <ul class="unstyled dotted">
                      <li><a href="javascript:;">October (24)</a></li>
                      <li><a href="javascript:;">September (52)</a></li>
                    </ul>
                  </li>
                  
                  
                  
                </ul>
              </div>
            </div>
            <div class="col-lg-3 hidden-md-down">
              
              <img class="mtpx-20 " src="{{$imgscr}}agents/new/img1.jpg" alt="Agents">

              <img class="mtpx-20" src="{{$imgscr}}agents/new/img2.jpg" alt="Agents">

              <img class="mtpx-20" src="{{$imgscr}}agents/new/img3.jpg" alt="Agents">

              <img class="mtpx-20" src="{{$imgscr}}agents/new/img4.jpg" alt="Agents">

              <img class="mtpx-20" src="{{$imgscr}}agents/new/img5.jpg" alt="Agents">
            </div>
            <div class="col-lg-9 col-nopadd">
              
            </div>
     
          </div>  </div>  </div>  </div>  </div>  </div>
            </div>  </div>  </div>  </div>  </div>
              </div>  </div>  </
                </div>  </div>  </div>
                 </div>  </div>  </div>
                </div>  </div>  </div>  </div>
                  </div>  </div>  </div>
           
          </div>  </div>  </div>  </div>  </div>  </div>
            </div>  </div>  </div>  </div>  </div>
              </div>  </div>  </
                </div>  </div>  </div>
                 </div>  </div>  </div>
                </div>  </div>  </div>  </div>
                  </div>  </div>  </div>       
 

@endsection