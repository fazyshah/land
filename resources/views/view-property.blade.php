@extends('layout.master')
@section('title', 'View Properties')

@section('content')

<div class="super-banner">
<div class="main-slider">
<section class="hero-banner small" style="background: url({{url('web_asset/images/banner-1.jpg')}});">


</section>

</div>




</div>



<section class="second-banner buy pro-detail sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <ul class="breadcrumb mbpx-0">
          <li><a href="/">Home</a></li>
          <li><a href="../buy">Buy</a></li>
          <li>Buy House Detail</li>
        </ul>
        <div class="row bg-white detail-shared">
            <div class="col-md-12 col-nopadd">
                <div class="first">
                    <p class=""><span class="icon-bookmark1"></span> Save to Facebook</p>
                    <ul class="unstyled inline ">
                        <li><a href=""><span class="icon-external-link"></span></a></li>
                        <li><a href=""><span class="icon-heart1"></span></a></li>
                        <li><a href=""><span class="icon-printer-text"></span></a></li>
                    </ul>
                </div>
                <div class="second-part pbpx-10 ptpx-15">
                    <div class="row">

                        <div class="col-md-9 matchheight">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="fs-medium fc-secondary fw-semi-bold">{{$property->title}}</p>
                                    <p class="fs-small">{{$property->propertyArea->name.', '.$property->propertyArea->areaCity->name}}</p>
                                </div>
                            </div>
                            <div class="row ptpx-5">
                                <div class="tip">
                                    <div class="col-md-10 ">
                                        <div class="big-slider-2 ">

                                            @if($property->img != "")
                                                @foreach(explode('|', $property->img) as $img)
                                                    <div>
                                                        <img src="{{ url('web_asset/images/properties/'.$img) }}" alt="{{$property->title}}">
                                                    </div>
                                                @endforeach
                                            @else
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-1.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-2.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-3.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-4.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-5.jpg')}}" alt="*">
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-md-2 col-nopadd ptpx-20">
                                        <div class="small-slider-2  ptpx-5">
                                            @if($property->img != "")
                                                @foreach(explode('|', $property->img) as $img)
                                                    <div>
                                                        <img src="{{ url('web_asset/images/properties/'.$img) }}" alt="{{$property->title}}">
                                                    </div>
                                                @endforeach
                                            @else
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-1.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-2.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-3.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-4.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{url('web_asset/images/slider/img-5.jpg')}}" alt="*">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row ptpx-5">
                                <div class="col-md-12">
                                   <ul class="sli-bottom unsyled inline">
                                        <li class="fc-primary">PKR <span> {{$property->price}}</span></li>
                                        <li>{{$property->beds}} Bedrooms <span class="icon-icon4"></span></li>
                                        <li>{{$property->baths}} Bathroom <span class="icon-icon2"></span></li>
                                        <li>{{$property->area}} <span class="icon-kanal"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       <div class="col-lg-3 col-md-4 col-sm-6   matchheight brdr-left">
                            <div class="right-contact">
                                <div class="head">
                                    <p class="fs-medium ta-center fc-white fw-medium ff-primary">Contact for More information</p>
                                </div>
                                <div class="bottom">
                                    <form action="" class="jform validate right-form ">
                                        <div class="control-group">

                                            <input type="text" placeholder="Name" name="cn" class="required" minlength="2" maxlength="60">
                                        </div>
                                        <div class="control-group">

                                            <input type="text" name="em" class="required email" placeholder="Email" maxlength="60">
                                        </div>
                                        <div class="control-group clearfix">
                                                <input type="text" name="pn" id="phone" class="number required" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />

                                        </div>
                                        <div class="control-group">
                                            <textarea name="msg" class="required" placeholder="Message" minlength="2" maxlength="1000"></textarea>
                                        </div>
                                        <div class="control-group">

                                            <input type="submit" value="Call" class="fw-normal tt-uppercase w-49 btn-detail orange">
                                            <input type="submit" value="Send Email" class="fw-normal tt-uppercase w-49 btn-detail blue">
                                        </div>
                                        <div class="control-group last">
                                            <div class="input">
                                                <input type="checkbox" id="informed">
                                            </div>
                                            <div class="lbl">
                                                <label class="fs-xsmall fw-light " for="informed">Keep me Informed about similar properties By Submitting this Form.I agree to <a class="fc-secondary fw-semi-bold" href="">Terms Of Use</a></label>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row ptpx-20">

            <div class="col-lg-9 col-sm-12 col-nopadd">
               <div class="roomate-desc ptpx-30 bg-white ">
                   <h6 class="heading-main fw-semi-bold abou-1 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Overview</h6>

                   <div class="content ">
                        <p class="fw-semi-bold fc-primary tt-uppercase">Details</p>
                        <div class="border-dashed mtpx-0 mbpx-30 pbpx-25">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="prop-table buy-pa w-100 mbpx-15 mtpx-15">
                                        <tr>
                                            <td>Type</td>
                                            <td>{{$property->propertyType->type}} </td>
                                        </tr>
                                        <tr>
                                            <td>Price</td>
                                            <td>PKR {{$property->price}}</td>
                                        </tr>
                                        <tr>
                                            <td>Location </td>
                                            <td>{{$property->propertyArea->name.', '.$property->propertyArea->areaCity->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Baths </td>
                                            <td>{{$property->baths}}</td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="prop-table buy-pa w-100 mbpx-15 mtpx-15">
                                        <tr>
                                            <td>Area</td>
                                            <td>{{$property->area}} </td>
                                        </tr>
                                        <tr>
                                            <td>Purpose</td>
                                            <td>{{$property->propertyPurpose->purpose}}</td>
                                        </tr>
                                        <tr>
                                            <td>Beds </td>
                                            <td>{{$property->beds}}</td>
                                        </tr>
                                        <tr>
                                            <td>Added </td>
                                            <td>{{ $property->created_at->diffForHumans() }} </td>
                                        </tr>

                                    </table>
                                </div>

                            </div>
                        </div>

                        <div id='grow'>
                            <div class='measuringWrapper'>
                                <div class="text">
                                    <p class="fw-semi-bold fc-primary tt-uppercase">Description</p>
                                     <p class="fc-grey fs-13">
                                        SUBHAN ESTATE OFFERING YOU One Kanal Brand New Bungalow For Sale In DHA Phase 5 lahore Design By Mazhar Munir<br><br>
                                    </p>
                                    <ul class="unstyled d-inline-block fs-13">
                                        <li>Property Overview</li>
                                        <li>05 Master Size Bed Rooms with attached bath</li>
                                        <li>2 huge size TV lounge</li>
                                        <li>3 kitchens</li>
                                        <li>4 parking space Double Terrace </li>
                                        <li>All baths are equipped with Grohi imported fittings</li>
                                        <li>House is located near park/mosque</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <input class="btn-detail" type="button" onclick="growDiv()" value="View more +" id="more-button">




                    </div>
                    <br>
                    <div class="mtpx-20">
                        <h6 class="heading-main fw-semi-bold abou-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Amenities</h6>
                        <p class="fc-primary fw-semi-bold ">MAIN FEATURES</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-calendar1"></span> Built in year: <b>2018</b></li>
                            <li class="w-33"><span class="icon-icon-8"></span> Parking Spaces: <b>4 Car</b></li>
                            <li class="w-33"><span class="icon-glazed"></span> Double Glazed Windows: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-10"></span> Central Air Conditioning: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-11"></span> Central Heating: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-12"></span> Flooring: <b>Wooden</b></li>
                            <li class="w-33"><span class="icon-icon-13"></span> Electricity Backup: <b>UPS</b></li>
                            <li class="w-33"><span class="icon-icon-14"></span> Waste Disposal: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-15"></span> Floors: <b>2 Floor</b></li>
                        </ul>

                        <p class="fc-primary fw-semi-bold ">ROOMS</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-icon-16"></span> Bedrooms: <b>5</b></li>
                            <li class="w-33"><span class="icon-bathroom"></span> Bathrooms: <b>6</b></li>
                            <li class="w-33"><span class="icon-icon-17"></span> Servant Quarters: <b>2</b></li>
                            <li class="w-33"><span class="icon-icon-18"></span> Drawing Room: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-19"></span> Dining Room: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-21"></span> Kitchens: <b>3</b></li>
                            <li class="w-33"><span class="icon-icon-20"></span> Study Room: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-22"></span> Prayer Room: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-23"></span> Powder Room: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-24"></span> Store Rooms: <b>2</b></li>
                            <li class="w-33"><span class="icon-icon-25"></span> Steam Room: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-26"></span> Lounge or Sitting Room: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-27"></span> Laundry Room: <b>Yes</b></li>

                        </ul>

                        <p class="fc-primary fw-semi-bold ">BUSINESS AND COMMUNICATION</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33 prpx-0"><span class="icon-icon-28"></span> Broadband Internet Access: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-29"></span> Satellite or Cable TV Ready: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-30"></span> Intercom: <b>Yes</b></li>
                        </ul>

                        <p class="fc-primary fw-semi-bold ">HEALTHCARE RECREATIONAL</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33 prpx-0"><span class="icon-icon-31"></span> Lawn or Garden <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-32"></span> Sauna: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-icon-33"></span> Jacuzzi: <b>Yes</b></li>
                        </ul>

                        <p class="fc-primary fw-semi-bold ">OTHER FACILITIES</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-maintenance-staff"></span> Maintenance Staff: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-security-staff"></span> Security Staff: <b>Yes</b></li>
                        </ul>



                    </div>

                    <div class="mtpx-20">
                        <h6 class="heading-main fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Location &amp; Commute</h6>
                        <p class="fs-large fc-primary tt-uppercase fw-semi-bold pbpx-5">COMMUTE AND NEARBY</p>
                        <div class="map-direct" id="zoomife">
                            <iframe src="https://www.google.com/maps/d/embed?mid=1wMZLD-KoIBt-zG0r8ziRkZeqgUA&hl=en" width="100%" height="480"></iframe>

                        </div>

                    </div>



                    <div class="mtpx-50">
                        <h6 class="heading-main fw-semi-bold small-6 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">home finance calculator</h6>
                        <p class="fc-primary fw-semi-bold ptpx-20 pbpx-10">Bank </p>
                        <figure>
                            <img class="w-100" src="{{url('web_asset/images/bank-chart.jpg')}}" alt="">
                        </figure>


                    </div>

                    <div class="mtpx-50">
                        <h6 class="heading-main fw-semi-bold abou-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">price index</h6>
                        <p class=" tt-uppercase fc-grey pbpx-15 fw-semi-bold ptpx-20">Lahore DHA Defence, 1 Kanal Houses Price Index</p>
                        <figure>
                            <img class="w-100" src="{{url('web_asset/images/price-chart.jpg')}}" alt="">
                        </figure>
                    </div>

                    <div class="mtpx-50">
                        <p class=" fs-large fc-grey pbpx-25 fw-semi-bold ptpx-20">Trends - Most Searched Locations in DHA Defence</p>
                        <figure>
                            <img class="w-100" src="{{url('web_asset/images/search-location.jpg')}}" alt="">
                        </figure>
                    </div>



               </div>
            </div>
            <div class="col-lg-3 col-sm-12 nopadd-right">
                <div class="estate-links">
                    <p class="usef fw-bold tt-uppercase fc-primary fs-large">TAKAFUL LINKS</p>
                    <ul class="unstyled inline grid-block --type-two-blocks ">
                        <li>
                            <img class="ptpx-15" src="{{url('web_asset/images/estates/takaful-estate.jpg')}}" alt="*">
                        </li>
                        <li class="brdr-left">
                            <ul class="unstyled insider">
                                <li><a href="javascript:;"><span class="icon-person"><sup class="icon-dot-single"></sup></span>Shaikh Asif</a></li>
                                <li><a href="javascript:;"><span class="icon-phone_in_talk"></span>0210602168</a></li>
                                <li><a href="javascript:;"><span class="icon-messages"></span>Chat Now</a></li>
                                <li><a class="btn-detail green" href="javascript:;">View Profile</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="category-box forum mtpx-15  bg-white">
                    <div class="head">
                      <p class="fs-default fc-secondary fw-semi-bold tt-uppercase "><span class="icon-icon-66 fc-primary"></span> Legal Experts In your Area</p>
                    </div>
                    <ul class="ask-expert unstyled">
                        @foreach($legal as $legals)
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{url('web_asset/images/top-expert.png')}}" alt="*">
                            </figure>
                            <div class="content">

                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">
                                        {{$legals->name}}</p>
                                    <p class="fs-11 fc-grey lh-medium">{{$legals->description}}</p>

                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">test</p></li>
                                      <li><img src="{{url('web_asset/images/rating.png')}}" alt="*"></li>
                                    </ul>

                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>

                            </div>
                        </li>
                        @endforeach
                    </ul>

                </div>

                <div class="useful-links mtpx-15">
                    <p class="usef fw-bold tt-uppercase fc-primary fs-large">USEFUL LINKS</p>
                    <ul class="unstyled">
                        <li><a href="">Property for sale in Karachi</a></li>
                        <li><a href="">Property for sale in Jamshed Town</a></li>
                        <li><a href="">Property for sale in PECHS</a></li>
                        <li><a href="">Upper Portions for sale in Karachi</a></li>
                        <li><a href="">Upper Portions for sale in Jamshed Town</a></li>
                        <li><a href="">Upper Portions for sale in PECHS</a></li>
                    </ul>
                </div>


            </div>

        </div>

    </div>
</section>





<!-- FEATURED PROJECTS -->
<div class="container">
<h6 class="heading-main small-2 fw-semi-bold ff-primary lh-normal  fc-nblack tt-uppercase wow slideInLeft">FEATURED Property</h6>

<div class="shared-slider">
    @forelse ($featured_properties as $fproperty)
        <div class="item matchheight">
            <div class="prop-new-box">
                @php
                    if (preg_match("/\|/", $fproperty->img)){
                        $fp_img = explode('|', $fproperty->img);
                        $fp_img = $fproperty[0];
                    }elseif(!empty($fproperty->img)){
                        $fp_img = $fproperty->img;
                    }else{
                        $fp_img = 'no-image.png';
                    }
                @endphp

                <div class="top" style="background: url({{ asset('web_asset/images/properties/'.$fp_img) }});">

                    <div class="ico">
                        <a class="btn-s" href="javascript:;">Featured</a>
                        <a class="btn-s orange" href="javascript:;">{{$fproperty->propertyPurpose->purpose}}</a>
                        <ul class="unstyled inline">
                            <li><a href="javascript:;"><img src="{{ asset('web_asset/images/hot.png') }}" alt=""></a></li>
                            <li><a href="javascript:;"><img src="{{ asset('web_asset/images/star.png') }}" alt=""></a></li>
                            <li><a href="javascript:;"><img src="{{ asset('web_asset/images/verified.png') }}" alt=""></a></li>
                        </ul>
                    </div>
                    <div class="profile">
                        <div class="left">
                            <img src="{{ asset('web_asset/images/user/img-1.png') }}" alt="*">
                            <p class="fc-white fs-medium lh-medium">{{$fproperty->propertyUser->name}} <span class="d-block fs-small">xyz Real Estate</span></p>
                        </div>
                        <div class="float-right ta-center">
                            <span class="icon-calendar3 fc-white d-block fs-large"></span>
                            <p class="fc-white fs-small">{{ \Carbon\Carbon::parse($fproperty['created_at'])->diffForHumans() }}</p>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <p class="fs-medium fc-nblack fw-semi-bold">{{$fproperty->title}}</p>
                    <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>{{$fproperty->propertyArea->name.', '.$fproperty->propertyArea->areaCity->name}}</p>
                    <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>{{$fproperty->price}}</p>
                </div>
                <div class="bottom">
                    <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                        <li>{{$fproperty->area}}</li>
                        <li>{{$fproperty->beds}} Beds</li>
                        <li>{{$fproperty->baths}} Baths</li>
                    </ul>
                </div>
                <div class="butons bord grid-block --type-two-blocks">
                    <a class="btn-orange fs-13 w-100" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                    <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                </div>
            </div>
        </div>
    @empty
        <p>No Fetured Properties found...!!</p>
    @endforelse
</div>

<figure class="adver-shadow mtpx-30">
    <img class="w-100" src="{{ asset('web_asset/images/advertisement/advert-1.jpg') }}" alt="advertisement">
</figure>

</div>
@endsection

