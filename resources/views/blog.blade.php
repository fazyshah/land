@extends('layout.master')
@section('title', 'blog')

@section('content')




<section class=" sec-padding --small bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-10 mbpx-5">
          <li><a href="/">Home </a></li>
          <li> Blog</li>
        </ul>

        <div class="row">
          <div class="col-lg-9">
            <div class="row blog-bbox">
                <div class="col-md-5 col-sm-5">
                  <figure>
                    <img src="{{$imgscr}}main-blog/blog-1.jpg" alt="*">
                  </figure>
                </div>
                <div class="col-md-7 col-sm-7 nopadd-left">
                  <div class="text-side">
                    <div class="date-tag">
                       <p class="fc-white fs-small lh-nmedium">Sep<span class="d-block">17</span></p>
                    </div>
                    <p class="fs-medium fw-semi-bold fc-secondary">Latest Updates on DHA Rahbar Phase II</p>
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="ptpx-12 fs-small fc-grey lh-large">HA Rahbar Phase II is one of those few housing schemes where plots of smaller sizes are available and rates are consistently increasing. Due to smaller plot sizes it has open doors for investors with limited budget who want to live in DHA Lahore. Consequently, the demand is high and DHA Rahbar Phase II and hence the hiked prices. DHA Rahbar Phase II encompasses of nine blocks that areh F, G, H, J, K, L, M, N, and P. Project Location: The project is situated at a decent location of Defence Road.</p>
                    <div class="ta-right">
                      <a class="btn-detail " href="../blog-detail">Read More <span class="icon-keyboard_arrow_right"></span></a>  
                    </div>
                    
                  </div>
                  
                </div>
            </div>

            <div class="row blog-bbox">
                <div class="col-md-5 col-sm-5">
                  <figure>
                    <img src="{{$imgscr}}main-blog/blog-2.jpg" alt="*">
                  </figure>
                </div>
                <div class="col-md-7 col-sm-7 nopadd-left">
                  <div class="text-side">
                    <div class="date-tag">
                       <p class="fc-white fs-small lh-nmedium">Sep<span class="d-block">17</span></p>
                    </div>
                    <p class="fs-medium fw-semi-bold fc-secondary">Latest Updates on DHA Rahbar Phase II</p>
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="ptpx-12 fs-small fc-grey lh-large">HA Rahbar Phase II is one of those few housing schemes where plots of smaller sizes are available and rates are consistently increasing. Due to smaller plot sizes it has open doors for investors with limited budget who want to live in DHA Lahore. Consequently, the demand is high and DHA Rahbar Phase II and hence the hiked prices. DHA Rahbar Phase II encompasses of nine blocks that areh F, G, H, J, K, L, M, N, and P. Project Location: The project is situated at a decent location of Defence Road.</p>
                    <div class="ta-right">
                      <a class="btn-detail " href="../blog-detail">Read More <span class="icon-keyboard_arrow_right"></span></a>  
                    </div>
                    
                  </div>
                  
                </div>
            </div>

            <div class="row blog-bbox">
                <div class="col-md-5 col-sm-5">
                  <figure>
                    <img src="{{$imgscr}}main-blog/blog-3.jpg" alt="*">
                  </figure>
                </div>
                <div class="col-md-7 col-sm-7 nopadd-left">
                  <div class="text-side">
                    <div class="date-tag">
                       <p class="fc-white fs-small lh-nmedium">Sep<span class="d-block">17</span></p>
                    </div>
                    <p class="fs-medium fw-semi-bold fc-secondary">Latest Updates on DHA Rahbar Phase II</p>
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="ptpx-12 fs-small fc-grey lh-large">HA Rahbar Phase II is one of those few housing schemes where plots of smaller sizes are available and rates are consistently increasing. Due to smaller plot sizes it has open doors for investors with limited budget who want to live in DHA Lahore. Consequently, the demand is high and DHA Rahbar Phase II and hence the hiked prices. DHA Rahbar Phase II encompasses of nine blocks that areh F, G, H, J, K, L, M, N, and P. Project Location: The project is situated at a decent location of Defence Road.</p>
                    <div class="ta-right">
                      <a class="btn-detail " href="../blog-detail">Read More <span class="icon-keyboard_arrow_right"></span></a>  
                    </div>
                    
                  </div>
                  
                </div>
            </div>

            <div class="row blog-bbox">
                <div class="col-md-5 col-sm-5">
                  <figure>
                    <img src="{{$imgscr}}main-blog/blog-4.jpg" alt="*">
                  </figure>
                </div>
                <div class="col-md-7 col-sm-7 nopadd-left">
                  <div class="text-side">
                    <div class="date-tag">
                       <p class="fc-white fs-small lh-nmedium">Sep<span class="d-block">17</span></p>
                    </div>
                    <p class="fs-medium fw-semi-bold fc-secondary">Latest Updates on DHA Rahbar Phase II</p>
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="ptpx-12 fs-small fc-grey lh-large">HA Rahbar Phase II is one of those few housing schemes where plots of smaller sizes are available and rates are consistently increasing. Due to smaller plot sizes it has open doors for investors with limited budget who want to live in DHA Lahore. Consequently, the demand is high and DHA Rahbar Phase II and hence the hiked prices. DHA Rahbar Phase II encompasses of nine blocks that areh F, G, H, J, K, L, M, N, and P. Project Location: The project is situated at a decent location of Defence Road.</p>
                    <div class="ta-right">
                      <a class="btn-detail " href="../blog-detail">Read More <span class="icon-keyboard_arrow_right"></span></a>  
                    </div>
                    
                  </div>
                  
                </div>
            </div>

            <div class="row blog-bbox">
                <div class="col-md-5 col-sm-5">
                  <figure>
                    <img src="{{$imgscr}}main-blog/blog-5.jpg" alt="*">
                  </figure>
                </div>
                <div class="col-md-7 col-sm-7 nopadd-left">
                  <div class="text-side">
                    <div class="date-tag">
                       <p class="fc-white fs-small lh-nmedium">Sep<span class="d-block">17</span></p>
                    </div>
                    <p class="fs-medium fw-semi-bold fc-secondary">Latest Updates on DHA Rahbar Phase II</p>
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="ptpx-12 fs-small fc-grey lh-large">HA Rahbar Phase II is one of those few housing schemes where plots of smaller sizes are available and rates are consistently increasing. Due to smaller plot sizes it has open doors for investors with limited budget who want to live in DHA Lahore. Consequently, the demand is high and DHA Rahbar Phase II and hence the hiked prices. DHA Rahbar Phase II encompasses of nine blocks that areh F, G, H, J, K, L, M, N, and P. Project Location: The project is situated at a decent location of Defence Road.</p>
                    <div class="ta-right">
                      <a class="btn-detail " href="../blog-detail">Read More <span class="icon-keyboard_arrow_right"></span></a>  
                    </div>
                    
                  </div>
                  
                </div>
            </div>

            <div class="row blog-bbox">
                <div class="col-md-5 col-sm-5">
                  <figure>
                    <img src="{{$imgscr}}main-blog/blog-6.jpg" alt="*">
                  </figure>
                </div>
                <div class="col-md-7 col-sm-7 nopadd-left">
                  <div class="text-side">
                    <div class="date-tag">
                       <p class="fc-white fs-small lh-nmedium">Sep<span class="d-block">17</span></p>
                    </div>
                    <p class="fs-medium fw-semi-bold fc-secondary">Latest Updates on DHA Rahbar Phase II</p>
                    <ul class="unstyled inline">
                      <li><a href="javascript:;">5 Marla House</a></li>
                      <li><a href="javascript:;">Mahzaib Fatima</a></li>
                      <li><a href="javascript:;">Leave a comment</a></li>
                    </ul>
                    <p class="ptpx-12 fs-small fc-grey lh-large">HA Rahbar Phase II is one of those few housing schemes where plots of smaller sizes are available and rates are consistently increasing. Due to smaller plot sizes it has open doors for investors with limited budget who want to live in DHA Lahore. Consequently, the demand is high and DHA Rahbar Phase II and hence the hiked prices. DHA Rahbar Phase II encompasses of nine blocks that areh F, G, H, J, K, L, M, N, and P. Project Location: The project is situated at a decent location of Defence Road.</p>
                    <div class="ta-right">
                      <a class="btn-detail " href="../blog-detail">Read More <span class="icon-keyboard_arrow_right"></span></a>  
                    </div>
                    
                  </div>
                  
                </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="lawyer-box bg-white">
              <div class="head pl-6">
                <p class="fs-17 fc-primary fw-semi-bold tt-uppercase">Subscribe Blog Alert</p>
              </div>
              <div class="bottom pbpx-15">
                <form class="email" action="">
                  <input type="email" placeholder="Enter Email">
                  <div class="ta-right">
                    <button class="btn-detail">Subscribe</button>
                  </div>
                  
                </form>
                

              </div>
            </div>

            <div class="category-box mtpx-20 bg-white">
                <div class="head">
                  <p class="fs-17 fc-primary fw-semi-bold tt-uppercase">Categories</p>
                </div>
                <ul class="unstyled acco">
                  <li class="drop"><a href="javascript:;">5 Marla House</a>
                    <ul class="unstyled">
                      <li>house</li>
                      <li>location</li>
                      <li>sq. yd</li>
                    </ul>
                  </li>
                  <li class="drop"><a href="javascript:;">LandTrack</a>
                    <ul class="unstyled">
                      <li>asdasdas asdas</li>
                    </ul>
                  </li>
                  <li class="drop"><a href="javascript:;">Landtrack.PK</a>
                    <ul class="unstyled">
                      <li>asdasdas asdas</li>
                    </ul>
                  </li>
                  <li class="drop"><a href="javascript:;">Advice</a>
                    <ul class="unstyled">
                      <li>asdasdas asdas</li>
                    </ul>
                  </li>
                  <li class="drop"><a href="javascript:;">Investments</a>
                    <ul class="unstyled">
                      <li>asdasdas asdas</li>
                    </ul>
                  </li>
                  <li class="drop"><a href="javascript:;">News & Updates</a>
                    <ul class="unstyled">
                      <li>asdasdas asdas</li>
                    </ul>
                  </li>
                  <li class="drop"><a href="javascript:;">Plots</a>
                    <ul class="unstyled">
                      <li>asdasdas asdas</li>
                    </ul>
                  </li>
                  <li class="drop"><a href="javascript:;">Tips & Tools</a>
                    <ul class="unstyled">
                      <li>asdasdas asdas</li>
                    </ul>
                  </li>
                </ul>
            </div>
  </div>  </div>  </div>  </div>  </div>  </div>  </div>  </div>  </div>
    </div>  </div>  </div>  </div>  </div>  </div>  </div>
  </div>

@endsection