@extends('layout.master')
@section('title', 'ask-an-expert')

@section('content')




<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner " style="background: url({{$imgscr}}banner-1.jpg);">
    
</section>
</div>
<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 hidden-md-down">
                    
                </div>
                <div class="col-lg-8  col-md-10 col-sm-12">
                    
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            {{ Form::open(['class' => 'form-1', 'route' => 'search']) }}

                                            <div class="control-group w-20 d-inline-block">
                                                {{ Form::select('city', $cities, old('city'), ['placeholder' => 'Select City', 'required']) }}

                                                {{-- <select name="" id="">
                                                    <option value="">Select City</option>
                                                    <option value="">asdasda</option>
                                                </select> --}}
                                            </div>
                                            <div class="control-group w-62  d-inline-block mar">
                                                <input name="keyword" type="text" placeholder="Type Location, area or keyword">
                                            </div>
                                            <div class="control-group  w-17  d-inline-block ">
                                                <button class="btn-submit" type="submit" value="">SEARCH</button>

                                            </div>
                                            <div class="filter-btn">
                                                <a class=" fs-small fw-semi-bold fc-black tt-uppercase" href="http://onlinekidstoy.com/landtrack.com/#"><span class="icon-plus-circle "></span>Add Filter</a>
                                            </div>

                                            {{ Form::close() }}

                                        </div>

                                    </div>

                                    
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>    
       
</section>
</div>



<section class="respoi sec-padding --small bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-10 mbpx-5">
          <li><a href="/">Home </a></li>
          <li> Ask An Expert</li>
        </ul>
        <div class="row">
            <div class="col-lg-3">
                <div class="lawyer-box bg-white">
                  <div class="head pl-6">
                    <p class="fs-default fc-secondary fw-semi-bold tt-uppercase pl-13"><span class="icon-icon-62 fc-primary"></span>ASK YOUR QUESTION </p>
                  </div>
                  <div class="bottom pbpx-15">
                    <form class="email" action="">
                      <textarea name="" placeholder="Get your questions answered by real estate experts..." id="" cols="30" rows="10"></textarea>
                      <div class="ta-center">
                        <button class="btn-detail ask">Ask Now</button>
                      </div>
                      
                    </form>
                    <a class="anchor-text" href="javascript:;"><span class="icon-icon-63"></span>Start New Discussion</a>
                  </div>
                </div>

                <div class="category-box forum mtpx-20 bg-white">
                    <div class="head">
                      <p class="fs-default fc-secondary fw-semi-bold tt-uppercase "><span class="icon-icon-64 fc-primary"></span> Forum Categories</p>
                    </div>
                    <ul class="unstyled acco">
                      <li class="drop"><a href="javascript:;">5 Marla House<span class="">(2911)</span></a>
                        <ul class="unstyled">
                          <li>house </li>
                          <li>location</li>
                          <li>sq. yd</li>
                        </ul>
                      </li>
                      <li class="drop"><a href="javascript:;">LandTrack<span class="">(2911)</span></a>
                        <ul class="unstyled">
                          <li>asdasdas asdas</li>
                        </ul>
                      </li>
                      <li class="drop"><a href="javascript:;">Landtrack.PK<span class="">(94)</span></a>
                        <ul class="unstyled">
                          <li>asdasdas asdas</li>
                        </ul>
                      </li>
                      <li class="drop"><a href="javascript:;">Advice<span class="">(193)</span></a>
                        <ul class="unstyled">
                          <li>asdasdas asdas</li>
                        </ul>
                      </li>
                      <li class="drop"><a href="javascript:;">Investments<span class="">(94)</span></a>
                        <ul class="unstyled">
                          <li>asdasdas asdas</li>
                        </ul>
                      </li>
                      <li class="drop"><a href="javascript:;">News & Updates<span class="">(638)</span></a>
                        <ul class="unstyled">
                          <li>asdasdas asdas</li>
                        </ul>
                      </li>
                      <li class="drop"><a href="javascript:;">Plots<span class="">(45)</span></a>
                        <ul class="unstyled">
                          <li>asdasdas asdas</li>
                        </ul>
                      </li>
                      <li class="drop"><a href="javascript:;">Tips & Tools<span class="">(2911)</span></a>
                        <ul class="unstyled">
                          <li>asdasdas asdas</li>
                        </ul>
                      </li>
                    </ul>
                </div>

                <div class="category-box forum  mtpx-20 bg-white">
                    <div class="head">
                      <p class="fs-default fc-secondary fw-semi-bold tt-uppercase "><span class="icon-icon-66 fc-primary"></span> TOP CONTRIBUTORS</p>
                    </div>
                    <ul class="ask-expert unstyled">
                        <li class="contributor-box">
                            <figure>
                                <img src="{{$imgscr}}contributors/img1.jpg" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left">
                                    <p class="fs-small fc-secondary pbpx-3">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey">Property Guru</p>
                                </div>
                                <div class="right ta-right">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box">
                            <figure>
                                <img src="{{$imgscr}}contributors/img2.jpg" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left">
                                    <p class="fs-small fc-secondary pbpx-3">Falcon</p>
                                    <p class="fs-11 fc-grey">Property Guru</p>
                                </div>
                                <div class="right ta-right">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box">
                            <figure>
                                <img src="{{$imgscr}}contributors/img3.jpg" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left">
                                    <p class="fs-small fc-secondary pbpx-3">Pakistani-1</p>
                                    <p class="fs-11 fc-grey">Property Guru</p>
                                </div>
                                <div class="right ta-right">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box">
                            <figure>
                                <img src="{{$imgscr}}contributors/img4.jpg" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left">
                                    <p class="fs-small fc-secondary pbpx-3">Sayeein</p>
                                    <p class="fs-11 fc-grey">Property Guru</p>
                                </div>
                                <div class="right ta-right">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box">
                            <figure>
                                <img src="{{$imgscr}}contributors/img5.jpg" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left">
                                    <p class="fs-small fc-secondary pbpx-3">Sheikh Abdul</p>
                                    <p class="fs-11 fc-grey">Property Guru</p>
                                </div>
                                <div class="right ta-right">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box">
                            <figure>
                                <img src="{{$imgscr}}contributors/img6.jpg" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left">
                                    <p class="fs-small fc-secondary pbpx-3">Khalid Javai</p>
                                    <p class="fs-11 fc-grey">Property Guru</p>
                                </div>
                                <div class="right ta-right">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                    </ul>
                    
                </div>
            </div>
            <div class="col-lg-6">
                <figure class="adver-shadow mbpx-20">
                    <img class="w-100" src="{{$imgscr}}expert-top.jpg" alt="advertisement">
                </figure>
                <div class="category-box forum  bg-white">
                    <div class="head">
                      <p class="fs-default fc-secondary fw-semi-bold tt-uppercase "><span class="icon-icon-65 fc-primary"></span> RECENT POSTS</p>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rec-po-box">
                        <div class="row">
                            <div class="col-md-2 nopadd-right ">
                                <figure>
                                    <img class="w-100" src="{{$imgscr}}expert-profile.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-7  ">
                               <div class="content">
                                   <p class="fs-small fc-secondary fw-semi-bold fis pbpx-3">Bahria town - ring road impact</p>
                                   <a class="fs-11 td-underline fc-primary fw-medium" href="">In Residential Schemes</a>
                                   <ul class="unstyled inline ptpx-10">
                                       <li>Latest Answer by Naveed</li>
                                       <li><span class="icon-icon-54 fc-red"></span>&nbsp;57 minutes ago</li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-3  ">
                                <div class="views ">
                                    <p class="one fs-small fc-secondary">Syed Usman Nasir</p>
                                    <p class="two fs-11 fc-lgrey">1 Answer - 39 Views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>
            <div class="col-lg-3">
                <!-- <div class="category-box forum  bg-white">
                    <div class="head">
                      <p class="fs-default fc-secondary fw-semi-bold tt-uppercase "><span class="icon-icon-66 fc-primary"></span> TOP Experts</p>
                    </div>
                </div> -->
                <div class="category-box forum  bg-white">
                    <div class="head">
                      <p class="fs-default fc-secondary fw-semi-bold tt-uppercase "><span class="icon-icon-66 fc-primary"></span> TOP Experts</p>
                    </div>
                    <ul class="ask-expert unstyled">
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{$imgscr}}top-expert.png" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey lh-medium">Property Guru</p>
                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">Rating 3.0</p></li>
                                      <li><img src="{{$imgscr}}rating.png" alt="*"></li>
                                    </ul>
                                    
                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{$imgscr}}top-expert.png" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey lh-medium">Property Guru</p>
                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">Rating 3.0</p></li>
                                      <li><img src="{{$imgscr}}rating.png" alt="*"></li>
                                    </ul>
                                    
                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{$imgscr}}top-expert.png" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey lh-medium">Property Guru</p>
                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">Rating 3.0</p></li>
                                      <li><img src="{{$imgscr}}rating.png" alt="*"></li>
                                    </ul>
                                    
                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{$imgscr}}top-expert.png" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey lh-medium">Property Guru</p>
                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">Rating 3.0</p></li>
                                      <li><img src="{{$imgscr}}rating.png" alt="*"></li>
                                    </ul>
                                    
                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{$imgscr}}top-expert.png" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey lh-medium">Property Guru</p>
                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">Rating 3.0</p></li>
                                      <li><img src="{{$imgscr}}rating.png" alt="*"></li>
                                    </ul>
                                    
                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{$imgscr}}top-expert.png" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey lh-medium">Property Guru</p>
                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">Rating 3.0</p></li>
                                      <li><img src="{{$imgscr}}rating.png" alt="*"></li>
                                    </ul>
                                    
                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        <li class="contributor-box top-expert">
                            <figure>
                                <img src="{{$imgscr}}top-expert.png" alt="*">
                            </figure> 
                            <div class="content">
                                <div class="left matchheight">

                                    <p class="fs-small fc-secondary lh-normal">Nadeem Ahmed</p>
                                    <p class="fs-11 fc-grey lh-medium">Property Guru</p>
                                    <ul class="unstyled inline">
                                      <li><p class="fs-xsmall fc-nblack ptpx-5 lh-normal">Rating 3.0</p></li>
                                      <li><img src="{{$imgscr}}rating.png" alt="*"></li>
                                    </ul>
                                    
                                </div>
                                <div class="right ta-right  matchheight">
                                    <p class="fs-11 pbpx-3">7344 posts</p>
                                    <p class="fs-11">24 best</p>
                                </div>
                                
                            </div>                           
                        </li>
                        
                    </ul>
                    
                </div>

                <div>
                    <img class="mtpx-20 " src="{{$imgscr}}agents/new/img1.jpg" alt="Agents">

                    <img class="mtpx-20" src="{{$imgscr}}agents/new/img2.jpg" alt="Agents">

                    <img class="mtpx-20" src="{{$imgscr}}agents/new/img3.jpg" alt="Agents">

                    <img class="mtpx-20" src="{{$imgscr}}agents/new/img4.jpg" alt="Agents">

                    <img class="mtpx-20" src="{{$imgscr}}agents/new/img5.jpg" alt="Agents">

                    <img class="mtpx-20" src="{{$imgscr}}agents/new/img1.jpg" alt="Agents">

                    <img class="mtpx-20" src="{{$imgscr}}agents/new/img2.jpg" alt="Agents">
                </div>
            </div>
        </div>
        
    </div>
    



@endsection
