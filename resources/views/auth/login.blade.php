@extends('layout.master')

@section('content')

<section class=" login-main " style="background: url(web_asset/images/login-banner.jpg);">
    <div class="container ">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-sm-10">
                <div class="row small" style="background: url(web_asset/images/login-small-banner.jpg);">
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">
                        <div class="login-de ta-center">
                            <h4 class="fs-28 fc-primary fw-bold">Sign In</h4>

                            <form method="POST" action="{{ route('login') }}" class="login-form ptpx-15" >
                                @csrf
                                <div class="control-group">
                                    <input  id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="email" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="control-group">
                                    <input  id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="password" required>
                                     @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="control-group buto">
                                    <input class="btn-new-form" type="submit" value="Login" >
                                    <span class="icon-keyboard_arrow_right"></span>
                                </div>
                                <div class="control-group w-48 d-inline-block check ta-left">
                                    <ul class="unstyled inline">
                                        <li><input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="sale"  checked><label for="styled-checkbox-1">Remember Me </label></li>
                                    </ul>
                                </div>
                                <div class="control-group w-48 d-inline-block ta-right">
                                    <a class="fc-secondary ta-right" href="">Forgot Password?</a>
                                </div>
                            </form>

                            <figure>
                                <img class="m-auto" src="web_asset/images/log-after.png" alt="*">
                            </figure>

                            <div class="bottom mtpx-25">
                                <a class="button-soc facebook w-100" href="">Sign in With Facebook</a>

                                <a class="button-soc google w-100 mtpx-15" href="">Sign in With Gmail</a>

                                <p class="fs-large fc-secondary fw-semi-bold ta-center ptpx-15 lh-large">Not a Member Yet?</p>
                                <a href="../sign-up"><p class="fs-large fc-primary tt-uppercase fw-semi-bold  ta-center">Join Us Now</p></a>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>


</section>

@endsection