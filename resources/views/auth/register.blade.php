@extends('layout.master')
@section('title', 'Register')


@section('style')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('content')

<section class="second-banner buy sec-padding --small ptpx-20" >
    <div class="container">
        <div class="row">
            <div class="col-lg-9 nopadd-right mtpx-10">
                <div class="architect">
                    <p class="fs-medium tt-uppercase fw-bold fc-dblue">Create Account</p>
                    <div id="bnrForm" class="clearfix ctaForm">
                        {{ Form::open(['class' => 'jform validate architect-form ptpx-15', 'url' => 'register', 'method' => 'post', 'files' => true]) }}

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="Name">Full Name <span class="fc-red fs-medium">*</span></label>
                                        <input  id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                         @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                         @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="Name">Display / Username <span class="fc-red fs-medium">*</span></label>
                                        <input id="username" type="text" class="{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>
                                        @if ($errors->has('username'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                         @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="control-group">
                                        <label for="usertype">Register As <span class="fc-red fs-medium">*</span></label>
                                        {{ Form::select('usertype', $usertype, old('usertype'), ['class' => 'form-control', 'placeholder' => 'Select from list', 'id' => 'usertype']) }}

                                         @if ($errors->has('usertype'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('usertype') }}</strong>
                                            </span>
                                         @endif
                                    </div>
                                </div>

                                <div id="result" style="display: none">
                                    <div class="col-md-12">
                                <div class="control-group">
                                    <label for="subtype">Sub Type <span class="fc-red fs-medium">*</span></label>
                                    {{ Form::select('subtype', $subtype, old('subtype'), ['class' => 'type_fld', 'placeholder' => 'Selectt', 'id' => 'subtype']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">City<span class="fc-red fs-medium">*</span></label>
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Region <span class="fc-red fs-medium">*</span></label>
                                        <select name="region" >
                                            <option value="" disabled="" selected="" hidden="">Region</option>
                                            <option value="" selected="&quot;selected&quot;">Select Region</option>
                                            <option value="Islamabad Capital Territory">Islamabad Capital Territory</option>
                                            <option value="Punjab">Punjab</option>
                                            <option value="Sindh">Sindh</option>
                                            <option value="Balochistan">Balochistan</option>
                                            <option value="Khyber Pakhtunkhwa">Khyber Pakhtunkhwa</option>
                                            <option value="Azad Jammu and Kashmir">Azad Jammu and Kashmir</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">House/Plot<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="House/Plot" required name="house">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Street</label>
                                    <input type="text" name="street" placeholder="Street" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Location<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" name="location" placeholder="Location" required>
                                </div>
                            </div>
                            <div class="col-md-12 txt">
                                <div class="control-group">
                                    <label for="Name">Description<span class="fc-red fs-medium">*</span></label>
                                    <textarea name="description" placeholder="Description" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Experience<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" name="experience" placeholder="Experience" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="experience_unit">Experience Unit<span class="fc-red fs-medium">*</span></label>
                                    <select  class="type_fld" name="experience_unit">
                                        <option value="" selected="" hidden="">Experience Unit</option>
                                        <option value="">Student</option>
                                        <option value="">Individual</option>
                                        <option value="">Business</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Expertise (Comma Separated)</label>
                                    <input type="text" name="expertise" placeholder="" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Facebook Link</label>
                                    <input type="text" placeholder="" name="f_link">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Linkdin Profile </label>
                                    <input type="text" placeholder=""  name="l_link">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Instagram Link</label>
                                    <input type="text" placeholder="" name="i_link">
                                </div>
                            </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="Email">Email Address<span class="fc-red fs-medium">*</span></label>
                                        <input  id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required/>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                   <div class="control-group">
                                       <label for="number">Contact No<span class="fc-red fs-medium">*</span></label>
                                       <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>
                                       @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif

                                   </div>
                                </div>

                                 <div class="col-md-12">
                                   <div class="control-group">
                                       <label for="img">Profile Image<span class="fc-red fs-medium">*</span></label>
                                       <input id="img" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" required>
                                       @if ($errors->has('image'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif

                                   </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="pwd">Password<span class="fc-red fs-medium">*</span></label>
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="pwd">Confirm Password<span class="fc-red fs-medium">*</span></label>
                                        <input  id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
                                <div class="col-md-8 mtpx-20">
                                    <div class="control-group">
                                        <input type="checkbox" id="basket" name="basket-4" value="0"><label for="basket" class="basket fc-dblue fs-small tt-uppercase fw-bold"> &nbsp; I Agree</label>
                                        <p class="fs-small clicking">By clicking <span>Register</span> you agree to the <a class="fc-primary" href="../terms-of-use">Terms and Conditions</a> <br>set out by this site, including our Cookie Use.</p>
                                    </div>
                                </div>


                                <div class="col-md-4 mtpx-20">
                                    <div class="control-group buto">
                                        <input class="btn-new-form" type="submit" value="Submit" placeholder="" >
                                        <span class="icon-keyboard_arrow_right"></span>

                                    </div>
                                </div>
                            </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>

                @include('layout./partials.side-advertisement-new')
        </div>

    </div>
</section>


<script>
$(document).ready(function(){
    $("select[name='usertype']").change(function(){
        // alert("sad");
        // alert($(this).val());
        if($(this).val()!=1 && $(this).val()!=""){
            $('#result').css('display','block');
        }
        else{
           $('#result').css('display','none');
        }
    });
});
</script>

@endsection