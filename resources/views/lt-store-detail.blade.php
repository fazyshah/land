@extends('layout.master')
@section('title', 'lt-store-detail')
@section('content')



<div class="bg-cream  ptpx-20 pbpx-20">
    <section class="e-com">
        <div class="container">
            <div class="row">
                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                    <form class="form-1 " action="">
                        <div class="control-group w-26 d-inline-block">
                            <select name="" id="category">
                                <option value="">Select Category</option>
                                <option value="">asdasda</option>
                            </select>
                        </div>
                        <div class="control-group w-61  d-inline-block mar">
                            <select class="two" name="" id="product">
                                <option value="">Search Product</option>
                                <option value="">asdasda</option>
                            </select>
                        </div>
                        <div class="control-group  w-12  d-inline-block">
                            <button class="btn-submit" type="submit" value="">SEARCH</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-4 nopadd-left">
                    <div class="cart-btn">
                        <a class="btn-cart" href="javascript:;"><span class="icon-cart cart-1"></span>10 <span class="icon-keyboard_arrow_down cart-2"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="second-banner  sec-padding --small ptpx-20" >
        <div class="container">
            <div class="row bg-white detail-shared">
                <div class="col-md-12 col-nopadd">
                    <div class="first">
                        <p class=""><span class="icon-bookmark1"></span> Save to Facebook</p>
                        <ul class="unstyled inline ">
                            <li><a href=""><span class="icon-external-link"></span></a></li>
                            <li><a href=""><span class="icon-heart1"></span></a></li>
                            <li><a href=""><span class="icon-printer-text"></span></a></li>
                        </ul>
                    </div>
                    <div class="second-part pbpx-10 ptpx-15">
                        <div class="row">
                            
                            <div class="col-md-9 matchheight">
                                <div class="row">
                                    <div class="col-md-5">
                                        <p class="fs-medium fc-secondary fw-semi-bold">Product Name</p>
                                        
                                        <ul class="unstyled inline fs-small fc-black fw-semi-bold">
                                            <li><span class="fc-lgrey">Brand:</span> Unifoam |</li>
                                            <li>More Furniture from Unifoam</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5">
                                        <ul class="unstyled inline fs-small fc-lgrey sixty-ans">
                                            <li><img src="{{$imgscr}}rating-2.png" alt="*"> </li>
                                            <li class="ml-3">7 Ratings &nbsp; | &nbsp;</li>
                                            <li>60 Answered Questions</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row ptpx-5">
                                    <div class="tip">
                                        <div class="col-md-10 ">
                                            <div class="big-slider-2 ">
                                                
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-1.jpg" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-2.jpg" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-3.jpg" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-4.jpg" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-5.jpg" alt="*">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-nopadd ptpx-20">
                                            <div class="small-slider-2  ptpx-5">
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-1.jpg" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-2.jpg" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-3.jpg" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{$imgscr}}deals/detail/detail-4.jpg" alt="*">
                                                </div>
                                                <div class="">
                                                    <img src="{{$imgscr}}deals/detail/detail-5.jpg" alt="*">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ptpx-5">
                                    <div class="col-md-2">
                                        <p class="fc-primary fs-medium fw-semi-bold">Rs <span class="fs-22 ">19,481</span></p>                                       
                                    </div>
                                    <div class="col-md-6 ta-center">
                                        <ul class="unstyled inline color-family mtpx-8">
                                            <li class="fc-lgrey">Color family:</li>
                                            <li class="fs-small"><span class="icon-icon-16 colr"></span><span class="new"> Light Grey</span></li>
                                            <li class="fs-small"><span class="icon-icon-16 colr"></span><span class="new"> Light Grey</span></li>
                                            <li class="fs-small"><span class="icon-icon-16 colr"></span><span class="new"> Light Grey</span></li>
                                            <li class="fs-small"><span class="icon-icon-16 colr"></span><span class=""> Light Grey</span></li>
                                            

                                        </ul>
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn-detail orange cart mtpx-2" href="">Add To Cart <span class="icon-keyboard_arrow_right"></span></a>
                                    </div>
                                </div>
                            </div>
                           <div class="col-lg-3 col-md-4 col-sm-6   matchheight brdr-left">
                                <div class="right-contact">
                                    <div class="head">
                                        <p class="fs-medium ta-center fc-white fw-medium ff-primary">Contact for More information</p>
                                    </div>
                                    <div class="bottom">
                                        <form action="" class="jform validate right-form ">
                                            <div class="control-group">
                                                
                                                <input type="text" placeholder="Name" name="cn" class="required" minlength="2" maxlength="60">
                                            </div>
                                            <div class="control-group">
                                                
                                                <input type="text" name="em" class="required email" placeholder="Email" maxlength="60">
                                            </div>
                                            <div class="control-group clearfix">                                            
                                                    <input type="text" name="pn" id="phone" class="number required" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                                               
                                            </div>
                                            <div class="control-group">
                                                <textarea name="msg" class="required" placeholder="Message" minlength="2" maxlength="1000"></textarea>
                                            </div> 
                                            <div class="control-group">
                                                
                                                <input type="submit" value="Call" class="fw-normal tt-uppercase w-49 btn-detail orange">
                                                <input type="submit" value="Send Email" class="fw-normal tt-uppercase w-49 btn-detail blue">
                                            </div>
                                            <div class="control-group last">
                                                <div class="input">
                                                    <input type="checkbox" id="informed">
                                                </div>
                                                <div class="lbl">
                                                    <label class="fs-xsmall fw-light " for="informed">Keep me Informed about similar properties By Submitting this Form.I agree to <a class="fc-secondary fw-semi-bold" href="">Terms Of Use</a></label>
                                                </div>
                                                
                                            </div>                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="row ptpx-20">
                   
                <div class="col-lg-9 col-sm-12 nopadd-left">
                   <div class="roomate-desc ptpx-20 pbpx-40 bg-white ">
                       <h6 class="heading-main fw-semi-bold abou-1 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Description</h6>
                       <div class="content ptpx-20">
                            <p class=" fs-13 pbpx-20"><span class="fc-primary">Lorem ipsum dolor sit amet.</span></p>
                             <p class="fc-grey fs-13">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                <br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                             <div id='grow'>
                                 <div class='measuringWrapper'>
                                     <div class="text">
                                          
                                         <p class="fs-large fw-semi-bold fc-black ptpx-20">Salient features:</p>
                                         <ul class="unstyled inline-block salient-features">
                                             <li>Located on Lahore Islamabad Motorway</li>
                                             <li>4-year instalment plan</li>
                                             <li>Lush, green surroundings</li>
                                             <li>Wide roads</li>
                                             <li>Modern amenities</li>
                                             <li>Security & maintenance</li>
                                             <li>The Green Club</li>
                                             <li>Parks & recreation</li>
                                         </ul>
                                     </div>
                                 </div>
                             </div>
                             <br>
                             <input class="btn-detail" type="button" onclick="growDiv()" value="View more +" id="more-button">
                        </div>
                   </div>
                </div>
                <div class="col-lg-3 col-sm-12 ">
                    <div class="estate-links">
                        <p class="usef fw-bold tt-uppercase fc-primary fs-large">TAKAFUL LINKS</p>
                        <ul class="unstyled inline grid-block --type-two-blocks ">
                            <li>
                                <img class="ptpx-15" src="{{$imgscr}}estates/takaful-estate.jpg" alt="*">
                            </li>
                            <li class="brdr-left">
                                <ul class="unstyled insider">
                                    <li><a href="javascript:;"><span class="icon-person"><sup class="icon-dot-single"></sup></span>Shaikh Asif</a></li>
                                    <li><a href="javascript:;"><span class="icon-phone_in_talk"></span>0210602168</a></li>
                                    <li><a href="javascript:;"><span class="icon-messages"></span>Chat Now</a></li>
                                    <li><a class="btn-detail green" href="javascript:;">View Profile</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="useful-links mtpx-15">
                        <p class="usef fw-bold tt-uppercase fc-primary fs-large">USEFUL LINKS</p>
                        <ul class="unstyled">
                            <li><a href="">Property for sale in Karachi</a></li>
                            <li><a href="">Property for sale in Jamshed Town</a></li>
                            <li><a href="">Property for sale in PECHS</a></li>
                            <li><a href="">Upper Portions for sale in Karachi</a></li>
                            <li><a href="">Upper Portions for sale in Jamshed Town</a></li>
                            <li><a href="">Upper Portions for sale in PECHS</a></li>
                        </ul>
                    </div>

                    
                </div>
                
            </div>
            
        </div>
    </section>

    <section class="">
        <div class="container">
            <h6 class="heading-main fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft"><span class="fc-lgrey">Similar to </span> furniture product</h6>
            <div class="grid-block --type-four-blocks --style-offsets  pbpx-10 --grid-ipad-full flash-slider" >
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-1.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-2.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure class="featured">
                            <img src="{{$imgscr}}deals/flash-3.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    <div class="deal-box">
                        <figure>
                            <img src="{{$imgscr}}deals/flash-4.jpg" alt="*">
                        </figure>
                        <a href="../lt-store-detail">
                        <div class="content">
                            <p class="fc-secondary fw-semi-bold pbpx-8">Uni Classic Sofa Cum Bed</p>
                            <p class="fs-large fc-primary strike">Rs. 22,000 <sup><s>12,000<span>PKR</span></s>   </sup></p>
                            <ul class="unstyled inline count">
                                <li><img src="{{$imgscr}}rating-2.png" alt="*"></li>
                                <li class="ml-4"><s class="fc-lgrey fs-small">(7)</s></li>
                                <li class="fc-lgrey fs-small">Pakistan</li>
                            </ul>
                        </div>
                        </a>
                        <div class="butons ">
                            <a class="add-to-cart fs-13 w-100" href="javascript:;">Add To Cart <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    

@endsection







<script>
function growDiv() {
var growDiv = document.getElementById('grow');
if (growDiv.clientHeight) {
  growDiv.style.height = 0;
} else {
  var wrapper = document.querySelector('.measuringWrapper');
  growDiv.style.height = wrapper.clientHeight + "px";
}
document.getElementById("more-button").value=document.getElementById("more-button").value=='View more +'?'View less -':'View more +';
}

</script> 

