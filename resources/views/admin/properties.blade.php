@extends('admin.layout.master')
@section('title', 'Properties')

@section('style')
    {{-- {{ Html::style('css/dataTables.bootstrap.min.css') }} --}}
@endsection

@section('content')

<!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid">

				<!-- Title -->
				<div class="row heading-bg bg-green">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-light">Properties</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="index.html">Dashboard</a></li>
						<li><a href="#"><span>table</span></a></li>
						<li class="active"><span>data-table</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">data Table</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_1" class="table table-hover display  pb-30" >
												<thead>
													<tr>
														<th>Property Title</th>
														<th>Adress</th>
														<th>City</th>
														<th>Type</th>
														<th>Created At</th>
														<th>Expired At</th>
														<th>Added By</th>
                                                        <th class="text-nowrap">Action</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Property Title</th>
														<th>Adress</th>
														<th>City</th>
														<th>Type</th>
														<th>Created At</th>
														<th>Expired At</th>
														<th>Added By</th>
                                                        <th class="text-nowrap">Action</th>
													</tr>
												</tfoot>
												<tbody>
												@forelse($properties as $property)
													<tr>
														<td>{{$property->title}}</td>
														<td>{{$property->propertyArea->name}}</td>
														<td>Karachi</td>
														<td>{{$property->propertyType->type}}</td>
                                                        <td>{{\Carbon\Carbon::parse($property['created_at'])->diffForHumans()}}</td>
                                                        <td>{{\Carbon\Carbon::parse($property['exp_date'])->diffForHumans()}}</td>
														<td>{{$property->propertyUser->name}}</td>
														<td class="text-nowrap"><a href="#" class="mr-25" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
													</tr>
												@empty
								                    <h1>Data not found!</h1>
								                @endforelse
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->



			<!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-5">
						<a href="index.html" class="brand mr-30"><img src="dist/img/logo-sm.png" alt="logo"/></a>
						<ul class="footer-link nav navbar-nav">
							<li class="logo-footer"><a href="#">help</a></li>
							<li class="logo-footer"><a href="#">terms</a></li>
							<li class="logo-footer"><a href="#">privacy</a></li>
						</ul>
					</div>
					<div class="col-sm-7 text-right">
						<p>2016 &copy; Kenny. Pampered by Hencework</p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->

		</div>
        <!-- /Main Content -->

@endsection


@section('script')
	<!-- Data table JavaScript -->
    {{ Html::script('admin/dist/js/dataTables-data.js') }}
@endsection