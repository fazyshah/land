@extends('layout.master')
@section('title', 'trend-detail')

@section('content')

<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>

</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 hidden-md-down">
                    
                </div>
                <div class="col-lg-8  col-md-10 col-sm-12">
                    
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="fs-medium fc-white fw-medium">FIND YOUR NEXT INVESTMENT HOME NOW</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-1 " action="">
                                            <div class="control-group w-20 d-inline-block">
                                                <select name="" id="">
                                                    <option value="">Select City</option>
                                                    <option value="">asdasda</option>
                                                </select>
                                            </div>
                                            <div class="control-group w-60  d-inline-block mar">
                                                <input type="text" placeholder="Type Location, area or keyword">
                                            </div>
                                            <div class="control-group  w-18  d-inline-block ml-1">
                                                <button class="btn-submit" type="submit" value="">SEARCH</button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class=" sec-padding bg-cream" >
    <div class="container">
        <ul class="breadcrumb ff-primary pbpx-0">
          <li><a href="/">Home&nbsp;</a></li>
          <li><a href="/trends">&nbsp;Trends</a></li>
          <li>&nbsp;Lahore </li>
        </ul>

        <div class="row">
            <div class="col-lg-12">
                <div class="about-us bg-white">
                    <ul class="unstyled inline tabbing-links">
                        <li class="current w-20 ta-center" data-targetit="tabs-overview">
                        Buying Trends</li><li class=" w-20 ta-center" data-targetit="tabs-board">
                        Rental Trends</li>
                    </ul>
                </div>
                <div class="tabs-overview  hidden visible trend-detail bg-white ">
                    <div class="city">
                        <h6 class=" fc-nblack">Lahore</h6>
                    </div>
                    <div class="td-detail">
                        <h6 class="heading-main abou-4 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Rental Trends</h6>
                        <div class="row mtpx-20 mbpx-40">
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td>No Change</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td><span class="fc-primary">2</span></td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td><span class="fc-primary">2</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <h6 class="heading-main abou-4 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Buying Trends</h6>
                        <div class="row mtpx-20 mbpx-25">
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td>No Change</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td><span class="fc-primary">2</span></td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td><span class="fc-primary">2</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="tabs-board  hidden  trend-detail bg-white">
                    <div class="city">
                        <h6 class=" fc-nblack">Lahore</h6>
                    </div>
                    <div class="td-detail">
                        <h6 class="heading-main abou-4 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase ">Rental Trends</h6>
                        <div class="row mtpx-20 mbpx-40">
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td>No Change</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td>No Change</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <h6 class="heading-main abou-4 fw-semi-bold lh-normal ff-primary  fc-nblack tt-uppercase">Rental Trends</h6>
                        <div class="row mtpx-20 mbpx-25">
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td>No Change</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="small-table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Locality</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="../trend-detail">DHA Defence</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="../trend-detail">Bahria Town</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="../trend-detail">Bahria Orchard</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="../trend-detail">Raiwind Road</a></td>
                                                <td>No Change</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="../trend-detail">Lake City</a></td>
                                                <td>No Change</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
 
        
    
                            </div>
                        </div>
                    </div>
                </div>

            </div>
  
  </div>
            
          </div>
            </div>
        </div>


   </div>
</div>



@endsection