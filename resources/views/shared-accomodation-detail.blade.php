@extends('layout.master')
@section('title', 'shared-accomodation-detail')

@section('content')
@php 
$imgscr = '../web_asset/images/';
use App\Images;
@endphp
<div class="super-banner">
<div class="main-slider">
<section class="hero-banner small" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>

</div>




</div>



<section class="second-banner buy sec-padding --small ptpx-20" >
    <div class="container">
        <ul class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li><a href="#">Shared Accomodation</a></li>
          <li>Shared Accomodation Detail</li>
        </ul>
        <div class="row bg-white detail-shared">
            <div class="col-md-12 col-nopadd">
                <div class="first">
                    <p class=""><span class="icon-bookmark1"></span> Save to Facebook</p>
                    <ul class="unstyled inline ">
                        <li><a href=""><span class="icon-external-link"></span></a></li>
                        <li><a href=""><span class="icon-heart1"></span></a></li>
                        <li><a href=""><span class="icon-printer-text"></span></a></li>
                    </ul>
                </div>
                <div class="second-part pbpx-10 ptpx-15">
                    <div class="row">
                        
                        <div class="col-md-9 matchheight">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="fs-medium fc-secondary fw-semi-bold">{{$accomodation->title}}</p>
                                    <p class="fs-small">{{$accomodation->location}}</p>
                                </div>
                            </div>
                            <div class="row ptpx-5">
                                <div class="tip">
                                    <div class="col-md-10 ">
                                        <div class="big-slider-2 ">
                                        @php
                                            $images = Images::where('ref_id',$accomodation->id)->where('tbl_name','accomodation')->get();
                                        @endphp
                                        @foreach($images as $image)
                                            <div>
                                                <img src="../{{$image->img_src}}" alt="*">
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-nopadd ptpx-20">
                                        <div class="small-slider-2  ptpx-5">
                                        
                                        @foreach($images as $image)
                                            <div>
                                                <img src="../{{$image->img_src}}" alt="*">
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row ptpx-5">
                                <div class="col-md-12">
                                   <ul class="sli-bottom unsyled inline">
                                        <li class="fc-primary">PKR<span>{{$accomodation->price}}</span></li>
                                        <li>2 Bedrooms <span class="icon-icon4"></span></li>
                                        <li>1 Bathroom <span class="icon-icon2"></span></li>
                                        <li>1 Flatmates <span class="icon-icon-44"></span></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                       <div class="col-lg-3 col-md-4 col-sm-6   matchheight brdr-left">
                            <div class="right-contact">
                                <div class="head">
                                    <p class="fs-medium ta-center fc-white fw-medium ff-primary">Contact for More information</p>
                                </div>
                                <div class="bottom">
                                    <form action="" class="jform validate right-form ">
                                        <div class="control-group">
                                            
                                            <input type="text" placeholder="Name" name="cn" class="required" minlength="2" maxlength="60">
                                        </div>
                                        <div class="control-group">
                                            
                                            <input type="text" name="em" class="required email" placeholder="Email" maxlength="60">
                                        </div>
                                        <div class="control-group clearfix">                                            
                                                <input type="text" name="pn" id="phone" class="number required" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                                           
                                        </div>
                                        <div class="control-group">
                                            <textarea name="msg" class="required" placeholder="Message" minlength="2" maxlength="1000"></textarea>
                                        </div> 
                                        <div class="control-group">
                                            
                                            <input type="submit" value="Call" class="fw-normal tt-uppercase w-49 btn-detail orange  call-form">
                                            <input type="submit" value="Send Email" class="fw-normal tt-uppercase w-49 btn-detail blue">
                                        </div>
                                        <div class="control-group last">
                                            <div class="input">
                                                <input type="checkbox" id="informed">
                                            </div>
                                            <div class="lbl">
                                                <label class="fs-xsmall fw-light " for="informed">Keep me Informed about similar properties By Submitting this Form.I agree to <a class="fc-secondary fw-semi-bold" href="">Terms Of Use</a></label>
                                            </div>
                                            
                                        </div>                        
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row ptpx-20">
               
            <div class="col-lg-9 col-sm-12 nopadd-left">
               <div class="roomate-desc ptpx-20 bg-white ">
                   <h6 class="heading-main fw-semi-bold abou-1 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Description</h6>
                   <div class="content ptpx-20">
                         <p class="more fs-13">
                            {{$accomodation->details}}
                        </p>
                    </div>
                    <br>
                    <div class="mtpx-20">
                    <h6 class="heading-main fw-semi-bold abou-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Amenities</h6>
                   <div class="content ">
                        <ul class="grid-block --type-four-blocks aminities mtpx-20">
                            <li><span class="icon-icon-37"></span><p>Air <br>Conditioning</p></li>
                            <li><span class="icon-icon-15"></span><p>Wood <br>Floors</p></li>
                            <li><span class="icon-icon-38"></span><p>Yard</p></li>
                            <li><span class="icon-icon-27"></span><p>Laundary</p></li>
                        </ul>
                        <ul class="grid-block --type-four-blocks aminities mtpx-20">
                            <li><span class="icon-icon-39"></span><p>Dishwasher</p></li>
                            <li><span class="icon-icon-40"></span><p>Private <br>Closet</p></li>
                            <li><span class="icon-icon-41"></span><p>Internet</p></li>
                            <li><span class="icon-wifi"></span><p>Wireless <br>Internet</p></li>
                        </ul>
                         
                    </div>
                    </div>
                       
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                             <div class="desc-box two">
                                <div class="icon" ">
                                    <span class="icon-icon-42"></span>
                                </div>
                                <div class="detail">
                                    <p class="fs-medium fc-primary fw-semi-bold pbpx-10">Household</p>
                                    <ul class="unstyled">
                                        <li>Household age: 25 - 31</li>
                                        <li>People in household: 2</li>
                                        <li>Household Sex: Male</li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="desc-box two">
                                <div class="icon" ">
                                    <span class="icon-icon-51"></span>
                                </div>
                                <div class="detail">
                                    <p class="fs-medium fc-primary fw-semi-bold pbpx-10">Residence</p>
                                    <ul class="unstyled">
                                        <li>Building Type: House</li>
                                        <li>Furnished: Yes</li>                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="desc-box two brdr-no">
                                <div class="icon" ">
                                    <span class="icon-icon-44"></span>
                                </div>
                                <div class="detail">
                                    <p class="fs-medium fc-primary fw-semi-bold pbpx-10">Roommate Preference</p>
                                    <ul class="unstyled">
                                        <li>Age: 18 - 35</li>
                                        <li>Smoking: Outside only</li>
                                        <li>Students Only: No</li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="desc-box two pbpx-43">
                                <div class="icon"  ">
                                    <span class="icon-icon-43"></span>
                                </div>
                                <div class="detail">
                                    <p class="fs-medium fc-primary fw-semi-bold pbpx-10">Lifestyle</p>
                                    <ul class="unstyled">
                                        <li>My Cleanliness: Clean</li>
                                        <li>Overnight Guests: Occasionally</li>
                                        <li>Party Habits: Rarely</li>
                                        <li>I Get Up / Go To Bed :</li>
                                        <li>6AM - 8AM / 10PM - 12AM</li>
                                        <li>Smoker: No</li>
                                        <li>Work Schedule: Professional (9-5)</li>
                                        <li>Occupation: Engineering</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="mtpx-20">
                        <h6 class="heading-main fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Location &amp; Commute</h6>
                        <div class="map-direct" id="zoomife">
                            <a class="fc-primary" href="javascript:;">More Lahore Maps</a>
                            <!-- <div id="myContainer" style="width:100%; height:450px; margin:auto; " ></div> -->
                            <iframe src="https://www.google.com/maps/d/embed?mid=1wMZLD-KoIBt-zG0r8ziRkZeqgUA&hl=en" width="100%" height="480"></iframe>

                        </div>
                      
                    </div>  
                        
                    
                   
               </div>
            </div>
            <div class="col-lg-3 col-sm-12 ">
                <div class="estate-links">
                    <p class="usef fw-bold tt-uppercase fc-primary fs-large">TAKAFUL LINKS</p>
                    <ul class="unstyled inline grid-block --type-two-blocks ">
                        <li>
                            <img class="ptpx-15" src="{{$imgscr}}estates/takaful-estate.jpg" alt="*">
                        </li>
                        <li class="brdr-left">
                            <ul class="unstyled insider">
                                <li><a href="javascript:;"><span class="icon-person"><sup class="icon-dot-single"></sup></span>Shaikh Asif</a></li>
                                <li><a href="javascript:;"><span class="icon-phone_in_talk"></span>0210602168</a></li>
                                <li><a href="javascript:;"><span class="icon-messages"></span>Chat Now</a></li>
                                <li><a class="btn-detail green" href="javascript:;">View Profile</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="useful-links mtpx-15">
                    <p class="usef fw-bold tt-uppercase fc-primary fs-large">USEFUL LINKS</p>
                    <ul class="unstyled">
                        <li><a href="">Property for sale in Karachi</a></li>
                        <li><a href="">Property for sale in Jamshed Town</a></li>
                        <li><a href="">Property for sale in PECHS</a></li>
                        <li><a href="">Upper Portions for sale in Karachi</a></li>
                        <li><a href="">Upper Portions for sale in Jamshed Town</a></li>
                        <li><a href="">Upper Portions for sale in PECHS</a></li>
                    </ul>
                </div>

                
            </div>
            
        </div>
        <div class="row ptpx-20">
            
            <div class="col-lg-12 col-sm-12 col-nopadd">
               
                   <h6 class="heading-main fw-semi-bold small-4 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Similar SHARED ACCOMODATION</h6>

                   <div class="grid-block --type-four-blocks --style-offsets --grid-ipad-full --grid-mobile-full ">
                      
                       <div class="item matchHeight">
                           <div class="prop-new-box-3">
                               <div class="head">
                                       <img class="w-100" src="{{$imgscr}}sa-2/img-1.jpg" alt="*">
                                       <ul class="unstyled inline">
                                           <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                           </li>
                                           <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                       </ul>
                               </div>
                               <div class="content">
                                   <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                                   <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                   <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                               </div>
                               <div class="bottom">
                                   <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                       <li>800 Sqft</li>
                                       <li>2 Beds</li>
                                       <li>2 Baths</li>
                                   </ul>
                               </div>
                               <div class="butons grid-block --type-two-blocks">
                                   <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                   <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                               </div>
                           </div>
                       </div>
                       <div class="item matchHeight">
                           <div class="prop-new-box-3">
                               <div class="head">
                                       <img class="w-100" src="{{$imgscr}}sa-2/img-2.jpg" alt="*">
                                       <ul class="unstyled inline">
                                           <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                           </li>
                                           <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                       </ul>
                               </div>
                               <div class="content">
                                   <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                                   <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                   <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                               </div>
                               <div class="bottom">
                                   <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                       <li>800 Sqft</li>
                                       <li>2 Beds</li>
                                       <li>2 Baths</li>
                                   </ul>
                               </div>
                               <div class="butons grid-block --type-two-blocks">
                                   <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                   <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                               </div>
                           </div>
                       </div>
                       <div class="item matchHeight">
                           <div class="prop-new-box-3">
                               <div class="head">
                                       <img class="w-100" src="{{$imgscr}}sa-2/img-3.jpg" alt="*">
                                       <ul class="unstyled inline">
                                           <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                           </li>
                                           <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                       </ul>
                               </div>
                               <div class="content">
                                   <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                                   <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                   <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                               </div>
                               <div class="bottom">
                                   <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                       <li>800 Sqft</li>
                                       <li>2 Beds</li>
                                       <li>2 Baths</li>
                                   </ul>
                               </div>
                               <div class="butons grid-block --type-two-blocks">
                                   <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                   <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                               </div>
                           </div>
                       </div>
                       <div class="item matchHeight">
                           <div class="prop-new-box-3">
                               <div class="head">
                                       <img class="w-100" src="{{$imgscr}}sa-2/img-4.jpg" alt="*">
                                       <ul class="unstyled inline">
                                           <li ><a href=""><span class="icon-map-pin" tabindex="0" data-toggle="tooltip" title="location" data-placement="top"></span></a>
                                           </li>
                                           <li><a href=""><span class="icon-external-link" tabindex="0" data-toggle="tooltip" title="Share" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-heart1" tabindex="0" data-toggle="tooltip" title="like" data-placement="top"></span></a></li>
                                           <li><a href=""><span class="icon-camera3" tabindex="0" data-toggle="tooltip" title="Add Photo" data-placement="top"></span> 30</a></li>
                                       </ul>
                               </div>
                               <div class="content">
                                   <p class="fs-medium fc-nblack fw-semi-bold">Luxury Villa Bharia Town</p>
                                   <p class="fc-primary fw-medium fs-small fs-italic"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                   <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>4.75 Crore</p>
                               </div>
                               <div class="bottom">
                                   <ul class="list1 grid-block --type-three-blocks  unstyled inline">
                                       <li>800 Sqft</li>
                                       <li>2 Beds</li>
                                       <li>2 Baths</li>
                                   </ul>
                               </div>
                               <div class="butons grid-block --type-two-blocks">
                                   <a class="btn-orange fs-13 w-100 call-form" href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                   <a class="btn-blue fs-13 w-100" href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                               </div>
                           </div>
                       </div>
                       
                       
                       
                   </div>
              
                



            </div>
          
            
    </div>
</div>



@endsection
    