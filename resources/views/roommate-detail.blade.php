@extends('layout.master')
@section('title', 'roommate-detail')

@section('content')



<div class="super-banner">
<div class="main-slider">
<section class="hero-banner small" style="background: url({{$imgscr}}banner-1.jpg);">
    
    
</section>

</div>




</div>




</div>



<section class="second-banner buy sec-padding --small ptpx-20" >
    <div class="container">
        <ul class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li><a href="#">Room-Mate</a></li>
          <li>Room Mate Detail</li>
        </ul>
        <div class="row bg-white detail-shared">
            <div class="col-md-12 col-nopadd">
                <div class="first">
                    <p class=""><span class="icon-bookmark1"></span> Save to Facebook</p>
                    <ul class="unstyled inline ">
                        <li><a href=""><span class="icon-external-link"></span></a></li>
                        <li><a href=""><span class="icon-heart1"></span></a></li>
                        <li><a href=""><span class="icon-printer-text"></span></a></li>
                    </ul>
                </div>
                <div class="second-part">
                    <div class="row">
                       
                        <div class="col-lg-9 col-md-8 col-sm-12 nopadd-right">
                             <div class="roommate">
                                <div class=" shred-box">
                                        <figure class="matchheight">
                                            <img class="" src="{{$imgscr}}img-1.jpg" alt="*">
                                        </figure>
                                    
                                        <div class="content matchheight">
                                            <p class="fs-medium tt-uppercase fc-secondary fw-semi-bold">LOOKING FOR NOT IMPORTANT IN LONDON (NW1)</p>
                                            <p class="fs-medium tt-uppercase fc-lgrey">ROOM WANTED</p>
                                            <ul class="unstyled inline fs-small fc-black fw-semi-bold move-date">
                                                <li><span class="icon-calendar3"></span> MOVE DATE 17TH SEP 2018</li>
                                                <li><span class="icon-envelope-o"></span> FREE TO CONTACT</li>
                                            </ul>
                                            <p class="tt-uppercase lh-large">STUDENT LOOKING FOR A ROOM :)</p>
                                            <p class="fs-13">I’m looking for a single or double room to rent while I’m studying at university. I’d like to...</p>
                                            <div class="ptpx-20">
                                                <a class="btn-detail orange mr-1" href="">Add to Favourites</a>
                                                <a class="btn-detail" href="">More Details</a>
                                            </div>
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 ">
                            <div class="right-contact">
                                <div class="head">
                                    <p class="fs-medium ta-center fc-white fw-medium ff-primary">Contact for More information</p>
                                </div>
                                <div class="bottom">
                                    <form action="" class="jform validate right-form ">
                                        <div class="control-group">
                                            
                                            <input type="text" placeholder="Name" name="cn" class="required" minlength="2" maxlength="60">
                                        </div>
                                        <div class="control-group">
                                            
                                            <input type="text" name="em" class="required email" placeholder="Email" maxlength="60">
                                        </div>
                                        <div class="control-group clearfix">                                            
                                                <input type="text" name="pn" id="phone" class="number required" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                                           
                                        </div>
                                        <div class="control-group">
                                            <textarea name="msg" class="required" placeholder="Message" minlength="2" maxlength="1000"></textarea>
                                        </div> 
                                        <div class="control-group">
                                            
                                            <input type="submit" value="Call" class="fw-normal tt-uppercase w-49 btn-detail orange">
                                            <input type="submit" value="Send Email" class="fw-normal tt-uppercase w-49 btn-detail blue">
                                        </div>
                                        <div class="control-group last">
                                            <div class="input">
                                                <input type="checkbox" id="informed">
                                            </div>
                                            <div class="lbl">
                                                <label class="fs-xsmall fw-light " for="informed">Keep me Informed about similar properties By Submitting this Form.I agree to <a class="fc-secondary fw-semi-bold" href="">Terms Of Use</a></label>
                                            </div>
                                            
                                        </div>                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row ptpx-20">
               
            <div class="col-lg-9 col-sm-12 nopadd-left">
               <div class="roomate-desc ptpx-20 bg-white ">
                    <h6 class="heading-main fw-semi-bold v-small lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Similar accommodation roommate</h6>
                   <div class="content">
                        <p class=" fs-13">Hi, <br><br>
                            Looking for a room to share in Hoboken/ Jersey City area. About me- Clean, polite and respectful. Quiet guy- work a regular job in Hoboken and like exploring around on the weekends. Budget ideally 1200-1400, can stretch up very slightly if necessary. Contact me for further details!
                        </p>

                        <div class="desc-box">
                            <div class="icon" >
                                 <span class="icon-icon-43"></span>
                            </div>
                            <div class="detail">
                                <p class="fs-medium fc-primary fw-semi-bold pbpx-10">Lifestyle</p>
                                <ul class="unstyled">
                                    <li>My Cleanliness: Clean</li>
                                    <li>Overnight Guests: Occasionally</li>
                                    <li>Party Habits: Rarely</li>
                                    <li>I Get Up / Go To Bed :</li>
                                    <li>6AM - 8AM / 10PM - 12AM</li>
                                    <li>Smoker: No</li>
                                    <li>Work Schedule: Professional (9-5)</li>
                                    <li>Occupation: Engineering</li>
                                </ul>
                            </div>
                        </div>
                         <div class="desc-box">
                            <div class="icon " >
                                 <span class="icon-icon-44"></span>
                            </div>
                            <div class="detail">
                                <p class="fs-medium fc-primary fw-semi-bold pbpx-10">Roommate Preference</p>
                                <ul class="unstyled">
                                    <li>Age: 18 - 35</li>
                                    <li>Smoking: Outside only</li>
                                    <li>Students Only: No</li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                   
               </div>
            </div>
           
        

            
        </div>
        <div class="row ptpx-30 hidden-sm-down">
            <div class="col-lg-12 col-sm-12 nopadd-left">
                    <h6 class="heading-main fw-semi-bold small-6 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Similar accommodation roommate</h6>

                    <div class="grid-block --type-four-blocks --style-offsets --grid-ipad-full --grid-mobile-full ptpx-10">
                       
                        <div class="item matchHeight">
                            <div class="prop-new-box-3">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}roommates/slide-1.jpg" alt="*">
                                        
                                </div>
                                <div class="content">
                                    <p class="fs-medium fc-secondary fw-semi-bold lh-medium">Room Looking for not important in PECHS...</p>
                                    <p class="fc-primary fw-medium fs-small fs-italic ptpx-5"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                    
                                </div>
                                <div class="bottom">
                                    <p class="ta-center ptpx-8 pbpx-8 fs-13 fc-lgrey"><span class="icon-calendar1 mr-3"></span>Move Date <b>17th  Sep 2018</b></p>
                                </div>
                                <div class="butons grid-block --type-two-blocks">
                                    <a class="btn-orange fs-13 w-100" href="javascript:;">Add to Favourite</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;">Detail</a>
                                </div>
                            </div>
                        </div>
                        <div class="item matchHeight">
                            <div class="prop-new-box-3">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}roommates/slide-2.jpg" alt="*">
                                        
                                </div>
                                <div class="content">
                                    <p class="fs-medium fc-secondary fw-semi-bold lh-medium">Room Looking for not important in PECHS...</p>
                                    <p class="fc-primary fw-medium fs-small fs-italic ptpx-5"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                    
                                </div>
                                <div class="bottom">
                                    <p class="ta-center ptpx-8 pbpx-8 fs-13 fc-lgrey"><span class="icon-calendar1 mr-3"></span>Move Date <b>17th  Sep 2018</b></p>
                                </div>
                                <div class="butons grid-block --type-two-blocks">
                                    <a class="btn-orange fs-13 w-100" href="javascript:;">Add to Favourite</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;">Detail</a>
                                </div>
                            </div>
                        </div>
                        <div class="item matchHeight">
                            <div class="prop-new-box-3">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}roommates/slide-3.jpg" alt="*">
                                        
                                </div>
                                <div class="content">
                                    <p class="fs-medium fc-secondary fw-semi-bold lh-medium">Room Looking for not important in PECHS...</p>
                                    <p class="fc-primary fw-medium fs-small fs-italic ptpx-5"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                    
                                </div>
                                <div class="bottom">
                                    <p class="ta-center ptpx-8 pbpx-8 fs-13 fc-lgrey"><span class="icon-calendar1 mr-3"></span>Move Date <b>17th  Sep 2018</b></p>
                                </div>
                                <div class="butons grid-block --type-two-blocks">
                                    <a class="btn-orange fs-13 w-100" href="javascript:;">Add to Favourite</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;">Detail</a>
                                </div>
                            </div>
                        </div>
                        <div class="item matchHeight">
                            <div class="prop-new-box-3">
                                <div class="head">
                                        <img class="w-100" src="{{$imgscr}}roommates/slide-4.jpg" alt="*">
                                        
                                </div>
                                <div class="content">
                                    <p class="fs-medium fc-secondary fw-semi-bold lh-medium">Room Looking for not important in PECHS...</p>
                                    <p class="fc-primary fw-medium fs-small fs-italic ptpx-5"><span class="icon-location4 mr-1"></span>DHA Phase 6, D.H.A, Karachi Pakistan</p>
                                    
                                </div>
                                <div class="bottom">
                                    <p class="ta-center ptpx-8 pbpx-8 fs-13 fc-lgrey"><span class="icon-calendar1 mr-3"></span>Move Date <b>17th  Sep 2018</b></p>
                                </div>
                                <div class="butons grid-block --type-two-blocks">
                                    <a class="btn-orange fs-13 w-100" href="javascript:;">Add to Favourite</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;">Detail</a>
                                </div>
                            </div>
                        </div>
                       
                        
                        
                        
                    </div>
                 </div>
        </div>

        
    
                        
                        
                        
                    </div>
                
            </div>
  
        
    



@endsection