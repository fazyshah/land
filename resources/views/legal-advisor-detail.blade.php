@extends('layout.master')
@section('title', 'legal-advisor-detail')
@section('content')




<div class="super-banner">
<div class="main-slider">
<section class="hero-banner" style="background: url(web_asset/images/banner-1.jpg);">
    
    
</section>

</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                   <h2 class="fw-light ta-center fc-white lh-normal wow fadeInDown pbpx-20 ff-primary">Your Partners For <span class="d-block tt-uppercase fw-bold ls-large">Reliable Properties</span></h2>
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p class=" fc-white fw-medium">Property for Sale in Pakistan</p>
                                    </div>
                                    <div class="col-md-8">
                                        <ul class="unstyled inline tabbing-links">
                                            <li class="current" data-targetit="tabs-one"><a href="javascript:;">BUY</a></li><li data-targetit="tabs-two"><a href="javascript:;">RENT</a></li><li  data-targetit="tabs-three"><a href="javascript:;">SHARED</a></li><li data-targetit="tabs-five"><a href="javascript:;">AGENTS</a></li><li  data-targetit="tabs-four"><a href="javascript:;">PROJECTS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            <form class="form-1 ptpx-20" action="">
                                                <div class="control-group w-20 d-inline-block">
                                                    <select name="" id="">
                                                        <option value="">Select City</option>
                                                        <option value="">asdasda</option>
                                                    </select>
                                                </div>
                                                <div class="control-group w-62  d-inline-block mar">
                                                    <input type="text" placeholder="Type Location, area or keyword">
                                                </div>
                                                <div class="control-group  w-17  d-inline-block ">
                                                    <button class="btn-submit" type="submit" value="">SEARCH</button>

                                                </div>
                                                
                                                
                                            </form>

                                        </div>
                                        <div  class="tabs-two hidden">
                                            <form class="form-1 ptpx-20" action="">
                                                <div class="control-group w-20 d-inline-block">
                                                    <select name="" id="">
                                                        <option value="">Select City</option>
                                                        <option value="">asdasda</option>
                                                    </select>
                                                </div>
                                                <div class="control-group w-62  d-inline-block mar">
                                                    <input type="text" placeholder="Type Location, area or keyword">
                                                </div>
                                                <div class="control-group  w-17  d-inline-block ">
                                                    <button class="btn-submit" type="submit" value="">SEARCH</button>

                                                </div>
                                                
                                                
                                            </form>
                                        </div>
                                        <div  class="tabs-three hidden">
                                            <form class="form-1 ptpx-20" action="">
                                                <div class="control-group w-20 d-inline-block">
                                                    <select name="" id="">
                                                        <option value="">Select City</option>
                                                        <option value="">asdasda</option>
                                                    </select>
                                                </div>
                                                <div class="control-group w-62  d-inline-block mar">
                                                    <input type="text" placeholder="Type Location, area or keyword">
                                                </div>
                                                <div class="control-group  w-17  d-inline-block ">
                                                    <button class="btn-submit" type="submit" value="">SEARCH</button>

                                                </div>
                                                
                                                
                                            </form>
                                        </div>
                                        <div  class="tabs-four hidden">
                                            <form class="form-1 ptpx-20" action="">
                                                <div class="control-group w-20 d-inline-block">
                                                    <select name="" id="">
                                                        <option value="">Select City</option>
                                                        <option value="">asdasda</option>
                                                    </select>
                                                </div>
                                                <div class="control-group w-62  d-inline-block mar">
                                                    <input type="text" placeholder="Type Location, area or keyword">
                                                </div>
                                                <div class="control-group  w-17  d-inline-block ">
                                                    <button class="btn-submit" type="submit" value="">SEARCH</button>

                                                </div>
                                                
                                                
                                            </form>
                                        </div>
                                        <div  class="tabs-five hidden">
                                            <form class="form-1 ptpx-20" action="">
                                                <div class="control-group w-20 d-inline-block">
                                                    <select name="" id="">
                                                        <option value="">Select City</option>
                                                        <option value="">asdasda</option>
                                                    </select>
                                                </div>
                                                <div class="control-group w-62  d-inline-block mar">
                                                    <input type="text" placeholder="Type Location, area or keyword">
                                                </div>
                                                <div class="control-group  w-17  d-inline-block ">
                                                    <button class="btn-submit" type="submit" value="">SEARCH</button>

                                                </div>                                                
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class="second-banner buy sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <ul class="breadcrumb mbpx-0">
          <li><a href="#">Home&nbsp;</a></li>
          <li>&nbsp;Legal Advisor Listing</li>
        </ul>
        <h4 class="ta-center fc-secondary fw-medium pbpx-15">Top Real Estate Lawyers in Pakistan</h4>
        <div class="row">
            <div class="col-lg-9 nopadd-right">
                <div class="legal-box">
                    <a href="../legal-advisor-detail"><p class="fs-medium fc-blue pbpx-5 fw-semi-bold">Irfan Mir Halepota & Associates Law Firm Advocates Civil Litiga</p>
                    <p class="fs-13 fc-grey fw-medium pbpx-5 fs-italic">Teen Talwar, Kheyaban-e-Iqbal Rd</p></a>
                    <!-- Rating Stars Box -->
                      <div class='rating-stars '>
                        <ul id='stars'>
                          <li class='star'  data-value='1'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star' data-value='2'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='3'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='4'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='5'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>

                        </ul>
                        <p class="fs-small fc-grey fw-medium">4.6 Rating</p>
                      </div>
                    <div class="deta ptpx-10">
                        <ul class="unstyled inline">
                            <li><span class="icon-tag"></span>Property Lawyers </li>
                            <li><a href="javascript:;"><span class="icon-phone-call"></span>+91 94481 39192</a></li>
                            <li><span class="icon-icon-45"></span>DHA KARACHI Real Estate, 75500 4th Sunset St, D.H.A. Phase 4 Sunset Commercial Area Phase 4 Defence Housing Authority, Karachi, Karachi City, Sindh 75500 </li>
                        </ul>
                    </div>
                    <div class="butons ">
                        <a class="btn-orange fs-13 call-form " href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                        <a class="btn-blue lb fs-13 " href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                    </div>
                </div>

                <div class="legal-box">
                    <a href="../legal-advisor-detail"><p class="fs-medium fc-blue pbpx-5 fw-semi-bold">Irfan Mir Halepota & Associates Law Firm Advocates Civil Litiga</p>
                    <p class="fs-13 fc-grey fw-medium pbpx-5 fs-italic">Teen Talwar, Kheyaban-e-Iqbal Rd</p></a>
                    <!-- Rating Stars Box -->
                      <div class='rating-stars '>
                        <ul id='stars'>
                          <li class='star'  data-value='1'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star' data-value='2'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='3'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='4'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='5'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>

                        </ul>
                        <p class="fs-small fc-grey fw-medium">4.6 Rating</p>
                      </div>
                    <div class="deta ptpx-10">
                        <ul class="unstyled inline">
                            <li><span class="icon-tag"></span>Property Lawyers </li>
                            <li><a href="javascript:;"><span class="icon-phone-call"></span>+91 94481 39192</a></li>
                            <li><span class="icon-icon-45"></span>DHA KARACHI Real Estate, 75500 4th Sunset St, D.H.A. Phase 4 Sunset Commercial Area Phase 4 Defence Housing Authority, Karachi, Karachi City, Sindh 75500 </li>
                        </ul>
                    </div>
                    <div class="butons ">
                        <a class="btn-orange fs-13 call-form " href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                        <a class="btn-blue lb fs-13 " href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                    </div>
                </div>

                <div class="legal-box">
                    <a href="../legal-advisor-detail"><p class="fs-medium fc-blue pbpx-5 fw-semi-bold">Irfan Mir Halepota & Associates Law Firm Advocates Civil Litiga</p>
                    <p class="fs-13 fc-grey fw-medium pbpx-5 fs-italic">Teen Talwar, Kheyaban-e-Iqbal Rd</p></a>
                    <!-- Rating Stars Box -->
                      <div class='rating-stars '>
                        <ul id='stars'>
                          <li class='star'  data-value='1'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star' data-value='2'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='3'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='4'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='5'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>

                        </ul>
                        <p class="fs-small fc-grey fw-medium">4.6 Rating</p>
                      </div>
                    <div class="deta ptpx-10">
                        <ul class="unstyled inline">
                            <li><span class="icon-tag"></span>Property Lawyers </li>
                            <li><a href="javascript:;"><span class="icon-phone-call"></span>+91 94481 39192</a></li>
                            <li><span class="icon-icon-45"></span>DHA KARACHI Real Estate, 75500 4th Sunset St, D.H.A. Phase 4 Sunset Commercial Area Phase 4 Defence Housing Authority, Karachi, Karachi City, Sindh 75500 </li>
                        </ul>
                    </div>
                    <div class="butons ">
                        <a class="btn-orange fs-13 call-form " href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                        <a class="btn-blue lb fs-13 " href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                    </div>
                </div>

                <div class="legal-box">
                    <a href="../legal-advisor-detail"><p class="fs-medium fc-blue pbpx-5 fw-semi-bold">Irfan Mir Halepota & Associates Law Firm Advocates Civil Litiga</p>
                    <p class="fs-13 fc-grey fw-medium pbpx-5 fs-italic">Teen Talwar, Kheyaban-e-Iqbal Rd</p></a>
                    <!-- Rating Stars Box -->
                      <div class='rating-stars '>
                        <ul id='stars'>
                          <li class='star'  data-value='1'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star' data-value='2'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='3'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='4'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='5'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>

                        </ul>
                        <p class="fs-small fc-grey fw-medium">4.6 Rating</p>
                      </div>
                    <div class="deta ptpx-10">
                        <ul class="unstyled inline">
                            <li><span class="icon-tag"></span>Property Lawyers </li>
                            <li><a href="javascript:;"><span class="icon-phone-call"></span>+91 94481 39192</a></li>
                            <li><span class="icon-icon-45"></span>DHA KARACHI Real Estate, 75500 4th Sunset St, D.H.A. Phase 4 Sunset Commercial Area Phase 4 Defence Housing Authority, Karachi, Karachi City, Sindh 75500 </li>
                        </ul>
                    </div>
                    <div class="butons ">
                        <a class="btn-orange fs-13 call-form " href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                        <a class="btn-blue lb fs-13 " href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                    </div>
                </div>

                <div class="legal-box">
                    <a href="../legal-advisor-detail"><p class="fs-medium fc-blue pbpx-5 fw-semi-bold">Irfan Mir Halepota & Associates Law Firm Advocates Civil Litiga</p>
                    <p class="fs-13 fc-grey fw-medium pbpx-5 fs-italic">Teen Talwar, Kheyaban-e-Iqbal Rd</p></a>
                    <!-- Rating Stars Box -->
                      <div class='rating-stars '>
                        <ul id='stars'>
                          <li class='star'  data-value='1'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star' data-value='2'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='3'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='4'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='5'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>

                        </ul>
                        <p class="fs-small fc-grey fw-medium">4.6 Rating</p>
                      </div>
                    <div class="deta ptpx-10">
                        <ul class="unstyled inline">
                            <li><span class="icon-tag"></span>Property Lawyers </li>
                            <li><a href="javascript:;"><span class="icon-phone-call"></span>+91 94481 39192</a></li>
                            <li><span class="icon-icon-45"></span>DHA KARACHI Real Estate, 75500 4th Sunset St, D.H.A. Phase 4 Sunset Commercial Area Phase 4 Defence Housing Authority, Karachi, Karachi City, Sindh 75500 </li>
                        </ul>
                    </div>
                    <div class="butons ">
                        <a class="btn-orange fs-13 call-form " href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                        <a class="btn-blue lb fs-13 " href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                    </div>
                </div>

                <div class="legal-box">
                    <a href="../legal-advisor-detail"><p class="fs-medium fc-blue pbpx-5 fw-semi-bold">Irfan Mir Halepota & Associates Law Firm Advocates Civil Litiga</p>
                    <p class="fs-13 fc-grey fw-medium pbpx-5 fs-italic">Teen Talwar, Kheyaban-e-Iqbal Rd</p></a>
                    <!-- Rating Stars Box -->
                      <div class='rating-stars '>
                        <ul id='stars'>
                          <li class='star'  data-value='1'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star' data-value='2'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='3'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='4'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>
                          <li class='star'  data-value='5'>
                            <i class='fa icon-star-o icon-star'></i>
                          </li>

                        </ul>
                        <p class="fs-small fc-grey fw-medium">4.6 Rating</p>
                      </div>
                    <div class="deta ptpx-10">
                        <ul class="unstyled inline">
                            <li><span class="icon-tag"></span>Property Lawyers </li>
                            <li><a href="javascript:;"><span class="icon-phone-call"></span>+91 94481 39192</a></li>
                            <li><span class="icon-icon-45"></span>DHA KARACHI Real Estate, 75500 4th Sunset St, D.H.A. Phase 4 Sunset Commercial Area Phase 4 Defence Housing Authority, Karachi, Karachi City, Sindh 75500 </li>
                        </ul>
                    </div>
                    <div class="butons ">
                        <a class="btn-orange fs-13 call-form " href="javascript:;"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                        <a class="btn-blue lb fs-13 " href="javascript:;"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                    </div>
                </div>
                
            </div>
    @include('layout./partials.side-advertisement-new')
   
    
    


@endsection
