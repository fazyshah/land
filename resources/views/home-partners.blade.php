@extends('layout.master')
@section('title', 'Home')

@section('content')
<div class="super-banner">
<div class="main-slider">
<section class="hero-banner" style="background: url(web_asset/images/banner-1.jpg);">


</section>

</div>

<section class="form-section">
    
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class=" fc-white fw-medium fs-medium tt-uppercase">find a partner</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                            <form class="hp-form" action="">
                                                <div class="row">
                                                    <div class="col-md-10 matchheight">
                                                        <div class="control-group w-50 d-inline-block mr-1">
                                                        <select name="" id="">
                                                            <option value="-1">Enter Services Keyword</option>
                                                            <option value="">asdasda</option>
                                                        </select>
                                                        </div>
                                                        <div class="control-group w-48 d-inline-block">
                                                        <select name="" id="">
                                                            <option value="-1">Please Select City</option>
                                                            <option value="">asdasda</option>
                                                        </select>
                                                        </div>

                                                        <div class="control-group w-50 d-inline-block mr-1">
                                                        <select name="" id="">
                                                            <option value="-1">Please Select Category</option>
                                                            <option value="">asdasda</option>
                                                        </select>
                                                        </div>
                                                        <div class="control-group w-48 d-inline-block">
                                                        <select name="" id="">
                                                            <option value="-1">Please Select Partner</option>
                                                            <option value="">asdasda</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 matchheight d-table nopadd-left">
                                                        <div class="d-table-cell va-middle">
                                                            <div class="control-group">
                                                                <button class="btn-submit" type="submit" value="">SEARCH</button>
                                                            </div>    
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                
                                                
                                            </form>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>    
       
</section>


</div>



<section class="sec-padding hp --small bg-cream ptpx-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb ff-primary  mbpx-0">
                  <li><a href="/">Home&nbsp;</a></li>
                  <li>&nbsp;Home Partners</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="about-us bg-white">
                    <ul class="unstyled inline tabbing-links tt-uppercase">
                        <li class="current" data-targetit="tabs-one">
                        architects</li><li data-targetit="tabs-second">
                        builders</li><li data-targetit="tabs-third">interior designers</li><li data-targetit="tabs-fourth">Financial solution</li>
                    </ul>
                </div>
                <div  class="tabs-one  hidden visible">
                    <p class="fs-small fc-lgrey ptpx-20 pbpx-20">Showing <b class="fc-nblack">1</b> to <b class="fc-nblack">8</b> of <b class="fc-nblack">8</b> Services found </p>

                    <div class="grid-block --type-three-blocks --style-offsets --style-bottom-offsets --bottom-offsets-small">
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Carpenter  -  Woodenwork</p>
                                    <p class="fs-13 fc-grey ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                        
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Project Management Firm  - Project Management</p>
                                    <p class="fs-13 fc-grey  ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                        
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Residential Contractors  - Grey Structure</p>
                                    <p class="fs-13 fc-grey ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                        
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Carpenter  -  Woodenwork</p>
                                    <p class="fs-13 fc-grey ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                        
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Project Management Firm  - Project Management</p>
                                    <p class="fs-13 fc-grey  ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                       
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Residential Contractors  - Grey Structure</p>
                                    <p class="fs-13 fc-grey ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                        
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Carpenter  -  Woodenwork</p>
                                    <p class="fs-13 fc-grey ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                        
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Project Management Firm  - Project Management</p>
                                    <p class="fs-13 fc-grey  ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                       
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="main-box">
                                <div class="pp-box">
                                    <figure class="first">
                                        
                                    </figure>
                                    <a href="../partner-profile">
                                    <p class="fs-medium fw-semi-bold fc-black lh-medium">Residential Contractors  - Grey Structure</p>
                                    <p class="fs-13 fc-grey ptpx-10">We wish to introduce ourselves (Art Aluminum and Glass)as a leading Aluminum and Glass company based at Lahore, since 1997. For the last many years our multiple range of Product in Aluminum, Glass and blind add beauty and glamour of Residential, Commercial complexes, showroom, town planers, hotels, bungalows and villas.</p>
                                    </a>
                                    <div class="ta-right ptpx-10">
                                       
                                    </div>
                                </div>
                                <div class="butons grid-block --type-two-blocks d-block w-100">
                                    <a class="btn-orange fs-13 w-100 call-form" href="javascript:;" tabindex="0"><span class="mr-3 icon-phone_in_talk"></span>Call Now</a>
                                    <a class="btn-blue fs-13 w-100" href="javascript:;" tabindex="0"><span class="mr-3 icon-paperplane"></span>Send Email</a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="tabs-second hidden">
                    <p class="fs-small fc-lgrey ptpx-20 pbpx-20">Showing <b class="fc-nblack">1</b> to <b class="fc-nblack">8</b> of <b class="fc-nblack">8</b> Services found </p>
                </div>
                <div class="tabs-third hidden">
                    <p class="fs-small fc-lgrey ptpx-20 pbpx-20">Showing <b class="fc-nblack">1</b> to <b class="fc-nblack">8</b> of <b class="fc-nblack">8</b> Services found </p>
                </div>
                <div class="tabs-fourth hidden">
                    <p class="fs-small fc-lgrey ptpx-20 pbpx-20">Showing <b class="fc-nblack">1</b> to <b class="fc-nblack">8</b> of <b class="fc-nblack">8</b> Services found </p>
                </div>
            </div>


            
       
    
            </div>


            
        </div>
 
    
            </div>


            
     </div>
                 
          </div>
            </div>
        </div>


   </div>
</div>



@endsection