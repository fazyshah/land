@extends('layout.master')
@section('title', 'Sell Properties')

@section('content')


<section class="second-banner buy sec-padding --small ptpx-20" >
    <div class="container">
        <ul class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li>Sell Property</li>
        </ul>
        <div class="row">
            <div class="col-lg-12 pbpx-15">
                <div class="posts-search">
                    <ul class="unstyled">
                        <li>

                                <p class=" fs-medium fc-primary ta-left">Showing 1 to 25 of 32,625 properties</p>

                                <p class="fc-primary ta-right">Sort by:
                                    <select name="" id="">
                                        <option value="0">Posted newest first</option>
                                        <option value="1"></option>
                                    </select>
                                </p>
                        </li>
                        <li>
                            <p class="fc-black fs-small">Get an alert with the newest ads for "pakistani" in Toronto (GTA)</p>
                            <p class="fc-black fs-small ta-right">Register for <span class="fc-primary">LandTrack</span> Alerts  [?]
                                <a class="btn-sign" href="sign-up">Sign Up<span class="icon-caret-right"></span></a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-9 nopadd-right">
                @forelse($properties as $property)
                    <div class="property-detail">
                        <div class="top-right">
                            <ul class="unstyled inline">
                                <!-- <li><span class="tag-1"></span></li> -->
                                <!-- <li><span class="tag-2"></span></li> -->
                                <li><p class="fs-small fc-lgrey">Added: {{ \Carbon\Carbon::parse($property['created_at'])->diffForHumans() }}</p></li>
                                <li><span class="tag-3"></span></li>
                            </ul>


                        </div>
                        <div class="bottom-right">
                            <img class="fig-1" src="web_asset/images/trusted-1.jpg" alt="trusted">
                        </div>
                        <div class="slide-detail">
                            <div class="ultra-hot">
                            <img class="fig-1" src="web_asset/images/sell.png" alt="hot">
                            </div>
                            <div class="shaded-icons">

                                <ul class="unstyled inline">
                                    <li><a href=""><span class="icon-map-pin"></span></a></li>
                                    <li><a href=""><span class="icon-external-link"></span></a></li>
                                    <li><a href=""><span class="icon-heart-o"></span></a></li>
                                    <li><a href=""><span class="icon-camera3"></span> 30</a></li>
                                </ul>
                            </div>
                            <div>
                                <img src="web_asset/images/new-projects/farm1.jpg" alt="*">
                            </div>
                        </div>
                        <div class="middle-detail">
                            <p class="fc-secondary fs-medium fw-semi-bold lh-normal">{{$property->title}}</p>
                            <p class="fc-primary fw-semi-bold lh-xlarge"><i>{{$property->propertyArea->name.', '.$property->propertyArea->areaCity->name}}</i></p>
                            <div class="bed-box item">
                                <ul class="unstyled inline">
                                    <li><p class="fc-secondary fw-semi-bold"><span class="icon-icon4"></span>{{$property->beds}} Bedrooms</p></li>
                                    <li><p class="fc-secondary fw-semi-bold"><span class="icon-icon2"></span>{{$property->baths}} Bathroom</p></li>
                                    <li><p class="fc-secondary fw-semi-bold"><span class="icon-home1"></span>{{$property->area}}</p></li>
                                    <li> <h4 class="ff-primary fc-primary fw-medium"><span class="fs-medium">PKR </span>{{$property->price}}</h4></li>
                                </ul>
                            </div>
                            <p class="fc-ngrey text">{{ str_limit($property->details, 120) }} <a class="fc-primary" href="#">more</a></p>

                            <div class="buttons">
                                <!-- <a class="btn-detail " href="">View Detail</a> -->
                                <a class="btn-detail green " href="">Send Email</a>
                                <a class="btn-detail orange " href="">View Number</a>
                            </div>
                            <div class="bottom d-inline">
                                <a class="btn-detail blue " href="{{route('view.property', ['id' => $property->id])}}">Details</a>
                                {{-- <input type="checkbox" id="basket" name="basket-4" value="0"> <label for="basket" class="fc-grey fs-small fw-semi-bold">EMAIL BASKET</label><span class="two fs-xsmall fc-lgrey">Added: 1 day ago (Updated: 1 day ago) </span> --}}
                            </div>
                        </div>
                    </div>
                @empty
                    <h1>No Searches found</h1>
                @endforelse

                {{ $properties->links() }}

            </div>

            @include('layout./partials.side-advertisement-new')

        </div>
    </div>
</section>

@endsection