@extends('layout.master')
@section('title', 'Project Listing')

@section('content')


<div class="super-banner">
<div class="main-slider">
<section class="hero-inner-banner" style="background: url(web_asset/images/banner-1.jpg);">


</section>

</div>

<section class="form-section">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="middle-form wow fadeInUp">
                            <div class="top">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class=" fc-white fw-medium">Property for Sale in Pakistan</p>
                                    </div>

                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="tabs-one  hidden visible">
                                            {{ Form::open(['class' => 'form-1', 'route' => 'search']) }}

                                            <div class="control-group w-20 d-inline-block">
                                                {{ Form::select('city', $cities, old('city'), ['placeholder' => 'Select City', 'required']) }}

                                                {{-- <select name="" id="">
                                                    <option value="">Select City</option>
                                                    <option value="">asdasda</option>
                                                </select> --}}
                                            </div>
                                            <div class="control-group w-62  d-inline-block mar">
                                                <input name="keyword" type="text" placeholder="Type Location, area or keyword">
                                            </div>
                                            <div class="control-group  w-17  d-inline-block ">
                                                <button class="btn-submit" type="submit" value="">SEARCH</button>

                                            </div>
                                            <div class="filter-btn">
                                                <a class=" fs-small fw-semi-bold fc-black tt-uppercase" href="http://onlinekidstoy.com/landtrack.com/#"><span class="icon-plus-circle "></span>Add Filter</a>
                                            </div>

                                            {{ Form::close() }}

                                        </div>
                                        <div  class="tabs-two hidden">
                                            4565454654 asdasd a456s4d6as4d56
                                        </div>
                                        <div  class="tabs-three hidden">
                                            ashdajsdh adsjkask absdb
                                        </div>
                                        <div  class="tabs-four hidden">
                                            zeeshan
                                        </div>
                                    </div>


                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

</section>


</div>



<section class="second-banner  sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <ul class="breadcrumb mbpx-0">
          <li><a href="#">Home&nbsp;</a></li>
          <li><a href="#">&nbsp;Buy&nbsp;</a></li>
          <li>&nbsp;Homes</li>
        </ul>
        <div class="row">
            <div class="col-lg-12 pbpx-15">
                <div class="posts-search">
                    <ul class="unstyled">
                        <li>

                                <p class=" fs-medium fc-primary ta-left">Showing 1 to 25 of 32,625 properties</p>

                                <p class="fc-primary ta-right">Sort by:
                                    <select name="" id="">
                                        <option value="0">Posted newest first</option>
                                        <option value="1"></option>
                                    </select>
                                </p>
                        </li>
                        <li>
                            <p class="fc-black fs-small">Get an alert with the newest ads for "pakistani" in Toronto (GTA)</p>
                            <p class="fc-black fs-small ta-right">Register for <span class="fc-primary">LandTrack</span> Alerts  [?]
                                <a class="btn-sign" href="sign-up">Sign Up<span class="icon-caret-right"></span></a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-9 nopadd-right">
                @forelse($projects as $project)

                    <div class="property-detail new-project">
                        <div class="top-right">
                            <ul class="unstyled inline">
                                <li><p class="fs-small fc-lgrey">Added: {{ \Carbon\Carbon::parse($project['created_at'])->diffForHumans() }}</p></li>
                                <li><span class="tag-3"></span></li>
                            </ul>


                        </div>
                        <div class="bottom-right">
                            <img class="fig-1" src="web_asset/images/trusted-1.jpg" alt="*">
                        </div>
                        <div class="slide-detail">
                            <div class="ultra-hot">
                                <img src="web_asset/images/hot1.png" alt="*">
                            </div>
                            <div class="shaded-icons">

                                <ul class="unstyled inline">
                                    <li><a href=""><span class="icon-map-pin"></span></a></li>

                                    <li><a href=""><span class="icon-camera3"></span> 30</a></li>
                                </ul>
                            </div>
                            <div>
                                <img src="web_asset/images/new-projects/farm1.jpg" alt="*">
                            </div>
                        </div>
                        <div class="middle-detail">
                            <p class="fc-secondary fs-medium fw-semi-bold lh-normal">{{$project->title}}</p>
                            <p class="fc-lgrey fw-medium lh-xlarge fs-small"><i>{{$project->projectArea->name.', '.$project->projectArea->areaCity->name}}</i></p>

                            <h4 class="fc-primary fw-semi-bold lh-large"><span class="fs-default ">PKR</span> {{$project->price}}</h4>
                            <p class="fc-ngrey text pbpx-15">{{ str_limit($project->details, 120) }}</p>

                            <a class="btn-detail " href="{{route('view.project', ['id' => $project->id])}}">Details <span class="icon-keyboard_arrow_right"></span></a>
                        </div>

                    </div>

                @empty
                    <h1>No Searches found</h1>
                @endforelse

                {{ $projects->links() }}


                <figure class="adver-shadow mtpx-30 hidden-md-down">
                    <img class="w-100" src="web_asset/images/advertisement/advert-1.jpg" alt="advertisement">
                </figure>

            </div>

            @include('layout./partials.side-advertisement-new')


        </div>
    </div>
</section>


@endsection
