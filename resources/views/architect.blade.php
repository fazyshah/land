@extends('layout.master')
@section('title', 'architect')

@section('content')



../inc/inner-header.php




<section class="second-banner buy sec-padding --small ptpx-20" >
    <div class="container">
        <div class="row">
            <div class="col-lg-9 nopadd-right">
                <div class="architect">
                    <p class="fs-medium tt-uppercase fw-bold fc-dblue">ARCHITECT</p>
                    <form class="architect-form ptpx-15" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">First Name <span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Last Name <span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Display Name <span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Register As <span class="fc-red fs-medium">*</span></label>
                                    <select name="category" >
                                            <option value="" disabled="" selected="" hidden="">Register as:</option>
                                            <option value="Individual">Individual</option>
                                            <option value="Real Estate or Business">Real Estate or Business</option>
                                            <option value="Builder">Builder</option>
                                            <option value="Architect">Architect</option>
                                            <option value="Interior Designer">Interior Designer</option>
                                             <option value="legal-advisor">Legal Service Advisor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Sub Type <span class="fc-red fs-medium">*</span></label>
                                    <select  class="type_fld">
                                            <option value="Student">Student</option>
                                            <option value="Individual">Individual</option>
                                            <option value="Business">Business</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">City<span class="fc-red fs-medium">*</span></label>
                                    <select  class="type_fld">
                                            <option value="" disabled="" selected="" hidden="">City</option>
                                            <option value="Student">Student</option>
                                            <option value="Individual">Individual</option>
                                            <option value="Business">Business</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Region <span class="fc-red fs-medium">*</span></label>
                                    <select name="region" >
                                        <option value="" disabled="" selected="" hidden="">Region</option>
                                        <option value="" selected="&quot;selected&quot;">Select Region</option>
                                        <option value="Islamabad Capital Territory">Islamabad Capital Territory</option>
                                        <option value="Punjab">Punjab</option>
                                        <option value="Sindh">Sindh</option>
                                        <option value="Balochistan">Balochistan</option>
                                        <option value="Khyber Pakhtunkhwa">Khyber Pakhtunkhwa</option>
                                        <option value="Azad Jammu and Kashmir">Azad Jammu and Kashmir</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">House/Plot<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="House/Plot" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Street</label>
                                    <input type="text" placeholder="Street" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Location<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="Location" required>
                                </div>
                            </div>
                            <div class="col-md-12 txt">
                                <div class="control-group">
                                    <label for="Name">Description<span class="fc-red fs-medium">*</span></label>
                                    <textarea name="Descrition" placeholder="Description" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="Name">Experience<span class="fc-red fs-medium">*</span></label>
                                    <input type="text" placeholder="Experience" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="experience_unit">Experience Unit<span class="fc-red fs-medium">*</span></label>
                                    <select  class="type_fld">
                                            <option value="" disabled="" selected="" hidden="">Experience Unit</option>
                                            <option value="">Student</option>
                                            <option value="">Individual</option>
                                            <option value="">Business</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Expertise (Comma Separated)</label>
                                    <input type="text" placeholder="" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Facebook Link</label>
                                    <input type="text" placeholder="" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Linkdin Profile </label>
                                    <input type="text" placeholder="" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="Name">Instagram Link</label>
                                    <input type="text" placeholder="" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group">
                                    <label for="email">Email Address<span class="fc-red fs-medium">*</span></label>
                                    <input type="email" placeholder="Email Address" >
                                </div>
                            </div>
                            <div class="col-md-12">
                               <div class="control-group">
                                   <label for="number">Contact No<span class="fc-red fs-medium">*</span></label>
                                   <input type="text" name="pn" placeholder="Phone Number" class="required number">
                                  
                               </div>
                            </div>
                            <div class="col-md-6">
                               <div class="control-group">
                                       <label for="number">Cell Number 1<span class="fc-red fs-medium">*</span></label>
                                       <input type="number" name="pn1" placeholder="0123-456789123" required>
                                   
                               </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                        <label for="number">Cell Number 2<span class="fc-red fs-medium">*</span></label>
                                        <input type="number" name="pn2" placeholder="0123-456789123" required>
                                   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="pwd">Password<span class="fc-red fs-medium">*</span></label>
                                    <input type="password" placeholder="Password" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="pwd">Confirm Password<span class="fc-red fs-medium">*</span></label>
                                    <input type="password" placeholder="Confirm Password" required>
                                </div>
                            </div>
                            <div class="col-md-8 mtpx-20">
                                <div class="control-group">
                                    <input type="checkbox" id="basket" name="basket-4" value="0"><label for="basket" class="basket fc-dblue fs-small tt-uppercase fw-bold"> &nbsp; I Agree</label>
                                    <p class="fs-small clicking">By clicking <span>Register</span> you agree to the <a class="fc-primary" href="../terms-of-use">Terms and Conditions</a> <br>set out by this site, including our Cookie Use.</p>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-4 mtpx-20">
                                <div class="control-group buto">
                                    <input class="btn-new-form" type="submit" value="Submit" placeholder="" >
                                    <span class="icon-keyboard_arrow_right"></span>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
          side-advertisement-new
        </div>
        
 </div>
</div>



@endsection