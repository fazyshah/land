<section class="blogs hidden-md-down bg-cream" data-img="url(assets/images/blog-bg.jpg)">
    <div class="container">
        <div class="mains">
            <h6 class="our-blog fc-white ff-primary fw-semi-bold">Our Blog</h6>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="grid-block --type-three-blocks --style-offsets main-left">
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    <img class="" src="<?php echo $imgsrc ?>blog/blog-1.jpg" alt="*">
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>                                
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    <img class="" src="<?php echo $imgsrc ?>blog/blog-2.jpg" alt="*">
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    <img class="" src="<?php echo $imgsrc ?>blog/blog-3.jpg" alt="*">
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    <img class="" src="<?php echo $imgsrc ?>blog/blog-4.jpg" alt="*">
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                        <div class="blog-box">
                            <p class="head">12<br>Aug</p>
                            <figure>
                                <img class="" src="<?php echo $imgsrc ?>blog/blog-5.jpg" alt="*">
                            </figure>
                            <div class="content">
                                <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="blog">
                            <div class="blog-box">
                                <p class="head">12<br>Aug</p>
                                <figure>
                                    <img class="" src="<?php echo $imgsrc ?>blog/blog-6.jpg" alt="*">
                                </figure>
                                <div class="content">
                                    <p class="fs-small fc-white fw-semi-bold first lh-normal">Lorem ipsum dolor sit</p>
                                    <p class="fs-small fc-white fw-light second">Ut enim ad minim veniam,quis nostrud...</p>
                                </div>
                                
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="right">
                    <h6 class="right-head fc-white fw-medium ff-primary"><span class="icon-icon1"></span>JOIN OUR DISCUSSION FORUM</h6>
                    
                    <div class="ptpx-20">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Buying Property  (9029)</p>
                        <p class="fs-13 fc-white lh-medium">Need to buy property but not sure what to do and where to go? Post your query here and let us take care of the rest.</p>
                        </a>
                    </div>
                    
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Renting Property  (444)</p>
                        <p class="fs-13 fc-white lh-medium">Whether you’re looking to rent property or find tenants, post all of your rent related queries in this section for expert advice.</p>
                        </a>
                    </div>
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Selling Properties  (2036)</p>
                        <p class="fs-13 fc-white lh-medium">Selling your property in Pakistan but not sure how to proceed. Post your concerns here and let the experts do the rest.</p>
                        </a>
                    </div>
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Projects & Developments  (1447)</p>
                        <p class="fs-13 fc-white lh-medium">Learn about the various existing and upcoming projects & developments in Pakistan.</p>
                        </a>
                    </div>
                    <div class="ptpx-18">
                        <a href="ask-an-expert">
                        <p class="fw-medium fc-sblue pbpx-3 lh-normal">Maintenance & Safety  (102)</p>
                        <p class="fs-13 fc-white lh-medium">Voice your concerns about property maintenance & safety in this section and find out what our experts have to say about your issues.</p>
                        </a>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>