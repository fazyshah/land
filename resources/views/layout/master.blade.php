<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>@yield('title') | LandTrack</title>
        <meta name="description" content="lkh">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Rokkitt:100,200,300,400,500,600,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{ asset('web_asset/css/style.css') }}">
        @yield('style')

    </head>


    <header class="primary" style="background: url({{ asset('web_asset/images/topnav-bg.jpg') }});">
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <a href="/">
                <img class="ptpx-3" src="{{ asset('web_asset/images/logo.png') }}" alt="*">
                </a>
            </div>
            <div class="col-lg-10 nopadd-left hidden-md-down">
                <nav class="primary  float-left">
                    <ul class="list-left unstyled">
                        <li><span class="icon-home1 fc-white pr-1"></span><a href="ask-an-expert">Ask An Expert</a></li>
                        <li><a href="blog">Blog</a></li>
                        <li><a href="society-maps">Landtrack Maps</a></li>
                        <li><a href="home-partners">Home Partners</a></li>
                        <li><a href="legal-advisor-listing">Legal Services</a></li>
                        <li><a href="auction-page">Auction</a></li>



                    </ul>
                    <ul class="login-panel inline fc-white">
                            <li><a href="#">Language <span class="icon-caret-down"></span></a></li>


                        @guest
                            <li><a href="{{ route('register') }}"><span class="icon-profile"></span>SignUp</a></li>
                            <li><a href="{{ route('login')}}"><span class="icon-user2"></span>Login</a></li>
                        @else

                         <li><a href="/"><span class="icon-profile"></span>{{ Auth::user()->name }} </a></li>
                          <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><span class="icon-profile"></span>Logout </a></li>




                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                               </form>
                        @endguest




                        </ul>
                </nav>

                <nav class="secondary float-left">
                    <ul class="inline tt-uppercase float-left">
                        <li><a href="{{route('buy.property')}}">Buy</a></li>
                        <li><a href="{{route('sell.property')}}">Sell</a></li>
                        <li><a href="{{route('rent.property')}}">Rent</a></li>
                        <li><a href="shared-accomodation">Shared Accomodation</a></li>
                        <li><a href="projects">New Projects<!-- <span class="icon-caret-down"></span> --></a></li>
                        <li><a href="agent-directory">Agent Directory</a></li>
                        <li><a href="lt-store">LT Store</a></li>
                        <li><a href="trends">Property Trends</a></li>

                    </ul>
                    @if(Request::segment(1) == "auction-page" || Request::segment(1) == "auction-page-details")
                    <a class="btn-secondary fs-small" href="{{route('add.property')}}"><span class="icon-plus"></span>Add Auction</a>
                    @else
                    <a class="btn-secondary fs-small" href="{{route('add.property')}}"><span class="icon-plus"></span>Add Property</a>
                    @endif
                </nav>

            </div>
        </div>
    </div>

</header>


<div class="mobile-nav-btn ">
    <span class="lines"></span>
</div>

<div class="mobile-nav">

      <nav class="pl">
        <ul class="unstyled">
            <li><a href="buy">Buy</a></li>
            <li><a href="buy">Sell</a></li>
            <li><a href="buy">Rent</a></li>
            <li><a href="shared-accomodation">Shared Accomodation</a></li>
            <li><a href="javascript:;">New Projects<span class="icon-caret-down"></span></a></li>
            <li><a href="agent-directory">Agent Directory</a></li>
            <li><a href="javascript:;">LT Store</a></li>
            <li><a href="trends">Property Trends</a></li>
            <li><a href="">Ask An Expert</a></li>
            <li><a href="blog">Blog</a></li>
            <li><a href="society-maps">Landtrack Maps</a></li>
            <li><a href="home-partners">Home Partners</a></li>
            <li><a href="legal-advisor-listing">Legal Services</a></li>
            <li><a href="auction-page">Auction</a></li>

        </ul>
    </nav>
</div>


@yield('content')

<footer class="primary" data-img="{{url('web_asset/images/footer-bg.jpg')}}">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p class="head fc-secondary fw-bold">Landtrack contact</p>
				<ul class="unstyled first">
					<li><span class="icon-map"></span>123, Main Road, Your City, NY 1234</li>
					<li><span class="icon-mobile1"></span>+92 0311-2296605</li>
					<li><span class="icon-envelope"></span>info@landtrack.com</li>
				</ul>
			</div>
			<div class="col-md-3">
				<p class="head fc-secondary fw-bold">Company</p>
				<ul class="unstyled second">
					<li><a href="about-us">About Us</a></li>
					<li><a href="blog">Blog</a></li>
					<li><a href="#">Expo</a></li>
					<li><a href="privacy-policy">Privacy Policy</a></li>
					<li><a href="terms-of-use">Terms Of Use</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<p class="head fc-secondary fw-bold">Connect</p>
				<ul class="unstyled second">
					<!-- <li><a href="#">News</a></li> -->

					<li><a href="contact-us">Contact Us</a></li>
					<li><a href="careers">Work With Us</a></li>
					<li><a href="help-and-support">Help & Support</a></li>
					<li><a href="advertise">Advertise On Landtrack</a></li>
					<li><a href="ask-an-expert">Ask An Expert</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<p class="head fc-secondary fw-bold">Follow Us</p>
				<ul class="unstyled inline social">
					<li><a href=""><span class="icon-facebook"></span></a></li>
					<li><a href=""><span class="icon-google-plus1"></span></a></li>
					<li><a href=""><span class="icon-twitter"></span></a></li>
				</ul>
				<div class="app-btns">
					<a href="#"><img src="{{ asset('web_asset/images/app-btn.png') }}" alt="*"></a>
					<a href="#"><img src="{{ asset('web_asset/images/android-btn.png') }}" alt="*"></a>
				</div>
			</div>
		</div>
	</div>
</footer>
<footer class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<ul class="unstyled inline">
					<li><a class="fc-white fs-small" href="buy">Lahore Property</a></li>
					<li><a class="fc-white fs-small" href="buy">Karachi Property</a></li>
					<li><a class="fc-white fs-small" href="buy">Islamabad Property</a></li>
				</ul>
				<ul class="unstyled inline">
					<li><a class="fc-white fs-small" href="/">Landtrack.com - Pakistan Property Portal</a></li>
					<!-- <li><a class="fc-white fs-small" href="">Home Finance</a></li>	 -->
				</ul>
				<p class="fs-small ptpx-10">Copyright © <script>document.write(new Date().getFullYear())</script> LandTrack All Rights Reserved</p>
			</div>
			<div class="offset-lg-6 col-lg-2 nopadd-left">
				<p class="pbpx-10">Official Home Partners</p>
				<img src="{{ asset('web_asset/images/home-partner-1.jpg') }}" alt="*">
			</div>
		</div>

	</div>
</footer>





<footer>
	<script type="text/javascript" src="{{ asset('web_asset/js/xlib.js') }}"></script>
	<script type="text/javascript" src="{{ asset('web_asset/js/script.js') }}"></script>
	<script type="text/javascript" src="{{ asset('web_asset/js/bootstrap.min.js') }}"></script>
    @yield('script')
</footer>
