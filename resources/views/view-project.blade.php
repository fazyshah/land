@extends('layout.master')
@section('title', 'View Properties')

@section('content')

<div class="super-banner">
<div class="main-slider">
<section class="hero-banner small" style="background: url({{asset('web_asset/images/banner-1.jpg')}});">


</section>

</div>




</div>



<section class="second-banner buy pro-detail sec-padding --small ptpx-20 bg-cream" >
    <div class="container">
        <ul class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li><a href="#">Project</a></li>
          <li>Project Detail</li>
        </ul>
        <div class="row bg-white detail-shared">
            <div class="col-md-12 col-nopadd">
                <div class="first">
                    <p class=""><span class="icon-bookmark1"></span> Save to Facebook</p>
                    <ul class="unstyled inline ">
                        <li><a href=""><span class="icon-external-link"></span></a></li>
                        <li><a href=""><span class="icon-heart1"></span></a></li>
                        <li><a href=""><span class="icon-printer-text"></span></a></li>
                    </ul>
                </div>
                <div class="second-part pbpx-10 ptpx-15">
                    <div class="row">

                        <div class="col-md-9 matchheight">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="fs-medium fc-secondary fw-semi-bold">{{$project->title}}</p>
                                    <p class="fs-small">{{$project->projectArea->name.', '.$project->projectArea->areaCity->name}} </p>
                                </div>
                            </div>
                            <div class="row ptpx-5">
                                <div class="tip">
                                    <div class="col-md-10 ">
                                        <div class="big-slider-2 ">
                                            @if($project->img != "")
                                                @foreach(explode('|', $project->img) as $img)
                                                    <div>
                                                        <img src="{{ asset('web_asset/images/projects/'.$img) }}" alt="{{$project->title}}">
                                                    </div>
                                                @endforeach
                                            @else
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-1.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-2.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-3.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-4.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-5.jpg')}}" alt="*">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-nopadd ptpx-20">
                                        <div class="small-slider-2  ptpx-5">
                                            @if($project->img != "")
                                                @foreach(explode('|', $project->img) as $img)
                                                    <div>
                                                        <img src="{{ asset('web_asset/images/projects/'.$img) }}" alt="{{$project->title}}">
                                                    </div>
                                                @endforeach
                                            @else
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-1.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-2.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-3.jpg')}}" alt="*">
                                                </div>
                                                <div>
                                                    <img src="{{asset('web_asset/images/slider/img-4.jpg')}}" alt="*">
                                                </div>
                                                <div class="">
                                                    <img src="{{asset('web_asset/images/slider/img-5.jpg')}}" alt="*">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row ptpx-5">
                                <div class="col-md-12">
                                   <ul class="sli-bottom unsyled inline">
                                        <li class="fc-primary">PKR <span>{{$project->price}}</span></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                       <div class="col-lg-3    matchheight brdr-left">
                            <div class="right-contact">
                                <div class="head">
                                    <p class="fs-medium ta-center fc-white fw-medium ff-primary">Contact for More information</p>
                                </div>
                                <div class="bottom">
                                    <form action="" class="jform validate right-form ">
                                        <div class="control-group">

                                            <input type="text" placeholder="Name" name="cn" class="required" minlength="2" maxlength="60">
                                        </div>
                                        <div class="control-group">

                                            <input type="text" name="em" class="required email" placeholder="Email" maxlength="60">
                                        </div>
                                        <div class="control-group clearfix">
                                                <input type="text" name="pn" id="phone" class="number required" placeholder="Phone Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />

                                        </div>
                                        <div class="control-group">
                                            <textarea name="msg" class="required" placeholder="Message" minlength="2" maxlength="1000"></textarea>
                                        </div>
                                        <div class="control-group">

                                            <input type="submit" value="Call" class="fw-normal tt-uppercase w-49 btn-detail orange">
                                            <input type="submit" value="Send Email" class="fw-normal tt-uppercase w-49 btn-detail blue">
                                        </div>
                                        <div class="control-group last">
                                            <div class="input">
                                                <input type="checkbox" id="informed">
                                            </div>
                                            <div class="lbl">
                                                <label class="fs-xsmall fw-light " for="informed">Keep me Informed about similar properties By Submitting this Form.I agree to <a class="fc-secondary fw-semi-bold" href="">Terms Of Use</a></label>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row ptpx-20">

            <div class="col-lg-9 col-sm-12 col-nopadd">
               <div class="roomate-desc ptpx-30 bg-white ">
                   <h6 class="heading-main fw-semi-bold abou-1 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Overview</h6>

                   <div class="content ">
                        <p class=" fs-17 fw-semi-bold fc-black">Details & Description</p>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="prop-table w-100 mbpx-15 mtpx-15">
                                    <tr>
                                        <td>Landtrack ID: </td>
                                        {{-- <td>{{ $project->created_at->diffForHumans() }} </td> --}}
                                        <td>{{ $project->id }} </td>
                                    </tr>
                                    <tr>
                                        <td>Locality:</td>
                                        <td>{{ $project->locality }}</td>
                                    </tr>
                                    <tr>
                                        <td>Types: </td>
                                        <td>{{$project->projectType->type}}</td>
                                    </tr>

                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="prop-table w-100 mbpx-15 mtpx-15">
                                    <tr>
                                        <td>City </td>
                                        <td>{{$project->projectArea->name.', '.$project->projectArea->areaCity->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Price </td>
                                        <td>PKR {{ $project->price }} </td>
                                    </tr>
                                    <tr>
                                        <td>Developer </td>
                                        <td>Green Orchard Farms (Pvt) Ltd.</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div id='grow'>
                            <div class='measuringWrapper'>
                                <div class="text">
                                     <p class="fc-grey fs-13">
                                        {{ $project->details }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <input class="btn-detail" type="button" onclick="growDiv()" value="View more +" id="more-button">


                    </div>
                    <br>
                    <div class="mtpx-20">
                        <h6 class="heading-main fw-semi-bold small-3 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Project Features</h6>
                        <p class="fc-primary fw-semi-bold ">MAIN FEATURES</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-electric-meter"></span> Electricity Backup: <b>None</b></li>
                        </ul>

                        <p class="fc-primary fw-semi-bold ">PLOT FEATURES</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-sewerage"></span> Sewerage: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-flash-2"></span> Electricity: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-tap"></span> Water Supply: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-sui-gas"></span> Sui Gas: <b>Yes</b></li>
                            <li class="w-66"><span class="icon-road"></span> Accessible by Road: <b>Yes</b></li>
                        </ul>

                        <p class="fc-primary fw-semi-bold ">BUSINESS AND COMMUNICATION</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33 prpx-0"><span class="icon-icon-28"></span> Broadband Internet Access: <b>Yes</b></li>
                            <li class="w-66"><span class="icon-icon-29"></span> Satellite or Cable TV Ready: <b>Yes</b></li>
                        </ul>

                        <p class="fc-primary fw-semi-bold ">NEARBY LOCATIONS AND OTHER FACILITIES</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-university"></span> Nearby Schools: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-hospital"></span> Nearby Hospitals: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-restaurant"></span> Nearby Restaurants: <b>Yes</b></li>
                            <li class="w-100"><span class="icon-bus"></span> Nearby Public Transport Service: <b>Yes</b></li>
                        </ul>

                        <p class="fc-primary fw-semi-bold ">OTHER FACILITIES</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-maintenance-staff"></span> Maintenance Staff: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-security-staff"></span> Security Staff: <b>Yes</b></li>
                            <li class="w-33"><span class="icon-cctv"></span> CCTV Security: <b>Yes</b></li>
                        </ul>



                    </div>

                    <div class="mtpx-20">
                        <h6 class="heading-main fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Location &amp; Commute</h6>
                        <p class="fs-large fc-primary tt-uppercase fw-semi-bold pbpx-5">COMMUTE AND NEARBY</p>
                        <div class="map-direct" id="zoomife">
                            <iframe src="https://www.google.com/maps/d/embed?mid=1wMZLD-KoIBt-zG0r8ziRkZeqgUA&hl=en" width="100%" height="480"></iframe>

                        </div>

                    </div>

                    <div class="mtpx-20">
                        <h6 class="heading-main fw-semi-bold small-5 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Property Types</h6>
                        <p class=" fc-primary tt-uppercase fw-semi-bold">RESIDENTIAL PLOTS (FARM HOUSE LAND)</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-area"></span> Area: <b>102.22 Marla</b></li>
                            <li class="w-66"><span class="icon-price-tag1"></span> Price: <b>PKR 45 Lakh</b></li>

                        </ul>

                        <p class=" fc-primary tt-uppercase fw-semi-bold">RESIDENTIAL PLOTS (FARM HOUSE LAND)</p>
                        <ul class="unstyled inline stripper ptpx-15 pbpx-20">
                            <li class="w-33"><span class="icon-area"></span> Area: <b>45,000 Sq. Ft.</b></li>
                            <li class="w-66"><span class="icon-price-tag1"></span> Price: <b>PKR 80 Lakh</b></li>

                        </ul>

                    </div>

                    <div class="mtpx-20">
                        <h6 class="heading-main fw-semi-bold abou-1 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Payment Plan</h6>
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="mtpx-10 ">
                                    <img class="w-100" src="{{asset('web_asset/images/payment-plan.jpg')}}" alt="*">
                                </figure>
                            </div>
                            <div class="col-md-8 nopadd-left">
                                <div class="fig-cont mtpx-55">
                                    <p class="fs-large fc-nblack fc-grey lh-medium">Payment Plan For <br>Agriculture Farm House</p>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="mtpx-30 mbpx-30">
                        <h6 class="heading-main fw-semi-bold abou-1 lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft">Developer</h6>
                        <a class="green-big-button mtpx-15" href="">Green Orchard Farms (Pvt) Ltd.</a>
                    </div>



               </div>
            </div>
            <div class="col-lg-3 col-sm-12  nopadd-right">
                <div class="estate-links">
                    <p class="usef fw-bold tt-uppercase fc-primary fs-large">TAKAFUL LINKS</p>
                    <ul class="unstyled inline grid-block --type-two-blocks ">
                        <li>
                            <img class="ptpx-15" src="{{asset('web_asset/images/estates/takaful-estate.jpg')}}" alt="*">
                        </li>
                        <li class="brdr-left">
                            <ul class="unstyled insider">
                                <li><a href="javascript:;"><span class="icon-person"><sup class="icon-dot-single"></sup></span>Shaikh Asif</a></li>
                                <li><a href="javascript:;"><span class="icon-phone_in_talk"></span>0210602168</a></li>
                                <li><a href="javascript:;"><span class="icon-messages"></span>Chat Now</a></li>
                                <li><a class="btn-detail green" href="javascript:;">View Profile</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="useful-links mtpx-15">
                    <p class="usef fw-bold tt-uppercase fc-primary fs-large">USEFUL LINKS</p>
                    <ul class="unstyled">
                        <li><a href="">Property for sale in Karachi</a></li>
                        <li><a href="">Property for sale in Jamshed Town</a></li>
                        <li><a href="">Property for sale in PECHS</a></li>
                        <li><a href="">Upper Portions for sale in Karachi</a></li>
                        <li><a href="">Upper Portions for sale in Jamshed Town</a></li>
                        <li><a href="">Upper Portions for sale in PECHS</a></li>
                    </ul>
                </div>


            </div>

        </div>
        <div class="row ptpx-40">

            <div class="col-lg-12 col-sm-12 col-nopadd">
               <h6 class="heading-main fw-semi-bold small lh-normal ff-primary  fc-nblack tt-uppercase wow slideInLeft"><span class="fc-lgrey">Similar to</span> new project</h6>

               <div class="grid-block --type-four-blocks --style-offsets --grid-ipad-full --grid-mobile-full ">

                   <div class="item matchHeight">
                       <div class="prop-new-box-2">
                           <div class="top" style="background: url('{{asset('web_asset/images/new-projects/img-1.jpg')}}');">

                               <div class="text">
                                   <p class="fc-white fs-medium fw-medium">Green Orchard Farms <span class="fs-13 d-block ls-normal">Rawalpindi</span></p>
                               </div>
                           </div>
                           <div class="content">
                               <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>45 Lakh to 80 Lakh</p>
                               <ul class="unstyled fs-small lh-xmedium">
                                   <li><strong>City: </strong>Rawalpindi</li>
                                   <li><strong>Locality: </strong>Lahore Islamabad Motor...</li>
                                   <li><strong>Type: </strong>Residential Plots</li>
                               </ul>
                           </div>
                           <div class="butons ">
                               <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                           </div>
                       </div>
                   </div>
                   <div class="item matchHeight">
                       <div class="prop-new-box-2">
                           <div class="top" style="background: url('{{asset('web_asset/images/new-projects/img-2.jpg')}}');">

                               <div class="text">
                                   <p class="fc-white fs-medium fw-medium">Green Orchard Farms <span class="fs-13 d-block ls-normal">Rawalpindi</span></p>
                               </div>
                           </div>
                           <div class="content">
                               <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>45 Lakh to 80 Lakh</p>
                               <ul class="unstyled fs-small lh-xmedium">
                                   <li><strong>City: </strong>Rawalpindi</li>
                                   <li><strong>Locality: </strong>Lahore Islamabad Motor...</li>
                                   <li><strong>Type: </strong>Residential Plots</li>
                               </ul>
                           </div>
                           <div class="butons ">
                               <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                           </div>
                       </div>
                   </div>
                   <div class="item matchHeight">
                       <div class="prop-new-box-2">
                           <div class="top" style="background: url('{{asset('web_asset/images/new-projects/img-3.jpg')}}');">

                               <div class="text">
                                   <p class="fc-white fs-medium fw-medium">Green Orchard Farms <span class="fs-13 d-block ls-normal">Rawalpindi</span></p>
                               </div>
                           </div>
                           <div class="content">
                               <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>45 Lakh to 80 Lakh</p>
                               <ul class="unstyled fs-small lh-xmedium">
                                   <li><strong>City: </strong>Rawalpindi</li>
                                   <li><strong>Locality: </strong>Lahore Islamabad Motor...</li>
                                   <li><strong>Type: </strong>Residential Plots</li>
                               </ul>
                           </div>
                           <div class="butons ">
                               <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                           </div>
                       </div>
                   </div>
                   <div class="item matchHeight">
                       <div class="prop-new-box-2">
                           <div class="top" style="background: url('{{asset('web_asset/images/new-projects/img-3.jpg')}}');">

                               <div class="text">
                                   <p class="fc-white fs-medium fw-medium">Green Orchard Farms <span class="fs-13 d-block ls-normal">Rawalpindi</span></p>
                               </div>
                           </div>
                           <div class="content">
                               <p class="fc-secondary fs-large fw-semi-bold"><span class="fs-13 fw-normal mr-2">PKR</span>45 Lakh to 80 Lakh</p>
                               <ul class="unstyled fs-small lh-xmedium">
                                   <li><strong>City: </strong>Rawalpindi</li>
                                   <li><strong>Locality: </strong>Lahore Islamabad Motor...</li>
                                   <li><strong>Type: </strong>Residential Plots</li>
                               </ul>
                           </div>
                           <div class="butons ">
                               <a class="btn-blue fs-13 w-100" href="javascript:;">View More Details <span class="ml-2 icon-keyboard_arrow_right fs-large"></span></a>
                           </div>
                       </div>
                   </div>



               </div>
            </div>


        </div>
    </div>
</section>


@endsection