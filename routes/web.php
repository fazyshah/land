<?php

Auth::routes();

Route::get('/', ['uses' => 'MainController@home', 'as' => 'home']);
Route::get('search', ['uses' => 'MainController@search', 'as' => 'search']);
Route::get('add-property', ['uses' => 'MainController@addProperty', 'as' => 'add.property']);
Route::get('area-ajax/{id}', ['uses' => 'MainController@areaAjax', 'as' => 'area.ajax']);
Route::post('save-property', ['uses' => 'MainController@saveProperty', 'as' => 'save.property']);
Route::get('view-propert/{id}', ['uses' => 'MainController@viewProperty', 'as' => 'view.property']);
Route::get('auction-page','MainController@getAuctionPage');

Route::post('submit/contact/form','MainController@contactFormSubmit');
Route::post('submit/careers/form','MainController@careerformsubmit');




Route::get('projects', ['uses' => 'MainController@Projects', 'as' => 'projects']);
Route::get('add-project', ['uses' => 'MainController@addProject', 'as' => 'add.project']);
Route::post('save-project', ['uses' => 'MainController@saveProject', 'as' => 'save.project']);
Route::get('view-project/{id}', ['uses' => 'MainController@viewProject', 'as' => 'view.project']);


Route::get('buy-property', ['uses' => 'MainController@buyProperty', 'as' => 'buy.property']);
Route::get('rent-property', ['uses' => 'MainController@rentProperty', 'as' => 'rent.property']);
Route::get('sell-property', ['uses' => 'MainController@sellProperty', 'as' => 'sell.property']);

Route::get('admin/dashboard', ['uses' => 'MainController@dashboard', 'as' => 'dashboard']);
Route::get('admin/users', ['uses' => 'MainController@users', 'as' => 'users']);
Route::get('admin/user-types', ['uses' => 'MainController@userTypes', 'as' => 'user.types']);
Route::get('admin/user-roles', ['uses' => 'MainController@userRoles', 'as' => 'user.roles']);
Route::get('admin/cities', ['uses' => 'MainController@cities', 'as' => 'cities']);
Route::get('admin/areas', ['uses' => 'MainController@areas', 'as' => 'areas']);
Route::get('admin/properties', ['uses' => 'MainController@properties', 'as' => 'properties']);
Route::get('admin/property-types', ['uses' => 'MainController@propertyTypes', 'as' => 'property.types']);
Route::get('admin/property-purposes', ['uses' => 'MainController@propertyPurposes', 'as' => 'property.purposes']);




Route::get('sana', function () {
   return view('sana');
});



Route::get('about-us', function () {
   return view('about-us', ['imgscr' => 'web_asset/images/']);
});




Route::get('ask-an-expert','MainController@askanexpert');


Route::get('agent-detail', function () {
   return view('agent-detail', ['imgscr' => 'web_asset/images/']);
});





Route::get('agent-directory-listing', function () {
   return view('agent-directory-listing', ['imgscr' => 'web_asset/images/']);
});




Route::get('agent-directory','MainController@getAgentDirectory');





Route::get('architect', function () {
   return view('architect', ['imgscr' => 'web_asset/images/']);
});






Route::get('auction-page-details', function () {
   return view('auction-page-details', ['imgscr' => 'web_asset/images/']);
});











Route::get('blog-detail', function () {
   return view('blog-detail', ['imgscr' => 'web_asset/images/']);
});

Route::get('blog', function () {
   return view('blog', ['imgscr' => 'web_asset/images/']);
});


Route::get('careers', function () {
   return view('careers', ['imgscr' => 'web_asset/images/']);
});




Route::get('contact-us', function () {
   return view('contact-us', ['imgscr' => 'web_asset/images/']);
});


Route::get('help-and-support', function () {
   return view('help-and-support', ['imgscr' => 'web_asset/images/']);
});


Route::get('advertise', function () {
   return view('advertise', ['imgscr' => 'web_asset/images/']);
});

Route::get('home-partners', function () {
   return view('home-partners');
});




Route::get('legal-advisor-detail', function () {
   return view('legal-advisor-detail', ['imgscr' => 'web_asset/images/']);
});





//Route::get('legal-advisor-listing', function () {
//   return view('legal-advisor-listing', ['imgscr' => 'web_asset/images/']);
//});

Route::get('legal-advisor-listing','MainController@legalAdvisorListing');


Route::get('lt-store-detail', function () {
   return view('lt-store-detail', ['imgscr' => 'web_asset/images/']);
});




Route::get('lt-store', function () {
   return view('lt-store', ['imgscr' => 'web_asset/images/']);
});



Route::get('partner-profile', function () {
   return view('partner-profile', ['imgscr' => 'web_asset/images/']);
});



Route::get('privacy-policy', function () {
   return view('privacy-policy', ['imgscr' => 'web_asset/images/']);
});



Route::get('roommate-detail', function () {
   return view('roommate-detail', ['imgscr' => 'web_asset/images/']);
});





Route::get('roommate-listing', function () {
   return view('roommate-listing', ['imgscr' => 'web_asset/images/']);
});





Route::get('society-maps','MainController@getSocietyMaps');




Route::get('terms-of-use', function () {
   return view('terms-of-use', ['imgscr' => 'web_asset/images/']);
});




Route::get('trend-detail', function () {
   return view('trend-detail', ['imgscr' => 'web_asset/images/']);
});





Route::get('trends-cities-list', function () {
   return view('trends-cities-list', ['imgscr' => 'web_asset/images/']);
});






//Route::get('trends', function () {
//   return view('trends', ['imgscr' => 'web_asset/images/']);
//});
Route::get('trends','MainController@TrendsView');



/* Isfhan Route add-accomodation*/
Route::get('add-accomodation','MainController@addAccomodation');

Route::post('save-accomodation', ['uses' => 'MainController@saveAccomodation', 'as' => 'save.accomodation']);

Route::get('shared-accomodation','MainController@ShowAccomodation');

Route::get('shared-accomodation-detail/{id}','MainController@accomodationDetail');


/*Route::get('shared-accomodation', function () {
   return view('shared-accomodation', ['imgscr' => 'web_asset/images/']);
});*/

/*Route::get('shared-accomodation-detail', function () {
   return view('shared-accomodation-detail', ['imgscr' => 'web_asset/images/']);
});*/



Route::get('shared-accomodation-listing', function () {
   return view('shared-accomodation-listing', ['imgscr' => 'web_asset/images/']);
});

